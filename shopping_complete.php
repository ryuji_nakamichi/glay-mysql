﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//購入処理実行class生成
$itemPurchaseObj = new itemPurchaseClass;


//会員登録されていない状態で来た場合
if ($_POST['shopping_confirm_flg'] != 1) {

  if ($_SESSION['shopping_flg'] !== true) {
    header("Location: ./");
    exit;
  } 

}


//会員登録されている状態で来た場合
if ($_POST['shopping_confirm_flg'] === '1') {


  //購入処理実行
  $itemPurchaseObj->itemPurchaseInsert($connect); 

}
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <ol class="list-shopping-flow">
        <li>STEP1<br><span class="pc">お届け先指定</span></li>
        <li>STEP2<br><span class="pc">お支払い方法等選択</span></li>
        <li>STEP3<br><span class="pc">内容確認</span></li>
        <li class="active">STEP4<br><span class="pc">完了</span></li>
      </ol>

      <h2>Shopping</h2>

      <p>お買い上げありがとうございました。</p>

      <p>ただいま、ご注文の確認メールをお送りさせていただきました。<br>
万一、ご確認メールが届かない場合は、トラブルの可能性もありますので大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問い合わせくださいませ。<br>
今後ともご愛顧賜りますようよろしくお願い申し上げます。</p>

    <div class="shop_information">
      <p class="name">GLAY ~ SHOP~</p>
      <p>TEL：090-0000-0000 （受付時間/12:00 ～ 20:00）<br>
      E-mail：<a href="mailto:%72%79%75%6a%69%67%6c%61%79%40%79%61%68%6f%6f%2e%63%6f%2e%6a%70">ryujiglay@yahoo.co.jp</a>
      </p>
    </div>

      <p class="backBtn"><a href="./">HOME</a></p>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>