﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/shopping_check.php';


// クロスサイドスクリプティング対策
foreach ($_REQUEST AS $key => $val) {
  if (!is_array($val)){
    $_REQUEST[$key] = htmlspecialchars($val);
    $_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
  }
}

if ($_POST['shopping_deliv_flg']) {
  $shopping_deliv_flg = $_POST['shopping_deliv_flg'];
  $_SESSION['shopping']['shopping_deliv_flg'] = $shopping_deliv_flg;
} 

if ($_POST['deliv_check']) {
  $deliv_check = $_POST['deliv_check'];
  $_SESSION['shopping']['deliv_check'] = $deliv_check;
}


//商品の個数
if ($_POST['quantity']) {

  $i = 0;

  $_SESSION['shopping_multiple']['quantity'] = $_POST['quantity'];

  foreach ( (array) $_POST['quantity'] AS $key => $value ) {
    $_SESSION['cartInfo']['cartInfoCopyArray'][$i]['quantity'] = $value;

    $i++;
  }

}


//other_delivテーブルのcd番号
if ($_POST['shipping']) {

  $i = 0;

  $_SESSION['shopping_multiple']['shipping'] = $_POST['shipping'];

  foreach ( (array) $_POST['shipping'] AS $key => $value ) {
    $_SESSION['cartInfo']['cartInfoCopyArray'][$i]['shipping'] = $value;

    $i++;
  }

}

// echo '<pre>';
// print_r($_SESSION['cartInfo']['cartInfoCopyArray']);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <ol class="list-shopping-flow">
        <li>STEP1<br><span class="pc">お届け先指定</span></li>
        <li class="active">STEP2<br><span class="pc">お支払い方法等選択</span></li>
        <li>STEP3<br><span class="pc">内容確認</span></li>
        <li>STEP4<br><span class="pc">完了</span></li>
      </ol>

      <h2>お支払方法・お届け時間等の指定</h2>

      <form name="form1" id="form1" method="post" action="./shopping_confirm_member.php">
        <input type="hidden" name="deliv_id" value="<?=$deliv_check?>" id="deliv_id">
            
        <div class="pay_area">
          <h3>お支払方法の指定</h3>
          <p class="select-msg">お支払方法をご選択ください。</p>
          <p class="non-select-msg" style="display: none;">まずはじめに、配送方法を選択ください。</p>

          <table id="paymentTbl">
            <thead>
              <tr>
                <th class="alignC">選択</th>
                <th class="alignC" colspan="2" id="payment_method">お支払方法</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="alignC"><input type="radio" id="pay_1" name="payment_id" value="1" <?php echo ($_SESSION['shopping']['payment_id'] == 1 || !$_SESSION['shopping']['payment_id']) ? 'checked': ''; ?>></td>
                <td>
                    <label for="pay_1">郵便振替</label>
                </td>
              </tr>
              <tr>
                <td class="alignC"><input type="radio" id="pay_2" name="payment_id" value="2" <?php echo ($_SESSION['shopping']['payment_id'] == 2) ? 'checked': ''; ?>></td>
                <td>
                    <label for="pay_2">現金書留</label>
                </td>
              </tr>
              <tr>
                <td class="alignC"><input type="radio" id="pay_3" name="payment_id" value="3" <?php echo ($_SESSION['shopping']['payment_id'] == 3) ? 'checked': ''; ?>></td>
                <td>
                    <label for="pay_3">銀行振込</label>
                </td>
              </tr>
              <tr>
                <td class="alignC"><input type="radio" id="pay_4" name="payment_id" value="4" <?php echo ($_SESSION['shopping']['payment_id'] == 4) ? 'checked': ''; ?>></td>
                <td>
                    <label for="pay_4">代金引換</label>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="pay_area02">
          <h3>お届け時間の指定</h3>
          <p class="select-msg">ご希望の方は、お届け時間を選択してください。</p>
          <p class="non-select-msg" style="display: none;">まずはじめに、配送方法を選択ください。</p>
          <div class="delivdate top">
            <!--★お届け日★-->
            <span class="attention"></span>
              お届け日：ご指定頂けません。
            <!--★お届け時間★-->
            <span class="attention"></span>
              お届け時間：
            <select name="deliv_time_id0" id="deliv_time_id0" style="">
              <option value="" <?php echo ($_SESSION['shopping']['deliv_time_id0'] == 0 || !$_SESSION['shopping']['deliv_time_id0']) ? 'selected': ''; ?>>指定なし</option>
              <option label="午前" value="1" <?php echo ($_SESSION['shopping']['deliv_time_id0'] == 1) ? 'selected': ''; ?>>午前</option>
              <option label="午後" value="2" <?php echo ($_SESSION['shopping']['deliv_time_id0'] == 2) ? 'selected': ''; ?>>午後</option>
            </select>
          </div>
        </div>
            
            <!-- ▼ポイント使用 -->
            <div class="point_area">
              <h3>ポイント使用の指定</h3>
              <p><span class="attention">1ポイントを1円</span>として使用する事ができます。<br>
                  使用する場合は、「ポイントを使用する」にチェックを入れた後、使用するポイントをご記入ください。
              </p>
              <div class="point_announce">
                <p><span class="user_name"><?=$_SESSION['user']["user_info"]['full_name']?>様</span>の、現在の所持ポイントは「<span class="point"><?=$_SESSION['cartInfo']['point']?>Pt</span>」です。<br>
                      今回ご購入合計金額：<span class="price"><?=number_format($_SESSION['cartInfo']['all_total_price'])?>円</span> <span class="attention">(送料、手数料を含みません。)</span>
                </p>
                <ul>
                    <li>
                    <input type="radio" id="point_on" name="point_check" value="1" <?php echo ($_SESSION['shopping']['point_check'] == 1) ? 'checked': ''; ?>><label for="point_on">ポイントを使用する</label>
                        <br>
                    今回のお買い物で、<input type="text" id="use_point" name="use_point" value="<?php echo ($_SESSION['shopping']['point_check'] == 1 && $_SESSION['shopping']['use_point'])  ? $_SESSION['shopping']['use_point']: ''; ?>" maxlength="9" class="box60" <?php echo ($_SESSION['shopping']['point_check'] == 2 || !$_SESSION['shopping']['point_check']) ? 'disabled': ''; ?>>&nbsp;Ptを使用する。<span class="attention"></span>
                    </li>
                    <li><input type="radio" id="point_off" name="point_check" value="2" <?php echo ($_SESSION['shopping']['point_check'] == 2 || !$_SESSION['shopping']['point_check']) ? 'checked': ''; ?>><label for="point_off">ポイントを使用しない</label></li>
                </ul>
              </div>
            </div>
            <!-- ▲ポイント使用 -->

            <div class="pay_area02">
                <h3>その他お問い合わせ</h3>
                <p>その他お問い合わせ事項がございましたら、こちらにご入力ください。</p>
                <div>
                  <!--★その他お問い合わせ事項★-->
                  <span class="attention"></span>
                  <textarea name="message" cols="70" rows="8" class="txtarea"><?=$_SESSION['shopping']['message']?></textarea>
                  <p class="attention"> (3000文字まで)</p>
                </div>
            </div>

            <ul class="btnList">
                <li><a class="backBtn" href="./shopping_deliv.php" onclick="history.back();">戻る</a></li>
                <li><input class="nextBtn" type="submit" name="next" value="次へ" id="next"></li>
            </ul>
        </form>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
