﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


// クロスサイドスクリプティング対策
foreach ($_REQUEST as $key => $val) {

  if ( !is_array($val) ) {
    $_REQUEST[$key] = htmlspecialchars($val);
    $_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
  }

}


//変数定義ここから
$name    = $_POST['name'];
$kana    = $_POST['kana'];
$mail    = $_POST['mail'];
$tel     = $_POST['tel'];
$zip1    = $_POST['zip1'];
$zip2    = $_POST['zip2'];
$address = $_POST['address'];
//変数定義ここまで

?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>Shopping</h2>

      <p>以下の内容でよろしいですか？</p>

      <form id="form1" class="confirm" name="form1" method="post" action="./shopping_send.php">

        <table class="form_1">
          <tr>
            <th>名前<em>必須</em></th>
            <td><?=$name?></td>
          </tr>

          <tr>
            <th>ふりがな<em>必須</em></th>
            <td><?=$kana?></td>
          </tr>

          <tr>
            <th>メールアドレス<em>必須</em></th>
            <td><?=$mail?></td>
          </tr>

          <tr>
            <th>電話番号<em>必須</em></th>
            <td><?=$tel?></td>
          </tr>

          <tr>
            <th>住所<em>必須</em></th>
            <td class="addressCell">
              〒<?=$zip1?>-<?=$zip2?><br>
              <?=$address?>
            </td>
          </tr>
            
        </table>

        <ul class="btnList">
          <li><input type="button" id="back" class="button" value="修正" onclick="javascript:history.back()"></li>
          <li><input type="submit" id="submit" class="button" value="送信"></li>
        </ul>

        <input type="hidden" name="flg" value="1">
        <input type="hidden" name="name" value="<?=$name?>">
        <input type="hidden" name="kana" value="<?=$kana?>">
        <input type="hidden" name="mail" value="<?=$mail?>">
        <input type="hidden" name="tel" value="<?=$tel?>">
        <input type="hidden" name="zip1" value="<?=$zip1?>">
        <input type="hidden" name="zip2" value="<?=$zip2?>">
        <input type="hidden" name="address" value="<?=$address?>">

      </form>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
