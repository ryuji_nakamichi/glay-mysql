﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


//会員の情報を取得
$mypageClassObj      = new mypageClass;
$other_delivAllArray = $mypageClassObj->getDelivDetail($connect, $_REQUEST['cd']);
$other_delivArray    = $other_delivAllArray['other_delivArray'];
$btn_text            = $other_delivAllArray['btn_text'];

// echo '<pre>';
// print_r($other_delivAllArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>お届け先の追加･変更</h2>


      <?php if ($_GET['flg'] == 1) { ?>

      <p class="completeMessage">お届け先の追加･変更しました。</p>
      <a class="backBtn" href="./mypage_delivery.php">戻る</a>

      <?php } else { ?>

      <?php if ($_GET['flg'] == 2) { ?>
      <p style="color: #bf0000;">入力されてない項目があります。</p>
      <?php } ?>

      <form id="form1" name="form1" method="post">

        <table class="form_1">
          <tr>
            <th><label for="name1">姓名<em>必須</em></label></th>
            <td class="column2_cell">
              <p id="name1Error" class="errorMessage"></p>
              <p id="name2Error" class="errorMessage"></p>
              <input id="name1" class="column2_input" type="text" name="name1" value="<?=$other_delivArray[0]['name1']?>">
              <input id="name2" class="column2_input" type="text" name="name2" value="<?=$other_delivArray[0]['name2']?>">
            </td>
          </tr>

          <tr>
            <th><label for="kana1">かな<em>必須</em></label></th>
            <td class="column2_cell">
              <p id="kana1Error" class="errorMessage"></p>
              <p id="kana2Error" class="errorMessage"></p>
              <input id="kana1" class="column2_input" type="text" name="kana1" value="<?=$other_delivArray[0]['kana1']?>">
              <input id="kana2" class="column2_input" type="text" name="kana2" value="<?=$other_delivArray[0]['kana2']?>">
            </td>
          </tr>

          <tr>
            <th><label for="company_name">会社名</label></th>
            <td>
              <p id="company_nameError"></p>
              <input id="company_name" type="text" name="company_name" value="<?=$other_delivArray[0]['company_name']?>">
            </td>
          </tr>

          <tr>
            <th><label for="post_code1">郵便番号<em>必須</em></label></th>
            <td class="addressCell">
              <p id="post_code1Error" class="errorMessage"></p>
              <p id="post_code2Error" class="errorMessage"></p>
              <input id="post_code1" type="text" name="post_code1" value="<?=$other_delivArray[0]['zip1']?>">
              -
              <input id="post_code2" type="text" name="post_code2" value="<?=$other_delivArray[0]['zip2']?>">
            </td>
          </tr>

          <tr>
            <th><label for="address">住所<em>必須</em></label></th>
            <td>
              <p id="addressError" class="errorMessage"></p>
              <input id="address" type="text" name="address" value="<?=$other_delivArray[0]['address1']?>">
            </td>
          </tr>

          <tr>
            <th><label for="tel">電話番号<em>必須</em></label></th>
            <td>
              <p id="telError" class="errorMessage"></p>
              <input type="text" id="tel" name="tel" value="<?=$other_delivArray[0]['tel']?>">
            </td>
          </tr>

          <tr>
            <th><label for="fax">FAX</label></th>
            <td>
              <p id="faxError"></p>
              <input id="fax" type="text" name="fax" value="<?=$other_delivArray[0]['fax']?>">
            </td>
          </tr>
    
        </table>

        <ul class="btnList">
          <li>
            <input type="reset" id="reset" class="button" value="リセット">
          </li>
          <li>
            <input type="button" id="submit" class="button" value="<?=$btn_text?>">
          </li>
        </ul>

        <input type="hidden" name="flg" value="1">

        <?php if ($cd) { ?>
        <input type="hidden" name="update_flg" value="1">
        <input type="hidden" name="cd" value="<?=$cd?>">
        <?php } ?>

      </form>

      <?php } ?>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
