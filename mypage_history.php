﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


//mypageClass(購入履歴詳細)呼び出し
$mypageObj    = new mypageClass;
$historyArray = $mypageObj->historyDetailGet($connect, $_REQUEST['order_cd']);

$order_cd                = $historyArray['order_cd'];
$order_detailArray       = $historyArray['order_detailArray'];
$other_delivArray        = $order_detailArray['other_deliv'];
$item_order_detailArray  = $historyArray['item_order_detailArray'];
$o_d_price_max_total     = $historyArray['o_d_price_max_total'];


// echo '<pre>';
// print_r($other_delivArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>MYページ</h2>

      <div id="mynavi_area">
        <ul class="mynavi_list clearfix">
          <li><a href="./mypage.php">購入履歴一覧</a></li>
          <li><a href="./mypage_favorite.php">お気に入り一覧</a></li>
          <li><a href="./mypage_change.php">会員登録内容変更</a></li>
          <li><a href="./mypage_delivery.php">お届け先追加・変更</a></li>
          <li><a href="./mypage_refusal.php">退会手続き</a></li>
        </ul>

        <!--▼現在のポイント-->
        <div class="point_announce">
          <p>ようこそ&nbsp;／&nbsp;<span class="user_name"><?=$order_detailArray['o_order_name1'].$order_detailArray['o_order_name2']?>様</span>
            &nbsp;現在の所持ポイントは&nbsp;<span class="point st"><?=$_SESSION['user']['user_info']['point']?>pt</span>&nbsp;です。</p>
        </div>
        <!--▲現在のポイント-->

      </div>

      <h3>購入履歴詳細</h3>
      <div class="mycondition_area clearfix">
        <p>
            <span class="st">購入日時：&nbsp;</span><?=$order_detailArray['o_new_record_time']?><br>
            <span class="st">注文番号：&nbsp;</span><?=$order_cd?><br>
            <span class="st">お支払い方法：&nbsp;</span><?=$order_detailArray['o_payment_cd_name']?><br>
                                <span class="st">ご注文状況：&nbsp;</span>
                                        注文受付
        </p>
      <form action="order.php" method="post">
          <input type="hidden" name="transactionid" value="4331d3b12b9ca2a8550eb24a90276101a4e1fcdc">
          <p class="btn">
              <input type="hidden" name="order_id" value="<?=$order_cd?>">
              <input type="button" value="この購入内容で再注文する">
          </p>
      </form>
      </div>

      <table class="shoppingTbl">
          <tbody>
            <tr>
              <th>商品写真</th>
              <th>商品名</th>
              <th>単価</th>
              <th>数量</th>
              <th>小計</th>
            </tr>

            <?php foreach ($item_order_detailArray AS $key => $value) { ?>
            <tr>
              <td class="alignC">
                <a href="<?=$value['img_pass1']?>" target="_blank"><img src="<?=$value['img_pass1']?>" alt="<?=$value['name']?>" style="width: 100px;"></a>
              </td>
              <td class="alignC"><?=$value['i_name']?></td>
              <td class="alignR"><?=$value['o_d_price']?>円</td>
              <td class="alignR"><?=$value['o_d_item_quantity']?></td>
              <td class="alignR"><?=$value['o_d_price'] * $value['o_d_item_quantity']?>円</td>
            </tr>
            <?php } ?>

            <tr>
              <th colspan="4" class="alignR">小計</th>
              <td class="alignR"><?=$o_d_price_max_total?>円</td>
            </tr>
            <?php if ($order_detailArray['o_use_point']) { ?>
            <tr>
              <th colspan="4" class="alignR">値引き（ポイントご使用時）</th>
              <td class="alignR">-<?=number_format($order_detailArray['o_use_point'])?>円</td>
            </tr>
            <?php } ?>
            <tr>
              <th colspan="4" class="alignR">送料</th>
              <td class="alignR"><?=number_format($order_detailArray['o_deliv_fee'])?>円</td>
            </tr>
            <tr>
              <th colspan="4" class="alignR">手数料</th>
              <td class="alignR">0円</td>
            </tr>
            <tr>
              <th colspan="4" class="alignR">合計</th>
              <td class="alignR"><span class="price"><?=number_format($o_d_price_max_total+$order_detailArray['o_deliv_fee']-$order_detailArray['o_use_point'])?>円</span></td>
            </tr>
          </tbody>
        </table>

        <table class="shoppingTbl delivname">
          <tbody>
            <tr>
                <th>ご使用ポイント</th>
                <td>-<?=$order_detailArray['o_use_point']?>Pt</td>
            </tr>
          </tbody>
        </table>
                        
          <h3>ご注文者</h3>
          <table class="shoppingTbl customerTbl">
            <tbody>
              <tr>
                <th>お名前</th>
                <td><?=$order_detailArray['o_order_name1'].$order_detailArray['o_order_name2']?></td>
              </tr>
              <tr>
                <th>お名前(かな)</th>
                <td><?=$order_detailArray['o_order_kana1'].$order_detailArray['o_order_kana2']?></td>
              </tr>
              <tr>
                <th>会社名</th>
                <td><?=$order_detailArray['s_company_name']?></td>
              </tr>
              <tr>
                <th>郵便番号</th>
                <td>〒<?=$order_detailArray['o_order_zip1']?>-<?=$order_detailArray['o_order_zip2']?></td>
              </tr>
              <tr>
                <th>住所</th>
                <td><?=$order_detailArray['o_order_addr']?></td>
              </tr>
              <tr>
                <th>電話番号</th>
                <td><?=$order_detailArray['o_order_tel1']?></td>
              </tr>
              <tr>
                <th>FAX番号</th>
                <td><?=$order_detailArray['s_fax']?></td>
              </tr>
              <tr>
                <th>メールアドレス</th>
                <td><?=$order_detailArray['o_order_email']?></td>
              </tr>
              <tr>
                <th>携帯メールアドレス</th>
                <td><?=$order_detailArray['s_phone_mail_address']?></td>
              </tr>
              <tr>
                <th>性別</th>
                <td><?=$order_detailArray['o_order_sex_name']?></td>
              </tr>
              <tr>
                <th>職業</th>
                <td><?=$order_detailArray['s_job_type_name']?></td>
              </tr>
              <tr>
                <th>生年月日</th>
                <td><?=$order_detailArray['s_full_birth']?></td>
              </tr>
            </tbody>
          </table>

          <h3>お届け先</h3>
          <table class="shoppingTbl delivname">
            <tbody>
              <tr>
                <th>お名前</th>
                <td><?=$other_delivArray['name1']?> <?=$other_delivArray['name2']?></td>
              </tr>
              <tr>
                <th>お名前(かな)</th>
                <td><?=$other_delivArray['kana1']?> <?=$other_delivArray['kana2']?></td>
              </tr>
              <tr>
                <th>会社名</th>
                <td><?=$other_delivArray['company_name']?></td>
              </tr>
              <tr>
                <th>郵便番号</th>
                <td>〒<?=$other_delivArray['zip1']?>-<?=$other_delivArray['zip2']?></td>
              </tr>
              <tr>
                <th>住所</th>
                <td><?=$other_delivArray['address1']?></td>
              </tr>
              <tr>
                  <th>電話番号</th>
                  <td><?=$other_delivArray['tel']?></td>
              </tr>
              <tr>
                <th>FAX番号</th>
                <td><?=$other_delivArray['fax']?></td>
              </tr>
              <tr>
                <th>お届け日</th>
                <td>指定なし</td>
              </tr>
              <tr>
                  <th>お届け時間</th>
                  <td><?=$order_detailArray['o_deliv_time_id0_']?></td>
              </tr>
            </tbody>
          </table>
                        
          <h3>配送方法・お支払方法・その他お問い合わせ</h3>
          <table class="shoppingTbl delivname">
            <tbody>

              <?php
              //配送方法(table)表示ここから
              $cartInfo_max = ($item_order_detailArray) ? count($item_order_detailArray): 0;

              $cartInfo_rowspan = ($cartInfo_max) ? " rowspan=\"{$cartInfo_max}\"": '';
              $cnt = 0;

              foreach ( (array) $item_order_detailArray AS $key => $value ) {
                $cartInfo_colspan  = ($cnt !== 0) ? ' colspan="2"': '';
                $cartInfo_tr_start = ($cnt !== 0) ? '<tr>': '';
                $cartInfo_tr_end   = ($cnt !== 0) ? '</tr>': '';
              ?>

                <?php if ($cnt === 0) { ?>
                <tr>
                    <th<?=$cartInfo_rowspan?>>配送方法</th>
                    <td><?=$value['i_name']?></td>
                    <td<?=$cartInfo_colspan?>><?=$value['s_name']?></td>
                </tr>
                <?php } else { ?>
                <tr>
                    <td><?=$value['i_name']?></td>
                    <td<?=$cartInfo_colspan?>><?=$value['s_name']?></td>
                </tr>
                <?php } ?>

              <?php $cnt++; ?>
              <?php
              }
              //配送方法(table)表示ここまで
              ?>

              <tr>
                  <th>お支払方法</th>
                  <td colspan="2"><?=$order_detailArray['o_payment_cd_name']?></td>
              </tr>
              <tr>
                  <th>その他お問い合わせ</th>
                  <td colspan="2"><?=$order_detailArray['o_message']?></td>
              </tr>
              </tbody>
          </table>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>