<?php

require_once './inc/session.php';
require_once './inc/db.php';

$resArray = array();

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {


	// クロスサイドスクリプティング対策
	foreach ($_REQUEST AS $key => $val) {

		if ( !is_array($val) ) {
			$_REQUEST[$key] = htmlspecialchars($val);
			$_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
		}

	}

	if ($_REQUEST['login_id']) {
		$user_id = $_REQUEST['login_id'];
	}

	if ($_REQUEST['login_password']) {
		$login_password = md5($_REQUEST['login_password']);
	}
	
	
	//処理分岐ここから
	switch ($_REQUEST['mode']) {


		//ログイン処理ここから
		case 'login':

		$_SELECT = "cd, name1, name2, kana1, kana2, company_name, birth_year, birth_month, birth_day, sex, job_type, post_code1, post_code2, address, tel, fax, mail, phone_mail_address, user_id, MD5(password) AS password, point, disp_flg, sort, new_record_time, update_record_time";
		$sql     = "SELECT {$_SELECT} FROM shopping_member WHERE user_id = '{$user_id}' AND MD5(password) = '{$login_password}' AND disp_flg = 1";

		$login_query = mysqli_query($connect, $sql);
		$login_max   = mysqli_num_rows($login_query);

		if ($login_max) {

			$userArray = mysqli_fetch_assoc($login_query);

			$_SESSION['user']["user_info"] = $userArray;
			$_SESSION['user']["user_info"]['full_birth'] = $userArray['birth_year'].'/'.$userArray['birth_month'].'/'.$userArray['birth_day'];
			$_SESSION['user']["user_info"]['full_name'] = $userArray['name1'].' '.$userArray['name2'];


			//職業取得ここから
			if ($_SESSION['user']["user_info"]['job_type']) {

				$sql = "SELECT cd, name FROM job_type WHERE disp_flg = 1 AND cd = {$_SESSION['user']["user_info"]['job_type']}";
				$user_job_query = mysqli_query($connect, $sql);
				$user_job_max   = mysqli_num_rows($user_job_query);

				if ($user_job_max) {
					$user_jobArray = mysqli_fetch_assoc($user_job_query);
					$_SESSION['user']["user_info"]['job_type_name'] = $user_jobArray['name'];
				}

				
			}
			//職業取得ここまで

			$_SESSION['user_login']["login_flg"] = 1;

			$resArray['status'] = 'success';
		} else {

			$resArray['status'] = 'faild';
			
			$_SESSION['user_login']["login_flg"] = 0;
		}

	}
	//ログイン処理ここまで

} else {
	$resArray['status'] = 'faild';
}

echo json_encode($resArray);