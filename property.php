﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/function.php';


/***************************************************************
検索(session、ページャー対応、POST通信)
***************************************************************/
//検索ここから


// クロスサイドスクリプティング対策
foreach ($_REQUEST as $key => $val) {

  if ( !is_array($val) ) {
    $_REQUEST[$key] = htmlspecialchars($val);
    $_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
  }

}


//検索のカラムの名前定義ここから
$name_column                   = 'name';
$area_cd_column                = 'area_cd';
$area_route_cd_column          = 'area_route_cd';
$area_route_line_cd_column     = 'area_route_line_cd';
$area_route_station_cd_column  = 'area_route_station_cd';
$age_cd_column                 = 'age_cd';
$commitment_cd_column          = 'commitment_cd';
//検索のカラムの名前定義ここまで


//違うページに行った場合はSESSION初期化
if ( !strpos($_SERVER['HTTP_REFERER'], $_SERVER['SCRIPT_NAME']) ) {
  $_SESSION['search'][$name_column]                  = array();
  $_SESSION['search'][$area_cd_column]               = array();
  $_SESSION['search'][$area_route_cd_column]         = array();
  $_SESSION['search'][$area_route_line_cd_column]    = array();
  $_SESSION['search'][$area_route_station_cd_column] = array();
  $_SESSION['search'][$age_cd_column]                = array();
  $_SESSION['search'][$commitment_cd_column]         = array();
}


//物件名（name）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分で物件名を入力された場合(セッションがない場合)
if ($_POST[$name_column] && !$_SESSION['search'][$name_column]) {
  
  $name   = $_POST[$name_column];
  $_WHERE .= " AND {$name_column} LIKE '%{$name}%'";
  
  
  //セッション変数に代入する
  $_SESSION['search'][$name_column] = $_POST[$name_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$name_column]) {

  $name   = $_SESSION['search'][$name_column];
  $_WHERE .= " AND {$name_column} LIKE '%{$name}%'";
  
  
//POSTして、セッションがある場合
} else if ($_POST[$name_column] && $_SESSION['search'][$name_column]) {

  $name = $_POST[$name_column];
  
  
  //セッション変数を空にする
  $_SESSION['search'][$name_column] = array();
  
  
    //セッション変数に代入する
  $_SESSION['search'][$name_column] = $_POST[$name_column];

  
  $_WHERE .= " AND {$name_column} LIKE '%{$name}%'";
  
} else if ($_POST[$name_column] == 0 || $_SESSION['search'][$name_column] == 0) {
  $name = $_POST[$name_column];
  
  
  //セッション変数を空にする
  $_SESSION['search'][$name_column] = array();
  
  
    //セッション変数に代入する
  $_SESSION['search'][$name_column] = $_POST[$name_column];

}
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/


//エリア（area_cd）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分でエリアを選択された場合(セッションがない場合)
if ($_POST[$area_cd_column] && !$_SESSION['search'][$area_cd_column]) {

  $area_cd = array();

  if ($_POST[$area_cd_column]) {
    $area_cd = $_POST[$area_cd_column];
  }


  //WHERE句生成処理ここから
  $area_cd_str = implode(", ", $area_cd);
  $area_checkBoxSql = "{$area_cd_column} IN ({$area_cd_str})";

  $_WHERE .= " AND {$area_checkBoxSql}";
  //WHERE句生成処理ここまで


  //セッション変数に代入する
  $_SESSION['search'][$area_cd_column] = $_POST[$area_cd_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$area_cd_column]) {

  $area_cd = array();

  $area_cd = $_SESSION['search'][$area_cd_column];


  //WHERE句生成処理ここから
  $area_cd_str = implode(", ", $area_cd);
  $area_checkBoxSql = "{$area_cd_column} IN ({$area_cd_str})";

  $_WHERE .= " AND {$area_checkBoxSql}";
  //WHERE句生成処理ここまで

  
//POSTして、セッションがある場合
} else if ($_POST[$area_cd_column] && $_SESSION['search'][$area_cd_column]) {


  $area_cd = array();

  if ($_POST[$area_cd_column]) {
    $area_cd = $_POST[$area_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_cd_column] = $_POST[$area_cd_column];


  //WHERE句生成処理ここから
  $area_cd_str = implode(", ", $area_cd);
  $area_checkBoxSql = "{$area_cd_column} IN ({$area_cd_str})";

  $_WHERE .= " AND {$area_checkBoxSql}";
  //WHERE句生成処理ここまで

  
} else if ($_POST[$area_cd_column] == 0 || $_SESSION['search'][$area_cd_column] == 0) {


  $area_cd = array();

  if ($_POST[$area_cd_column]) {
    $area_cd = $_POST[$area_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_cd_column] = $_POST[$area_cd_column];

}


//エリア検索結果表示用ここから
if ($area_cd) {


  //IN句用
  $area_cd_str       = implode(", ", $area_cd);
  $sql               = "SELECT cd, name FROM area WHERE disp_flg = 1 AND cd IN ({$area_cd_str})";
  $area_search_query = mysqli_query($connect, $sql);
  $area_search_max   = mysqli_num_rows($area_search_query);

  for ($i = 0; $i < $area_search_max; $i++) {
    $area_searchArray[]         = mysqli_fetch_assoc($area_search_query);
    $area_searchNamweArray[] = $area_searchArray[$i]['name'];
  }

  $area_searchNamwe_str = implode(", ", $area_searchNamweArray);

}
//エリア検索結果表示用ここまで
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/


//エリア（路線種別 area_route_cd）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分でエリアを選択された場合(セッションがない場合)
if ($_POST[$area_route_cd_column] && !$_SESSION['search'][$area_route_cd_column]) {

  $area_route_cd = array();

  if ($_POST[$area_route_cd_column]) {
    $area_route_cd = $_POST[$area_route_cd_column];
  }


  //WHERE句生成処理ここから
  $area_route_cd_str = implode(", ", $area_route_cd);
  $area_route_checkBoxSql = "{$area_route_cd_column} IN ({$area_route_cd_str})";

  $_WHERE .= " AND {$area_route_checkBoxSql}";
  //WHERE句生成処理ここまで


  //セッション変数に代入する
  $_SESSION['search'][$area_cd_column] = $_POST[$area_cd_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$area_route_cd_column]) {

  $area_route_cd = array();

  $area_route_cd = $_SESSION['search'][$area_route_cd_column];


  //WHERE句生成処理ここから
  $area_route_cd_str = implode(", ", $area_route_cd);
  $area_route_checkBoxSql = "{$area_route_cd_column} IN ({$area_route_cd_str})";

  $_WHERE .= " AND {$area_route_checkBoxSql}";
  //WHERE句生成処理ここまで

  
//POSTして、セッションがある場合
} else if ($_POST[$area_route_cd_column] && $_SESSION['search'][$area_route_cd_column]) {


  $area_route_cd = array();

  if ($_POST[$area_route_cd_column]) {
    $area_route_cd = $_POST[$area_route_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_route_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_route_cd_column] = $_POST[$area_route_cd_column];


  //WHERE句生成処理ここから
  $area_route_cd_str = implode(", ", $area_route_cd);
  $area_route_checkBoxSql = "{$area_route_cd_column} IN ({$area_route_cd_str})";

  $_WHERE .= " AND {$area_route_checkBoxSql}";
  //WHERE句生成処理ここまで

  
} else if ($_POST[$area_route_cd_column] == 0 || $_SESSION['search'][$area_route_cd_column] == 0) {


  $area_route_cd = array();

  if ($_POST[$area_route_cd_column]) {
    $area_route_cd = $_POST[$area_route_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_route_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_route_cd_column] = $_POST[$area_route_cd_column];

}
//エリア検索結果表示用ここまで
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/


//エリア（路線詳細 area_route_line_cd）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分でエリアを選択された場合(セッションがない場合)
if ($_POST[$area_route_line_cd_column] && !$_SESSION['search'][$area_route_line_cd_column]) {

  $area_route_line_cd = array();

  if ($_POST[$area_route_line_cd_column]) {
    $area_route_line_cd = $_POST[$area_route_line_cd_column];
  }


  //WHERE句生成処理ここから
  $area_route_line_cd_str = implode(", ", $area_route_line_cd);
  $area_route_line_checkBoxSql = "{$area_route_line_cd_column} IN ({$area_route_line_cd_str})";

  $_WHERE .= " AND {$area_route_line_checkBoxSql}";
  //WHERE句生成処理ここまで


  //セッション変数に代入する
  $_SESSION['search'][$area_route_line_cd_column] = $_POST[$area_route_line_cd_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$area_route_line_cd_column]) {

  $area_route_line_cd = array();

  $area_route_line_cd = $_SESSION['search'][$area_route_line_cd_column];


  //WHERE句生成処理ここから
  $area_route_line_cd_str = implode(", ", $area_route_line_cd);
  $area_route_line_checkBoxSql = "{$area_route_line_cd_column} IN ({$area_route_line_cd_str})";

  $_WHERE .= " AND {$area_route_line_checkBoxSql}";
  //WHERE句生成処理ここまで

  
//POSTして、セッションがある場合
} else if ($_POST[$area_route_line_cd_column] && $_SESSION['search'][$area_route_line_cd_column]) {


  $area_route_line_cd = array();

  if ($_POST[$area_route_line_cd_column]) {
    $area_route_line_cd = $_POST[$area_route_line_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_route_line_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_route_line_cd_column] = $_POST[$area_route_line_cd_column];


  //WHERE句生成処理ここから
  $area_route_line_cd_str = implode(", ", $area_route_line_cd);
  $area_route_line_checkBoxSql = "{$area_route_line_cd_column} IN ({$area_route_line_cd_str})";

  $_WHERE .= " AND {$area_route_line_checkBoxSql}";
  //WHERE句生成処理ここまで

  
} else if ($_POST[$area_route_line_cd_column] == 0 || $_SESSION['search'][$area_route_line_cd_column] == 0) {


  $area_route_line_cd = array();

  if ($_POST[$area_route_line_cd_column]) {
    $area_route_line_cd = $_POST[$area_route_line_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_route_line_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_route_line_cd_column] = $_POST[$area_route_line_cd_column];

}
//エリア検索結果表示用ここまで
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/


//エリア（駅名 area_route_station_cd）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分でエリアを選択された場合(セッションがない場合)
if ($_POST[$area_route_station_cd_column] && !$_SESSION['search'][$area_route_station_cd_column]) {

  $area_route_station_cd = array();

  if ($_POST[$area_route_station_cd_column]) {
    $area_route_station_cd = $_POST[$area_route_station_cd_column];
  }


  //WHERE句生成処理ここから
  $area_route_station_cd_str = implode(", ", $area_route_station_cd);
  $area_route_station_checkBoxSql = "{$area_route_station_cd_column} IN ({$area_route_station_cd_str})";

  $_WHERE .= " AND {$area_route_station_checkBoxSql}";
  //WHERE句生成処理ここまで


  //セッション変数に代入する
  $_SESSION['search'][$area_route_station_cd_column] = $_POST[$area_route_station_cd_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$area_route_station_cd_column]) {

  $area_route_station_cd = array();

  $area_route_station_cd = $_SESSION['search'][$area_route_station_cd_column];


  //WHERE句生成処理ここから
  $area_route_station_cd_str = implode(", ", $area_route_station_cd);
  $area_route_station_checkBoxSql = "{$area_route_station_cd_column} IN ({$area_route_station_cd_str})";

  $_WHERE .= " AND {$area_route_station_checkBoxSql}";
  //WHERE句生成処理ここまで

  
//POSTして、セッションがある場合
} else if ($_POST[$area_route_station_cd_column] && $_SESSION['search'][$area_route_station_cd_column]) {


  $area_route_station_cd = array();

  if ($_POST[$area_route_station_cd_column]) {
    $area_route_station_cd = $_POST[$area_route_station_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_route_station_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_route_station_cd_column] = $_POST[$area_route_station_cd_column];


  //WHERE句生成処理ここから
  $area_route_station_cd_str = implode(", ", $area_route_station_cd);
  $area_route_station_checkBoxSql = "{$area_route_station_cd_column} IN ({$area_route_station_cd_str})";

  $_WHERE .= " AND {$area_route_station_checkBoxSql}";
  //WHERE句生成処理ここまで

  
} else if ($_POST[$area_route_station_cd_column] == 0 || $_SESSION['search'][$area_route_station_cd_column] == 0) {


  $area_route_station_cd = array();

  if ($_POST[$area_route_station_cd_column]) {
    $area_route_station_cd = $_POST[$area_route_station_cd_column];
  }


  //セッション変数を空にする
  $_SESSION['search'][$area_route_station_cd_column] = array();


  //セッション変数に代入する
  $_SESSION['search'][$area_route_station_cd_column] = $_POST[$area_route_station_cd_column];

}
//エリア検索結果表示用ここまで
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/


//築年数（age_cd）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分で築年数を選択された場合(セッションがない場合)
if ($_POST[$age_cd_column] && !$_SESSION['search'][$age_cd_column]) {
  
  $age_cd = $_POST[$age_cd_column];
  $_WHERE .= " AND {$age_cd_column} = {$age_cd}";
  
  
  //セッション変数に代入する
  $_SESSION['search'][$age_cd_column] = $_POST[$age_cd_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$age_cd_column]) {

  $age_cd = $_SESSION['search'][$age_cd_column];
  $_WHERE .= " AND {$age_cd_column} = {$age_cd}";
  
  
//POSTして、セッションがある場合
} else if ($_POST[$age_cd_column] && $_SESSION['search'][$age_cd_column]) {

  $age_cd = $_POST[$age_cd_column];
  
  
  //セッション変数を空にする
  $_SESSION['search'][$age_cd_column] = array();
  
  
    //セッション変数に代入する
  $_SESSION['search'][$age_cd_column] = $_POST[$age_cd_column];

  
  $_WHERE .= " AND {$age_cd_column} = {$age_cd}";
  
} else if ($_POST[$age_cd_column] == 0 || $_SESSION['search'][$age_cd_column] == 0) {
  $age_cd = $_POST[$age_cd_column];
  
  
  //セッション変数を空にする
  $_SESSION['search'][$age_cd_column] = array();
  
  
    //セッション変数に代入する
  $_SESSION['search'][$age_cd_column] = $_POST[$age_cd_column];

}


//築年数検索結果表示用ここから
if ($age_cd) {
  $sql = "SELECT cd, name FROM age WHERE disp_flg = 1 AND cd = {$age_cd}";
  $age_search_query = mysqli_query($connect, $sql);
  $age_search_max   = mysqli_num_rows($age_search_query);

  if ($age_search_max) {
    $age_searchArray = mysqli_fetch_assoc($age_search_query);
  }
}
//築年数検索結果表示用ここまで
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/


//こだわり条件（commitment_cd）処理
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索部分でこだわり条件を選択された場合(セッションがない場合)
if ($_POST[$commitment_cd_column] && !$_SESSION['search'][$commitment_cd_column]) {
  
  $commitment_cd = $_POST[$commitment_cd_column];


  //WHERE句生成処理(チェックボックス)ここから
  $checkBoxArray = array();

  foreach($commitment_cd as $checkbox_value) {

    $checkBoxArray[] = "
      CONCAT(',',
        REPLACE(
          REPLACE(
            REPLACE(commitment_cd, '[', ''),
            ']', 
            ''
          ), 
          '\"', 
          ''
        )
       ,','
       ) LIKE '%,{$checkbox_value},%'
      ";

  }

  $checkBoxSql = implode(" AND ",$checkBoxArray);
  
  $_WHERE .= " AND ({$checkBoxSql})";
  //WHERE句生成処理(チェックボックス)ここまで


  //セッション変数に代入する
  $_SESSION['search'][$commitment_cd_column] = $_POST[$commitment_cd_column];


//(POSTしないで、ページャー移動だけする場合)
} else if (!$_POST['submit'] && $_SESSION['search'][$commitment_cd_column]) {

  $commitment_cd = $_SESSION['search'][$commitment_cd_column];


  //WHERE句生成処理(チェックボックス)ここから
  $checkBoxArray = array();

  foreach($commitment_cd as $checkbox_value) {

    $checkBoxArray[] = "
      CONCAT(',',
        REPLACE(
          REPLACE(
            REPLACE(commitment_cd, '[', ''),
            ']', 
            ''
          ), 
          '\"', 
          ''
        )
       ,','
       ) LIKE '%,{$checkbox_value},%'
      ";

  }

  $checkBoxSql = implode(" AND ",$checkBoxArray);
  
  $_WHERE .= " AND ({$checkBoxSql})";
  //WHERE句生成処理(チェックボックス)ここまで


//POSTして、セッションがある場合
} else if ($_POST[$commitment_cd_column] && $_SESSION['search'][$commitment_cd_column]) {

  $commitment_cd = $_POST[$commitment_cd_column];
  
  
  //セッション変数を空にする
  $_SESSION['search'][$commitment_cd_column] = array();
  
  
    //セッション変数に代入する
  $_SESSION['search'][$commitment_cd_column] = $_POST[$commitment_cd_column];

  
  //WHERE句生成処理(チェックボックス)ここから
  $checkBoxArray = array();

  foreach($commitment_cd as $checkbox_value) {

    $checkBoxArray[] = "
      CONCAT(',',
        REPLACE(
          REPLACE(
            REPLACE(commitment_cd, '[', ''),
            ']', 
            ''
          ), 
          '\"', 
          ''
        )
       ,','
       ) LIKE '%,{$checkbox_value},%'
      ";

  }

  $checkBoxSql = implode(" AND ",$checkBoxArray);
  
  $_WHERE .= " AND ({$checkBoxSql})";
  //WHERE句生成処理(チェックボックス)ここまで


} else if ($_POST[$commitment_cd_column] == 0 || $_SESSION['search'][$commitment_cd_column] == 0) {

  $commitment_cd = $_POST[$commitment_cd_column];
  
  
  //セッション変数を空にする
  $_SESSION['search'][$commitment_cd_column] = array();
  
  
    //セッション変数に代入する
  $_SESSION['search'][$commitment_cd_column] = $_POST[$commitment_cd_column];

}


//こだわり条件検索結果表示用ここから
if ($commitment_cd) {

  $commitment_str = implode(",", $commitment_cd);

  $sql                     = "SELECT cd, name FROM commitment WHERE disp_flg = 1 AND cd IN ($commitment_str)";
  $commitment_search_query = mysqli_query($connect, $sql);
  $commitment_search_max   = mysqli_num_rows($commitment_search_query);

  for ($i = 0; $i < $commitment_search_max; $i++) {
    $commitment_searchArray[] = mysqli_fetch_assoc($commitment_search_query);
    $commitment_searchName[]  = $commitment_searchArray[$i]['name'];
  }

  $commitment_search_name = implode(",", $commitment_searchName);

}
//こだわり条件検索結果表示用ここまで
/*★★★★★★★★★★★★★★★★★★★★★★★★★★★★★*/
//検索ここまで


//AREA取得ここから
$sql        = "SELECT cd, name FROM area WHERE disp_flg = 1 ORDER BY sort";
$area_query = mysqli_query($connect,$sql);
$area_max   = mysqli_num_rows($area_query);

for ($i = 0; $i < $area_max; $i++) {
  $areaArray[$i] = mysqli_fetch_assoc($area_query);
}
//AREA取得ここまで


//AGE取得ここから
$sql       = "SELECT cd, name FROM age WHERE disp_flg = 1 ORDER BY sort";
$age_query = mysqli_query($connect,$sql);
$age_max   = mysqli_num_rows($age_query);

for ($i = 0; $i < $age_max; $i++) {
  $ageArray[$i] = mysqli_fetch_assoc($age_query);
}
//AGE取得ここまで


//COMMITMENT取得ここから
$sql              = "SELECT cd, name FROM commitment WHERE disp_flg = 1 ORDER BY sort";
$commitment_query = mysqli_query($connect,$sql);
$commitment_max   = mysqli_num_rows($commitment_query);

for ($i = 0; $i < $commitment_max; $i++) {

  $commitmentArray[$i]   = mysqli_fetch_assoc($commitment_query);
  $commitmentCdArray[$i] = $commitmentArray[$i]['cd'];

}
//COMMITMENT取得ここまで


//AREA関係カテゴリー取得2ここから
$_SELECT1 = "a1.cd AS a1_cd, a1.name AS a1_name, ";
$_SELECT2 = "a2.cd AS a2_cd, a2.name AS a2_name, a2.area_cd AS a2_a1_cd";
$_FROM1   = "area AS a1";
$_FROM2   = "INNER JOIN area_route AS a2";
$_ON1     = "a1.cd = a2.area_cd";
$_WHERE1  = "a1.disp_flg = 1 AND a2.disp_flg = 1";
$_ORDER1  = "a1.sort, a2.sort";
$sql        = "SELECT {$_SELECT1} {$_SELECT2} FROM {$_FROM1} {$_FROM2} ON {$_ON1} WHERE {$_WHERE1} ORDER BY {$_ORDER1}";
//echo $sql;
$area2_query = mysqli_query($connect, $sql);
$area2_max   = mysqli_num_rows($area2_query);
$area2AllCateArray = array();
for ($i = 0; $i < $area2_max; $i++) {
  $area2Array1[$i] = mysqli_fetch_assoc($area2_query);


  //カテゴリー用に変数定義
  $area2CateArray1 = $area2Array1[$i]['a1_name'];
  $area2CateArray2 = $area2Array1[$i]['a2_name'];


  //カテゴリー階層毎にキーを挿入 全てのカテゴリーをセット
  $area2AllCateArray[$area2CateArray1][$area2CateArray2][] = $area2Array1[$i];

}
//AREA関係カテゴリー取得2ここまで


//AREA関係カテゴリー取得3ここから
$_SELECT1 = "a1.cd AS a1_cd, a1.name AS a1_name, ";
$_SELECT2 = "a2.cd AS a2_cd, a2.name AS a2_name, a2.area_cd AS a2_a1_cd, ";
$_SELECT3 = "a3.cd AS a3_cd, a3.name AS a3_name, a3.area_cd AS a3_a1_cd, a3.area_route_cd AS a3_a2_cd";
$_FROM1   = "area AS a1";
$_FROM2   = "INNER JOIN area_route AS a2";
$_FROM3   = "INNER JOIN area_route_line AS a3";
$_ON1     = "a1.cd = a2.area_cd";
$_ON2     = "a2.cd = a3.area_route_cd";
$_WHERE1  = "a1.disp_flg = 1 AND a2.disp_flg = 1 AND a3.disp_flg = 1";
$_ORDER1  = "a1.sort, a2.sort, a3.sort";
$sql        = "SELECT {$_SELECT1} {$_SELECT2} {$_SELECT3} FROM {$_FROM1} {$_FROM2} ON {$_ON1} {$_FROM3} ON {$_ON2} WHERE {$_WHERE1} ORDER BY {$_ORDER1}";
//echo $sql;
$area3_query = mysqli_query($connect, $sql);
$area3_max   = mysqli_num_rows($area3_query);
$area3AllCateArray = array();
for ($i = 0; $i < $area3_max; $i++) {
  $area3Array1[$i] = mysqli_fetch_assoc($area3_query);


  //カテゴリー用に変数定義
  $area3CateArray1 = $area3Array1[$i]['a1_name'];
  $area3CateArray2 = $area3Array1[$i]['a2_name'];
  $area3CateArray3 = $area3Array1[$i]['a3_name'];


  //カテゴリー階層毎にキーを挿入 全てのカテゴリーをセット
  $area3AllCateArray[$area3CateArray1][$area3CateArray2][$area3CateArray3][] = $area3Array1[$i];

}
//AREA関係カテゴリー取得3ここまで


//AREA関係カテゴリー取得ここから
$_SELECT1 = "a1.cd AS a1_cd, a1.name AS a1_name, ";
$_SELECT2 = "a2.cd AS a2_cd, a2.name AS a2_name, a2.area_cd AS a2_a1_cd, ";
$_SELECT3 = "a3.cd AS a3_cd, a3.name AS a3_name, a3.area_cd AS a3_a1_cd, a3.area_route_cd AS a3_a2_cd, ";
$_SELECT4 = "a4.cd AS a4_cd, a4.name AS a4_name, a4.area_cd AS a4_a1_cd, a4.area_route_cd AS a4_a2_cd, a4.area_route_line_cd AS a4_a3_cd";
$_FROM1   = "area AS a1";
$_FROM2   = "INNER JOIN area_route AS a2";
$_FROM3   = "INNER JOIN area_route_line AS a3";
$_FROM4   = "INNER JOIN area_route_station AS a4";
$_ON1     = "a1.cd = a2.area_cd";
$_ON2     = "a2.cd = a3.area_route_cd";
$_ON3     = "a3.cd = a4.area_route_line_cd";
$_WHERE1  = "a1.disp_flg = 1 AND a2.disp_flg = 1 AND a3.disp_flg = 1 AND a4.disp_flg = 1";
$_ORDER1  = "a1.sort, a2.sort, a3.sort, a4.sort";
$sql        = "SELECT {$_SELECT1} {$_SELECT2} {$_SELECT3} {$_SELECT4} FROM {$_FROM1} {$_FROM2} ON {$_ON1} {$_FROM3} ON {$_ON2} {$_FROM4} ON {$_ON3} WHERE {$_WHERE1} ORDER BY {$_ORDER1}";
//echo $sql;
$area1_query = mysqli_query($connect, $sql);
$area1_max   = mysqli_num_rows($area1_query);
$areaAllCateArray = array();
for ($i = 0; $i < $area1_max; $i++) {
  $areaArray1[$i] = mysqli_fetch_assoc($area1_query);


  //カテゴリー用に変数定義
  $areaCateArray1 = $areaArray1[$i]['a1_name'];
  $areaCateArray2 = $areaArray1[$i]['a2_name'];
  $areaCateArray3 = $areaArray1[$i]['a3_name'];
  $areaCateArray4 = $areaArray1[$i]['a4_name'];


  //カテゴリー階層毎にキーを挿入 全てのカテゴリーをセット
  $area_prefectureArray[$areaCateArray1][]                                 = $areaArray1[$i];
  $area_routeArray[$areaCateArray1][$areaCateArray2][]                     = $areaArray1[$i];
  $area_route_lineArray[$areaCateArray1][$areaCateArray2][$areaCateArray3][] = $areaArray1[$i];

  $areaAllCateArray[$areaCateArray1][$areaCateArray2][$areaCateArray3][$areaCateArray4][] = $areaArray1[$i];

}
//AREA関係カテゴリー取得ここまで


//物件取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = 4;

$sqlAdd = '';
$params = '';

$_select = "cd, date, name, comment, price, area_cd, age_cd, commitment_cd, disp_flg, img1, img2, sort";
$sql     = "SELECT {$_select} FROM property WHERE disp_flg = 1 {$_WHERE} {$sqlAdd}";
//echo $sql;
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql            = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$property_query = mysqli_query($connect,$sql);
$property_max   = mysqli_num_rows($property_query);

for ($i = 0; $i < $property_max; $i++) {

  $propertyArray[$i] = mysqli_fetch_assoc($property_query);

  if ($propertyArray[$i]['img1']) {
    $propertyArray[$i]['img_pass1'] = "{$img_pass}{$propertyArray[$i]['img1']}";
  } else {
    $propertyArray[$i]['img_pass1'] = "./image/no_image.png";
  }

}
//物件取得(ページャー有)ここまで


// ページャー(数字)
$pager = ($property_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


// echo '<pre>';
// print_r($area3AllCateArray);
// echo '</pre>';


?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>


</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Property</h2>

        <div class="searchBox">
          <h3>検索</h3>
          <form id="search" name="search" method="post" action="./property.php">
            <table class="form_1">
              <tr>
                <th>物件名</th>
                <td>
                  <label><input type="text" name="name" value="<?=$name?>"></label>
                </td>
              </tr>
<?php/*
              <tr>
                <th>エリア</th>
                <td>
                  <?php foreach ( (array)$areaArray AS $key => $value ) { ?>
                  <label><input type="checkbox" name="area_cd[]" value="<?=$value['cd']?>" <?php echo ( in_array($value['cd'], $area_cd) ) ? 'checked': ''; ?>><?=$value['name']?></label>
                  <?php } ?>
                </td>
              </tr>
*/?>
              <tr>
                <th>エリア(県名)</th>
                <td>
                  <?php foreach ( (array)$areaArray AS $key1 => $value1 ) { ?>

                    <label><input type="checkbox" name="area_cd[]" value="<?=$value1['cd']?>" <?php echo ( in_array($value1['cd'], $area_cd) ) ? 'checked': ''; ?>><?=$value1['name']?></label>

                  <?php } ?>
                </td>
              </tr>
              <tr>
                <th>エリア(路線種別名)</th>
                <td>
                  <?php foreach ( (array)$area2AllCateArray AS $key1 => $value1 ) { ?>

                  <p class="cateTitle1"><span><?=$key1?></span></p>
                    <?php foreach ( (array)$value1 AS $key2 => $value2 ) { ?>

                    <label><input type="checkbox" name="area_route_cd[]" value="<?=$value2[0]['a2_cd']?>" <?php echo ( in_array($value2[0]['a2_cd'], $area_route_cd) ) ? 'checked': ''; ?>><?=$value2[0]['a2_name']?></label>

                    <?php } ?>

                  <?php } ?>
                </td>
              </tr>
              <tr>
                <th>エリア(路線詳細名)</th>
                <td>
                  <?php foreach ( (array)$area3AllCateArray AS $key1 => $value1 ) { ?>

                  <p class="cateTitle1"><span><?=$key1?></span></p>
                    <?php foreach ( (array)$value1 AS $key2 => $value2 ) { ?>

                    <p class="cateTitle2"><span><?=$key2?></span></p>
                      <?php foreach ( (array)$value2 AS $key3 => $value3 ) { ?>

                      <label><input type="checkbox" name="area_route_line_cd[]" value="<?=$value3[0]['a3_cd']?>" <?php echo ( in_array($value3[0]['a3_cd'], $area_route_line_cd) ) ? 'checked': ''; ?>><?=$value3[0]['a3_name']?></label>

                      <?php } ?>

                    <?php } ?>

                  <?php } ?>
                </td>
              </tr>
              <tr>
                <th>エリア(駅名)</th>
                <td>
                  <?php foreach ( (array)$areaAllCateArray AS $key1 => $value1 ) { ?>
                  <p class="cateTitle1"><span><?=$key1?></span></p>
                    <?php foreach ( (array)$value1 AS $key2 => $value2 ) { ?>

                    <p class="cateTitle2"><span><?=$key2?></span></p>
                      <?php foreach ( (array)$value2 AS $key3 => $value3 ) { ?>

                      <p class="cateTitle3"><span><?=$key3?></span></p>
                        <?php foreach ( (array)$value3 AS $key4 => $value4 ) { ?>

                          <label><input type="checkbox" name="area_route_station_cd[]" value="<?=$value4[0]['a4_cd']?>" <?php echo ( in_array($value4[0]['a4_cd'], $area_route_station_cd) ) ? 'checked': ''; ?>><?=$value4[0]['a4_name']?></label>

                        <?php } ?>

                      <?php } ?>

                    <?php } ?>

                  <?php } ?>
                </td>
              </tr>
              <tr>
                <th>築年数</th>
                <td class="checkboxCell">
                  <ul>
                    <li>
                      <label>指定なし<input type="radio" name="age_cd" value="" <?php echo ($age_cd == "") ? 'checked': ''; ?>></label>
                    </li>
                    <?php for ($i = 0; $i < $age_max; $i++) { ?>
                    <li>
                      <label><?=$ageArray[$i]['name']?><input type="radio" name="age_cd" value="<?=$ageArray[$i]['cd']?>" <?php echo ($age_cd == $ageArray[$i]['cd']) ? 'checked': ''; ?>></label>
                    </li>
                    <?php } ?>
                  <ul>               
                </td>
              </tr>

              <tr>
                <th>こだわり条件</th>
                <td class="checkboxCell">
                  <ul>
                    <?php for ($i = 0; $i < $commitment_max; $i++) { ?>
                    <li>
                      <label><?=$commitmentArray[$i]['name']?><input type="checkbox" name="commitment_cd[]" value="<?=$commitmentArray[$i]['cd']?>" <?php echo ( in_array($commitmentArray[$i]['cd'], $commitment_cd) ) ? 'checked': ''; ?>></label>
                    <?php } ?>
                    </li>
                  </ul>
                </td>
              </tr>
            </table>
            <input type="hidden" name="search_flg" value="1">
            <input type="submit" name="submit" value="SEARCH">
          </form>

          <?php if ($_POST['search_flg'] == '1') { ?>
          <div class="resultBox">
            <p>検索結果は以下の通りです。</p>
            <p>物件名 : <?=$name?></p>
            <p>エリア : <?=$area_searchNamwe_str?></p>
            <p>築年数 : <?=$age_searchArray['name']?></p>
            <p>こだわり条件 : <?=$commitment_search_name?></p>
          </div>
          <?php } ?>

        </div>
        
        <ul class="itemList">

            <?php foreach ( (array)$propertyArray AS $key => $val ) { ?>
            <li>          
                <dl>
                    <dt><a href="./property_detail.php?cd=<?=$val['cd']?>"><img src="<?=$val['img_pass1']?>" alt="<?=$val['name']?>"></a></dt>
                    <dd>
                        <h3 class="name"><?=$val['name']?></h3>
                        <p class="price"><?=number_format($val['price'])?>円</p>
                        <p class="comment"><?=nl2br($val['comment'])?></p>
                        <p class="linkBtn"><a href="./property_detail.php?cd=<?=$val['cd']?>">Detail</a></p>
                    </dd>
                </dl>
            </li>
            <?php } ?>
        
        </ul>

        <?=$pager?>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
