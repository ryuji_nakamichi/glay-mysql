﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Member_TERU</h2>
        
        <ul class="member">
        
            <li>
              <img src="image/teru02_member.jpg" width="235" height="330" alt="teru">
            </li>
            
            <li>
              <img src="image/teru01_member.jpg" width="235" height="330" alt="teru">
            </li>
        </ul>
        
        <p class="bio_honbun">
        
          <section>
          
          <h3>1987年</h3>
         
        夏頃、小・中学校と一緒である同じサッカー部の先輩に「ちょっとドラムをやってみないか？」と勧誘され、聖飢魔IIのコピーバンドに加入。ドラムを担当し、文化祭で披露した。しかし、加入当初TERUの家にはドラムセットなどなく、親戚の叔母が営むスナック（TERUがよく手伝って料理をしていた）の椅子などを使って練習していたという。後に、購入。本格的にドラムに熱中していく。TAKUROと共にGLAYの活動を始めた頃には、TERUの部屋は防音処置が施されていたとTAKUROは語っている。
        
         </section>

          <section>
          
          <h3>1988年</h3>
         
       3月、小・中学校の同級生であるTAKUROに誘われ、GLAYを結成。当初はドラマーとしての加入であった。しかし、TAKUROがTERUの家にまだ歌の入っていないデモテープを置き忘れ、歌があったほうが何かとやりやすいだろうと考えたTERUが歌を入れてみたところ、それを聴いたTAKUROが「神がかった声」と感じて腰を抜かした。TAKUROが人生において腰を抜かすほど驚いたのは、この時以外にはBOØWYの「B・BLUE」を初めて聴いた時だけだという。これがボーカリストへ転向するきっかけとなり、新しいドラマーが見つかるまでマイクを天井からぶら下げてドラムを叩きながら歌っていた。しかし、TERU本人はTAKUROが受けた衝撃をあまり理解出来ていなかったらしく、先輩を新しいボーカルとして連れて来て自分はドラマーに戻ろうとしたが、TAKUROにより却下されボーカルに戻されたというエピソードがある。また、TAKUROは「TERUは自分がボーカルではなく、ドラマーでありたかった」と語っている。同年7月には、GLAYとしての初ライブを行う。
        
         </section>

          <section>
          
          <h3>1989年</h3>
         
       同じ学校の同学年の友達と「花とゆめ」と言うバンドを結成。GLAYがライヴハウス等での活動がメインだったのに対して、こちらは主に学校内でのイベントや文化祭でのライブが中心の所謂「学園祭バンド」だった。TERUが、姉の結婚式でTAKUROと「ずっと2人で…」の原曲を演奏・披露したのも、この頃の話である。
        
         </section>

          <section>
          
          <h3>1990年~1993年</h3>
         
高校卒業と同時にGLAYの成功を夢見て上京。赤羽の凸版印刷に勤めるかたわらバンド活動を続けていたが、両立出来ず、試用期間明けで辞めてしまった。以降、居所を埼玉県所沢市や飯能市等に移し、その後は様々な仕事をしながら、バンド活動を行う。        
         </section>

          <section>
          
          <h3>1994年</h3>
         
       5月25日 - GLAYのボーカリストとしてシングル「RAIN」でメジャーデビュー。
        
         </section>

          <section>
          
          <h3>1999年</h3>
         
12月6日 - メンバープロデュース・ファンクラブ限定ライブの一環として、TERUプロデュース「THE MEN WHO SOLD THE WORLD」をZepp Tokyoで行う。男性限定のライブであったことから、「男ナイト」と呼ばれ、以後福岡、大阪でも行われることとなる。        
         </section>

          <section>
          
          <h3>2001年</h3>
         
4月 - 坂本龍一を中心とする地雷排除支援活動地雷ZEROに協力し、チャリティーソング「ZERO LANDMINE」に参加。         </section>

          <section>
          
          <h3>2002年</h3>
         
4月29日 - PUFFYの大貫亜美と再婚したことを発表。
8月27日 - TERUプロデュース・男性限定ライブ「BOYS ONLY NIGHT」を大阪BIGCATで行う。通算3回目の男ナイトで、チケットはSOLD OUT。このライブ限定のグッズは現代女浮世絵師であるツバキアンナとTERUのコラボレーションであった。
         </section>

         <section>

          <h3>2003年</h3>
         
3月9日 - 大貫亜美との間に女児が誕生（TERUにとっては第3子）。
         </section>

         <section>

          <h3>2004年</h3>
         
HISASHIと共に禁煙を開始。
         </section>

         <section>

          <h3>2005年</h3>
         
7月18日 - 千葉ポートパーク円形広場野外ステージにて、自身がDJを務めるラジオ番組の開始10周年を記念して、イベント『TERU ME NIGHT GLAY 10th ANNIVERSARY SPECIAL 〜Summer of 05〜』を行う。
ほっとけない 世界のまずしさキャンペーンに参加。
12月3日、4日に行われた『White Band FES.（ホワイトバンドフェス）』にGLAYとして参加。
12月21日 - BUCK-TICKの20周年を記念したトリビュート・アルバム『PARADE〜RESPECTIVE TRACKS OF BUCK-TICK〜』に「悪の華」でHISASHIと共にrallyとして参加。
         </section>

         <section>

          <h3>2007年</h3>
         
7月1日から2年間、公共広告機構（現：ACジャパン）のCMに出演。CMテーマは「エイズ」。
9月8日 - 「BUCK-TICK FES 2007 ON PARADE」にrallyとして出演。
10月14日 - レッドシューズのイベントライブWEAR RED SHOES Vol.4にゲストミュージシャンとして参加。
12月3日 - レッドリボンキャンペーンRED RIBBON LIVE 2007にTAKUROとともに出演。
12月27日 - JACK IN THE BOX 2007にTAKUROと共にシークレットゲストとして出演。L'Arc〜en〜Cielのhydeとツインボーカルをとり、「誘惑」「HONEY」の二曲を披露した。
         </section>

         <section>

          <h3>2008年</h3>
         
11月29日 - レッドリボンキャンペーンRED RIBBON LIVE 2008に出演。         </section>

         <section>

          <h3>2009年</h3>
         
3月11日 - MCUのアルバム『SHU・HA・RI～STILL LOVE～』の収録曲「STILL LOVE」に、ボーカルとして参加[10]。
11月28日 - レッドリボンキャンペーンRED RIBBON LIVE 2009に出演。
         </section>

         <section>

          <h3>2010年</h3>
         
8月28日 - ドラマー永井利光が発起人として開催された宮崎口蹄疫チャリティイベント「MGプロジェクト『宮崎がんばろう！』チャリティー IN 福岡」のプレミアムイベント「"SONG FOR MIYAZAKI" PREMIUM NIGHT」に、友情出演。
11月7日 - 宮崎口蹄疫チャリティイベント「口蹄疫義援イベント『がんばっど宮崎』水平線の花火と音楽～SUPER HANABI ILLUSION 1万発～」に、出演。
         </section>

         <section>

          <h3>2011年</h3>
         
3月16日 - 東日本大震災発生の際に、自身のTwitter上で被災者に向けての新曲を無料公開した。この曲は、後にチャリティソング「Thank you for your love」として発表された。
5月20日･22日 - 布袋寅泰「30th ANNIVERSARY 第二弾 HOTEI THE ANTHOLOGY “威風堂々”TONIGHT I'M YOURS! ～GUITARHYTHM GREATEST HITS & REQUEST～」にスペシャル・ゲストとして参加。
10月11日 - MONGOL800主催の野外フェス「What a Wonderful World!! 11」に出演。
11月27日 - レッドリボンキャンペーンRED RIBBON LIVE 2011に出演。
         </section>

         <section>

          <h3>2012年</h3>
         
2月 - グリコのCM「みんなに笑顔を届けたい。冬」編に出演。
7月11日 - 江崎グリコとTOKYO FM系「やまだひさしのラジアンリミテッドF」による東日本大震災の被災地支援活動にTAKUROと共に参加し、宮城県本吉郡南三陸町にてシークレットライブを開催した。
11月25日 - レッドリボンキャンペーンRED RIBBON LIVE 2012に出演(ゲストとして、TAKUROも出演)。
12月4日 - GLAYのマスコットキャラ「ズラー」のモバイル育成ゲーム「ズラーコレクション」(GLAY MOBILE限定)をプロデュース。
         </section>

         <section>

          <h3>2013年</h3>
         
1月 - グリコの「みんなに笑顔を届けたい CM」の新シリーズに出演。
3月14日 - ライブイベント「みんなに笑顔を届けたいLIVE!2013 supported by Glico」に出演。
10月27日 - VAMPS主催のライブイベント「HALLOWEEN PARTY 2013」に、TAKUROと共に出演[24]。
11月13日 - 台湾のロックバンドMayday(五月天)のベストアルバム「Mayday×五月天 the Best of 1999‐2013」のリード曲「Dancin' Dancin' feat. TERU (GLAY)」に、TAKUROと共に参加[25]。
11月24日 - レッドリボンキャンペーンRED RIBBON LIVE 2013に出演(ゲストとして、TAKUROも出演)。
         </section>

         <section>

          <h3>2014年</h3>
         
9月12日 ・22日 - 日本テレビ「ZIP!」あさアニメ「おはようハクション大魔王」にてアニメアフレコ初挑戦の上、本人役で出演。        
 </section>


         <section>

          <h3>人物</h3>
         
家族構成は両親、姉、妹。<br>

両肩と胸、右手人差し指にタトゥーを入れている。<br>

抜群の運動神経の持ち主であり、ライブなどでもしばしばマイクスタンドを使ったハイジャンプを披露する。中学校時代は野球部、高校時代はサッカー部に所属。2013年からJIROと共にフットサルを開始。2014年現在は「TFA」というチームを作り、活動している。<br>

石原さとみやEXILE、TAKAHIROなどTERUのファンを公言する芸能人も多い。<br>

趣味は料理であり、その腕前はメンバーも絶賛するほどである。<br>

好きな食べ物は鶏の唐揚げ。本人曰く、「肉料理の王様」。しかし、幼い頃に鳥（雀）に襲われた経験から、鳥（特に小鳥）自体は嫌いである。「BEAUTIFUL DREAMER」のPVで鷹を手懐けるシーンでは後に「とても怖かった」と語っている。<br>

好きな街のひとつに京都をあげていて、Twitterや京都でのライブやイベントの度に公言したり、プライベートでも比較的頻繁に京都を訪ねては散策するほどお気に入りである。<br>

GLAYとしてはボーカルの他にアコースティック・ギター、ブルースハープ、ピアノ、タンバリンなどを演奏することもある。また、前述のようにGLAY結成当初はドラムを担当していた。自身作詞作曲の「Little Lovebirds」ではドラムを演奏している他、「SOUL LOVE」のPVでドラムを叩いているシーンが見られる。<br>

ライブ等で腕を大きく広げるクセはお笑いなどでネタにされるほか、アルバム『GLAY』でも「GLAYを象徴するもの」として腕を広げるTERUの写真がジャケットになっている。<br>

ブログをはじめとしたインターネット上での活動も精力的で、2008年からブログ「365の光り」の更新を開始した（2009年4月に更新終了）。また、メンバーの中で最も行動力があり、社会問題などが起きると率先的に動く（RED RIBBONから始まり、2011年に発生した東日本大震災の際には発生直後から自身のTwitterで避難所の情報拡散や被災者に向けての新曲の無料公開、完全な非公式での現地への支援活動に参加するなどの行動力をみせた）。

 </section>

        
        </p>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
