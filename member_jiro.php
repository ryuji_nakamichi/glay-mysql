﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Member_JIRO</h2>
        
        <ul class="member">
        
            <li>
              <img src="image/jiro02_member.jpg" width="235" height="330" alt="teru">
            </li>
            
            <li>
              <img src="image/jiro01_member.jpg" width="235" height="330" alt="teru">
            </li>
        </ul>
        
        <p class="bio_honbun">
        
          <section>
          
          <h3>出生～小学校時代</h3>
         
        頑固な職人の父を持ち、母、そして姉が1人、妹が2人いる。
子供の頃、JIROの家では父親の言う事は絶対に従わなければならず、口答えをすれば殴られ、テレビのチャンネル優先権も父親が握っており、まさに"亭主関白"であった。
JIROは小さいながらも力があり、父親が仕事に出て行くのに付いて行こうとしたのを、とめようとした母親に抑えられ、肩を脱臼したこともある。


        
         </section>

          <section>
          
          <h3>中学時代</h3>
         
          バスケットボール部に入部するが1年程で退部する。時間的に余裕があったJIROは音楽に興味を持ち、エレキギターをもっている友人にギターを借りる。しかし、母親に「そんな高価なものはすぐに返しなさい」と叱られ、渋々ギターは返したがこのことがJIROに「何が何でもギターを買う」という決心をもち、新聞配達のアルバイトで貯めたお金でギターを買った。その後、友人と「ネクストビート」というバンドを結成。    
         </section>

          <section>
          
          <h3>高校時代</h3>
         
          高校入学後に「セラヴィ」というバンドにギターとして加入。高3の頃にベースに転向、「ピエロ」に加入した。理由は「ピエロに入れるならベースでもいい。」ということから。友人の持っているベースと自分の持っているギターを交換した。
1年早く上京していたGLAYが帰郷した際に対バンする。その打ち上げでTAKUROに東京へ行くことを薦められ、高校卒業後はピエロのメンバーと上京することになった。
        
  </section>

          <section>
          
          <h3>上京～インディーズ時代</h3>
         
          上京したものの、「ピエロ」は解散。その後はいくつかのバンドを渡り歩く。TAKUROに誘われ、GLAYに加入。当初は「社交辞令程度の軽い気持ち」だったようだ。しかし、持ち前の積極的な行動力によりGLAYの人気は次第に上がっていた。


  
          </section>

          <section>
          
          <h3>メジャーデビュー以降</h3>
         
         1994年5月 - GLAYのベーシストとしてシングル「RAIN」でメジャーデビュー。デビュー時にはバンド内で唯一短髪で髪も逆立てない普通の格好をしていたが、90年代後半は奇抜なヘアメイクと派手な衣装を着るようになる。特に1998年辺りの「アロエヘア」（当時出されたアルバム『pure soul』と掛けて“pure soulヘア”とも呼ばれる）はとても特徴的なものとなった。
         

           

         </section>

          <section>
          
          <h3>1999年</h3>
         
          音楽雑誌『WHAT's IN?』で連載していたものをまとめた、初の単行本『キャラメルブックス』を発売。

           </section>

          <section>
          
          <h3>2000年</h3>
         
          GLAY ARENA TOUR 2000 ”HEAVY GAUGE”の頃、精神的に参っていた時期があった。ライブ中もずっと下を向くなどの行為が目立ち、ファンからも心配の声が上がった。TAKUROも「解散してもいいからJIROを休ませてあげたい」と発言するほどであったが、その時期を乗り越え現在に至る。その時期を支えてくれた、JIROが毎月連載していた音楽雑誌『WHAT's IN?』の編集者と同年12月に結婚。
          </section>

          <section>
          
          <h3>2001年</h3>
         
         『キャラメルブックス』の続編、『キャラメルパビリオン』を発売。  
          </section>

         <section>

          <h3>2004年</h3>
         
          the pillowsのトリビュート・アルバム『シンクロナイズド・ロッカーズ』に参加。               
          </section>

         <section>

          <h3>2005年</h3>
         
          the pillowsの山中さわおとストレイテナーのナカヤマシンペイと共にTHE PREDATORSを結成。<br>
7月6日 - インディーズレーベルよりミニアルバム『Hunting!!!!』を発売。<br>
7月19日 - 横浜BLITZでのフリーライブを皮切りに、『FM802 MEET THE WORLD BEAT 2005やRISING SUN ROCK FESTIVAL 2005 in Ezo』に出演した。

         </section>

         <section>

          <h3>2006年</h3>
         
          THE PREDATORSとして『ARABAKI ROCK FEST. 06』に出演。
         </section>

         <section>

          <h3>2008年</h3>
         
          THE PREDATORSの活動を再開。<br>
10月15日-セカンドミニアルバム『牙をみせろ』を発売。<br>
SHOOT THE MOON TOURを開催。

         </section>

         <section>

          <h3>2009年</h3>
         
          2月4日『LIVE DVD-SHOOT THE MOON TOUR 2008.11.4 Zepp Zokyo』を発売。         </section>




        
        </p>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
