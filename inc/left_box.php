<?php


//新着情報取得
$_SELECT = "cd, DATE_FORMAT(date, '%Y/%m/%d') AS date, title, comment, link, target, disp_flg, img1, img2";
$_TABLE  = "news";
$_WHERE  = "WHERE disp_flg = 1";
$_ORDER  = "date DESC";

$recordControldObj  = new recordControlClass;
$newsArray          = $recordControldObj->getRecord($connect, $_SELECT, $_TABLE, $_WHERE, $_ORDER);

$news_max = count($newsArray);
$img_max  = 2;

for ($i = 0; $i < $news_max; $i++) {
	
	if ($newsArray[$i]['link']) {
		$newsArray[$i]['link_comment'] = "<p class=\"honbun1\"><a href=\"{$newsArray[$i]['link']}\" target=\"{$newsArray[$i]['target']}\">{$newsArray[$i]['comment']}</a></p>";
	} else {
		$newsArray[$i]['link_comment'] = "<p class=\"honbun1\">{$newsArray[$i]['comment']}</p>";
	}

	$img_cnt = 1;

	for ($j = 0; $j < $img_max; $j++) {

		if ($newsArray[$i]["img{$img_cnt}"]) {
			$newsArray[$i]["img_tag"][] = "<li><img src=\"{$img_pass}{$newsArray[$i]["img{$img_cnt}"]}\" alt=\"{$newsArray[$i]['title']}\"></li>";
		}

		$img_cnt++;

	}
	
}
?>

<div class="left_box">

	<h2>News</h2>

	<?php if ($news_max) { ?>
	<ul class="topics">

		<?php foreach ( (array) $newsArray AS $key => $val ) { ?>
		<li class="f_list">
			<p class="date"><?=$val['date']?></p>
			<h3><?=$val['title']?></h3>
			<ul class="imgList">

				<?php foreach ( (array) $val["img_tag"] AS $img_key => $img_val ) { ?>
					<?=$img_val?>
				<?php } ?>

			</ul>
			<?=nl2br($val['link_comment'])?>
		</li>
		<?php } ?>

	</ul>
	<?php } else { ?>
	<p style="text-align: center;">現在、Newsはございません。</p>
	<?php } ?>

</div>