<?php


//live取得
$_SELECT = "cd, title, date, comment";
$_TABLE  = "live";
$_WHERE  = "WHERE disp_flg = 1";
$_ORDER  = "date DESC";
$_LIMIT  = "LIMIT 3";

$getLive2RecordObj  = new recordControlClass;
$liveArray2         = $getLive2RecordObj->getRecord($connect, $_SELECT, $_TABLE, $_WHERE, $_ORDER, $_LIMIT);

$live_max2 = count($liveArray2);


?>

<div class="right_box">

    <h2>Live Schedule</h2>

    <p class="more">
        <a href="./live.php"><img src="image/more.png" width="60" height="20" alt="more"></a>
    </p>

    <ul class="topics">

        <?php if ($live_max2) { ?>

            <?php for ($i = 0; $i < $live_max2; $i++) { ?>
            <li class="f_list">

            <p class="date"><?=$liveArray2[$i]['date']?></p>

            <h3><?=$liveArray2[$i]['title']?></h3>

            <p class="honbun2"><?=nl2br($liveArray2[$i]['comment'])?></p>

            </li>
            <?php } ?>

        <?php } else { ?>
        <li class="f_list">※現在、LIVE情報はございません。</li>
        <?php } ?>

    </ul>

</div>