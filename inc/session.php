<?php

define("SESSIONUSERNAME", "GLAY");


//セッションに名前を付ける
session_name(SESSIONUSERNAME.'PHPSESSID');


//SESSION開始
session_start();


//セッションID(PHPSESSID)を毎回変更する（セッションハイジャック対策）
session_regenerate_id(true);

if ($_REQUEST['mode'] && $_REQUEST['mode'] == 'logout') {


	//ログアウトしたらログインフラグを初期化
	$_SESSION['user_login']["login_flg"] = array();


	//ログアウトしたらユーザー情報を初期化
	$_SESSION['user']["user_info"]       = array();


	//ログアウトしたらお買い物の流れでの情報を初期化
	$_SESSION['shopping']                = array();

	//クッキーのセッションIDも削除
	//setcookie(SESSIONUSERNAME.'PHPSESSID', '', time()-42000, '/');

}


// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';


//sessionユーザー名
$session_user_name = SESSIONUSERNAME;