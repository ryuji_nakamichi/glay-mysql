<?php


//ログインしていないが、カートに商品がある場合
if ($_SESSION['user_login']['login_flg'] !== 1 && $_SESSION['cartInfo']['cartArrayMax']) {
  header("Location: shopping.php");
  exit;


//ログインしているが、カートに商品がない場合
} else if ($_SESSION['user_login']['login_flg'] === 1 && !$_SESSION['cartInfo']['cartArrayMax']) {
  header("Location: ./item.php");
  exit;
}