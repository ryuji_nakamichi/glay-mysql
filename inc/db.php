<?php


//db接続情報
define("DBHOST", "localhost");
define("DBUSER", "glay");
define("DBPASS", "glay");
define("DBNAME", "glay");


//定数定義
define("SITEURL", "//gos00191.go-server.jp/glay/admin/");
define("SITEURLHP", "//gos00191.go-server.jp/glay/");


//db接続
$connect = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);

if (!$connect) {
    die( '接続失敗です。'.mysqli_error() );
}


//url
$site_url_hp = SITEURLHP;
$site_url    = SITEURL;


//画像パス
$img_pass = "{$site_url}upload/";


//headerに表示する現在時刻取得
$now_day  = date("Y年m月d日");
$now_hour = date("G時i分");


//基本情報取得ここから
$_SELECT          = "*";
$sql              = "SELECT {$_SELECT} FROM basic WHERE disp_flg = 1";
$basic_query = mysqli_query($connect, $sql);
$basic_max   = mysqli_num_rows($basic_query);

if ($basic_max) {
	$basicArray = mysqli_fetch_assoc($basic_query);
}

$user_name              = $basicArray['user_name'];
$title                  = $basicArray['title'];
$keyword                = $basicArray['keyword'];
$description            = $basicArray['description'];
$h1                     = $basicArray['h1'];
$user_mail              = $basicArray['mail'];
$user_mobile_phone_mail = $basicArray['mobile_phone_mail'];
//基本情報取得ここまで


//ログインID・ログインPASSWORD・税率・ポイント加算条件・ポイント加算率取得ここから
$_SELECT          = "login_id, login_password, item_tax, shipping_cost, shipping_cost_free, add_point, add_point_increment";
$sql              = "SELECT {$_SELECT} FROM setting WHERE disp_flg = 1";
$login_info_query = mysqli_query($connect, $sql);
$login_info_max   = mysqli_num_rows($login_info_query);

if ($login_info_max) {
	$login_infoArray = mysqli_fetch_assoc($login_info_query);
}

$login_id       = $login_infoArray['login_id'];
$login_password = $login_infoArray['login_password'];
$item_tax       = $login_infoArray['item_tax'];


//念の為int型に変換しておく
$shipping_cost       = (int) $login_infoArray['shipping_cost'];
$shipping_cost_free  = (int) $login_infoArray['shipping_cost_free'];
$add_point           = (int) $login_infoArray['add_point'];
$add_point_increment = (int) $login_infoArray['add_point_increment'];
//ログインID・ログインPASSWORD・税率・ポイント加算条件・ポイント加算率取得ここまで


// 1ページ表示件数
$kensu_  = 30;