<meta http-equiv=”X-UA-Compatible”content=”IE=Edge,chrome=1>
<meta charset="utf-8">
<title><?=$title?></title>
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scaleble=no">
<meta name="keywords" content="<?=$keyword?>">
<meta name="description" content="<?=$description?>">
<meta name="robots" content="noindex,nofollow,noarchive">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="stylesheet" href="<?=$site_url_hp?>css/reset.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=$site_url_hp?>css/common.css">
<link href="<?=$site_url_hp?>plugin/jquery-ui.css" rel="stylesheet">
<link href="<?=$site_url_hp?>css/slick.css" rel="stylesheet">
<link href="<?=$site_url_hp?>css/slick-theme.css" rel="stylesheet">

<script src="<?=$site_url_hp?>js/jquery-1.11.0.min.js"></script>
<script src="<?=$site_url_hp?>plugin/external/jquery/jquery.js"></script>
<script src="<?=$site_url_hp?>plugin/jquery-ui.js"></script>
<script src="<?=$site_url_hp?>js/jquery.easing.1.3.js"></script>
<script src="<?=$site_url_hp?>js/common.js"></script>
<script src="<?=$site_url_hp?>js/slick.js"></script>
<script src="<?=$site_url_hp?>js/main_visual.js"></script>
<script src="<?=$site_url_hp?>js/pageTop.js"></script>
<script src="<?=$site_url_hp?>js/loading.js"></script>
<?php/*<script src="//embed.small.chat/T3VMYQD3MG6R0FUY8K.js" async></script>*/?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'contact.php') ) { ?>
<script src="<?=$site_url_hp?>js/jquery.autoKana.js"></script>
<script src="<?=$site_url_hp?>js/mail_send.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'entry.php') ) { ?>
<script src="<?=$site_url_hp?>js/jquery.autoKana.js"></script>
<script src="<?=$site_url_hp?>js/jquery.jpostal.js"></script>
<script src="<?=$site_url_hp?>js/entry_send.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'mypage_change.php') ) { ?>
<script src="<?=$site_url_hp?>js/jquery.autoKana.js"></script>
<script src="<?=$site_url_hp?>js/jquery.jpostal.js"></script>
<script src="<?=$site_url_hp?>js/mypage_change_send.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'mypage_delivery.php') ) { ?>
<script src="<?=$site_url_hp?>js/mypage_delivery_delete.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'mypage_favorite.php') || strpos($_SERVER['SCRIPT_NAME'], 'item_detail.php') ) { ?>
<script src="<?=$site_url_hp?>js/favorite_item_control.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'mypage_delivery_addr.php') ) { ?>
<script src="<?=$site_url_hp?>js/jquery.autoKana.js"></script>
<script src="<?=$site_url_hp?>js/jquery.jpostal.js"></script>
<script src="<?=$site_url_hp?>js/mypage_delivery_addr_send.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'shopping.php') ) { ?>
<script src="<?=$site_url_hp?>js/jquery.autoKana.js"></script>
<script src="<?=$site_url_hp?>js/jquery.jpostal.js"></script>
<script src="<?=$site_url_hp?>js/shopping_input.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'shopping_payment.php') ) { ?>
<script src="<?=$site_url_hp?>js/shopping.js"></script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'login.php') ) { ?>
<script src="<?=$site_url_hp?>js/login_control.js"></script>
<?php } ?>

<script>
$(function() {
	$('#slideBtn, .closeBtn').on('click', function() {
		$('.slideNavi').stop().slideToggle();
		return false;
	});
});
</script>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'item_detail.php') || strpos($_SERVER['SCRIPT_NAME'], 'cart.php') ) { ?>
<script>
$(function() {
  $( ".item_number" ).spinner();
});
</script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'cart.php') ) { ?>
<script>

$(function() {
  //半角数字チェック(半角数字以外削除)ここから

  $('.item_number').on('focus', function() {
    var moto_str = $(this).val();


      $(this).on('blur', function() {

        var str = $(this).val();

        if ( str.match( /[^0-9]+/ ) ) {
          
          alert('半角数字をご入力ください。');
          $(this).val(str.replace(/[^0-9]/g, ""));

          return false;
          
        } else if (str > 10) {
          alert('一度に11個以上はご購入できません。');
          $(this).val(moto_str);

          return false;
        }

    });
    
  });

  $('input[type="checkbox"]').one('change', function() {

    if ( $(this).prop('checked') ) {
      alert('カートから削除する場合は、チェックが入っている状態で「内容を変更する」ボタンを押してください。');
    }

  });



  //半角数字チェック(半角数字以外削除)ここまで
});
</script>
<?php } ?>

<?php if ( strpos($_SERVER['SCRIPT_NAME'], 'item_detail.php') ) { ?>
<script>

$(function() {
  //半角数字チェック(半角数字以外削除)ここから
  $('.cartBtn').on('click', function() {

  var str = $('input[name="item_number"]').val();

    if ( str.match( /[^0-9]+/ ) ) {
      
      alert('半角数字をご入力ください。');
      $('input[name="item_number"]').val(str.replace(/[^0-9]/g, ""));

      return false;
    } else if (str > 10) {
      alert('一度に11個以上はご購入できません。');
      $('input[name="item_number"]').val(10);
      
      return false;
    }

  });
  //半角数字チェック(半角数字以外削除)ここまで
});
</script>
<?php } ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112912462-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112912462-1');
</script>