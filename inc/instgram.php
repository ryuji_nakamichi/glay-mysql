<?php


	//2017_03_09 nakamichi インスタグラム取得関数(アクセストークン取得タイプ)
	 function getInstgram ($_user_acount_name, $_access_token) {
		
		/***************************************************************************************
		インスタグラム取得ここから
		***************************************************************************************/
		/* 可変箇所 ****************************************************************************************/


		//アクセストークン 
		define("INSTAGRAM_ACCESS_TOKEN", $_access_token); 


		// ユーザアカウント名 
		$user_account = $_user_acount_name; 


		// 取得件数
		$count = 4;
		 
		/**************************************************************************************** 可変箇所 */

		$photos = array(); 
		$photos_data = array(); 


		// ユーザアカウント名からユーザデータを取得する。 
		$user_api_url = 'https://api.instagram.com/v1/users/search?q=' . $user_account . '&access_token=' . INSTAGRAM_ACCESS_TOKEN;
		$user_data = json_decode(@file_get_contents($user_api_url)); 
		//echo $user_api_url;
		/*
		if ($user_data) {
			echo 'OK';
		} else {
			echo 'NG';
		}
		*/

		// 取得したデータの中から正しいデータを選出 
		foreach ($user_data->data AS $user_data) {

		    if ($user_account == $user_data->username) {
		        $user_id = $user_data->id;
		    }

		}


		// 特定ユーザの投稿データ最新××件を取得する
		$photos_api_url = 'https://api.instagram.com/v1/users/'.$user_id.'/media/recent?access_token=' . INSTAGRAM_ACCESS_TOKEN . "&count=".$count;
		$photos_data    = json_decode(@file_get_contents($photos_api_url));
		
		/*
		echo '<pre style="display: none;">';
		print_r($photos_data);
		echo '</pre>'.'test';
		*/

		/***************************************************************************************
		インスタグラム取得ここまで
		***************************************************************************************/
		
		return $photos_data;
		
	}

		//インスタグラム取得(第一引数に「ユーザーアカウントネーム」、第二引数に「アクセストークン」)
		$photos_data = getInstgram("nakamichiryuji", "3611053369.aa641dc.1d097932b05e406db523b3b7076c5427");
//print_r($photos_data);

?>

<div class="instgramBox">

	<ul class="instgram">

		<?php foreach ( (array)$photos_data->data AS $photo) { ?>
		<li>
		    <a href="<?=$photo->link?>" class="link">
		        <span class="image"><img src="<?=$photo->images->standard_resolution->url?>"></span>
		        <span class="title"><?=$photo->caption->text?></span>
		    </a>
		</li>
		<?php } ?>

	</ul>

</div>