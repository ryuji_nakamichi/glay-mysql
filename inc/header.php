<header>

  <h1><a href="./"><img src="image/rogo_03.png" alt="GLAY ~Official Site~"></a></h1>
  <nav class="g_nav">
    <ul>
      <li><a href="./">HOME</a></li>
      <li><a href="./biography.php">BIOGRAPHY</a></li>
      <li><a href="./member.php">MEMBER</a></li>
      <li><a href="./discography.php">DISCOGRAPHY</a></li>
      <li><a href="./live.php">LIVE</a></li>
      <li><a href="./contact.php">CONTACT</a></li>
    </ul>
  </nav>
  <div class="hamburger">
    <span></span>
    <span></span>
    <span></span>
  </div>
  <!--slide_navi-->
  <?php require_once './inc/slide_navi.php'; ?>
  <!--/slide_navi-->

  <?php if ($_SESSION['user_login']["login_flg"] == 1) { ?>
  <div class="shopping_memberBox">
    <p class="user_textBox">こんにちは、<span class="user_name"><?=$_SESSION['user']['user_info']['name1'].$_SESSION['user']['user_info']['name2']?></span>さん</p>

    <ul class="user_linkList">
      <li class="mypage"><a href="./mypage.php"><i class="fa fa-user" aria-hidden="true"></i>マイページ</a></li>
      <li class="shopping"><a href="./item.php"><i class="fa fa-shopping-basket" aria-hidden="true"></i>お買い物をする</a></li>
      <li class="cart"><a href="./cart.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i>カゴの中を見る</a></li>
      <li class="search"><a href="./property.php"><i class="fa fa-search" aria-hidden="true"></i>物件検索をする</a></li>
      <li class="logout"><a href="./?mode=logout"><i class="fa fa-key" aria-hidden="true"></i>ログアウト</a></li>
    </ul>
  </div>
  <?php } ?>

</header>