<?php

require_once './inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "other_deliv";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイトスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);


	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここから☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/
	switch ($_REQUEST['mode']) {


		/******************************************************
		削除処理ここから
		******************************************************/
		case 'delete':

			$recordControlObj->recordDelete($connect, $table_name, $cd);

			//並び順更新
			$recordControlObj->recordSort($connect, $table_name, $kensu_, 'delete');

		break;
		/******************************************************
		削除処理ここまで
		******************************************************/

	}
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここまで☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
