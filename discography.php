﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//discography取得ここから
$_SELECT = "cd, title1, title2, date, content, img1";
$_TABLE  = "discography";
$_WHERE  = "WHERE disp_flg = 1";
$_ORDER  = "sort";

$getDiscographyRecordObj  = new recordControlClass;
$discographyArray         = $getDiscographyRecordObj->getRecord($connect, $_SELECT, $_TABLE, $_WHERE, $_ORDER);

$discography_max = count($discographyArray);

for ($i = 0; $i < $discography_max; $i++) {

    if ($discographyArray[$i]['img1']) {
        $discographyArray[$i]['img1_path'] = $img_pass.$discographyArray[$i]['img1'];
    }

    if ($discographyArray[$i]['date']) {
        $discographyArray[$i]['release_date'] = date("Y.m.d" ,strtotime($discographyArray[$i]['date']) );
    }

    if ($i == 0) {
        $discographyArray[$i]['styleArray'][] = " class=\"fd_list\"";
    } else {
        $discographyArray[$i]['styleArray'][] = '';
    }

}
//discography取得ここまで


?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Discography</h2>
        
        <?php if ($discography_max) { ?>
        <ul class="d_list">

            <?php for ($i = 0; $i < $discography_max; $i++) { ?>
            <li<?=$discographyArray[$i]['styleArray'][$i]?>>
                <img src="<?=$discographyArray[$i]['img1_path']?>" width="150" height="150" alt="<?=$discographyArray[$i]['title1']?>">
                <p class="number"><?=$discographyArray[$i]['title2']?>e</p>
                <h3 class="title"><?=$discographyArray[$i]['title1']?></h3>
                <p class="date"><?=$discographyArray[$i]['release_date']?> Release</p>
                <p class="setsumei">
                <?=nl2br($discographyArray[$i]['content'])?>
                </p>
            </li>
            <?php } ?>
       
        </ul>
        <?php } else { ?>
        <li class="fd_list">現在、Discographyはございません。</li>
        <?php } ?>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
