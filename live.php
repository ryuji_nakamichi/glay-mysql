﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//live取得
$_SELECT = "cd, title, date, comment";
$_TABLE  = "live";
$_WHERE  = "WHERE disp_flg = 1";
$_ORDER  = "date DESC";

$getLiveRecordObj  = new recordControlClass;
$liveArray         = $getLiveRecordObj->getRecord($connect, $_SELECT, $_TABLE, $_WHERE, $_ORDER);

$live_max = count($liveArray);


?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
        
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Live Schedule</h2>
        
        <ul class="topics liveList">
        
          <?php if ($live_max) { ?>

            <?php for ($i = 0; $i < $live_max; $i++) { ?>
            <li class="f_list">
          
              <p class="date"><?=$liveArray[$i]['date']?></p>
          
              <h3><?=$liveArray[$i]['title']?></h3>
            
              <p class="honbun1"><?=nl2br($liveArray[$i]['comment'])?></p>
          
            </li>
            <?php } ?>

          <?php } else { ?>
          <li class="f_list">※現在、LIVE情報はございません。</li>
          <?php } ?>

      </ul>
      
      </div>
      
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>