﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


//会員のお届け先一覧を取得
$mypageClassObj      = new mypageClass;
$other_delivAllArray = $mypageClassObj->getDelivList($connect);
$other_delivArray    = $other_delivAllArray['other_delivArray'];


// echo '<pre>';
// print_r($other_delivArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>お届け先追加･変更</h2>

      <p class="inforamtion">登録住所以外への住所へ送付される場合等にご利用いただくことができます。<br>
        ※最大<span class="attention">20件</span>まで登録できます。</p>

      <?php if ($other_deliv_max < 20) { ?>
      <p>
        <a href="./mypage_delivery_addr.php">新しいお届け先を追加する</a>
      </p>
      <?php } ?>

    <table class="shoppingTbl">
      <tr>
        <th colspan="5">お届け先</th>
      </tr>

      <?php foreach ($other_delivArray AS $key => $value) { ?>
        
      <tr>
        <td class="alignC"><?=$value['sort']?></td>
        <td><label for="add1">お届け先住所</label></td>
        <td>
        〒<?=$value['zip1']?>-<?=$value['zip2']?><br>
        <?=$value['address1']?><br>
        <?=$value['company_name']?>&nbsp;<?=$value['name1']?>&nbsp;<?=$value['name2']?>
        </td>
        <td class="alignC">
          <a href="./mypage_delivery_addr.php?cd=<?=$value['cd']?>">変更</a>
        </td>
        <td class="alignC">
          <input type="button" name="delete" value="削除" class="del_mode" del_value="./other_deliv_control.php?cd=<?=$value['cd']?>">
        </td>
      </tr>

      <?php } ?>
    </table>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
