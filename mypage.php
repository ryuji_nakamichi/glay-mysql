﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


//mypageClass(購入履歴一覧)呼び出し
$mypageObj = new mypageClass;
$itemArray = $mypageObj->historyGet($connect);

$item_order_count = $itemArray['item_order_count'];
$item_orderArray  = $itemArray['item_orderArray'];

// echo '<pre>';
// print_r($item_orderArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>MYページ</h2>

      <div id="mynavi_area">
        <ul class="mynavi_list clearfix">
          <li><a href="./mypage.php">購入履歴一覧</a></li>
          <li><a href="./mypage_favorite.php">お気に入り一覧</a></li>
          <li><a href="./mypage_change.php">会員登録内容変更</a></li>
          <li><a href="./mypage_delivery.php">お届け先追加・変更</a></li>
          <li><a href="./mypage_refusal.php">退会手続き</a></li>
        </ul>

        <!--▼現在のポイント-->
        <div class="point_announce">
          <p>ようこそ&nbsp;／&nbsp;<span class="user_name"><?=$_SESSION['user']['user_info']['name1'].$_SESSION['user']['user_info']['name2']?>様</span>
            &nbsp;現在の所持ポイントは&nbsp;<span class="point st"><?=$_SESSION['user']['user_info']['point']?>pt</span>&nbsp;です。</p>
        </div>
        <!--▲現在のポイント-->

      </div>

      <h3>購入履歴一覧</h3>
      <p><span class="attention"><?=$item_order_count?>件</span>の購入履歴があります。</p>
      <table class="shoppingTbl">
        <tr>
          <th class="alignC">購入日時</th>
          <th class="alignC">注文番号</th>
          <th class="alignC">お支払い方法</th>
          <th class="alignC">合計金額</th>
          <th class="alignC">ご注文状況</th>
          <th class="alignC">詳細</th>
        </tr>

        <?php foreach ( (array) $item_orderArray AS $key => $value ) { ?>
         <tr>
            <td class="alignC"><?=$value['new_record_time']?></td>
            <td class="alignC"><?=$value['cd']?></td>
            <td class="alignC"><?=$value['payment_method']?></td>
            <td class="alignR"><?=$value['subtotal']?>円</td>
            <td class="alignC">注文受付</td>
            <td class="alignC"><a href="./mypage_history.php?order_cd=<?=$value['cd']?>">詳細</a></td>
          </tr>
        <?php } ?>
      </table>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
