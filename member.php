<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Member</h2>
        
        <div class="imagebox">
        <ul class="member">
        
          
            <li>
              <a href="member_teru.php"><img src="image/teru_member.jpg" width="235" height="500" alt="teru"></a> 
            </li>
          
          
            
            <li> 
              <a href="member_takuro.php"><img src="image/takuro_member.jpg" width="235" height="500" alt="takuro"></a>        
            </li>
          
        
         
            <li>            
              <a href="member_hisashi.php"><img src="image/hisashi_member.jpg" width="235" height="500" alt="hisashi"></a>
        
            </li>
         
          
          
            <li>
              <a href="member_jiro.php"><img src="image/jiro_member.jpg" width="235" height="500" alt="jiro"></a>
            </li>
          
        </ul>
        </div>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
