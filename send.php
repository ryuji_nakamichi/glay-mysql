<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

$table_name = "contact";


	//クロスサイドスクリプティング対策
	foreach ($_REQUEST as $key => $val) {

		if ( !is_array($val) ){
			$_REQUEST[$key] = htmlspecialchars($val);
			$_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
		}

	}


//POSTされてきたデータを変数に定義
$date = date( 'Y-m-d', strtotime("today") );

if ($_REQUEST['date']) {
	$dateArray = explode("-", $date);
	$year      = $dateArray[0];
	$month     = $dateArray[1];
	$day       = $dateArray[2];
	$date      = $year."-".$month."-".$day;
}

$name     = $_REQUEST['name'];
$kana     = $_REQUEST['kana'];
$mail     = $_REQUEST['mail'];
$tel      = $_REQUEST['tel'];
$msg      = $_REQUEST['msg'];
$sort     = 0;
$disp_flg = 1;


//------------------------------------
// メール本文の整形 
//------------------------------------

$body = '名前 :' . $name . "\n";
$body .= 'ふりがな :' . $kana . "\n";
$body .= 'メールアドレス :' . $mail . "\n";
$body .= '電話番号 :' . $tel . "\n";
$body .='お問い合わせ内容 :' . $msg . "\n";


//------------------------------------
// 言語・文字コード指定
//------------------------------------

mb_language("Japanese");
mb_internal_encoding("UTF-8");


//------------------------------------
// ユーザーに自動返信メール
//------------------------------------

$subject = "お問い合わせありがとうございました。";
$mailto = $mail;
$from = "From:ryujiglay@yahoo.co.jp";
$return1 = mb_send_mail($mailto,$subject,$body,$from);


//------------------------------------
// 管理者に送るメール
//------------------------------------

$subject = "お問い合わせがありました。";
$mailto = "ryujiglay@yahoo.co.jp";
$from = "From:ryujiglay@yahoo.co.jp";
$return2 = mb_send_mail($mailto,$subject,$body,$from);


//------------------------------------
// メールが正常に送信されたかを判定
//------------------------------------

$res = array();

if ($return1 && $return2)
{
	$res['status'] = 'ok';
	//echo"正常にメールが送信されました。";


	//メールが正常に送信されたらデータベースに登録するここから
	$sql = "INSERT INTO {$table_name} (
		date,
		name,
		kana,
		mail,
		tel,
		msg,
		disp_flg,
		sort
		)
	VALUES(
		'{$date}',
		'{$name}',
		'{$kana}',
		'{$mail}',
		'{$tel}',
		'{$msg}',
		'{$disp_flg}',
		 {$sort}
		)";

	$queryResult = mysqli_query($connect, $sql);
	

	//並び順更新
	$sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
	$sort_query = mysqli_query($connect, $sort_sql);
	$sort_max   = mysqli_num_rows($sort_query);

	for ($i = 0; $i < $sort_max; $i++) {
		$sortArray = mysqli_fetch_assoc($sort_query);

		$countSortArray = $i + 1;

		$sort_up_sql   = "UPDATE {$table_name} SET ";
		$sort_up_sql  .= "sort = ".$countSortArray." ";
		$sort_up_sql  .= "WHERE cd = ".$sortArray['cd']."";
		$sort_up_query = mysqli_query($connect, $sort_up_sql);

	}
	//メールが正常に送信されたらデータベースに登録するここまで


} else {
	$res['status'] = 'ng';
	$res['errMsg'] = '正常にメールが送信されませんでした。';
	//echo"正常にメールが送信されませんでした。";
}


//3秒処理を止める
sleep(3);

echo json_encode($res);