<?php

/******************************************************************************************************************************
XSSClassここから
******************************************************************************************************************************/
class XSSClass {


	/**
	 * クロスサイトスクリプティング対策関数
	 * @connect: db接続情報
	 * @dataArray: データ配列
	 * @return $dataArray
	 */

	public function XSSEscape ($dataArray, $connect) {

		foreach ( (array) $dataArray AS $key => $val) {

			if ( !is_array($val) ){
				$dataArray[$key] = htmlspecialchars($val);
				$dataArray[$key] = mysqli_real_escape_string($connect, $val);
			}

		}

		return $dataArray;

	}


	/**
	 * cd番号チェック関数
	 * @_cd: 対象cd番号
	 * @return $cd
	 */

	public function cdCheck ($_cd) {
		
		if ( ctype_digit(strval($_cd)) ) {
			$cd = $_cd;
		}

		return $cd;

	}

}
/******************************************************************************************************************************
XSSClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
商品一覧用クラスここから
******************************************************************************************************************************/
class itemListClass {

	public $name;
	public $price;
	public $item_tax;

	public $site_url;
	public $img_pass;

	public $sqlAdd;


	/**
	 * 商品一覧取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $categoryCd カテゴリーcd
	 * @return array $dataArray
	 * @todo パフォーマンス改善の余地あり
	 */

	public function getItemList ($connect = NULL, $categoryCd = NULL) {


		//ベースの画像パス生成
		$this->site_url = SITEURL;
		$this->img_pass = SITEURL.'upload/';


		//税率取得・設定（設定がなければ、8が適用される）
		if ( is_numeric($GLOBALS['item_tax']) ) {
			$this->item_tax = $GLOBALS['item_tax'];
		} else {
			$this->item_tax = 8;
		}
		

		//item_category_cdチェック
		if ( ctype_digit($categoryCd) ) {
			$sqlAdd = "AND item_category_cd = {$categoryCd}";
		} else {
			$sqlAdd = "";
		}

		$_select = "cd, date, name, comment, link, target, disp_flg, img1, img2, item_category_cd, price, sort";
		$sql     = "SELECT {$_select} FROM item WHERE disp_flg = 1 {$sqlAdd} ORDER BY sort";

		$data_query = mysqli_query($connect, $sql);
		$data_max   = mysqli_num_rows($data_query);

		for ($i = 0; $i < $data_max; $i++) {
			$dataArray[$i] = mysqli_fetch_assoc($data_query);
			$dataArray[$i]["item_tax"] = $dataArray[$i]["price"] * ($this->item_tax / 100);
			$dataArray[$i]['item_tax'] = round($dataArray[$i]['item_tax']);
			$dataArray[$i]["price_plus_tax"]    = $dataArray[$i]["price"] + $dataArray[$i]["item_tax"];
			$dataArray[$i]["price_format"] = number_format($dataArray[$i]["price"] + $dataArray[$i]["item_tax"]);


			//画像パス取得
			if ($dataArray[$i]['img1']) {
				$dataArray[$i]['img_pass1'] = "{$this->img_pass}{$dataArray[$i]['img1']}";
			} else {
				$dataArray[$i]['img_pass1'] = "./image/no_image.png";
			}

			if ($dataArray[$i]['img1']) {
				$dataArray[$i]['img_pass2'] = "{$this->img_pass}{$dataArray[$i]['img2']}";
			} else {
				$dataArray[$i]['img_pass2'] = "./image/no_image.png";
			}
			
			
		}

		return $dataArray;

	}

}
/******************************************************************************************************************************
商品一覧用クラスここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
商品詳細用クラスここから
******************************************************************************************************************************/
class itemDetailClass {

	public $name;
	public $price;
	public $item_tax;

	public $site_url;
	public $img_pass;

	public $sqlAdd;


	/**
	 * 商品詳細取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $itemCd cd番号
	 * @return array $dataArray
	 * @todo パフォーマンス改善の余地あり
	 */

	public function getItemDetail ($connect = NULL, $itemCd = NULL) {

		$this->site_url = SITEURL;
		$this->img_pass = SITEURL.'upload/';


		//item_category_cdチェック
		if ( is_numeric($GLOBALS['item_tax']) ) {
			$this->item_tax = $GLOBALS['item_tax'];
		} else {
			$this->item_tax = 8;
		}


		//商品のcdチェック
		if ( ctype_digit($itemCd) ) {
			$sqlAdd = "AND cd = {$itemCd}";
		}

		$_SELECT = "cd, date, name, comment, link, target, disp_flg, img1, img2, item_category_cd, price, sort";
		$sql     = "SELECT {$_SELECT} FROM item WHERE disp_flg = 1 {$sqlAdd} LIMIT 1";

		$data_query = mysqli_query($connect, $sql);
		$data_max   = mysqli_num_rows($data_query);

		for ($i = 0; $i < $data_max; $i++) {
			$dataArray[$i] = mysqli_fetch_assoc($data_query);

			$dataArray[$i]["item_tax"] = $dataArray[$i]["price"] * ($this->item_tax / 100);
			$dataArray[$i]['item_tax'] = round($dataArray[$i]['item_tax']);
			$dataArray[$i]["price_plus_tax"]    = $dataArray[$i]["price"] + $dataArray[$i]["item_tax"];
			$dataArray[$i]["price_format"] = number_format($dataArray[$i]["price"] + $dataArray[$i]["item_tax"]);


			//画像パス生成
			for ($j = 1; $j < 3; $j++) {

				if ($dataArray[$i]["img{$j}"]) {
					$dataArray[$i]["img_pass{$j}"] = $this->img_pass.$dataArray[$i]["img{$j}"];
					$dataArray[$i]['imgArray'][]   = $dataArray[$i]["img_pass{$j}"];
				} else {
					$dataArray[$i]['imgArray'][$j] = "./image/no_image.png";
				}

			}


			//カテゴリー取得ここから
			if ( ctype_digit( strval($dataArray[$i]['item_category_cd']) ) ) {

				$sql = "SELECT cd, name FROM item_category WHERE disp_flg = 1 AND cd = {$dataArray[$i]['item_category_cd']}";
				$item_category_query = mysqli_query($connect, $sql);
				$item_category_max   = mysqli_num_rows($item_category_query);

				if ($item_category_max) {
					$item_categoryArray = mysqli_fetch_assoc($item_category_query);

					$dataArray[$i]['item_categoryArray'] = $item_categoryArray;
				}

			}
			//カテゴリー取得ここまで
			
		}

		return $dataArray;

	}

}
/******************************************************************************************************************************
商品詳細用クラスここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
getUserInfoClassここから
******************************************************************************************************************************/
class getUserInfoClass {

	public $connect;

	function __construct() {
		$this->connect = $GLOBALS['connect'];
	}


	/**
	 * 会員の最新情報取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array $userInfoArray
	 */

	public function getUserInfo ($connect) {

		$_SELECT = "cd, name1, name2, kana1, kana2, company_name, birth_year, birth_month, birth_day, sex, job_type, post_code1, post_code2, address, tel, fax, mail, phone_mail_address, user_id, MD5(password) AS password, point, disp_flg, sort, new_record_time, update_record_time";
		$sql     = "SELECT {$_SELECT} FROM shopping_member WHERE cd = {$_SESSION['user']["user_info"]['cd']} AND disp_flg = 1";

		$user_query = mysqli_query($this->connect, $sql);
		$user_max   = mysqli_num_rows($user_query);

		if ($user_max) {

		  $userArray = mysqli_fetch_assoc($user_query);

		  $_SESSION['user']["user_info"] = $userArray;
		  $_SESSION['user']["user_info"]['full_birth'] = $userArray['birth_year'].'/'.$userArray['birth_month'].'/'.$userArray['birth_day'];
		  $_SESSION['user']["user_info"]['full_name'] = $userArray['name1'].' '.$userArray['name2'];


		  //職業取得ここから
		  if ($_SESSION['user']["user_info"]['job_type']) {

		    $sql = "SELECT cd, name FROM job_type WHERE disp_flg = 1 AND cd = {$_SESSION['user']["user_info"]['job_type']}";
		    $user_job_query = mysqli_query($this->connect, $sql);
		    $user_job_max   = mysqli_num_rows($user_job_query);

		    if ($user_job_max) {
		      $user_jobArray = mysqli_fetch_assoc($user_job_query);
		      $_SESSION['user']["user_info"]['job_type_name'] = $user_jobArray['name'];
		    }

		    
		  }
		  //職業取得ここまで

		  $userInfoArray = $_SESSION['user'];

		}
		//会員の最新情報を取得ここまで

		return $userInfoArray;

	}

}
/******************************************************************************************************************************
getUserInfoClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
ログイン用クラスここから
******************************************************************************************************************************/
class loginClass {

	public $error_text;
	public $site_url;
	public $session_user_name;


	/**
	 * ログイン判定理用の関数
	 *
	 * @return string $error_text
	 * @todo パフォーマンス改善の余地あり
	 */

	public function login () {
		
		$this->site_url          = SITEURLHP;
		$this->session_user_name = SESSIONUSERNAME;

		if ($_REQUEST['mode'] && $_REQUEST['mode'] == 'logout') {


			//ログアウトしたらログインフラグを初期化
			$_SESSION['user_login']["login_flg"] = array();


			//ログアウトしたらユーザー情報を初期化
			$_SESSION['user']["user_info"]       = array();


			//ログアウトしたらお買い物の流れでの情報を初期化
			$_SESSION['shopping']                = array();


			//クッキーのセッションIDも削除
			//setcookie(SESSIONUSERNAME.'PHPSESSID', '', time()-42000, '/');

		}

		if ($_SESSION['user_login']["login_flg"] === 1) {
			header("Location: ".$this->site_url.'mypage.php'.'?mode=login_success');
			exit;
		}

		$this->error_text = '';

		if ($_SESSION['user_login']["login_flg"] === 3) {
			
			$_SESSION['user_login']["login_flg"] = array();
			$this->error_text = '<span style="color: #ff0000;">ID・PASSWORD、またはどちらかがが正しくありません。</span>';

		} else if ($_SESSION['user_login']["login_flg"] === 2) {

			$_SESSION['user_login']["login_flg"] = array();
			$this->error_text = '<span style="color: #ff0000;">ログアウトしました。</span>';

		}

		return $this->error_text;

	}

}
/******************************************************************************************************************************
ログイン用クラスここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
ログインユーザーの情報取得クラスここから
******************************************************************************************************************************/
class userInfoClass {

	public $user_infoArray;


	/**
	 * ログインユーザーの情報取得処理用の関数
	 *
	 * @return array $user_infoArray
	 * @todo パフォーマンス改善の余地あり
	 */

	public function userInfo () {

		$this->user_infoArray = $_SESSION['user']['user_info'];

		//名前
		$this->user_infoArray['name'] = $this->user_infoArray['name1'].$this->user_infoArray['name2'];

		//ふりがな
		$this->user_infoArray['kana'] = $this->user_infoArray['kana1'].$this->user_infoArray['kana2'];

		//郵便番号
		$this->user_infoArray['post_code'] = $this->user_infoArray['post_code1'].$this->user_infoArray['post_code2'];

		return $this->user_infoArray;

	}

}
/******************************************************************************************************************************
ログインユーザーの情報取得クラスここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
カート取得クラスここから
******************************************************************************************************************************/
class cartClass {

	public $img_pass;
	public $item_tax;
	public $cartArrayMax;
	public $cartInArray;
	public $cartNoneText;
	public $shipping_cost;
	public $shipping_cost_free;
	public $add_point;
	public $add_point_increment;


	/**
	 * カート取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array $_SESSION['cartInfo']
	 * @todo パフォーマンス改善の余地あり
	 */


	public function cart ($connect = NULL) {


		//画像パス生成
		$this->img_pass = $GLOBALS['img_pass'];


		//商品送料・ポイント増加率など設定
		$this->shipping_cost       = $GLOBALS['shipping_cost'];
		$this->shipping_cost_free  = $GLOBALS['shipping_cost_free'];
		$this->add_point           = $GLOBALS['add_point'];
		$this->add_point_increment = $GLOBALS['add_point_increment'];


		//カート内の商品の税金定義
		if ( is_numeric($GLOBALS['item_tax']) ) {
			$this->item_tax = $GLOBALS['item_tax'];
		} else {
			$this->item_tax = 8;
		}


		/***************************************************************
		カート関連処理ここから
		***************************************************************/

		//cartに商品を追加する($_POSTがある)場合
		if ( ctype_digit( strval($_POST['itemCd']) ) ) {
		  $cartArray = array();

		  if ($_SESSION['cart']['itemCd']) {
		    $cartArray = $_SESSION['cart']['itemCd'];
		  }

		  if ($_SESSION['cart']['item_number']) {
		    $item_numberArray = $_SESSION['cart']['item_number'];
		  }

		  if ( !in_array($_POST['itemCd'], $cartArray) ) {
		    $cartArray[]                = $_POST['itemCd'];
		    $_SESSION['cart']['itemCd'] = $cartArray;

		    $item_numberArray[]              = $_POST['item_number'];
		    $_SESSION['cart']['item_number'] = $item_numberArray;

		  } else {
		    $errorText = 'すでにカートに入っています。';
		  }


		//cartに商品を追加しないで来る($_POSTがない)場合
		} else {

		  if ($_SESSION['cart']['itemCd']) {
		    $cartArray = $_SESSION['cart']['itemCd'];
		  }

		  if ($_SESSION['cart']['item_number'] && $_POST['submit']) {
		    $item_numberArray = $_POST['item_number'];
		  }


		  //違うページから来た場合、カート内商品の個数が消えてしまうので追加
		  $item_numberArray = $_SESSION['cart']['item_number'];

		}


		//セッション内にカート内商品があるが、カート画面で個数を変更した場合
		if ($_POST['submit']) {

			$change_item_number_max = count($_POST) - 1;
			for ($i = 0; $i < $change_item_number_max; $i++) {
				$_SESSION['cart']['item_number'][$i] = $_POST["cart{$i}"];
			}

			$item_numberArray = $_SESSION['cart']['item_number'];

		}


		/******************************
		cart削除ここから
		(配列の順番のずれを防ぐため
		逆順に削除している)
		******************************/
		$delete_max = count($cartArray);

		for ($i = $delete_max; 0 <= $i; $i--) {

		  if ($_POST["cart_delete_flg{$i}"] == 'on') {

		    if ($cartArray && $item_numberArray) {
		      array_splice($cartArray, $i, 1);
		      array_splice($item_numberArray, $i, 1);
		    }

		    $_SESSION['cart']['itemCd']      = $cartArray;
		    $_SESSION['cart']['item_number'] = $item_numberArray;
		  }

		  $delete_cnt++;

		}
		/******************************
		cart削除ここまで
		(配列の順番のずれを防ぐため
		逆順に削除している)
		******************************/


		/******************************
		カートに入っている商品情報取得
		ここから
		******************************/
		$this->cartArrayMax = count($cartArray);


		//カートに何も入っていない場合
		if (!$this->cartArrayMax) {
		  $this->cartNoneText = '現在、カートの中身は空です。';
		  $_SESSION['cart'] = array();
		}

		for ($i = 0; $i < $this->cartArrayMax; $i++) {
		  $_SELECT    = '*';
		  $sql        = "SELECT {$_SELECT} FROM item WHERE disp_flg = 1 AND cd = {$cartArray[$i]} ORDER BY sort";
		  $cart_query = mysqli_query($connect, $sql);
		  $cart_max   = mysqli_num_rows($cart_query);

		  if ($cart_max) {
		    $this->cartInArray[] = mysqli_fetch_assoc($cart_query);

		    if ($this->cartInArray[$i]['img1']) {
		    	$this->cartInArray[$i]['img_pass1'] = "{$this->img_pass}{$this->cartInArray[$i]['img1']}";

		    } else if ($this->cartInArray[$i]['img2']) {
		    	$this->cartInArray[$i]['img_pass1'] = "{$this->img_pass}{$this->cartInArray[$i]['img2']}";
		    } else {
		    	$this->cartInArray[$i]['img_pass1'] = "./image/no_image.png";
		    }


		    //各商品に紐づいてるカテゴリー取得ここから
		    if ($this->cartInArray[$i]['item_category_cd']) {

		    	$sql = "SELECT cd, name FROM item_category WHERE disp_flg = 1 AND cd = {$this->cartInArray[$i]['item_category_cd']}";
		    	$cart_item_category_query = mysqli_query($connect, $sql);
		    	$cart_item_category_max   = mysqli_num_rows($cart_item_category_query);

		    	if ($cart_item_category_max) {
		    		$cart_item_categoryArray = mysqli_fetch_assoc($cart_item_category_query);

		    		$this->cartInArray[$i]['item_category_name'] = $cart_item_categoryArray['name'];
		    	}

		    }
		    //各商品に紐づいてるカテゴリー取得ここまで


		    //各商品に紐づいてる配送会社取得ここから
		    if ($this->cartInArray[$i]['shipping_type']) {

		    	$sql = "SELECT cd, name FROM shipping_type WHERE disp_flg = 1 AND cd = {$this->cartInArray[$i]['shipping_type']}";
		    	$cart_item_shipping_query = mysqli_query($connect, $sql);
		    	$cart_item_shipping_max   = mysqli_num_rows($cart_item_shipping_query);

		    	if ($cart_item_shipping_max) {
		    		$cart_item_shippingArray = mysqli_fetch_assoc($cart_item_shipping_query);

		    		$this->cartInArray[$i]['shipping_type_name'] = $cart_item_shippingArray['name'];
		    	}

		    }
		    //各商品に紐づいてる配送会社取得ここまで

		    
			/******************************
			カートに入っている商品の個数と
			値段の計算ここから
			******************************/

		    //税金なしの値段(format)
			$this->cartInArray[$i]['price_format']          = number_format($this->cartInArray[$i]['price']);

			//商品点数
		    $this->cartInArray[$i]['item_number']           = $item_numberArray[$i];

		    //税率
		    $this->cartInArray[$i]['item_tax']              = $this->cartInArray[$i]['price'] * ($this->item_tax / 100);

		    //税金
		    $this->cartInArray[$i]['item_tax']              = round($this->cartInArray[$i]['item_tax']);

		    //税あり値段(1つ)
		    $this->cartInArray[$i]['price_plus_tax']        = $this->cartInArray[$i]['price'] + $this->cartInArray[$i]['item_tax'];

		    //税あり値段(1つ、format)
		    $this->cartInArray[$i]['price_plus_tax_format'] = number_format($this->cartInArray[$i]['price'] + $this->cartInArray[$i]['item_tax']);

		    //税なし値段(点数分)
		    $this->cartInArray[$i]['total_price']           = $this->cartInArray[$i]['price'] * $this->cartInArray[$i]['item_number'];

		    //税あり値段(点数分)
		    $this->cartInArray[$i]['total_price_tax']       = $this->cartInArray[$i]['price_plus_tax'] * $this->cartInArray[$i]['item_number'];

		    //税あり値段(点数分、format)
		    $this->cartInArray[$i]['total_price_format']    = number_format($this->cartInArray[$i]['price_plus_tax'] * $this->cartInArray[$i]['item_number']);


		    //カートに入っている商品の個数と値段の計算(内容を変更した場合)
		    if ($_POST["cart{$i}"]) {
		      $this->cartInArray[$i]['item_number']          = $_POST["cart{$i}"];

		      $this->cartInArray[$i]['total_price']          = $this->cartInArray[$i]['price'] * $this->cartInArray[$i]['item_number'];

		      $this->cartInArray[$i]['total_price_format']   = number_format($this->cartInArray[$i]['price_plus_tax'] * $this->cartInArray[$i]['item_number']);


		    }
		    /******************************
			カートに入っている商品の個数と
			値段の計算ここまで
			******************************/

		  }

		}
		/******************************
		カートに入っている商品情報取得
		ここまで
		******************************/


		//カート内商品の合計金額・合計税額計算
		foreach( (array)$this->cartInArray AS $key => $value ) {
			$all_total_price += $value['total_price_tax'];
			$all_total_tax   += $value['item_tax'];
		}

		//カート内の商品点数（商品毎）
		$cartInfoArray['cartArrayMax']                  = $this->cartArrayMax;

		//カート内の商品配列
		$cartInfoArray['cartInArray']                   = $this->cartInArray;

		//カートテキスト
		$cartInfoArray['cartNoneText']                  = $this->cartNoneText;

		//カート内の商品点数（商品毎、セッション）
		$_SESSION['cartInfo']['cartArrayMax']           = $this->cartArrayMax;

		//カート内の商品配列（セッション）
		$_SESSION['cartInfo']['cartInfoArray']          = $this->cartInArray;

		//カート内商品の合計金額（セッション）
		$_SESSION['cartInfo']['all_total_price']        = $all_total_price;

		//カート内商品の合計税額（セッション）
		$_SESSION['cartInfo']['all_total_tax']          = $all_total_tax;

		//カート内商品の合計金額（format、セッション）
		$_SESSION['cartInfo']['all_total_price_format'] = number_format($all_total_price);

		//カートテキスト（セッション）
		$_SESSION['cartInfo']['cartNoneText']           = $this->cartNoneText;


		//送料計算
		if ($this->shipping_cost <= $_SESSION['cartInfo']['all_total_price']) {
			$this->shipping_cost = 0;
		}

		//商品送料（セッション）
		$cartInfoArray['shipping_cost']        = $this->shipping_cost;
		$_SESSION['cartInfo']['shipping_cost'] = $cartInfoArray['shipping_cost'];


		/*ポイントが0の場合は判定を厳密にしておかないと、前のログインしたユーザーのポイントが残ってしまう可能性がある*/
		if ($_SESSION['user']['user_info']['point'] === 0) {
			$_SESSION['cartInfo']['point'] = 0;
		} else {
			$_SESSION['cartInfo']['point'] = $_SESSION['user']['user_info']['point'];
		}


		//商品合計金額が「ポイント加算条件」以上だったら、「ポイント加算増加率」分だけポイントを加算する
		if ($_SESSION['cartInfo']['all_total_price'] >= $this->add_point) {
			$_SESSION['cartInfo']['add_point_increment'] = $this->add_point_increment;
		} else {
			$_SESSION['cartInfo']['add_point_increment'] = 0;
		}

		return $_SESSION['cartInfo'];

		/***************************************************************
		カート関連処理ここまで
		***************************************************************/

	}

}
/******************************************************************************************************************************
カート取得クラスここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
recordControlClassここから
******************************************************************************************************************************/
class recordControlClass {


	/**
	 * レコード並び替え関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @kensu_: レコード表示件数(db.phpで定義)
	 * @return bool
	 */

	public function recordSort ($connect, $table_name, $kensu_, $_mode = '') {

		$recordSortResultArray = array();


		//データ整形
		$sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
		$sort_query = mysqli_query($connect, $sort_sql);
		$sort_max   = mysqli_num_rows($sort_query);

		if ($_mode == '') {

			if ($sort_max > $kensu_) {
				$sort_max = $kensu_;
			}

		} else if ($_mode == 'delete') {
			$sort_max = $sort_max;
		}

		for ($i = 0; $i < $sort_max; $i++) {
			$sortArray = mysqli_fetch_assoc($sort_query);

			if ($_mode == '') {
				$countSortArray = $_POST['sort'][$i];
			} else if ($_mode == 'delete') {
				$countSortArray = $i + 1;
			} else {
				$countSortArray = $_POST['sort'][$i];
			}
			
			$sort_up_sql = "UPDATE {$table_name} SET ";
			$sort_up_sql .= "sort = ".$countSortArray." ";

			if ($_mode == '') {
				$sort_up_sql .= "WHERE cd = ".$_POST['cd_hidden'][$i]."";
			} else if ($_mode == 'delete') {
				$sort_up_sql .= "WHERE cd = ".$sortArray['cd']."";
			} else {
				$sort_up_sql .= "WHERE cd = ".$_POST['cd_hidden'][$i]."";
			}

			$sort_up_query = mysqli_query($connect, $sort_up_sql);

			if ($sort_up_query) {
				$recordSortResultArray[] = true;
			} else {
				$recordSortResultArray[] = false;
			}

		}

		return $recordSortResultArray;

	}
	

	/**
	 * レコード並び替え(insert時)関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @return bool
	 */

	public function recordInsertSort ($connect, $table_name) {

		$sortUpResultArray = array();

		$sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
		$sort_query = mysqli_query($connect, $sort_sql);
		$sort_max   = mysqli_num_rows($sort_query);

		for ($i = 0; $i < $sort_max; $i++) {
			$sortArray = mysqli_fetch_assoc($sort_query);

			$countSortArray = $i + 1;

			$sort_up_sql   = "UPDATE {$table_name} SET ";
			$sort_up_sql  .= "sort = ".$countSortArray." ";
			$sort_up_sql  .= "WHERE cd = ".$sortArray['cd']."";
			$sort_up_query = mysqli_query($connect, $sort_up_sql);

			if ($sort_up_query) {
				$sortUpResultArray[] = true;
			} else {
				$sortUpResultArray[] = false;
			}

		}

		return $sortUpResultArray;

	}


	/**
	 * レコード削除関数
	 * @connect: db接続情報
	 * @table: 対象テーブル名
	 * @cd: 対象cd番号
	 * @return bool
	 */
	public function recordDelete($connect, $table_name, $cd) {
		$sql          = "DELETE FROM {$table_name} WHERE cd = {$cd}";
		$delete_query = mysqli_query($connect, $sql);

		return $delete_query;
	}


	/**
	 * レコード挿入関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @value: 挿入データ配列
	 * @return bool
	 */
	public function recordInsert ($connect, $table_name, $value) {

		$keyArray = array();
		$valArray = array();

		foreach ($value AS $key => $val) {

			$key = $key;

			if ($key == "new_record_time" || $key == "update_record_time") { 
				//$val = ($val === NULL) ? 'NULL' : "(SELECT to_char(NOW(), 'yyyy-mm-dd hh24:mm:ss')::timestamp)";
				$val = ($val === NULL) ? 'NULL' : "'" . date("Y-m-d H:i:s") . "'";
			} else if ($key == "sort") {
				//mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : mysqli_real_escape_string($connect, $val);
			} else {
				//mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : "'". mysqli_real_escape_string($connect, $val) ."'";
			}

			$keyArray[] = $key;
			$valArray[] = $val;
		}

		$key_str = implode(',', $keyArray);
		$val_str = implode(',', $valArray);

		$sql        = "INSERT INTO {$table_name} ({$key_str}) VALUES ({$val_str})";
		//echo $sql;
		$data_query = mysqli_query($connect, $sql);

		if ($data_query) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * レコード更新関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @value: 更新データ配列
	 * @cd: 対象cd番号
	 * @return bool
	 */

	public function recordUpdate ($connect, $table_name, $value, $cd) {

		$valArray = array();

		foreach ($value AS $key => $val) {

			$key = $key;

			if ($key == "new_record_time" || $key == "update_record_time") { 
				$val = ($val === NULL) ? 'NULL' : "'" . date("Y-m-d H:i:s") . "'";
			} else if ($key == "sort") {
				//mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : mysqli_real_escape_string($connect, $val);
			} else {
				//mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : "'". mysqli_real_escape_string($connect, $val) ."'";
			}

			$valArray[] = $key.' = '.$val;
		}

		$val_str = implode(',', $valArray);

		$sql        = "UPDATE {$table_name} SET {$val_str} WHERE cd = {$cd}";
		$data_query = mysqli_query($connect, $sql);
		
		if ($data_query) {
			return true;
		} else {
			return false;
		}

	}


	/**
	 * レコード取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $_SELECT SELECT句
	 * @param $_TABLE_NAME テーブル名
	 * @param $_WHERE_COLUMN WHERE句
	 * @param $_SORT_COLUMN ORDER BY句
	 * @param $_LIMIT LIMITの件数
	 * @return array $dataArray
	 * @todo パフォーマンス改善の余地あり
	 */

	public function getRecord ($connect = NULL, $_SELECT = '*', $_TABLE_NAME, $_WHERE_COLUMN = 'WHERE disp_flg = 1', $_SORT_COLUMN = 'sort', $_LIMIT = '') {

		$sql          = "SELECT {$_SELECT} FROM {$_TABLE_NAME} {$_WHERE_COLUMN} ORDER BY {$_SORT_COLUMN} {$_LIMIT}";
		$data_query   = mysqli_query($connect, $sql);
		$data_max     = mysqli_num_rows($data_query);

		for ($i = 0; $i < $data_max; $i++) {
			$dataArray[$i] = mysqli_fetch_assoc($data_query);
		}

		return $dataArray;

	}

}
/******************************************************************************************************************************
recordControlClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
mypageClassここから
******************************************************************************************************************************/
class mypageClass {

	public $connect;
	public $site_url;
	public $img_pass;

	function __construct() {

		//DB接続情報
		$this->connect = $GLOBALS['connect'];


		//ベースの画像パス生成
		$this->site_url = SITEURL;
		$this->img_pass = SITEURL.'upload/';

	}


	/**
	 * 購入履歴一覧取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array $itemArray
	 */

	public function historyGet ($connect) {

		//支払い方法が空の場合は「不明」
		$_SELECT = "*,
		(
		  CASE
		    WHEN payment_method != '' THEN payment_method
		    ELSE '不明'
		    END
		) AS payment_method
		";
		$sql     = "SELECT {$_SELECT} FROM item_order WHERE shopping_member_cd = {$_SESSION['user']['user_info']['cd']} AND disp_flg = 1 ORDER BY sort";
		$item_order_query = mysqli_query($this->connect, $sql);
		$item_order_max   = mysqli_num_rows($item_order_query);

		for ($i = 0; $i < $item_order_max; $i++) {
		  $item_orderArray[] = mysqli_fetch_assoc($item_order_query);
		}

		$item_order_count = $item_order_max;

		$itemArray = array();
		$itemArray['item_orderArray']  = $item_orderArray;
		$itemArray['item_order_count'] = $item_order_count;

		return $itemArray;

	}


	/**
	 * 購入履歴詳細取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array $orderAllArray
	 */

	public function historyDetailGet ($connect, $_order_cd) {

		if ( ctype_digit( strval($_order_cd) ) ) {
			$order_cd = $_order_cd;
		} else {
			header("Location: ./mypage.php");
			exit;
		}


		if ($order_cd) {

			$order_cd = $_order_cd;


			/************************************************************************************************************************
			item_orderテーブルから$order_cdの値で絞ったレコード取得ここから
			************************************************************************************************************************/
			$_SELECT  = "o.cd AS o_cd, o.shopping_member_cd AS o_shopping_member_cd, o.message AS o_message, o.order_name1 AS o_order_name1, o.order_name2 AS o_order_name2, o.order_kana1 AS o_order_kana1, o.order_kana2 AS o_order_kana2, o.order_company_name AS o_order_company_name, o.order_email AS o_order_email, o.order_tel1 AS o_order_tel1, o.order_fax1 AS o_order_fax1, o.order_zip1 AS o_order_zip1, o.order_zip2 AS o_order_zip2, o.order_addr AS o_order_addr, o.order_sex AS o_order_sex, o.subtotal AS o_subtotal, o.discount AS o_discount, o.deliv_cd AS o_deliv_cd, o.deliv_fee AS o_deliv_fee, o.charge AS o_charge, o.use_point AS o_use_point, o.add_point AS o_add_point, o.tax AS o_tax, o.payment_cd AS o_payment_cd, o.payment_method AS o_payment_method, o.new_record_time AS o_new_record_time, ";
			$_SELECT  .= "
			(
				CASE
					WHEN o.payment_cd = 1 THEN '郵便振替'
					WHEN o.payment_cd = 2 THEN '現金書留'
					WHEN o.payment_cd = 3 THEN '銀行振込'
					WHEN o.payment_cd = 4 THEN '代金引換'
					ELSE '不明'
				END
			) AS o_payment_cd_name, 
			";
			$_SELECT  .= "
			(
				CASE
					WHEN o.order_sex = 1 THEN '男性'
					WHEN o.order_sex = 2 THEN '女性'
				ELSE '不明'
				END
			) AS o_order_sex_name, 
			";
			$_SELECT  .= "s.cd AS s_cd, s.name1 AS s_name1, s.name2 AS s_name2, s.company_name AS s_company_name, s.fax AS s_fax, s.phone_mail_address AS s_phone_mail_address, s.birth_year AS s_birth_year, s.birth_month AS s_birth_month, s.birth_day AS s_birth_day, s.job_type AS s_job_type";
			$sql                = "SELECT {$_SELECT} FROM item_order AS o LEFT JOIN shopping_member AS s ON o.shopping_member_cd = s.cd WHERE o.disp_flg = 1 AND s.disp_flg = 1 AND o.cd = {$order_cd} LIMIT 1";
			$order_detail_query = mysqli_query($this->connect, $sql);
			$order_detail_max   = mysqli_num_rows($order_detail_query);

			if ($order_detail_max) {
				$order_detailArray = mysqli_fetch_assoc($order_detail_query);

				if ($order_detailArray['s_birth_year']) {
					$s_birth_year = $order_detailArray['s_birth_year'];
				}

				if ($order_detailArray['s_birth_month']) {
					$s_birth_month = $order_detailArray['s_birth_month'];
				}

				if ($order_detailArray['s_birth_day']) {
					$s_birth_day = $order_detailArray['s_birth_day'];
				}

				if ($s_birth_year && $s_birth_month && $s_birth_day) {
					$order_detailArray['s_full_birth'] = $s_birth_year.'/'.$s_birth_month.'/'.$s_birth_day;
				}


				if ($order_detailArray['o_deliv_cd']) {

					$_SELECT = "cd, member_cd, name1, name2, kana1, kana2, company_name, zip1, zip2, address1, tel, fax";

					$sql = "SELECT {$_SELECT} FROM other_deliv WHERE disp_flg = 1 AND cd = {$order_detailArray['o_deliv_cd']} LIMIT 1";

					$other_deliv_query      = mysqli_query($this->connect, $sql);
					$other_deliv_query_max  = mysqli_num_rows($other_deliv_query);

					$other_delivArray = array();


					//お届け先が注文者の住所と違う場合
					if ($other_deliv_query_max) {
						$other_delivArray = mysqli_fetch_assoc($other_deliv_query);


					//お届け先が注文者の住所と同じ場合
					} else {
						$other_delivArray = $order_detailArray;

						$other_delivArray['name1'] = $order_detailArray['o_order_name1'];
						$other_delivArray['name2'] = $order_detailArray['o_order_name2'];
						$other_delivArray['kana1'] = $order_detailArray['o_order_kana1'];
						$other_delivArray['kana2'] = $order_detailArray['o_order_kana2'];
						$other_delivArray['company_name'] = $order_detailArray['o_order_company_name'];
						$other_delivArray['tel'] = $order_detailArray['o_order_tel1'];
						$other_delivArray['fax'] = $order_detailArray['o_order_fax1'];
						$other_delivArray['zip1'] = $order_detailArray['o_order_zip1'];
						$other_delivArray['zip2'] = $order_detailArray['o_order_zip2'];
						$other_delivArray['address1'] = $order_detailArray['o_order_addr'];
					}

					$order_detailArray['other_deliv'] = $other_delivArray;

				}


				//職業取得ここから
				if ($order_detailArray['s_job_type']) {

					$sql = "SELECT cd, name FROM job_type WHERE disp_flg = 1 AND cd = {$order_detailArray['s_job_type']}";
					$user_job_query = mysqli_query($this->connect, $sql);
					$user_job_max   = mysqli_num_rows($user_job_query);

					if ($user_job_max) {
						$user_jobArray = mysqli_fetch_assoc($user_job_query);
						$order_detailArray['s_job_type_name'] = $user_jobArray['name'];
					}

				}
				//職業取得ここまで
			/************************************************************************************************************************
			item_orderテーブルから$order_cdの値で絞ったレコード取得ここまで
			************************************************************************************************************************/


			/************************************************************************************************************************
			item_order_detailテーブルと、itemテーブルと、shipping_typeテーブルをJOINさせてレコード取得ここから
			************************************************************************************************************************/

				$_SELECT = "o_d.cd AS o_d_cd, o_d.item_order_cd AS o_d_item_order_cd, o_d.item_cd AS o_d_item_cd, o_d.item_category_cd AS o_d_item_category_cd, o_d.item_name AS o_d_item_name, o_d.price AS o_d_price, o_d.item_quantity AS o_d_item_quantity, o_d.shipping_type AS o_d_shipping_type, o_d.new_record_time AS o_d_new_record_time, ";
				$_SELECT .= "i.cd AS i_cd, i.date AS i_date, i.name AS i_name, i.price AS i_price, i.item_category_cd AS i_item_category_cd, i.link AS i_link, i.target AS i_target, i.img1 AS i_img1, i.img2 AS i_img2, i.shipping_type AS i_shipping_type, ";
				$_SELECT .= "s.cd AS s_cd, s.name AS s_name";
				$sql     = "SELECT {$_SELECT} FROM item_order_detail AS o_d LEFT JOIN item AS i ON o_d.item_cd = i.cd LEFT JOIN shipping_type AS s ON i.shipping_type = s.cd WHERE o_d.disp_flg = 1 AND o_d.item_order_cd = {$order_detailArray['o_cd']} ORDER BY o_d.sort";

				$item_order_detail_query = mysqli_query($this->connect, $sql);
				$item_order_detail_max   = mysqli_num_rows($item_order_detail_query);

				for ($i = 0; $i < $item_order_detail_max; $i++) {

					$item_order_detailArray[] = mysqli_fetch_assoc($item_order_detail_query);


					//各種類ごとの値段合計（税別）
					$item_order_detailArray[$i]['o_d_price_max'] = $item_order_detailArray[$i]['o_d_price'] * $item_order_detailArray[$i]['o_d_item_quantity'];

					$o_d_price_max_total += $item_order_detailArray[$i]['o_d_price_max'];


					//画像パス生成
					for ($j = 1; $j < 3; $j++) {

						if ($item_order_detailArray[$i]["i_img{$j}"]) {
							$item_order_detailArray[$i]["img_pass{$j}"] = $this->img_pass.$item_order_detailArray[$i]["i_img{$j}"];
							$item_order_detailArray[$i]['imgArray'][]   = $item_order_detailArray[$i]["img_pass{$j}"];
						} else {
							$item_order_detailArray[$i]['imgArray'][$j] = "./image/no_image.png";
						}

					}

				}
			/************************************************************************************************************************
			item_order_detailテーブルから、itemテーブルと、shipping_typeテーブルをJOINさせてレコード取得ここまで
			************************************************************************************************************************/

			}

			$orderAllArray = array();

			$orderAllArray['order_cd']               = $order_cd;
			$orderAllArray['o_d_price_max_total']               = $o_d_price_max_total;
			$orderAllArray['order_detailArray']      = $order_detailArray;
			$orderAllArray['item_order_detailArray'] = $item_order_detailArray;

			return $orderAllArray;


		//$order_cdがなければmypage.phpにリダイレクト
		} else {
			header("Location: ./mypage.php");
			exit;
		}

	}


	/**
	 * 商品詳細の会員のお気に入り商品取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array
	 */

	public function getFavoriteItem ($connect, $_cd) {

		if ( ctype_digit(strval($_SESSION['user']['user_info']['cd'])) ) {
			$user_cd = $_SESSION['user']['user_info']['cd'];
		} else {
			$user_cd = 0;
		}

		$table_name = 'favorite_item';

		$cd = $_cd;

		if ( ctype_digit(strval($cd)) ) {
			$sqlAdd = "AND item_cd = {$cd}";
		} else {
			$sqlAdd = "";
		}

		$favorite_itemAllArray = array();

		$_SELECT = "cd, shopping_member_cd, item_cd";
		$sql     = "SELECT {$_SELECT} FROM {$table_name} WHERE disp_flg = 1 AND shopping_member_cd = {$user_cd} {$sqlAdd} LIMIT 1";
		$favorite_item_query = mysqli_query($this->connect, $sql);
		$favorite_item_max   = mysqli_num_rows($favorite_item_query);

		for ($i = 0; $i < $favorite_item_max; $i++) {
		  $favorite_itemArray[] = mysqli_fetch_assoc($favorite_item_query);
		}

		if ($favorite_item_max) {
		  $btn_text = 'お気に入りに登録されています';
		} else {
		  $btn_text = 'お気に入りに登録';
		}

		$favorite_itemAllArray['favorite_item_max'] = $favorite_item_max;
		$favorite_itemAllArray['table_name']        = $table_name;
		$favorite_itemAllArray['favorite_itemArray'] = $favorite_itemArray;
		$favorite_itemAllArray['btn_text']           = $btn_text;

		return $favorite_itemAllArray;

	}


	/**
	 * 会員のお気に入り商品一覧取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array
	 */

	public function getFavoriteList ($connect) {

		$shopping_member_cd = $_SESSION['user']['user_info']['cd'];

		$favoriteArray    = array();
		$favoriteAllArray = array();

		$_SELECT = "f.cd AS f_cd, f.shopping_member_cd, f.shopping_member_cd AS f_shopping_member_cd, f.item_cd AS f_item_cd, ";
		$_SELECT .= "i.cd AS i_cd, i.date AS i_date, i.name AS i_name, i.price AS i_price, i.item_category_cd AS i_item_category_cd, i.comment AS i_comment, i.link AS i_link, i.target AS i_target, i.img1 AS i_img1, i.img2 AS i_img2, i.shipping_type AS i_shipping_type";
		$sql     = "SELECT {$_SELECT} FROM favorite_item AS f LEFT JOIN item AS i ON f.item_cd = i.cd WHERE f.disp_flg = 1 AND i.disp_flg = 1 AND f.shopping_member_cd = {$shopping_member_cd} ORDER BY f.sort, i.sort, f.cd, i.cd";

		$favorite_query = mysqli_query($this->connect, $sql);
		$favorite_max   = mysqli_num_rows($favorite_query);

		for ($i = 0; $i < $favorite_max; $i++) {
			$favoriteArray[] = mysqli_fetch_assoc($favorite_query);


			//画像パス生成
			for ($j = 1; $j < 3; $j++) {

				if ($favoriteArray[$i]["i_img{$j}"]) {
					$favoriteArray[$i]["img_pass{$j}"] = $this->img_pass.$favoriteArray[$i]["i_img{$j}"];
					$favoriteArray[$i]['imgArray'][]   = $favoriteArray[$i]["img_pass{$j}"];
				} else {
					$favoriteArray[$i]['imgArray'][$j] = "./image/no_image.png";
				}

			}
		}

		$favoriteAllArray['favorite_max']  = $favorite_max;
		$favoriteAllArray['favoriteArray'] = $favoriteArray;

		return $favoriteAllArray;
		

	}


	/**
	 * 会員のお届け先一覧取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return array
	 */

	public function getDelivList ($connect) {

		$other_delivAllArray = array();


		//会員のcd番号格納
		$member_cd = $_SESSION['user']['user_info']['cd'];
		$_SELECT   = "cd, member_cd, name1, name2, kana1, kana2, company_name, zip1, zip2, address1, tel, fax, sort";

		$sql               = "SELECT {$_SELECT} FROM other_deliv WHERE member_cd = {$member_cd} AND disp_flg = 1 ORDER BY sort, cd";
		$other_deliv_query = mysqli_query($this->connect, $sql);
		$other_deliv_max   = mysqli_num_rows($other_deliv_query);

		for ($i = 0; $i < $other_deliv_max; $i++) {
			$other_delivArray[] = mysqli_fetch_assoc($other_deliv_query);
		}

		$other_delivAllArray['other_delivArray'] = $other_delivArray;

		return $other_delivAllArray;

	}


	/**
	 * 会員のお届け先詳細取得処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $_cd other_delivテーブルのcd番号
	 * @return array
	 */

	public function getDelivDetail ($connect, $_cd = NULL) {

		$other_delivAllArray = array();

		if ( ctype_digit (strval($_cd) ) ) {

			$cd      = $_cd;
			$sql_add1 = "AND cd = {$cd}";
			$sql_add2 = "LIMIT 1";

			$btn_text = '変更';


			//会員のcd番号格納
			$member_cd = $_SESSION['user']['user_info']['cd'];
			$_SELECT   = "cd, member_cd, name1, name2, kana1, kana2, company_name, zip1, zip2, address1, tel, fax, sort";

			$sql               = "SELECT {$_SELECT} FROM other_deliv WHERE member_cd = {$member_cd} {$sql_add1} AND disp_flg = 1 ORDER BY sort, cd {$sql_add2}";
			$other_deliv_query = mysqli_query($this->connect, $sql);
			$other_deliv_max   = mysqli_num_rows($other_deliv_query);

			for ($i = 0; $i < $other_deliv_max; $i++) {
				$other_delivArray[] = mysqli_fetch_assoc($other_deliv_query);
			}

			$other_delivAllArray['other_delivArray'] = $other_delivArray;
			$other_delivAllArray['btn_text']         = $btn_text;

		} else {
			$btn_text = '変更';
			$other_delivAllArray['btn_text']         = $btn_text;
		}

		return $other_delivAllArray;

	}


	/**
	 * 会員退会処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $_member_cd 会員cd番号
	 * @return bool
	 */

	public function memberRefusal ($connect, $_member_cd) {

		$table_name = 'shopping_member';

		$recordControlObj = new recordControlClass;

		$recordDelete_query = $recordControlObj->recordDelete($this->connect, $table_name, $_member_cd);

		if ($recordDelete_query) {
			return true;
		} else {
			return false;
		}

	}


	/**
	 * 会員退会完了処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $_member_cd 会員cd番号
	 * @return bool
	 */

	public function memberRefusalComplete () {

		$_SESSION['user_login']['login_flg'] = array();

		if ($_SESSION['user_login']['login_flg'] !== 1) {
			return true;
		} else {
			return false;
		}

	}

}
/******************************************************************************************************************************
mypageClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
商品購入後のレコード挿入クラスここから
******************************************************************************************************************************/

class itemPurchaseClass {


	//DB接続情報
	public $connect;

	//商品送料
	public $shipping_cost;

	//管理者メールアドレス
	public $user_mail;

	//item_orderのcd番号のauto_increment
	public $next_cd;


	//コンストラクタ内で行いたい（クラスを呼び出して最初に行いたい）処理（コンストラクタでは返り値を返せないので注意）ここから
	function __construct() {

		$this->connect = $GLOBALS['connect'];


		//管理者メールアドレスをclass内で使用可能にする
		if ( $GLOBALS['user_mail'] ) {
			$this->user_mail = $GLOBALS['user_mail'];
		}


		//テーブルLOCKここから
		$lock_sql = "LOCK TABLES item_order, item_order_detail WRITE";
		$lock_query = mysqli_query($this->connect, $lock_sql);


		//item_orderのcdとitem_order_detailのitem_order_cdで同じ番号を渡すために、auto_incrementを取得するここから
		$table_seq_name = 'information_schema.tables';
		$table_name     = 'item_order';
		$_SELECT        = "auto_increment";
		$_WHERE         = "table_name = '{$table_name}'";
		$sql            = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
		$next_query     = mysqli_query($this->connect, $sql);
		$next_max       = mysqli_num_rows($next_query);

		for ($i = 0; $i < $next_max; $i++) {
			$nextArray       = mysqli_fetch_assoc($next_query);
			$this->next_cd   = $nextArray['auto_increment'];
		}
		//item_orderのcdとitem_order_detailのitem_order_cdで同じ番号を渡すために、auto_incrementを取得するここまで

	}
	//コンストラクタ内で行いたい（クラスを呼び出して最初に行いたい）処理（コンストラクタでは返り値を返せないので注意）ここまで


	/**
	 * 商品購入後のレコード挿入処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @return bool
	 * @todo パフォーマンス改善の余地あり
	 */

	public function itemPurchaseInsert ($connect) {

		$recordControlObj = new recordControlClass;


		//商品送料をclass内で使用可能にする
		if ( is_int($GLOBALS['shipping_cost']) ) {
			$this->shipping_cost = $GLOBALS['shipping_cost'];
		}

		if ($_SESSION['shopping'] && $_SESSION['cartInfo'] && $_SESSION['cart']) {

			$table_name = 'item_order';

		    $shopping_member_cd = $_SESSION['user']['user_info']['cd'];
		    $message            = $_SESSION['shopping']['message'];
		    $order_name1        = $_SESSION['user']['user_info']['name1'];
		    $order_name2        = $_SESSION['user']['user_info']['name2'];
		    $order_kana1        = $_SESSION['user']['user_info']['kana1'];
		    $order_kana2        = $_SESSION['user']['user_info']['kana2'];
		    $order_company_name = '';
		    $order_email        = $_SESSION['user']['user_info']['mail'];
		    $order_tel1         = $_SESSION['user']['user_info']['tel'];
		    $order_fax1         = '';
		    $order_zip1         = $_SESSION['user']['user_info']['post_code1'];
		    $order_zip2         = $_SESSION['user']['user_info']['post_code2'];
		    $order_addr         = $_SESSION['user']['user_info']['address'];
		    $order_sex          = $_SESSION['user']['user_info']['sex'];
		    $subtotal           = $_SESSION['cartInfo']['all_total_price'];
		    $discount           = 'NULL';
		    $deliv_cd           = $_SESSION['shopping']['deliv_id'];
		    $deliv_fee          = $this->shipping_cost;
		    $charge             = 'NULL';
		    $use_point          = $_SESSION['shopping']['use_point'];
		    $add_point          = 'NULL';
		    $tax                = $_SESSION['cartInfo']['all_total_tax'];
		    $payment_cd         = $_SESSION['shopping']['payment_id'];
		    $payment_method     = $_SESSION['shopping']['payment_id_'];
		    $disp_flg           = 1;
		    $sort               = 0;


		    //商品購入が正常にできたらデータベースに登録するここから
			$insertDataArray = array(
				"cd"                 => $this->next_cd,
				"shopping_member_cd" => $shopping_member_cd,
				"message"            => $message,
				"order_name1"        => $order_name1,
				"order_name2"        => $order_name2,
				"order_kana1"        => $order_kana1,
				"order_kana2"        => $order_kana2,
				"order_company_name" => $order_company_name,
				"order_email"        => $order_tel1,
				"order_tel1"         => $order_tel1,
				"order_fax1"         => $order_fax1,
				"order_zip1"         => $order_zip1,
				"order_zip2"         => $order_zip2,
				"order_addr"         => $order_addr,
				"order_sex"          => $order_sex,
				"subtotal"           => $subtotal,
				"discount"           => $discount,
				"deliv_cd"           => $deliv_cd,
				"deliv_fee"          => $deliv_fee,
				"charge"             => $charge,
				"use_point"          => $use_point,
				"add_point"          => $add_point,
				"tax"                => $tax,
				"payment_cd"         => $payment_cd,
				"payment_method"     => $payment_method,
				"disp_flg"           => $disp_flg,
				"sort"               => $sort,
				"new_record_time"    => "current_timestamp"
			);
			
			$data_query = $recordControlObj->recordInsert($this->connect, $table_name, $insertDataArray);
		    //商品購入が正常にできたらデータベースに登録するここまで
		}


	  //商品購入が完了したら、並び替え処理をして、item_order_detailにもINSERTしてから、カートの中身・商品購入関係のセッション変数を初期化
	  if ($data_query) {


	  	//レコード挿入後の並び替え処理
	  	$item_detailQueryResult = $recordControlObj->recordInsertSort($this->connect, 'item_order');


	  	//商品購入後のレコード挿入(item_order_detail)
  		$this->itemDetailPurchaseInsert($this->connect, $this->next_cd);


  		//カートの中身・商品購入関係のセッション変数を初期化
	    $_SESSION['shopping'] = array();
	    $_SESSION['cart']     = array();
	    $_SESSION['cartInfo'] = array();

	    return true;

	  } else {
	  	return false;
	  }

  }


  	/**
	 * 商品購入後のレコード挿入(item_order_detail)処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $next_cd item_orderテーブルにINSERTされたcdと同じ番号を渡す
	 * @return bool
	 */

	public function itemDetailPurchaseInsert ($connect, $next_cd) {


		$itemPurchaseResultArray = array();

		$purchaseArray    = $_SESSION['cartInfo']['cartInfoArray'];
		$recordControlObj = new recordControlClass;

		$item_order_cd = $next_cd;


		//商品の種類の数だけINSERT文を実行するここから
		foreach ( (array)$purchaseArray AS $key => $val ) {

			$item_cd          = $val['cd'];
			$item_category_cd = $val['item_category_cd'];
			$item_name        = $val['name'];
			$price            = $val['price'];
			$item_quantity    = $val['item_number'];
			$shipping_type    = $val['shipping_type'];
			$disp_flg         = 1;
			$sort             = 0;


		    //購入した商品情報をitem_order_detailに登録するここから
		    $sql = "INSERT INTO item_order_detail (
		      item_order_cd,
		      item_cd,
		      item_category_cd,
		      item_name,
		      price,
		      item_quantity,
		      shipping_type,
		      disp_flg,
		      sort,
		      new_record_time
		      )
		    VALUES(
		      {$item_order_cd},
		      {$item_cd},
		      {$item_category_cd},
		      '{$item_name}',
		      {$price},
		      {$item_quantity},
		      {$shipping_type},
		      {$disp_flg},
		      {$sort},
		      current_timestamp
		      )";

		    $queryResult = mysqli_query($this->connect, $sql);
		    //購入した商品情報をitem_order_detailに登録するここまで

		    if ($queryResult) {


		    	//並び替え処理
			  	$recordControlObj->recordInsertSort($this->connect, 'item_order_detail');

			  	$itemPurchaseResultArray[] = true;

		    } else {
		    	$itemPurchaseResultArray[] = false;
		    }

		}
		//商品の種類の数だけINSERT文を実行するここまで


		//商品購入後、会員の保持ポイントを再計算する
		$purchaseMemberUpdate_query = $this->purchaseMemberUpdate($this->connect);


		//会員の保持ポイントを再計算した後、購入者と運営者にメールを送信する
		if ($purchaseMemberUpdate_query) {
			$purchaseSendMail_query = $this->purchaseSendMail($this->next_cd);
		}


		//テーブルLOCKここまで
		$unlock_sql = "UNLOCK TABLES";
		$unlock_query = mysqli_query($this->connect, $unlock_sql);

	}


  	/**
	 * 商品購入後のレコード更新(shopping_member)処理用の関数
	 *
	 * @param $connect DB接続情報
	 * @param $next_cd item_orderテーブルにINSERTされたcdと同じ番号を渡す
	 * @return bool
	 */

	public function purchaseMemberUpdate ($connect) {

		$purchaseMemberArray = $_SESSION['user']['user_info'];
		$recordControlObj    = new recordControlClass;

		$table_name         = 'shopping_member';
		$purchase_member_cd = $_SESSION['user']['user_info']['cd'];
		$point              = $_SESSION['user']['user_info']['point'] - $_SESSION['shopping']['use_point'] + $_SESSION['cartInfo']['add_point_increment'];

		$updateDataArray = array(
			"point"              => $point,
			"update_record_time" => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordUpdate($this->connect, $table_name, $updateDataArray, $purchase_member_cd);
	    //購入した商品情報をitem_order_detailに登録するここまで

	    if ($data_query) {


	    	//会員の保持ポイントを再計算するここから
	    	$sql = "SELECT cd, point FROM shopping_member WHERE cd = {$purchase_member_cd}";
	    	$user_point_query = mysqli_query($this->connect, $sql);
	    	$user_point_max   = mysqli_num_rows($user_point_query);

	    	if ($user_point_max) {
	    		$user_pointArray = mysqli_fetch_assoc($user_point_query);
	    		$user_point      = $user_pointArray['point'];
	    		$_SESSION['user']['user_info']['point'] = $user_point;
	    		$_SESSION['cart']['cart_info']['point'] = $user_point;
	    	}
	    	//会員の保持ポイントを再計算するここまで

	    	return true;
	    } else {
	    	return false;
	    }


	}


  	/**
	 * 商品購入後のメール送信処理用の関数
	 *
	 * @param $next_cd item_orderテーブルにINSERTされたcdと同じ番号を渡
	 * @return bool
	 */

	public function purchaseSendMail ($next_cd) {


		//購入者情報取得ここから
		$purchase_select_cd = $next_cd;

		$sql                   = "SELECT cd, shopping_member_cd FROM item_order WHERE disp_flg = 1 AND cd = {$purchase_select_cd}";
		$purchase_select_query = mysqli_query($this->connect, $sql);
		$purchase_select_max   = mysqli_num_rows($purchase_select_query);

		if ($purchase_select_max) {
			$purchase_selectArray = mysqli_fetch_assoc($purchase_select_query);
		}
		//購入者情報取得ここまで

		$purchaseMailSendArray           = array();

		$purchaseUserInfoArray           = $_SESSION['user']['user_info'];
		$purchasecartInfoArray           = $_SESSION['cartInfo']['cartInfoArray'];
		$purchase_all_total_price        = $_SESSION['cartInfo']['all_total_price'];
		$purchase_all_total_tax          = $_SESSION['cartInfo']['all_total_tax'];
		$purchase_all_total_price_format = $_SESSION['cartInfo']['all_total_price_format'];
		$purchase_shipping_cost          = $_SESSION['cartInfo']['shipping_cost'];
		$purchase_point                  = $_SESSION['cartInfo']['point'];
		$purchase_add_point_increment    = $_SESSION['cartInfo']['add_point_increment'];
		$purchase_shoppingArray          = $_SESSION['shopping'];


		//お届け先住所ここから
		// $other_delivSendArray = array();

		// if ($_SESSION['other_delivArray']) {
		// 	$other_delivSendArray = $_SESSION['other_delivArray'];
		// }

		//会員のお届け先を取得
		$mypageClassObj      = new mypageClass;
		$other_delivAllArray = $mypageClassObj->getDelivDetail($this->connect, $_SESSION['shopping']['deliv_check']);
		$other_delivArray    = $other_delivAllArray['other_delivArray'];


		//お届け先の住所が会員の住所を選択されている場合
		if (!$other_delivArray) {
		  $other_delivArray[0]['name1']        = $_SESSION['user']['user_info']['name1'];
		  $other_delivArray[0]['name2']        = $_SESSION['user']['user_info']['name2'];
		  $other_delivArray[0]['kana1']        = $_SESSION['user']['user_info']['kana1'];
		  $other_delivArray[0]['kana2']        = $_SESSION['user']['user_info']['kana2'];
		  $other_delivArray[0]['company_name'] = $_SESSION['user']['user_info']['company_name'];
		  $other_delivArray[0]['zip1']         = $_SESSION['user']['user_info']['post_code1'];
		  $other_delivArray[0]['zip2']         = $_SESSION['user']['user_info']['post_code2'];
		  $other_delivArray[0]['address1']     = $_SESSION['user']['user_info']['address'];
		  $other_delivArray[0]['tel']          = $_SESSION['user']['user_info']['tel'];
		  $other_delivArray[0]['fax']          = $_SESSION['user']['user_info']['fax'];

		} else {
		  $_SESSION['other_delivArray'] = $other_delivArray[0];
		}


		//お届け先住所(複数の場合)
		if ($_SESSION['cartInfo']['cartInfoCopyArray']) {
			$copyAllArray = $_SESSION['shopping_multiple']['mailSendArray'];
		}


		/*********************************************************************
		メール本文の整形（ユーザー用）ここから
		*********************************************************************/

		$body = " {$purchaseUserInfoArray['name1']} {$purchaseUserInfoArray['name2']}様\n";
		$body .= "この度はご注文いただき誠にありがとうございます。\n";
		$body .= "下記ご注文内容にお間違えがないかご確認下さい。\n";
		$body .= "" . "\n";
		$body .= "*" . "\n";
		$body .= "***********************************************\n";
		$body .= " ご請求金額\n";
		$body .= "*" . "\n";
		$body .= '***********************************************' . "\n";
		$body .= "" . "\n";
		$body .= "ご注文番号：{$purchase_selectArray['cd']}\n";
		$body .= "お支払い合計：{$purchase_all_total_price_format}\n";
		$body .= "お支払い方法：{$purchase_shoppingArray['payment_id_']}\n";
		$body .= "メッセージ：{$purchase_shoppingArray['message']}\n";
		$body .= "" . "\n";
		$body .= "" . "\n";
		$body .= "*" . "\n";
		$body .= "***********************************************\n";
		$body .= " ご注文商品明細\n";
		$body .= "*" . "\n";
		$body .= '***********************************************' . "\n";


		//商品の種類の数分ループここから
		foreach ($purchasecartInfoArray AS $key => $value) {
			$body .= "" . "\n";
			$body .= "商品コード：{$value['cd']}\n";
			$body .= "商品名：{$value['name']}\n";
			$body .= "単価：{$value['price']}\n";
			$body .= "数量：{$value['item_number']}\n";
			$body .= "" . "\n";
		}
		//商品の種類の数分ループここまで


		$body .= "-------------------------------------------------\n";
		$body .= "小　計 {$name} (うち消費税 {$name})\n";
		$body .= "送　料 {$purchase_shipping_cost}\n";
		$body .= "手数料 {$purchase_shipping_cost}\n";
		$body .= "============================================\n";
		$body .= "合　計 {$purchase_all_total_price_format}\n";
		$body .= "" . "\n";
		$body .= "*" . "\n";
		$body .= "***********************************************" . "\n";
		$body .= " ご注文者情報" . "\n";
		$body .= "*" . "\n";
		$body .= "***********************************************" . "\n";
		$body .= " 名前 :{$purchaseUserInfoArray['name1']} {$purchaseUserInfoArray['name2']}\n";
		$body .= " 会社名 :{$purchaseUserInfoArray['company_name']}\n";
		$body .= " 郵便番号 :〒{$purchaseUserInfoArray['post_code1']}-{$purchaseUserInfoArray['post_code2']}\n";
		$body .= " 住所 :{$purchaseUserInfoArray['address']}\n";
		$body .= " 電話番号 :{$purchaseUserInfoArray['tel']}\n";
		$body .= " FAX番号 :{$purchaseUserInfoArray['fax']}\n";
		$body .= " メールアドレス :{$purchaseUserInfoArray['mail']}\n";
		$body .= "" . "\n";

		if ($_SESSION['cartInfo']['cartInfoCopyArray']) {


			//お届け先複数の場合のループここから
			$cnt = 1;
			foreach ( (array) $copyAllArray AS $key => $value ) {

				$multiple_quantity_all_price = 0;

				$body .= " お届け先住所{$cnt}" . "\n";
				$body .= "*" . "\n";
				$body .= "***********************************************" . "\n";
				$body .= " お名前 :{$value[0]['shppingArray']['o_name1']} {$value[0]['shppingArray']['o_name2']}\n";
				$body .= " 会社名 :{$value[0]['shppingArray']['o_company_name']}\n";
				$body .= " 郵便番号 :〒{$value[0]['shppingArray']['o_zip1']}-{$value[0]['shppingArray']['o_zip2']}\n";
				$body .= " 住所 :{$value[0]['shppingArray']['o_address1']}\n";
				$body .= " 電話番号 :{$value[0]['shppingArray']['o_tel']}\n";
				$body .= " FAX番号 :{$value[0]['shppingArray']['o_fax']}\n";
				$body .= "***********************************************" . "\n";
				$body .= "" . "\n";

				foreach ( (array) $value AS $key2 => $value2 ) {

					//お届け先毎の商品の小計計算
					$multiple_quantity_all_price += $value2['multiple_quantity_total_price'];

					$body .= " 商品コード :{$value2['cd']}\n";
					$body .= " 商品名 :{$value2['name']}\n";
					$body .= " 単価 :{$value2['price_plus_tax_format']}\n";
					$body .= " 数量 :{$value2['quantity']}\n";
					$body .= "" . "\n";
				}

				$body .= "" . "\n";
				$cnt++;
			}
			//お届け先複数の場合のループここまで

		} else {

			$body .= " お届け先住所" . "\n";
			$body .= "*" . "\n";
			$body .= "***********************************************" . "\n";
			$body .= " 名前 :{$other_delivArray[0]['name1']} {$other_delivArray[0]['name2']}\n";
			$body .= " 会社名 :{$other_delivArray[0]['company_name']}\n";
			$body .= " 郵便番号 :〒{$other_delivArray[0]['zip1']}-{$other_delivArray[0]['zip2']}\n";
			$body .= " 住所 :{$other_delivArray[0]['address1']}\n";
			$body .= " 電話番号 :{$other_delivArray[0]['tel']}\n";
			$body .= " FAX番号 :{$other_delivArray[0]['fax']}\n";
			$body .= "***********************************************" . "\n";

		}



		$body .= "============================================\n";
		$body .= "ご使用ポイント {$purchase_shoppingArray['use_point']}pt\n";
		$body .= "今回加算される予定のポイント {$purchase_add_point_increment}pt\n";
		$body .= "現在の所持ポイント {$purchase_shoppingArray['result_point']}pt\n";
		$body .= "" . "\n";
		$body .= "============================================\n";
		$body .= "" . "\n";
		$body .= "" . "\n";
		$body .= "このメッセージはお客様へのお知らせ専用ですので、" . "\n";
		$body .= "このメッセージへの返信としてご質問をお送りいただいても回答できません。" . "\n";
		$body .= "ご了承ください。" . "\n";
		$body .= "" . "\n";
		$body .= "ご質問やご不明な点がございましたら、こちらからお願いいたします。" . "\n";
		/*********************************************************************
		メール本文の整形（ユーザー用）ここまで
		*********************************************************************/


		/*********************************************************************
		メール本文の整形管理者用）ここから
		*********************************************************************/

		$body2 = " {$purchaseUserInfoArray['name1']} {$purchaseUserInfoArray['name2']}様から商品の購入がありました。\n";
		$body2 .= "下記が注文内容になります。\n";
		$body2 .= "" . "\n";
		$body2 .= "*" . "\n";
		$body2 .= "***********************************************\n";
		$body2 .= " ご請求金額\n";
		$body2 .= "*" . "\n";
		$body2 .= '***********************************************' . "\n";
		$body2 .= "" . "\n";
		$body2 .= "ご注文番号：{$purchase_selectArray['cd']}\n";
		$body2 .= "お支払い合計：{$purchase_all_total_price_format}\n";
		$body2 .= "お支払い方法：{$purchase_shoppingArray['payment_id_']}\n";
		$body2 .= "メッセージ：{$purchase_shoppingArray['message']}\n";
		$body2 .= "" . "\n";
		$body2 .= "" . "\n";
		$body2 .= "*" . "\n";
		$body2 .= "***********************************************\n";
		$body2 .= " ご注文商品明細\n";
		$body2 .= "*" . "\n";
		$body2 .= '***********************************************' . "\n";


		//商品の種類の数分ループここから
		foreach ($purchasecartInfoArray AS $key => $value) {
			$body2 .= "" . "\n";
			$body2 .= "商品コード：{$value['cd']}\n";
			$body2 .= "商品名：{$value['name']}\n";
			$body2 .= "単価：{$value['price']}\n";
			$body2 .= "数量：{$value['item_number']}\n";
			$body2 .= "" . "\n";
		}
		//商品の種類の数分ループここまで


		$body2 .= "-------------------------------------------------\n";
		$body2 .= "小　計 {$name} (うち消費税 {$name})\n";
		$body2 .= "送　料 {$purchase_shipping_cost}\n";
		$body2 .= "手数料 {$purchase_shipping_cost}\n";
		$body2 .= "============================================\n";
		$body2 .= "合　計 {$purchase_all_total_price_format}\n";
		$body2 .= "" . "\n";
		$body2 .= "*" . "\n";
		$body2 .= "***********************************************" . "\n";
		$body2 .= " ご注文者情報" . "\n";
		$body2 .= "*" . "\n";
		$body2 .= "***********************************************" . "\n";
		$body2 .= " 名前 :{$purchaseUserInfoArray['name1']} {$purchaseUserInfoArray['name2']}\n";
		$body2 .= " 会社名 :{$purchaseUserInfoArray['company_name']}\n";
		$body2 .= " 郵便番号 :〒{$purchaseUserInfoArray['post_code1']}-{$purchaseUserInfoArray['post_code2']}\n";
		$body2 .= " 住所 :{$purchaseUserInfoArray['address']}\n";
		$body2 .= " 電話番号 :{$purchaseUserInfoArray['tel']}\n";
		$body2 .= " FAX番号 :{$purchaseUserInfoArray['fax']}\n";
		$body2 .= " メールアドレス :{$purchaseUserInfoArray['mail']}\n";
		$body2 .= "" . "\n";

		if ($_SESSION['cartInfo']['cartInfoCopyArray']) {


			//お届け先複数の場合のループここから
			$cnt = 1;
			foreach ( (array) $copyAllArray AS $key => $value ) {

				$multiple_quantity_all_price = 0;

				$body2 .= " お届け先住所{$cnt}" . "\n";
				$body2 .= "*" . "\n";
				$body2 .= "***********************************************" . "\n";
				$body2 .= " お名前 :{$value[0]['shppingArray']['o_name1']} {$value[0]['shppingArray']['o_name2']}\n";
				$body2 .= " 会社名 :{$value[0]['shppingArray']['o_company_name']}\n";
				$body2 .= " 郵便番号 :〒{$value[0]['shppingArray']['o_zip1']}-{$value[0]['shppingArray']['o_zip2']}\n";
				$body2 .= " 住所 :{$value[0]['shppingArray']['o_address1']}\n";
				$body2 .= " 電話番号 :{$value[0]['shppingArray']['o_tel']}\n";
				$body2 .= " FAX番号 :{$value[0]['shppingArray']['o_fax']}\n";
				$body2 .= "***********************************************" . "\n";
				$body2 .= "" . "\n";

				foreach ( (array) $value AS $key2 => $value2 ) {

					//お届け先毎の商品の小計計算
					$multiple_quantity_all_price += $value2['multiple_quantity_total_price'];

					$body2 .= " 商品コード :{$value2['cd']}\n";
					$body2 .= " 商品名 :{$value2['name']}\n";
					$body2 .= " 単価 :{$value2['price_plus_tax_format']}\n";
					$body2 .= " 数量 :{$value2['quantity']}\n";
					$body2 .= "" . "\n";
				}

				$body2 .= "" . "\n";
				$cnt++;
			}
			//お届け先複数の場合のループここまで

		} else {

			$body2 .= " お届け先住所" . "\n";
			$body2 .= "*" . "\n";
			$body2 .= "***********************************************" . "\n";
			$body2 .= " 名前 :{$other_delivArray[0]['name1']} {$other_delivArray[0]['name2']}\n";
			$body2 .= " 会社名 :{$other_delivArray[0]['company_name']}\n";
			$body2 .= " 郵便番号 :〒{$other_delivArray[0]['zip1']}-{$other_delivArray[0]['zip2']}\n";
			$body2 .= " 住所 :{$other_delivArray[0]['address1']}\n";
			$body2 .= " 電話番号 :{$other_delivArray[0]['tel']}\n";
			$body2 .= " FAX番号 :{$other_delivArray[0]['fax']}\n";
			$body2 .= "***********************************************" . "\n";

		}

		$body2 .= "============================================\n";
		$body2 .= "ご使用ポイント {$purchase_shoppingArray['use_point']}pt\n";
		$body2 .= "今回加算される予定のポイント {$purchase_add_point_increment}pt\n";
		$body2 .= "現在の所持ポイント {$purchase_shoppingArray['result_point']}pt\n";
		$body2 .= "" . "\n";
		$body2 .= "============================================\n";
		$body2 .= "" . "\n";
		$body2 .= "" . "\n";
		$body2 .= "このメッセージは管理者様へのお知らせ専用です。" . "\n";
		$body2 .= "" . "\n";
		/*********************************************************************
		メール本文の整形（管理者用）ここまで
		*********************************************************************/



		//------------------------------------
		// 言語・文字コード指定
		//------------------------------------

		mb_language("Japanese");
		mb_internal_encoding("UTF-8");


		//------------------------------------
		// ユーザーに自動返信メール
		//------------------------------------

		$subject = "ご購入ありがとうございました。";
		$mailto  = $purchaseUserInfoArray['mail'];
		$from    = "From:{$this->user_mail}";
		$return1 = mb_send_mail($mailto, $subject, $body, $from);


		//------------------------------------
		// 管理者に送るメール
		//------------------------------------

		$subject = "商品購入がありました。";
		$mailto  = $this->user_mail;
		$from    = "From:{$this->user_mail}";
		$return2 = mb_send_mail($mailto, $subject, $body2, $from);


		//------------------------------------
		// メールが正常に送信されたかを判定
		//------------------------------------

		if ($return1 && $return2) {
			return true;
		} else {
			return false;
		}

	}

}
/******************************************************************************************************************************
商品購入後のレコード挿入クラスここまで
******************************************************************************************************************************/