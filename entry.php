﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//job_type取得ここから
$_SELECT = "cd, name";
$_TABLE  = "job_type";
$_WHERE  = "WHERE disp_flg = 1";
$_ORDER  = "sort";

$job_typeControldObj  = new recordControlClass;
$job_typeArray2       = $job_typeControldObj->getRecord($connect, $_SELECT, $_TABLE, $_WHERE, $_ORDER);

$job_type_max2 = count($job_typeArray2);
//job_type取得ここまで
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>Entry</h2>


      <?php if ($_GET['flg'] == 1) { ?>

      <p class="completeMessage">会員登録完了しました。</p>
      <a class="backBtn" href="./">戻る</a>

      <?php } else { ?>

      <?php if ($_GET['flg'] == 2) { ?>
      <p style="color: #bf0000;">入力されてない項目があります。</p>
      <?php } ?>

      <?php if ($_GET['mode'] === 'query_faild') { ?>
      <p style="color: #bf0000;">ご入力いただいたユーザーID、またはパスワードはすでにすでに使用されています。<br>
        違うユーザーID・パスワードで再度ご登録ください。
      </p>
      <?php } ?>

      <form id="form1" name="form1" method="post">

        <table class="form_1">
          <tr>
            <th><label for="name1">姓名<em>必須</em></label></th>
            <td class="column2_cell">
              <p id="name1Error" class="errorMessage"></p>
              <p id="name2Error" class="errorMessage"></p>
              <input id="name1" class="column2_input" type="text" name="name1" value="">
              <input id="name2" class="column2_input" type="text" name="name2" value="">
            </td>
          </tr>

          <tr>
            <th><label for="kana1">かな<em>必須</em></label></th>
            <td class="column2_cell">
              <p id="kana1Error" class="errorMessage"></p>
              <p id="kana2Error" class="errorMessage"></p>
              <input id="kana1" class="column2_input" type="text" name="kana1" value="">
              <input id="kana2" class="column2_input" type="text" name="kana2" value="">
            </td>
          </tr>

          <tr>
            <th><label for="company_name">会社名</label></th>
            <td>
              <p id="company_nameError"></p>
              <input id="company_name" type="text" name="company_name" value="<?=$shopping_memberArray2[0]['company_name']?>">
            </td>
          </tr>

          <tr>
            <th><label for="sex1">性別<em>必須</em></label></th>
            <td class="addressCell">
              <p id="sexError" class="errorMessage"></p>
              <label><input id="sex1" type="radio" name="sex" value="1" checked>男性<label>
              <label><input id="sex2" type="radio" name="sex" value="2">女性<label>
            </td>
          </tr>

          <tr>
            <th><label for="job_type">職業</label></th>
            <td>
              <p id="job_typeError"></p>
              <div class="column1_select">
                <select id="job_type" name="job_type">

                  <option value="">選択してください。</option>

                  <?php for ($i = 0; $i < $job_type_max2; $i++) { ?>

                    <?php if ($job_typeArray2[$i]['cd'] == $shopping_memberArray2[0]['job_type']) { ?>
                    <option value="<?=$job_typeArray2[$i]['cd']?>" selected><?=$job_typeArray2[$i]['name']?></option>
                    <?php } else { ?>
                    <option value="<?=$job_typeArray2[$i]['cd']?>"><?=$job_typeArray2[$i]['name']?></option>
                    <?php } ?>

                  <?php } ?>

                </select>
              </div>
            </td>
          </tr>

          <tr>
            <th><label for="birth_year">生年月日</label></th>
            <td>
              <p id="birth_yearError"></p>

              <?php
              $bith_yearArray  = range(1900, date("Y"));
              $bith_monthArray = range(1, 12);
              $bith_dayArray   = range(1, 31);
              $birth_year_max  = count($bith_year);
              ?>

              <div class="column3_select">
                <select id="birth_year" name="birth_year">

                  <option value="">選択してください。</option>

                  <?php foreach ( (array) $bith_yearArray AS $key => $value ) { ?>

                    <?php if ($value == $shopping_memberArray2[0]['birth_year']) { ?>
                    <option value="<?=$value?>" selected><?=$value?></option>
                    <?php } else { ?>
                    <option value="<?=$value?>"><?=$value?></option>
                    <?php } ?>

                  <?php } ?>

                </select>
                <span>年</span>
              </div>

              <div class="column3_select">
                <select id="birth_month" name="birth_month">

                  <option value="">選択してください。</option>

                  <?php foreach ( (array) $bith_monthArray AS $key => $value ) {
                    $value = sprintf("%02d" ,$value);
                    ?>

                    <?php if ($value == $shopping_memberArray2[0]['birth_month']) { ?>
                    <option value="<?=$value?>" selected><?=$value?></option>
                    <?php } else { ?>
                    <option value="<?=$value?>"><?=$value?></option>
                    <?php } ?>

                  <?php } ?>

                </select>
                <span>月</span>
              </div>

              <div class="column3_select">
                <select id="birth_day" name="birth_day">

                  <option value="">選択してください。</option>

                  <?php foreach ( (array) $bith_dayArray AS $key => $value ) {
                    $value = sprintf("%02d" ,$value);
                    ?>

                    <?php if ($value == $shopping_memberArray2[0]['birth_day']) { ?>
                    <option value="<?=$value?>" selected><?=$value?></option>
                    <?php } else { ?>
                    <option value="<?=$value?>"><?=$value?></option>
                    <?php } ?>

                  <?php } ?>

                </select>
                <span>日</span>
              </div>
            </td>
          </tr>

          <tr>
            <th><label for="post_code1">郵便番号<em>必須</em></label></th>
            <td class="addressCell">
              <p id="post_code1Error" class="errorMessage"></p>
              <p id="post_code2Error" class="errorMessage"></p>
              <input id="post_code1" type="text" name="post_code1" value="">
              -
              <input id="post_code2" type="text" name="post_code2" value="">
            </td>
          </tr>

          <tr>
            <th><label for="address">住所<em>必須</em></label></th>
            <td>
              <p id="addressError" class="errorMessage"></p>
              <input id="address" type="text" name="address" value="">
            </td>
          </tr>

          <tr>
            <th><label for="tel">電話番号<em>必須</em></label></th>
            <td>
              <p id="telError" class="errorMessage"></p>
              <input type="text" id="tel" name="tel" value="">
            </td>
          </tr>

          <tr>
            <th><label for="fax">FAX</label></th>
            <td>
              <p id="faxError"></p>
              <input id="fax" type="text" name="fax" value="<?=$shopping_memberArray2[0]['fax']?>">
            </td>
          </tr>

          <tr>
            <th><label for="mail">メールアドレス<em>必須</em></label></th>
            <td>
              <p id="mailError" class="errorMessage"></p>
              <input type="text" id="mail" name="mail" value="">
            </td>
          </tr>

          <tr>
            <th><label for="phone_mail_address">携帯メールアドレス</label></th>
            <td>
              <p id="phone_mail_addressError"></p>
              <input id="phone_mail_address" type="text" name="phone_mail_address" value="<?=$shopping_memberArray2[0]['phone_mail_address']?>">
            </td>
          </tr>

          <tr>
            <th><label for="login_id">ユーザーID<em>必須</em></label></th>
            <td>
              <p id="login_idError" class="errorMessage"></p>
              <input id="login_id" type="text" name="login_id" value="<?=$shopping_memberArray2[0]['user_id']?>">
            </td>
          </tr>

          <tr>
            <th><label for="password">パスワード<em>必須</em></label></th>
            <td>
              <p id="passwordError" class="errorMessage"></p>
              <input id="password" type="text" name="password" value="<?=$shopping_memberArray2[0]['password']?>">
            </td>
          </tr>
            
        </table>

        <ul class="btnList">
          <li>
            <input type="reset" id="reset" class="button" value="リセット">
          </li>
          <li>
            <input type="button" id="submit" class="button" value="登録">
          </li>
        </ul>

        <input type="hidden" name="flg" value="1">

      </form>

      <?php } ?>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
