﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Biography</h2>
        
        <p class="bio_mainvisual">
        <img src="image/biography_mainvisual.jpg" width="470" height="330" alt="baigraphy_mainvisual">
        </p>
        
        <p class="bio_honbun">
        
        1994年のメジャーデビュー以降、CDセールス、ライブ動員数など常に日本の音楽シーンをリードし続け、数々の金字塔を打ち立ててきた日本のロックバンド。<br>
 2010年6月、「15周年を終えてGLAYがもっと音楽に対して真っすぐである為に」と、メンバー主体の自主レーベル『loversoul music & associates』を設立。同年8月より、GLAY official Store G-DIRECTをスタートさせ、3ヶ月連続CDリリースや限定商品の販売、ライブ音源のダウンロード販売など「直接ユーザーに音楽を届ける」ために独自のアプローチを展開している。<br>
 2012年7月28日（土）、29日（日）には、大阪・長居スタジアムで「GLAY STADIUM LIVE 2012 THE SUITE ROOM in OSAKA NAGAI STADIUM supported by Glico」と題した夏の野外イベントを行った。両日とも「HOTEL GLAY」をコンセプトに、28日（土）は「Super Welcome Party」、7月29日（日）は「Big Surprise Party」というそれぞれ異なったテーマで行われた。29日には7BIG SURPRISEが発表された。<br>
そして、2013年1月23日には、GLAY史上初となるアルバム「JUSTICE」「GUILTY」2枚同時リリース。<br>
2月からは25万人を動員する全国アリーナツアー「GLAY ARENA TOUR 2013 "JUSTICE & GUILTY」とGLAY ASIA TOUR 2013 "JUSTICE & GUILTY"と題して、香港、台北にてアジア公演を敢行し大盛況となった。<br>
7月27日（土）＆28日（日）にはGLAYの故郷である、函館・緑の島野外特設ステージにて開催され、DVD & Blu-rayが11月27日に発売、また同日には49枚目のトリプルAサイドシングル「DIAMOND SKIN/虹のポケット/CRAZY DANCE」がリリースされた。<br>

2014年、GLAYはデビュー20周年を迎え、5月から3ヶ月連続リリース。
9月20日(土)、宮城スタジアムにて「GLAY EXPO 2014 TOHOKU 20th Anniversary」を開催。
10月15日にはニューシングル「百花繚乱/疾走れ！ミライ」、11月5日にはニューアルバム「MUSIC LIFE」が発売される。
        
        </p>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
