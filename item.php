﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//item一覧取得
$itemObj   = new itemListClass;
$itemArray = $itemObj -> getItemList($connect);


/*
echo '<pre>';
print_r($img_pass);
echo '</pre>';
*/

?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Item</h2>
        
        <ul class="itemList">

            <?php foreach ( (array)$itemArray AS $key => $val ) { ?>
            <li>          
                <dl>
                    <dt><a href="./item_detail.php?cd=<?=$val['cd']?>"><img src="<?=$val['img_pass1']?>" alt="<?=$val['name']?>"></a></dt>
                    <dd>
                        <h3 class="name"><?=$val['name']?></h3>
                        <p class="price"><?=$val['price_format']?>円</p>
                        <p class="comment"><?=nl2br($val['comment'])?></p>
                        <p class="linkBtn"><a href="./item_detail.php?cd=<?=$val['cd']?>">Detail</a></p>
                    </dd>
                </dl>
            </li>
            <?php } ?>
        
        </ul>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
