<?php

require_once '../inc/db.php';
require_once './class/class.php';

$XSSObj = new XSSClass;

$resArray = array();

$table_name = "basic";

if($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);


	//処理分岐ここから
	switch ($_REQUEST['mode']) {


		//登録処理ここから
		case 'insert':


		//データ整形
		$user_name         = $_REQUEST['user_name'];
		$company_name      = $_REQUEST['company_name'];
		$tel1              = $_REQUEST['tel1'];
		$tel2              = $_REQUEST['tel2'];
		$fax1              = $_REQUEST['fax1'];
		$fax2              = $_REQUEST['fax2'];
		$mobile_phone1     = $_REQUEST['mobile_phone1'];
		$mobile_phone2     = $_REQUEST['mobile_phone2'];
		$mail              = $_REQUEST['mail'];
		$mobile_phone_mail = $_REQUEST['mobile_phone_mail'];
		$post_number1      = $_REQUEST['post_number1'];
		$post_number2      = $_REQUEST['post_number2'];
		$address1          = $_REQUEST['address1'];
		$address2          = $_REQUEST['address2'];
		$title             = $_REQUEST['title'];
		$keyword           = $_REQUEST['keyword'];
		$description       = $_REQUEST['description'];
		$h1                = $_REQUEST['h1'];
		$president         = $_REQUEST['president'];


		//SQL文
		$sql = "INSERT INTO {$table_name} (
			user_name,
			company_name,
			tel1,
			tel2,
			fax1,
			fax2,
			mobile_phone1,
			mobile_phone2,
			mail,
			mobile_phone_mail,
			post_number1,
			post_number2,
			address1,
			address2,
			title,
			keyword,
			description,
			h1,
			president,
			new_record_time
			)
		VALUES(
			'{$user_name}',
			'{$company_name}',
			'{$tel1}',
			'{$tel2}',
			'{$fax1}',
			'{$fax2}',
			'{$mobile_phone1}',
			'{$mobile_phone2}',
			'{$mail}',
			'{$mobile_phone_mail}',
			'{$post_number1}',
			'{$post_number2}',
			'{$address1}',
			'{$address2}',
			'{$title}',
			'{$keyword}',
			'{$description}',
			'{$h1}',
			'{$president}',
			current_timestamp
			)";

				break;
			//登録処理ここまで
	}
	
	//処理分岐ここから
	switch ($_REQUEST['mode']) {


		//更新処理ここから
		case 'update':


		//データ整形
		$user_name         = $_REQUEST['user_name'];
		$company_name      = $_REQUEST['company_name'];
		$tel1              = $_REQUEST['tel1'];
		$tel2              = $_REQUEST['tel2'];
		$fax1              = $_REQUEST['fax1'];
		$fax2              = $_REQUEST['fax2'];
		$mobile_phone1     = $_REQUEST['mobile_phone1'];
		$mobile_phone2     = $_REQUEST['mobile_phone2'];
		$mail              = $_REQUEST['mail'];
		$mobile_phone_mail = $_REQUEST['mobile_phone_mail'];
		$post_number1      = $_REQUEST['post_number1'];
		$post_number2      = $_REQUEST['post_number2'];
		$address1          = $_REQUEST['address1'];
		$address2          = $_REQUEST['address2'];
		$title             = $_REQUEST['title'];
		$keyword           = $_REQUEST['keyword'];
		$description       = $_REQUEST['description'];
		$h1                = $_REQUEST['h1'];
		$president         = $_REQUEST['president'];


		//SQL文
		$sql = "UPDATE {$table_name} SET
			user_name          = '{$user_name}',
			company_name       = '{$company_name}',
			tel1               = '{$tel1}',
			tel2               = '{$tel2}',
			fax1               = '{$fax1}',
			fax2               = '{$fax2}',
			mobile_phone1      = '{$mobile_phone1}',
			mobile_phone2      = '{$mobile_phone2}',
			mail               = '{$mail}',
			mobile_phone_mail  = '{$mobile_phone_mail}',
			post_number1       = '{$post_number1}',
			post_number2       = '{$post_number2}',
			address1           = '{$address1}',
			address2           = '{$address2}',
			title              = '{$title}',
			keyword            = '{$keyword}',
			description        = '{$description}',
			h1                 = '{$h1}',
			president          = '{$president}',
			update_record_time = current_timestamp


		WHERE cd = {$cd}
			";

				break;
			//更新処理ここまで


			//削除処理ここから
			case 'delete':
			$sql = "DELETE FROM {$table_name} WHERE cd = {$cd}";

				break;
			//削除処理ここまで
	}
	//処理分岐ここまで
	mysqli_query($connect, $sql);

} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);