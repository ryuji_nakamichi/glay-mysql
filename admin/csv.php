<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './class/class.php';
require_once './inc/function.php';
require_once './inc/csv.php';

$titleDispText = 'CSV';
$img_max       = 0;


//データベース内のテーブル一覧取得ここから
$sql         = "SHOW TABLES FROM glay";
$table_query = mysqli_query($connect, $sql);
$table_max   = mysqli_num_rows($table_query);

for ($i = 0; $i < $table_max; $i++) {
	$tableArray[] = mysqli_fetch_assoc($table_query);
}
//データベース内のテーブル一覧取得ここまで

$table_name = $_GET['table_name'];
if($_GET['csv_inport_flg'] === 'success') {
	$csv_error_text = "<p class=\"success_text\">{$table_name}にCSVインポートしました。</p>";
} else if ($_GET['csv_inport_flg'] === 'faild') {
	$csv_error_text = "<p class=\"error_text\">{$table_name}にCSVインポート失敗しました。<br>もう一度お試しください。</p>";
} else if ($_GET['csv_inport_flg'] === 'csv_file_none') {
	$csv_error_text = "<p class=\"error_text\">CSVファイルをアップロードしてください。</p>";
}

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop" class="colum1">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$titleDispText?>エクスポート</h2>
					<form id="csvExport" method="POST" enctype="multipart/form-data">
						<div class="table-wrap">
							<table class="adminTable">

								<tr>
									<th>テーブル選択</th>
									<td>
										<select id="csv_export_table_name" name="table_name">

											<?php foreach ( (array)$tableArray AS $key => $value) { ?>
											<option value="<?=$value['Tables_in_glay']?>"><?=$value['Tables_in_glay']?></option>
											<?php } ?>

										</select>
									</td>
								</tr>

								<tr>
									<td colspan="2">
										<div class="csvBox">
											<input class="csvExButton" name="csvExport" type="submit" value="csvExport">
										</div>
									</td>
								</tr>

							</table>
							<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
						</div>
					</form>
				</div>

				<div class="contents">
					<h2><?=$titleDispText?>インポート</h2>
					<form id="csvInport" method="POST" enctype="multipart/form-data">

						<?=$csv_error_text?>

						<div class="table-wrap">
							<table class="adminTable">

								<tr>
									<th>テーブル選択</th>
									<td>
										<select id="csv_inport_table_name" name="table_name">

											<?php foreach ( (array)$tableArray AS $key => $value) { ?>
											<option value="<?=$value['Tables_in_glay']?>"><?=$value['Tables_in_glay']?></option>
											<?php } ?>

										</select>
									</td>
								</tr>

								<tr>
									<th>CSVアップロード</th>
									<td>
										<label for="csv_file_up" class="fileInpit"><span>＋CSVを選択</span><input type="file" id="csv_file_up" name="csv_file_up" accept="text/csv" value="" style="display:none;"></label>
									</td>
								</tr>

								<tr>
									<td colspan="2">
										<div class="csvBox">
											<input class="csvInButton" name="csvInport" type="submit" value="csvInport">
										</div>
									</td>
								</tr>

							</table>
							<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>