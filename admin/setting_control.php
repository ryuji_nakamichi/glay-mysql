<?php

require_once '../inc/db.php';
require_once './class/class.php';

$XSSObj = new XSSClass;

$resArray = array();

$table_name = "setting";

if($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);


	//処理分岐ここから
	switch ($_REQUEST['mode']) {


		//登録処理ここから
		case 'insert':


		//データ整形
		$login_id       = $_REQUEST['login_id'];
		$login_password = $_REQUEST['login_password'];

		if ( is_numeric($_REQUEST['item_tax']) ) {
			$item_tax = $_REQUEST['item_tax'];
		} else {
			$item_tax = $item_tax;
		}

		if ( is_numeric($_REQUEST['shipping_cost']) ) {
			$shipping_cost = $_REQUEST['shipping_cost'];
		}

		if ( is_numeric($_REQUEST['shipping_cost_free']) ) {
			$shipping_cost_free = $_REQUEST['shipping_cost_free'];
		}

		if ( is_numeric($_REQUEST['add_point']) ) {
			$add_point = $_REQUEST['add_point'];
		}

		if ( is_numeric($_REQUEST['add_point_increment']) ) {
			$add_point_increment = $_REQUEST['add_point_increment'];
		}

		if ( is_numeric($_REQUEST['img_width']) ) {
			$img_width = $_REQUEST['img_width'];
		}

		if ( is_numeric($_REQUEST['img_height']) ) {
			$img_height = $_REQUEST['img_height'];
		}

		if ( is_numeric($_REQUEST['img_thum_width_l']) ) {
			$img_thum_width_l = $_REQUEST['img_thum_width_l'];
		}

		if ( is_numeric($_REQUEST['img_thum_height_l']) ) {
			$img_thum_height_l = $_REQUEST['img_thum_height_l'];
		}

		if ( is_numeric($_REQUEST['img_thum_width_s']) ) {
			$img_thum_width_s = $_REQUEST['img_thum_width_s'];
		}

		if ( is_numeric($_REQUEST['img_thum_height_s']) ) {
			$img_thum_height_s = $_REQUEST['img_thum_height_s'];
		}


		//SQL文
		$sql = "INSERT INTO {$table_name} (
			login_id,
			login_password,
			item_tax,
			shipping_cost,
			shipping_cost_free,
			add_point,
			add_point_increment,
			img_width,
			img_height,
			img_thum_width_l,
			img_thum_height_l,
			img_thum_width_s,
			img_thum_height_s,
			new_record_time
			)
		VALUES(
			'{$login_id}',
			'{$login_password}',
			'{$item_tax}',
			{$shipping_cost},
			{$shipping_cost_free},
			{$add_point},
			{$add_point_increment},
			{$img_width},
			{$img_height},
			{$img_thum_width_l},
			{$img_thum_height_l},
			{$img_thum_width_s},
			{$img_thum_height_s},
			current_timestamp
			)";

				break;
			//登録処理ここまで
	}
	
	//処理分岐ここから
	switch ($_REQUEST['mode']) {


		//更新処理ここから
		case 'update':


		//データ整形
		$login_id       = $_REQUEST['login_id'];
		$login_password = $_REQUEST['login_password'];

		if ( is_numeric($_REQUEST['item_tax']) ) {
			$item_tax       = $_REQUEST['item_tax'];
		} else {
			$item_tax       = $item_tax;
		}

		if ( is_numeric($_REQUEST['shipping_cost']) ) {
			$shipping_cost = $_REQUEST['shipping_cost'];
		}

		if ( is_numeric($_REQUEST['shipping_cost_free']) ) {
			$shipping_cost_free = $_REQUEST['shipping_cost_free'];
		}

		if ( is_numeric($_REQUEST['add_point']) ) {
			$add_point = $_REQUEST['add_point'];
		}

		if ( is_numeric($_REQUEST['add_point_increment']) ) {
			$add_point_increment = $_REQUEST['add_point_increment'];
		}

		if ( is_numeric($_REQUEST['img_width']) ) {
			$img_width = $_REQUEST['img_width'];
		}

		if ( is_numeric($_REQUEST['img_height']) ) {
			$img_height = $_REQUEST['img_height'];
		}

		if ( is_numeric($_REQUEST['img_thum_width_l']) ) {
			$img_thum_width_l = $_REQUEST['img_thum_width_l'];
		}

		if ( is_numeric($_REQUEST['img_thum_height_l']) ) {
			$img_thum_height_l = $_REQUEST['img_thum_height_l'];
		}

		if ( is_numeric($_REQUEST['img_thum_width_s']) ) {
			$img_thum_width_s = $_REQUEST['img_thum_width_s'];
		}

		if ( is_numeric($_REQUEST['img_thum_height_s']) ) {
			$img_thum_height_s = $_REQUEST['img_thum_height_s'];
		}

		//SQL文
		$sql = "UPDATE {$table_name} SET
			login_id            = '{$login_id}',
			login_password      = '{$login_password}',
			item_tax            = '{$item_tax}',
			shipping_cost       = {$shipping_cost},
			shipping_cost_free  = {$shipping_cost_free},
			add_point           = {$add_point},
			add_point_increment = {$add_point_increment},
			img_width           = {$img_width},
			img_height          = {$img_height},
			img_thum_width_l    = {$img_thum_width_l},
			img_thum_height_l   = {$img_thum_height_l},
			img_thum_width_s    = {$img_thum_width_s},
			img_thum_height_s   = {$img_thum_height_s},
			update_record_time  = current_timestamp
		WHERE cd = {$cd}
			";

				break;
			//更新処理ここまで


			//削除処理ここから
			case 'delete':
			$sql = "DELETE FROM {$table_name} WHERE cd = {$cd}";

				break;
			//削除処理ここまで
	}
	//処理分岐ここまで
	mysqli_query($connect, $sql);

} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
