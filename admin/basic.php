<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'basic';
$table_seq_name = 'information_schema.tables';
$img_max        = 0;


//基本情報取得
$_SELECT     = "cd, user_name, company_name, tel1, tel2, fax1, fax2, mobile_phone1, mobile_phone2, mail, mobile_phone_mail, post_number1, post_number2, address1, address2, title, keyword, description, h1, president";
$sql         = "SELECT {$_SELECT} FROM {$table_name}";
$basic_query = mysqli_query($connect,$sql);
$basic_max   = mysqli_num_rows($basic_query);

for ($i = 0; $i < $basic_max; $i++) {
	$basicArray[$i] = mysqli_fetch_assoc($basic_query);
}

$titleDispText = '編集フォーム';


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


/*
echo '<pre>';
print_r($basicArray);
echo '</pre>';
*/


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop" class="colum1">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
					<form id="basic" method="POST" enctype="multipart/form-data">
						<div class="table-wrap">
							<table class="adminTable">
								<tr>
									<th colspan="2">基本情報</th>
								</tr>

								<tr>
									<th><label for="user_name">ユーザー名</label></th>
									<td>
										<input id="user_name" type="text" name="user_name" value="<?=$basicArray[0]['user_name']?>">
									</td>
								</tr>

								<tr>
									<th><label for="company_name">会社名</label></th>
									<td>
										<input type="text" id="company_name" name="company_name" value="<?=$basicArray[0]['company_name']?>">
									</td>
								</tr>

								<tr>
									<th><label for="tel1">電話番号1</label></th>
									<td>
										<input type="text" id="tel1" name="tel1" value="<?=$basicArray[0]['tel1']?>">
									</td>
								</tr>

								<tr>
									<th><label for="tel2">電話番号2</label></th>
									<td>
										<input type="text" id="tel2" name="tel2" value="<?=$basicArray[0]['tel2']?>">
									</td>
								</tr>

								<tr>
									<th><label for="fax1">FAX番号1</label></th>
									<td>
										<input type="text" id="fax1" name="fax1" value="<?=$basicArray[0]['fax1']?>">
									</td>
								</tr>

								<tr>
									<th><label for="fax2">FAX番号2</label></th>
									<td>
										<input type="text" id="fax2" name="fax2" value="<?=$basicArray[0]['fax2']?>">
									</td>
								</tr>

								<tr>
									<th><label for="mobile_phone1">携帯電話番号1</label></th>
									<td>
										<input type="text" id="mobile_phone1" name="mobile_phone1" value="<?=$basicArray[0]['mobile_phone1']?>">
									</td>
								</tr>

								<tr>
									<th><label for="mobile_phone2">携帯電話番号2</label></th>
									<td>
										<input type="text" id="mobile_phone2" name="mobile_phone2" value="<?=$basicArray[0]['mobile_phone2']?>">
									</td>
								</tr>

								<tr>
									<th><label for="mail">メールアドレス</label></th>
									<td>
										<input type="text" id="mail" name="mail" value="<?=$basicArray[0]['mail']?>">
									</td>
								</tr>

								<tr>
									<th><label for="mobile_phone_mail">携帯メールアドレス</label></th>
									<td>
										<input type="text" id="mobile_phone_mail" name="mobile_phone_mail" value="<?=$basicArray[0]['mobile_phone_mail']?>">
									</td>
								</tr>

								<tr>
									<th><label for="post_number1">郵便番号1</label></th>
									<td>
										<input type="text" id="post_number1" name="post_number1" value="<?=$basicArray[0]['post_number1']?>">
									</td>
								</tr>

								<tr>
									<th><label for="address1">住所1</label></th>
									<td>
										<textarea cols="40" rows="4" id="address1" name="address1"><?=$basicArray[0]['address1']?></textarea>
									</td>
								</tr>

								<tr>
									<th><label for="post_number2">郵便番号2</label></th>
									<td>
										<input type="text" id="post_number2" name="post_number2" value="<?=$basicArray[0]['post_number2']?>">
									</td>
								</tr>

								<tr>
									<th><label for="address2">住所2</label></th>
									<td>
										<textarea cols="40" rows="4" id="address2" name="address2"><?=$basicArray[0]['address2']?></textarea>
									</td>
								</tr>

								<tr>
									<th><label for="title">タイトル</label></th>
									<td>
										<input type="text" id="title" name="title" value="<?=$basicArray[0]['title']?>">
									</td>
								</tr>

								<tr>
									<th><label for="keyword">キーワード</label></th>
									<td>
										<textarea cols="40" rows="4" id="keyword" name="keyword"><?=$basicArray[0]['keyword']?></textarea>
									</td>
								</tr>

								<tr>
									<th><label for="description">ディスクリプション</label></th>
									<td>
										<textarea cols="40" rows="4" id="description" name="description"><?=$basicArray[0]['description']?></textarea>
									</td>
								</tr>

								<tr>
									<th><label for="h1">h1</label></th>
									<td>
										<textarea cols="40" rows="4" id="h1" name="h1"><?=$basicArray[0]['h1']?></textarea>
									</td>
								</tr>

								<tr>
									<th colspan="2">会社概要</th>
								</tr>

								<tr>
									<th><label for="president">代表者</label></th>
									<td>
										<input type="text" id="president" name="president" value="<?=$basicArray[0]['president']?>">
									</td>
								</tr>
								
								<tr>
									<td class="button_cell" colspan="2">
										
										<?php if ($basic_max) { ?>
										<input class="editButton" name="editButton" type="button" value="編集" update_value="basic.php?cd=<?=$basicArray[0]['cd']?>">

										<?php } else { ?>
										<input class="addButton" name="addButton" type="button" value="登録">
										<?php } ?>

									</td>
								</tr>
							</table>
							<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
							<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
							<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>