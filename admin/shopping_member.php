<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'shopping_member';
$table_seq_name = 'information_schema.tables';
$img_max        = 0;

if ( $_GET['cd'] && ctype_digit( strval($_GET['cd']) ) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//shop_member(編集用)取得
	$_SELECT     = "cd, name1, name2, kana1, kana2, company_name, birth_year, birth_month, birth_day, sex, job_type, post_code1, post_code2, address, tel, fax, mail, phone_mail_address, user_id, password, point, disp_flg";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";

	$shopping_member_query2  = mysqli_query($connect, $sql);
	$shopping_member_max2    = mysqli_num_rows($shopping_member_query2);

	for ($i = 0; $i < $shopping_member_max2; $i++) {
		$shopping_memberArray2[$i] = mysqli_fetch_assoc($shopping_member_query2);
	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}




//shop_member取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, name1, name2, kana1, kana2, company_name, birth_year, birth_month, birth_day, sex, job_type, post_code1, post_code2, address, tel, fax, mail, phone_mail_address, password, user_id, point, disp_flg, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql       = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$shopping_member_query = mysqli_query($connect,$sql);
$shopping_member_max   = mysqli_num_rows($shopping_member_query);

for ($i = 0; $i < $shopping_member_max; $i++) {
	$shopping_memberArray[$i] = mysqli_fetch_assoc($shopping_member_query);
}
//shop_member取得(ページャー有)ここまで


//job_type取得ここから
$sql = "SELECT cd, name FROM job_type WHERE disp_flg = 1 ORDER BY sort";
$job_type_query2 = mysqli_query($connect, $sql);
$job_type_max2   = mysqli_num_rows($job_type_query2);

for ($i = 0; $i < $job_type_max2; $i++) {
	$job_typeArray2[$i] = mysqli_fetch_assoc($job_type_query2);
}
//job_type取得ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($shopping_member_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="name1">姓名</label></th>
										<td>
											<p id="name1Error"></p>
											<p id="name2Error"></p>
											<input id="name1" class="column2_input" type="text" name="name1" value="<?=$shopping_memberArray2[0]['name1']?>">
											<input id="name2" class="column2_input" type="text" name="name2" value="<?=$shopping_memberArray2[0]['name2']?>">
										</td>
									</tr>

									<tr>
										<th><label for="kana1">かな</label></th>
										<td class="column2_cell">
											<p id="kana1Error"></p>
											<p id="kana2Error"></p>
											<input id="kana1" class="column2_input" type="text" name="kana1" value="<?=$shopping_memberArray2[0]['kana1']?>">
											<input id="kana2" class="column2_input" type="text" name="kana2" value="<?=$shopping_memberArray2[0]['kana2']?>">

										</td>
									</tr>

									<tr>
										<th><label for="company_name">会社名</label></th>
										<td>
											<p id="company_nameError"></p>
											<input id="company_name" type="text" name="company_name" value="<?=$shopping_memberArray2[0]['company_name']?>">
										</td>
									</tr>

									<tr>
										<th><label for="sex1">性別</label></th>
										<td class="checkboxCell">
											<p id="sexError"></p>
											<label><input id="sex1" type="radio" name="sex" value="1" <?php echo ($shopping_memberArray2[0]['sex'] == 1 || !$shopping_memberArray2[0]['sex']) ? 'checked': ''; ?>>男性<label>
											<label><input id="sex2" type="radio" name="sex" value="2" <?php echo ($shopping_memberArray2[0]['sex'] == 2 ) ? 'checked': ''; ?>>女性<label>
										</td>
									</tr>

									<tr>
										<th><label for="job_type">職業</label></th>
										<td>
											<p id="job_typeError"></p>
											<select id="job_type" name="job_type">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $job_type_max2; $i++) { ?>

													<?php if ($job_typeArray2[$i]['cd'] == $shopping_memberArray2[0]['job_type']) { ?>
													<option value="<?=$job_typeArray2[$i]['cd']?>" selected><?=$job_typeArray2[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$job_typeArray2[$i]['cd']?>"><?=$job_typeArray2[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

									<tr>
										<th><label for="birth_year">生年月日</label></th>
										<td>
											<p id="birth_yearError"></p>

											<?php
											$bith_yearArray  = range(1900, date("Y"));
											$bith_monthArray = range(1, 12);
											$bith_dayArray   = range(1, 31);
											$birth_year_max  = count($bith_year);
											?>

											<div class="column3_select">
												<select id="birth_year" name="birth_year">

													<option value="">選択してください。</option>

													<?php foreach ( (array) $bith_yearArray AS $key => $value ) { ?>

														<?php if ($value == $shopping_memberArray2[0]['birth_year']) { ?>
														<option value="<?=$value?>" selected><?=$value?></option>
														<?php } else { ?>
														<option value="<?=$value?>"><?=$value?></option>
														<?php } ?>

													<?php } ?>

												</select>
												<span>年</span>
											</div>

											<div class="column3_select">
												<select id="birth_month" name="birth_month">

													<option value="">選択してください。</option>

													<?php foreach ( (array) $bith_monthArray AS $key => $value ) {
														$value = sprintf("%02d" ,$value);
														?>

														<?php if ($value == $shopping_memberArray2[0]['birth_month']) { ?>
														<option value="<?=$value?>" selected><?=$value?></option>
														<?php } else { ?>
														<option value="<?=$value?>"><?=$value?></option>
														<?php } ?>

													<?php } ?>

												</select>
												<span>月</span>
											</div>

											<div class="column3_select">
												<select id="birth_day" name="birth_day">

													<option value="">選択してください。</option>

													<?php foreach ( (array) $bith_dayArray AS $key => $value ) {
														$value = sprintf("%02d" ,$value);
														?>

														<?php if ($value == $shopping_memberArray2[0]['birth_day']) { ?>
														<option value="<?=$value?>" selected><?=$value?></option>
														<?php } else { ?>
														<option value="<?=$value?>"><?=$value?></option>
														<?php } ?>

													<?php } ?>

												</select>
												<span>日</span>
											</div>
										</td>
									</tr>

									<tr>
										<th><label for="post_code1">郵便番号</label></th>
										<td class="checkboxCell">
											<p id="post_code1Error"></p>
											<p id="post_code2Error"></p>
											<input id="post_code1" type="text" name="post_code1" value="<?=$shopping_memberArray2[0]['post_code1']?>" maxlength="3">
											-
											<input id="post_code2" type="text" name="post_code2" value="<?=$shopping_memberArray2[0]['post_code2']?>" maxlength="4">
										</td>
									</tr>

									<tr>
										<th><label for="address">住所</label></th>
										<td>
											<p id="addressError"></p>
											<input id="address" type="text" name="address" value="<?=$shopping_memberArray2[0]['address']?>">
										</td>
									</tr>

									<tr>
										<th><label for="tel">電話番号</label></th>
										<td>
											<p id="tellError"></p>
											<input id="tel" type="text" name="tel" value="<?=$shopping_memberArray2[0]['tel']?>">
										</td>
									</tr>

									<tr>
										<th><label for="fax">FAX</label></th>
										<td>
											<p id="faxError"></p>
											<input id="fax" type="text" name="fax" value="<?=$shopping_memberArray2[0]['fax']?>">
										</td>
									</tr>

									<tr>
										<th><label for="mail">メールアドレス</label></th>
										<td>
											<p id="mailError"></p>
											<input id="mail" type="text" name="mail" value="<?=$shopping_memberArray2[0]['mail']?>">
										</td>
									</tr>

									<tr>
										<th><label for="mail">携帯メールアドレス</label></th>
										<td>
											<p id="phone_mail_addressError"></p>
											<input id="phone_mail_address" type="text" name="phone_mail_address" value="<?=$shopping_memberArray2[0]['phone_mail_address']?>">
										</td>
									</tr>

									<tr>
										<th><label for="id">ユーザーID</label></th>
										<td>
											<p id="user_idError"></p>
											<input id="user_id" type="text" name="user_id" value="<?=$shopping_memberArray2[0]['user_id']?>">
										</td>
									</tr>

									<tr>
										<th><label for="password">パスワード</label></th>
										<td>
											<p id="passwordError"></p>
											<input id="password" type="password" name="password" value="<?=$shopping_memberArray2[0]['password']?>">
										</td>
									</tr>

									<tr>
										<th><label for="point">ポイント</label></th>
										<td class="spinner">
											<p id="pointError"></p>
											<input class="point" type="text" id="point" name="point" value="<?=$shopping_memberArray2[0]['point']?>">
										</td>
									</tr>
					
									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($shopping_memberArray2[0]['disp_flg'] == 1) ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && ctype_digit( strval($_REQUEST['cd']) ) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="shopping_member.php?cd=<?=$shopping_memberArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">

				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($shopping_member_max) { ?>
						<form id="<?=$table_name?>_del" method="POST">
							<div class="table-wrap">
								<table class="adminTable updateTable">
									<thead>
										<tr>
											<th>レコードNo.</th>
											<th>名前</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">
										<?php for ($i = 0; $i < $shopping_member_max; $i++) { ?>
											<?php

											if ($shopping_memberArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											?>
										<tr <?=$bg_color?> >
											<td><?=$shopping_memberArray[$i]['cd']?></td>
											<td><?=$shopping_memberArray[$i]['name1']?><?=$shopping_memberArray[$i]['name2']?></td>
											<td>
												<a class="update_button" href="shopping_member.php?cd=<?=$shopping_memberArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="shopping_member.php?cd=<?=$shopping_memberArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$shopping_memberArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$shopping_memberArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="6">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>
						
						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
				</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>