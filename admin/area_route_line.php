<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'area_route_line';
$table_seq_name = 'information_schema.tables';
$img_max        = 0;

if ( $_GET['cd'] && ctype_digit($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//area_route_line(編集用)取得
	$_SELECT                 = "cd, name, area_cd, area_route_cd, disp_flg";
	$sql                     = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$area_route_line_query2  = mysqli_query($connect, $sql);
	$area_route_line_max2    = mysqli_num_rows($area_route_line_query2);

	for ($i = 0; $i < $area_route_line_max2; $i++) {
		$area_route_lineArray2[$i] = mysqli_fetch_assoc($area_route_line_query2);


		//area取得ここから
		if ($area_route_lineArray2[$i]['area_cd']) {
			$sql = "SELECT cd, name FROM area WHERE disp_flg = 1 AND cd = {$area_route_lineArray2[$i]['area_cd']}";
				$area_cd_query2 = mysqli_query($connect,$sql);
				$area_cd_max2   = mysqli_num_rows($area_cd_query2);

				 if ($area_cd_max2) {
				 	$area_cdArray[$i]                     = mysqli_fetch_assoc($area_cd_query2);
				 	$area_routeArray2[$i]['area_cd_name'] = $area_cdArray[$i];
				 } 

		}
		//area取得ここまで


		//area_route取得ここから
		if ($area_route_lineArray2[$i]['area_route_cd']) {
			$sql                      = "SELECT cd, name FROM area_route WHERE disp_flg = 1 AND cd = {$area_route_lineArray2[$i]['area_route_cd']}";
				$area_route_cd_query2 = mysqli_query($connect, $sql);
				$area_route_cd_max2   = mysqli_num_rows($area_route_cd_query2);

				 if ($area_route_cd_max2) {
				 	$area_route_cdArray[$i]                          = mysqli_fetch_assoc($area_route_cd_query2);
				 	$area_route_lineArray2[$i]['area_route_cd_name'] = $area_route_cdArray[$i];
				 } 

		}
		//area_route取得ここまで

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}




//area_route_line取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, name, area_cd, area_route_cd, disp_flg, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql              = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$area_route_line_query = mysqli_query($connect, $sql);
$area_route_line_max   = mysqli_num_rows($area_route_line_query);

for ($i = 0; $i < $area_route_line_max; $i++) {
	$area_route_lineArray[$i] = mysqli_fetch_assoc($area_route_line_query);


		//area取得ここから
		if ($area_route_lineArray[$i]['area_cd']) {
			$sql = "SELECT cd, name FROM area WHERE disp_flg = 1 AND cd = {$area_route_lineArray[$i]['area_cd']}";
				$area_cd_query = mysqli_query($connect, $sql);
				$area_cd_max   = mysqli_num_rows($area_cd_query);

				 if ($area_cd_max) {
				 	$area_cdArray[$i]                         = mysqli_fetch_assoc($area_cd_query);
				 	$area_route_lineArray[$i]['area_cd_name'] = $area_cdArray[$i]['name'];
				 } 

		}
		//area取得ここまで


		//area_route取得ここから
		if ($area_route_lineArray[$i]['area_route_cd']) {
			$sql = "SELECT cd, name FROM area_route WHERE disp_flg = 1 AND cd = {$area_route_lineArray[$i]['area_route_cd']}";
				$area_route_cd_query = mysqli_query($connect,$sql);
				$area_route_cd_max   = mysqli_num_rows($area_route_cd_query);

				 if ($area_route_cd_max) {
				 	$area_route_cdArray[$i]                         = mysqli_fetch_assoc($area_route_cd_query);
				 	$area_route_lineArray[$i]['area_route_cd_name'] = $area_route_cdArray[$i]['name'];
				 } 

		}
		//area_route取得ここまで

}
//area_route_line取得(ページャー有)ここまで


//area取得ここから
$sql        = "SELECT cd, name FROM area WHERE disp_flg = 1 ORDER BY sort";
$area_query = mysqli_query($connect,$sql);
$area_max   = mysqli_num_rows($area_query);

for ($i = 0; $i < $area_max; $i++) {
	$areaArray[$i] = mysqli_fetch_assoc($area_query);
}
//area取得ここまで


//area_route取得ここから
$sql              = "SELECT cd, name FROM area_route WHERE disp_flg = 1 ORDER BY sort";
$area_route_query = mysqli_query($connect,$sql);
$area_route_max   = mysqli_num_rows($area_route_query);

for ($i = 0; $i < $area_route_max; $i++) {
	$area_routeArray[$i] = mysqli_fetch_assoc($area_route_query);
}
//area_route取得ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($area_route_line_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


// echo '<pre>';
// print_r($area_route_lineArray2);
// echo '</pre>';


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="area">AREA選択</label></th>
										<td>
											<p id="areaError"></p>
											<select id="area" name="area">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $area_max; $i++) { ?>

													<?php if ($areaArray[$i]['cd'] == $area_route_lineArray2[0]['area_cd']) { ?>
													<option value="<?=$areaArray[$i]['cd']?>" selected><?=$areaArray[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$areaArray[$i]['cd']?>"><?=$areaArray[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>
									<tr>
										<th><label for="area_route">AREA_ROUTE選択</label></th>
										<td>
											<p id="area_routeError"></p>
											<select id="area_route" name="area_route">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $area_route_max; $i++) { ?>

													<?php if ($area_routeArray[$i]['cd'] == $area_route_lineArray2[0]['area_route_cd']) { ?>
													<option value="<?=$area_routeArray[$i]['cd']?>" selected><?=$area_routeArray[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$area_routeArray[$i]['cd']?>"><?=$area_routeArray[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>
									<tr>
										<th><label for="name">路線詳細名</label></th>
										<td>
											<p id="nameError"></p>
											<input id="name" type="text" name="name" value="<?=$area_route_lineArray2[0]['name']?>">
										</td>
									</tr>
					
									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($area_route_lineArray2[0]['disp_flg'] == 1) ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="area_route_line.php?cd=<?=$area_route_lineArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">

				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($area_route_line_max) { ?>
						<form id="<?=$table_name?>_del" method="POST">
							<div class="table-wrap">
								<table class="adminTable updateTable">
									<thead>
										<tr>
											<th>エリア名</th>
											<th>路線名</th>
											<th>路線詳細名</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">
										<?php for ($i = 0; $i < $area_route_line_max; $i++) { ?>
											<?php

											if ($area_route_lineArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											?>
										<tr <?=$bg_color?> >
											<td><?=$area_route_lineArray[$i]['area_cd_name']?></td>
											<td><?=$area_route_lineArray[$i]['area_route_cd_name']?></td>
											<td><?=$area_route_lineArray[$i]['name']?></td>
											<td>
												<a class="update_button" href="area_route_line.php?cd=<?=$area_route_lineArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="area_route_line.php?cd=<?=$area_route_lineArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$area_route_lineArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$area_route_lineArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="6">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>
						
						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
				</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>