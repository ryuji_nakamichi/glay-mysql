<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './class/class.php';
require_once './inc/function.php';


//ログイン処理
$loginObj                 = new loginClass;
$loginArray['error_text'] = $loginObj->login();

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop" class="colum1">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2>ID・PASSWORD入力</h2>
					<?=$loginArray['error_text']?>
					<form id="login" method="POST">
						<div class="table-wrap">
							<table class="adminTable">
								<tr>
									<th><label for="login_id">ID</label></th>
									<td>
										<input id="login_id" type="text" name="login_id">
									</td>
								</tr>

								<tr>
									<th><label for="login_password">PASSWORD</label></th>
									<td>
										<input id="login_password" type="password" name="login_password">
									</td>
								</tr>

								<tr>
									<td class="button_cell" colspan="2">
										<input class="loginButton" name="loginButton" type="button" value="ログイン">
									</td>
								</tr>

							</table>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>