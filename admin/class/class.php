<?php

/******************************************************************************************************************************
XSSClassここから
******************************************************************************************************************************/
class XSSClass {


	/**
	 * クロスサイトスクリプティング対策関数
	 * @connect: db接続情報
	 * @dataArray: データ配列
	 * @return $dataArray
	 */

	public function XSSEscape ($dataArray, $connect) {

		foreach ( (array) $dataArray AS $key => $val) {

			if ( !is_array($val) ){
				$dataArray[$key] = htmlspecialchars($val);
				$dataArray[$key] = mysqli_real_escape_string($connect, $val);
			}

		}

		return $dataArray;

	}


	/**
	 * cd番号チェック関数
	 * @_cd: 対象cd番号
	 * @return $cd
	 */

	public function cdCheck ($_cd) {
		
		if ( ctype_digit(strval($_cd)) ) {
			$cd = $_cd;
		}

		return $cd;

	}

}
/******************************************************************************************************************************
XSSClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
loginClassここから
******************************************************************************************************************************/
class loginClass {

	public $error_text;
	public $site_url;
	public $session_user_name;

	function login () {
		
		$this->site_url = SITEURL;
		$this->session_user_name = SESSIONUSERNAME;

		if ($_REQUEST['mode'] && $_REQUEST['mode'] == 'logout') {
			$_SESSION['login']["login_flg_{$this->session_user_name}"] = array();

			//クッキーのセッションIDも削除
			setcookie(SESSIONUSERNAME.'PHPSESSID', '', time()-42000, '/');

		}

		if ($_SESSION['login']["login_flg_{$this->session_user_name}"] === 1) {
			header("Location: ".$this->site_url);
			exit;
		}

		$this->error_text = '';

		if ($_SESSION['login']["login_flg_{$this->session_user_name}"] === 0) {
			
			$_SESSION['login']["login_flg_{$this->session_user_name}"] = array();
			$this->error_text = '<span style="color: #ff0000;">ID・PASSWORD、またはどちらかがが正しくありません。</span>';

		} else if ($_SESSION['login']["login_flg_{$this->session_user_name}"] === 2) {

			$_SESSION['login']["login_flg_{$this->session_user_name}"] = array();
			$this->error_text = '<span style="color: #ff0000;">ログアウトしました。</span>';

		}

		return $this->error_text;

	}

}
/******************************************************************************************************************************
loginClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
masterMessageClassここから
******************************************************************************************************************************/
class masterMessageClass {

	public function masterMessage ($connect = '') {


		//masterデータベースのmessageテーブルからレコード取得ここから
		$_DATABASE     = "master";
		$_TABLE        = "{$_DATABASE}.message";
		$_SELECT       = "cd, title, comment";
		$DBNAME        = DBNAME;
		$_WHERE        = "disp_flg = 1 AND database_disp_flg = '{$DBNAME}'" ;
		$_ORDER        = "sort";
		$sql           = "SELECT {$_SELECT} FROM {$_TABLE} WHERE {$_WHERE} ORDER BY {$_ORDER}";
		$message_query = mysqli_query($connect, $sql);
		$message_max   = mysqli_num_rows($message_query);

		for ($i = 0; $i < $message_max; $i++) {
			$messageArray[] = mysqli_fetch_assoc($message_query);
		}
		//masterデータベースのmessageテーブルからレコード取得ここまで

		return $messageArray;

	}

}
/******************************************************************************************************************************
masterMessageClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
recordControlClassここから
******************************************************************************************************************************/
class recordControlClass {


	/**
	 * レコード削除関数
	 * @connect: db接続情報
	 * @table: 対象テーブル名
	 * @cd: 対象cd番号
	 * @return bool
	 */
	public function recordDelete($connect, $table_name, $cd) {
		$sql          = "DELETE FROM {$table_name} WHERE cd = {$cd}";
		$delete_query = mysqli_query($connect, $sql);

		if ($delete_query) {
			return true;
		} else {
			return false;
		}

	}


	/**
	 * レコード挿入関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @value: 挿入データ配列
	 * @return bool
	 */
	public function recordInsert ($connect, $table_name, $value) {

		$keyArray = array();
		$valArray = array();

		foreach ($value AS $key => $val) {

			$key = $key;

			if ($key == "new_record_time" || $key == "update_record_time") { 
				//$val = ($val === NULL) ? 'NULL' : "(SELECT to_char(NOW(), 'yyyy-mm-dd hh24:mm:ss')::timestamp)";
				$val = ($val === NULL) ? 'NULL' : "'" . date("Y-m-d H:i:s") . "'";
			} else if ($key == "sort") {
				mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : htmlspecialchars($val, ENT_QUOTES);
			} else {
				mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : "'". htmlspecialchars($val, ENT_QUOTES) ."'";
			}

			$keyArray[] = $key;
			$valArray[] = $val;
		}

		$key_str = implode(',', $keyArray);
		$val_str = implode(',', $valArray);

		$sql        = "INSERT INTO {$table_name} ({$key_str}) VALUES ({$val_str})";
		//echo $sql;
		$data_query = mysqli_query($connect, $sql);

		if ($delete_query) {
			return true;
		} else {
			return false;
		}

	}


	/**
	 * レコード更新関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @value: 更新データ配列
	 * @cd: 対象cd番号
	 * @return bool
	 */

	public function recordUpdate ($connect, $table_name, $value, $cd) {

		$valArray = array();

		foreach ($value AS $key => $val) {

			$key = $key;

			if ($key == "new_record_time" || $key == "update_record_time") { 
				$val = ($val === NULL) ? 'NULL' : "'" . date("Y-m-d H:i:s") . "'";
			} else if ($key == "sort") {
				mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : htmlspecialchars($val, ENT_QUOTES);
				$val = $val;
			} else {
				mysqli_real_escape_string($connect, $val);
				$val = ($val === NULL) ? 'NULL' : "'". htmlspecialchars($val, ENT_QUOTES) ."'";
				$val = $val;
			}

			$valArray[] = $key.' = '.$val;
		}

		$val_str = implode(',', $valArray);

		$sql        = "UPDATE {$table_name} SET {$val_str} WHERE cd = {$cd}";
		echo $sql;
		$data_query = mysqli_query($connect, $sql);
		
		if ($data_query) {
			return true;
		} else {
			return false;
		}

	}


	/**
	 * レコード並び替え関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @kensu_: レコード表示件数(db.phpで定義)
	 * @return bool
	 */

	public function recordSort ($connect, $table_name, $kensu_, $_mode = '') {

		$recordSortResultArray = array();


		//データ整形
		$sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
		$sort_query = mysqli_query($connect, $sort_sql);
		$sort_max   = mysqli_num_rows($sort_query);

		if ($_mode == '') {

			if ($sort_max > $kensu_) {
				$sort_max = $kensu_;
			}

		} else if ($_mode == 'delete') {
			$sort_max = $sort_max;
		}

		for ($i = 0; $i < $sort_max; $i++) {
			$sortArray = mysqli_fetch_assoc($sort_query);

			if ($_mode == '') {
				$countSortArray = $_POST['sort'][$i];
			} else if ($_mode == 'delete') {
				$countSortArray = $i + 1;
			} else {
				$countSortArray = $_POST['sort'][$i];
			}
			
			$sort_up_sql = "UPDATE {$table_name} SET ";
			$sort_up_sql .= "sort = ".$countSortArray." ";

			if ($_mode == '') {
				$sort_up_sql .= "WHERE cd = ".$_POST['cd_hidden'][$i]."";
			} else if ($_mode == 'delete') {
				$sort_up_sql .= "WHERE cd = ".$sortArray['cd']."";
			} else {
				$sort_up_sql .= "WHERE cd = ".$_POST['cd_hidden'][$i]."";
			}

			$sort_up_query = mysqli_query($connect, $sort_up_sql);

			if ($sort_up_query) {
				$recordSortResultArray[] = true;
			} else {
				$recordSortResultArray[] = false;
			}

		}

		return $recordSortResultArray;

	}


	/**
	 * レコード並び替え(insert時)関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル名
	 * @return bool
	 */

	public function recordInsertSort ($connect, $table_name) {

		$recordInsertSortResultArray = array();

		if ($_REQUEST['mode'] == 'insert') {

			$sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
			$sort_query = mysqli_query($connect, $sort_sql);
			$sort_max   = mysqli_num_rows($sort_query);

			for ($i = 0; $i < $sort_max; $i++) {
				$sortArray = mysqli_fetch_assoc($sort_query);

				$countSortArray = $i + 1;

				$sort_up_sql   = "UPDATE {$table_name} SET ";
				$sort_up_sql  .= "sort = ".$countSortArray." ";
				$sort_up_sql  .= "WHERE cd = ".$sortArray['cd']."";
				$sort_up_query = mysqli_query($connect, $sort_up_sql);

				if ($sort_up_query) {
					$recordInsertSortResultArray[] = true;
				} else {
					$recordInsertSortResultArray[] = false;
				}

			}

		}

		return $recordInsertSortResultArray;

	}

}
/******************************************************************************************************************************
recordControlClassここまで
******************************************************************************************************************************/


/******************************************************************************************************************************
csvControllここから
******************************************************************************************************************************/
class csvControll {


	/**
	 * CSVアップロード関数
	 * @return bool
	 */

	public function csvUpload($table_name) {

		if ($_FILES["csv_file_up"]['tmp_name']) {


			//ファイル名
			$name       = "{$table_name}.csv";
			$directry   = './csv/';
			$file       = $directry.$name;
			$upload_dir = $file;
		
		
			//ディレクトリを作成してその中にアップロードしている。
			if( !file_exists($directry) ){
				mkdir($directry,0777);
			}
			
			if ( move_uploaded_file( $_FILES["csv_file_up"]['tmp_name'], $upload_dir) ) {
				chmod($upload_dir, 0777);
			}

		}


		if( file_exists($upload_dir) ){
			return true;
		} else {
			return true;
		}

	}


	/**
	 * CSVインポート関数
	 * @connect: db接続情報
	 * @table_name: 対象テーブル
	 * @return bool
	 */

	public function csvInport($connect, $table_name) {

		$csv_error_text = '';


		// ファイル取得
	    $filepath = "./csv/{$table_name}.csv";
	    $file     = new SplFileObject($filepath); 
	    $file->setFlags(SplFileObject::READ_CSV);
	 

	    //全行のINSERTデータ格納用
	    $ins_values = "";
	 
		
	    //ファイル内のデータループ
	    foreach ($file AS $key => $line) {


	        //1行毎のINSERTデータ格納用
	        $values = "";
	 
	        foreach ($line AS $line_key => $str) {
	           
	 
	            //INSERT用のデータ作成
	            $values[] = "'".mb_convert_encoding( $str, "utf-8", "sjis" )."'";
	        }

	        $ins_values .= implode(', ', $values);
	 
	    }


	    //カラム名取得
	    $csv_column_sql   = "SHOW columns FROM {$table_name}";
	    $csv_column_query = mysqli_query($connect, $csv_column_sql);
	    $csv_column_max   = mysqli_num_rows($csv_column_query);

	    for ($i = 0; $i < $csv_column_max; $i++) {
	    	$csvColumnArray[] = mysqli_fetch_assoc($csv_column_query);

	    	$csvColumnStrArray[] = $csvColumnArray[$i]['Field'];
	    }
	    
	    $csv_column_str = implode(',', $csvColumnStrArray);
	 
	    $sql              = "INSERT INTO {$table_name} ({$csv_column_str}) VALUES ({$ins_values})";
	    $csv_inport_query = mysqli_query($connect, $sql);


	    //CSVファイル削除
		if ( file_exists($filepath) ) {
			unlink($filepath);
		}

	    if ($csv_inport_query) {
	    	return true;
	    } else {
	    	return false;
	    }

	}


	/**
	 * CSVエクスポート関数
	 * @table_name: 対象テーブル
	 * @csvDataArray: 対象レコード配列
	 * @return bool
	 */

	public function csvExport($table_name, $csvDataArray) {

		$csvData_max    = count($csvDataArray);
		$file_name_time = time();

		try {

			//CSV形式で情報をファイルに出力のための準備
			$csvFileName = './csv/' . time() . rand() . '.csv';
			$res = fopen($csvFileName, 'w');


			//catchに投げる(throw)する処理を記述、またtryの中で何かエラーが発生したら、どの時点でcatchに移る
			if ($res === FALSE) {
				throw new Exception('ファイルの書き込みに失敗しました。');
			}

			for ($i = 0; $i < $csvData_max; $i++) {
				$dataList[] = $csvDataArray[$i];
			}
			

			// ループしながら出力
			foreach($dataList AS $key => $value) {

				// 文字コード変換。エクセルで開けるようにする
				mb_convert_variables('SJIS', 'UTF-8', $value);

				// ファイルに書き出しをする
				fputcsv($res, $value);
			}


			// ハンドル閉じる
			fclose($res);

			// ダウンロード開始
			header('Content-Type: application/octet-stream');

			// ここで渡されるファイルがダウンロード時のファイル名になる
			header("Content-Disposition: attachment; filename={$table_name}_{$file_name_time}_Csv.csv"); 
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($csvFileName));
			readfile($csvFileName);

			return true;

			//CSVファイル出力し終わったら、処理を止めないとHTMLも出力される
			exit;

		} catch(Exception $e) {


			//例外処理をここに書きます
			echo $e->getMessage();

			return false;

		}
	}

}
/******************************************************************************************************************************
csvControllここまで
******************************************************************************************************************************/