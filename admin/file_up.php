<?php

require_once '../inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();


//クロスサイトスクリプティング対策
$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


//cd番号チェック
$cd = $XSSObj->cdCheck($_REQUEST['cd']);

$table_name = $_REQUEST['table_name'];
$img_max    = $_REQUEST['img_max'] + 1;
$mode       = $_REQUEST['mode'];


//画質(1~100 目安60~70)
$compression_ratio = 90;


//サムネイル画像生成関数
function imageThumbnail ($_imagePath, $_width, $_height, $_imgQuality, $_writeImagePath) {


	$writeImageFlg = false;


	 //加工する画像を指定する
	$imagePath = $_imagePath;


	//imagick objectを作る
	$imgObj = new Imagick($imagePath);


	//リサイズ時のサイズ（最大）
	$resize_width  = $_width;
	$resize_height = $_height;


	// オリジナルのサイズ取得
	$width_org  = $imgObj->getImageWidth();
	$height_org = $imgObj->getImageHeight();


	// 縮小比率を計算
	$ratio = $width_org / $height_org;

	if ($resize_width / $resize_height > $ratio) {
	    $resize_width = $resize_height * $ratio;
	} else {
	    $resize_height = $resize_width / $ratio;
	}


	//画像サイズがサムネイルサイズより小さい場合は実行しない
	if ($width_org > $resize_width || $height_org > $resize_height) {


		//リサイズ処理
		$imgObj->resizeImage($resize_width,$resize_height,Imagick::FILTER_LANCZOS,1);


		//リサイズした画像を保存する
		$resizeWriteImageFlg = $imgObj->writeImage($_writeImagePath);


		//中心を切り取り$widthx$heightのサムネイルを作成する
		$imgObj->cropThumbnailImage($resize_width, $resize_height);


		//サムネイル処理した画像の画質設定
		$imgQuality = $imgObj->setImageCompressionQuality($_imgQuality);


		//サムネイル処理した画像を保存する
		$writeImageFlg = $imgObj->writeImage($_writeImagePath);

	}


	//imagick objectを破棄する
	$imgObj->clear();

	if ($writeImageFlg === true) {
		return true;
	} else {
		return false;
	}

}



/*
Orientation = 1     回転無し
Orientation = 2     左右反転
Orientation = 3     180°回転
Orientation = 4     上下反転
Orientation = 5     時計回りに90°回転した後、左右反転
Orientation = 6     時計回りに90°回転
Orientation = 7     反時計回りに90°回転した後、左右反転
Orientation = 8     反時計回りに90°回転
*/


//画像の反転関数ここから
function image_FLIP($source, $orientation) {

	switch ($orientation) {
		case 1:

			break;

		case 2:
			imageflip($source, IMG_FLIP_VERTICAL);
			break;

		case 3:

			break;

		case 4:
			imageflip($source, IMG_FLIP_HORIZONTAL);
			break;

		case 5:
			imageflip($source, IMG_FLIP_HORIZONTAL);
			break;

		case 6:

			break;

		case 7:
			imageflip($source, IMG_FLIP_HORIZONTAL);
			break;

		case 8:

			break;
		
		default:

			break;
	}

}
//画像の反転関数ここまで


//画像登録用処理
function imgUpload ($connect, $img_max, $table_name) {

	$uploadResult = array();


	//画像幅設定取得
	$sql = "SELECT img_width, img_height, img_thum_width_l, img_thum_height_l, img_thum_width_s, img_thum_height_s FROM setting";
	$img_setting_query = mysqli_query($connect, $sql);
	$img_setting_max   = mysqli_num_rows($img_setting_query);

	if ($img_setting_max) {
		$img_settingArray  = mysqli_fetch_assoc($img_setting_query);
		$img_settingArray['img_width']       = intval($img_settingArray['img_width']);
		$img_settingArray['img_height']      = intval($img_settingArray['img_height']);
		$img_settingArray['img_thum_width_l']  = intval($img_settingArray['img_thum_width_l']);
		$img_settingArray['img_thum_height_l'] = intval($img_settingArray['img_thum_height_l']);
		$img_settingArray['img_thum_width_s']  = intval($img_settingArray['img_thum_width_s']);
		$img_settingArray['img_thum_height_s'] = intval($img_settingArray['img_thum_height_s']);
	}

	for ($k = 1; $k < $img_max; $k++) {
		$resArray['status_upload'] = '画像登録モード';

		$resArray['img_error_value'] = $_FILES["file_up{$k}"]["error"].' '.'error';

		if ($_FILES["file_up{$k}"]["error"] == 0) {

			if ($_REQUEST['upload_path_name']) {
				$upload_path_name = $_REQUEST['upload_path_name'];
			}

			if ($_REQUEST['old_path_name']) {
				$old_path_name = "./upload/{$_REQUEST['old_path_name']}";
			}


			//古い画像のサムネイルパス取得
			if ($_REQUEST['old_path_name']) {

				$old_path_nameArray = explode('.', $_REQUEST['old_path_name']);


				//古いリサイズ画像パス取得
				$old_resize_path_name = $old_path_nameArray[0]."_r.".$old_path_nameArray[1];
				$old_resize_path_name_dir = "./upload/{$old_resize_path_name}";


				//small
				$old_thum_path_name_s = $old_path_nameArray[0]."_thum_s.".$old_path_nameArray[1];
				$old_thum_path_name_dir_s = "./upload/{$old_thum_path_name_s}";


				//large
				$old_thum_path_name_l = $old_path_nameArray[0]."_thum_l.".$old_path_nameArray[1];
				$old_thum_path_name_dir_l = "./upload/{$old_thum_path_name_l}";
			}
			

			list($file_name, $file_type) = explode(".", $_FILES["file_up{$k}"]['name']);
			

			//元画像用変数定義ここから
			$name            = $upload_path_name.".".$file_type;
			$thumName        = $name;
			$directry        = './upload/';
			$file            = $directry.$name;
			$upload_dir      = $file;
			$_imagePath      = $upload_dir;
			$_width          = $img_settingArray['img_width'];
			$_height         = $img_settingArray['img_height'];
			$_imgQuality     = 70;
			$_writeImagePath = $upload_dir;
			//元画像用変数定義ここまで


			//リサイズ画像用変数定義ここから
			$resizeName            = $name;
			$resizeDirectry        = './upload/';
			$resizeFile            = $resizeDirectry.$resizeName;
			$resizeUpload_dir      = $resizeFile;
			$_resizeImagePath      = $resizeUpload_dir;
			$_resizeWidth          = $img_settingArray['img_width'];
			$_resizeHeight         = $img_settingArray['img_height'];
			$_resizeImgQuality     = 70;
			$_resizeWriteImagePath = $resizeDirectry.$upload_path_name."_r.".$file_type;
			//リサイズ画像用変数定義ここまで


			//imageThumbnail(small)用変数定義ここから
			$thumName_s        = $name;
			$thumDirectry_s    = './upload/';
			$thumFile_s        = $thumDirectry_s.$thumName_s;
			$thumUpload_dir_s  = $thumFile_s;
			$_imagePath_s      = $thumUpload_dir_s;
			$_width_s          = $img_settingArray['img_thum_width_s'];
			$_height_s         = $img_settingArray['img_thum_height_s'];
			$_imgQuality_s     = 70;
			$_writeImagePath_s = $thumDirectry_s.$upload_path_name."_thum_s.".$file_type;
			//imageThumbnail(small)用変数定義ここまで


			//imageThumbnail(large)用変数定義ここから
			$thumName_l        = $name;
			$thumDirectry_l    = './upload/';
			$thumFile_l        = $thumDirectry_l.$thumName_l;
			$thumUpload_dir_l  = $thumFile_l;
			$_imagePath_l      = $thumUpload_dir_l;
			$_width_l          = $img_settingArray['img_thum_width_l'];
			$_height_l         = $img_settingArray['img_thum_height_l'];
			$_imgQuality_l     = 70;
			$_writeImagePath_l = $thumDirectry_l.$upload_path_name."_thum_l.".$file_type;
			//imageThumbnail(large)用変数定義ここまで
			

			//ディレクトリを作成してその中にアップロードしている。
			if( !file_exists($directry) ){
				mkdir($directry,0777);
			}
			
			if ( move_uploaded_file( $_FILES["file_up{$k}"]['tmp_name'], $upload_dir) ) {
				chmod($upload_dir, 0777);

				$uploadResult[] = true;


				//ファイルと回転角
				$exifData = @exif_read_data($upload_dir, 0, true);

				if ($exifData['IFD0']['Orientation']) {

					switch ($exifData['IFD0']['Orientation']) {
						case 1:
							$degrees = 0;
							break;

						case 2:
							$degrees = 0;
							break;

						case 3:
							$degrees = 180;
							break;

						case 4:
							$degrees = 0;
							break;

						case 5:
							$degrees = 270;
							break;

						case 6:
							$degrees = 270;
							break;

						case 7:
							$degrees = 90;
							break;

						case 8:
							$degrees = 90;
							break;
						
						default:
							$degrees = 0;
							break;
					}


					//元画像の拡張子で処理分岐ここから
					switch ($file_type) {
						case 'jpg':
						case 'jpeg':
						case 'JPG':
						case 'JPEG':

							header('Content-type: image/jpeg');

							//読み込み
							$source = imagecreatefromjpeg($upload_dir);

							image_FLIP($source, $exifData['IFD0']['Orientation']);

							//回転
							$rotate = imagerotate($source, $degrees, 0);

							//出力
							imagejpeg($rotate, $upload_dir, $compression_ratio);

							break;

						case 'png':
						case 'PNG':
						case 'x-png':
						case 'X-PNG':

							header('Content-type: image/png');

							//読み込み
							$source = imagecreatefrompng($upload_dir);

							image_FLIP($source, $exifData['IFD0']['Orientation']);


							//回転
							$rotate = imagerotate($source, $degrees, 0);


							//出力
							imagepng($rotate, $upload_dir, $compression_ratio);
							break;

						case 'gif':
						case 'GIF':
							header('Content-type: image/gif');

							//読み込み
							$source = imagecreatefromgif($upload_dir);

							image_FLIP($source, $exifData['IFD0']['Orientation']);


							//回転
							$rotate = imagerotate($source, $degrees, 0);


							//出力
							imagegif($rotate, $upload_dir, $compression_ratio);
							break;
						
						default:

						
							header('Content-type: image/jpeg');

							//読み込み
							$source = imagecreatefromjpeg($upload_dir);

							image_FLIP($source, $exifData['IFD0']['Orientation']);


							//回転
							$rotate = imagerotate($source, $degrees, 0);


							//画像出力
							imagejpeg($rotate, $upload_dir, $compression_ratio);
						
							break;
					}
					//元画像の拡張子で処理分岐ここまで


					//メモリの解放
					imagedestroy($source);
					imagedestroy($rotate);

				}


				//アップロードが完了したら古い元画像を削除
				if ( file_exists($old_path_name) ) {
					$uploadResult[] = unlink($old_path_name);
				}


				/***************************************************
				リサイズ(元)ここから
				***************************************************/
				//リサイズ画像生成
				imageThumbnail($_resizeImagePath, $_resizeWidth, $_resizeHeight, $_resizeImgQuality, $_resizeWriteImagePath);


				//リサイズ画像パーミッション変更
				if ( file_exists($_resizeWriteImagePath) ) {
					chmod($_resizeWriteImagePath, 0777);
				}


				//アップロードが完了したら古いリサイズ画像を削除
				if ( file_exists($old_resize_path_name_dir) ) {
					unlink($old_resize_path_name_dir);
				}
				/***************************************************
				リサイズ(元)ここまで
				***************************************************/


				/***************************************************
				サムネイル(元)ここから
				***************************************************/

				//サムネイル(元画像)画像生成
				imageThumbnail($_imagePath, $_width, $_height, $_imgQuality, $_writeImagePath);


				//サムネイル(元画像)画像パーミッション変更
				if ( file_exists($_writeImagePath) ) {
					chmod($_writeImagePath, 0777);
				}


				//アップロードが完了したら古いサムネイル(元画像)画像を削除
				if ( file_exists($old_thum_path_name_dir) ) {
					unlink($old_thum_path_name_dir);
				}
				/***************************************************
				サムネイル(元)ここまで
				***************************************************/


				/***************************************************
				サムネイル(small)ここから
				***************************************************/

				//サムネイル(small)画像生成
				imageThumbnail($_imagePath_s, $_width_s, $_height_s, $_imgQuality_s, $_writeImagePath_s);


				//サムネイル(small)画像パーミッション変更
				if ( file_exists($_writeImagePath_s) ) {
					chmod($_writeImagePath_s, 0777);
				}


				//アップロードが完了したら古いサムネイル(small)画像を削除
				if ( file_exists($old_thum_path_name_dir_s) ) {
					unlink($old_thum_path_name_dir_s);
				}
				/***************************************************
				サムネイル(small)ここまで
				***************************************************/


				/***************************************************
				サムネイル(large)ここから
				***************************************************/

				//サムネイル(large)画像生成
				imageThumbnail($_imagePath_l, $_width_l, $_height_l, $_imgQuality_l, $_writeImagePath_l);


				//サムネイル(large)画像パーミッション変更
				if ( file_exists($_writeImagePath_l) ) {
					chmod($_writeImagePath_l, 0777);
				}


				//アップロードが完了したら古いサムネイル(large)画像を削除
				if ( file_exists($old_thum_path_name_dir_l) ) {
					unlink($old_thum_path_name_dir_l);
				}
				/***************************************************
				サムネイル(large)ここまで
				***************************************************/

			} else {
				$uploadResult[] = false;
			}
			
		}

	}

	return $uploadResult;

}


//画像削除用処理
function imgDelete ($connect, $img_max, $table_name, $cd) {

	$deleteResult = array();

	for ($k = 1; $k < $img_max; $k++) {

		if ($_FILES["file_up{$k}"]["error"] == 0) {
			$resArray['status_delete'] = '削除';

			$img_num = "img{$k}";
				
			$_SELECT          = "cd, {$img_num}";
			$sql              = "SELECT {$_SELECT} FROM {$table_name} WHERE cd = {$cd}";
			$img_delete_query = mysqli_query($connect, $sql);
			$img_delete_max   = mysqli_num_rows($img_delete_query);
			
			if ($img_delete_max) {

				$imgDeleteArray = mysqli_fetch_assoc($img_delete_query);

				if ($imgDeleteArray[$img_num]) {
					$imgDeletePath = "./upload/{$imgDeleteArray[$img_num]}";


					//削除画像取得（基本）
					$old_path_nameArray = explode('.', $imgDeleteArray[$img_num]);


					//削除リサイズ画像パス取得
					$old_resize_path_name = $old_path_nameArray[0]."_r.".$old_path_nameArray[1];
					$old_resize_path_name_dir = "./upload/{$old_resize_path_name}";


					//削除small画像パス取得
					$old_thum_path_name_s = $old_path_nameArray[0]."_thum_s.".$old_path_nameArray[1];
					$old_thum_path_name_dir_s = "./upload/{$old_thum_path_name_s}";


					//削除large画像パス取得
					$old_thum_path_name_l = $old_path_nameArray[0]."_thum_l.".$old_path_nameArray[1];
					$old_thum_path_name_dir_l = "./upload/{$old_thum_path_name_l}";


					//元画像が存在していれば削除処理
					if ( file_exists($imgDeletePath) ) {
						$deleteResult[] = unlink($imgDeletePath);
					}


					//リサイズ画像が存在していれば削除処理
					if ( file_exists($old_resize_path_name_dir) ) {
						$deleteResult[] = unlink($old_resize_path_name_dir);
					}


					//small画像が存在していれば削除処理
					if ( file_exists($old_thum_path_name_dir_s) ) {
						$deleteResult[] = unlink($old_thum_path_name_dir_s);
					}


					//large画像が存在していれば削除処理
					if ( file_exists($old_thum_path_name_dir_l) ) {
						$deleteResult[] = unlink($old_thum_path_name_dir_l);
					}

				}
				
			}
		}
	}//endfor

	return $deleteResult;

}


//insertここから
if ($mode === 'insert') {
	imgUpload($connect, $img_max, $table_name);
}


//updateここから
if ($mode === 'update') {
	imgUpload($connect, $img_max, $table_name);
}
//updateここまで


//deleteここから
if ($mode === 'delete') {
	imgDelete($connect, $img_max, $table_name, $cd, 'delete');
}
//deleteここまで

echo json_encode($resArray);