<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'discography';
$table_seq_name = 'information_schema.tables';
$img_max        = 1;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//discography(編集用)取得
	$_SELECT     = "cd, date, title1, title2, content, disp_flg, img1, sort";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} ORDER BY sort DESC LIMIT 1";
	$discography_query2 = mysqli_query($connect,$sql);
	$discography_max2   = mysqli_num_rows($discography_query2);

	for ($i = 0; $i < $discography_max2; $i++) {
		$discographyArray2[$i] = mysqli_fetch_assoc($discography_query2);

		$discographyArray2[$i]['img_pass1'] = "{$img_pass}{$discographyArray2[$i]['img1']}";

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}


//discography取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, date, title1, title2, content, disp_flg, img1, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql        = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$discography_query = mysqli_query($connect,$sql);
$discography_max   = mysqli_num_rows($discography_query);

for ($i = 0; $i < $discography_max; $i++) {
	$discographyArray[$i] = mysqli_fetch_assoc($discography_query);
}
//discography取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($discography_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


/*
echo '<pre>';
print_r($discographyArray);
echo '</pre>';
*/


//画像削除用処理
//remove_directory("./upload/");

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
					<form id="discography" method="POST" enctype="multipart/form-data">
						<div class="table-wrap">
							<table class="adminTable">
								<tr>
									<th><label for="title1">タイトル1</label></th>
									<td>
										<p id="title1Error"></p>
										<input id="title1" type="text" name="title1" value="<?=$discographyArray2[0]['title1']?>">
									</td>
								</tr>

								<tr>
									<th><label for="title2">タイトル2</label></th>
									<td>
										<p id="title2Error"></p>
										<input id="title2" type="text" name="title2" value="<?=$discographyArray2[0]['title2']?>">
									</td>
								</tr>

								<tr>
									<th><label for="datepicker">日付</label></th>
									<td>
										<p id="datepickerError"></p>
										<input type="text" id="datepicker" name="date" value="<?=$discographyArray2[0]['date']?>">
									</td>
								</tr>

								<tr>
									<th><label for="content">内容</label></th>
									<td>
										<p id="contentError"></p>
										<textarea id="content" class="tinyErea" name="content"><?=$discographyArray2[0]['content']?></textarea>
									</td>
								</tr>

								<tr>
									<th>画像1</th>
									<td>
										<label for="file_up1" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up1" name="file_up1" value="" style="display:none;"></label>
										<p id="preview1"></p>
										<div class="imgBox">
											<?php if ($discographyArray2[0]['img1']) { ?>
											<img src="<?=$discographyArray2[0]['img_pass1']?>">
											<?php } ?>
										</div>
									</td>
								</tr>

								<tr>
									<th><label for="disp_flg">表示設定</label></th>
									<td class="dispFlgCell">
										<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($discographyArray2[0]['disp_flg'] == '1') ? 'checked': ''; ?>>表示する</label>
									</td>
								</tr>
								
								<tr>
									<td class="button_cell" colspan="2">
										
										<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
										<input class="editButton" name="editButton" type="button" value="編集" update_value="discography.php?cd=<?=$discographyArray2[0]['cd']?>">
										<?php } else { ?>
										<input class="addButton" name="addButton" type="button" value="登録">
										<?php } ?>

									</td>
								</tr>
							</table>
							<input type="hidden" name="img_value1" id="img_value1" value="<?=$discographyArray2[0]['img1']?>">
							<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
							<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
							<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
						</div>
					</form>
				</div>
			</div>
		</div>


		<div class="contentsBoxWrap">

			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> レコード</h2>
					<p class="sortAttention"></p>

					<?php if ($discography_max) { ?>
					<form id="discography_del">
						<div class="table-wrap">
							<table class="adminTable updateTable">

								<thead>
									<tr>
										<th>タイトル1</th>
										<th>日付</th>
										<th>編集</th>
										<th>削除</th>
										<th>並び順</th>
									</tr>
								</thead>

								<tbody id="listSort">

									<?php for ($i = 0; $i < $discography_max; $i++) { ?>
										<?php

										if ($discographyArray[$i]['disp_flg'] != 1) {
											$bg_color = 'style="background-color: #ccc;"';
											$disp_text = '非表示中';
										} else {
											$bg_color = '';
											$disp_text = '表示中';
										}

										$discographyArray[$i]['date'] = date( "Y年m月d日",strtotime($discographyArray[$i]['date']) );
										?>
									<tr <?=$bg_color?> >
										<td><?=$discographyArray[$i]['title1']?></td>
										<td><?=$discographyArray[$i]['date']?></td>
										<td>
											<a class="update_button" href="discography.php?cd=<?=$discographyArray[$i]['cd']?>">編集</a>
										</td>
										<td>
											<input type="button" name="delete" value="削除" class="del_mode" del_value="discography.php?cd=<?=$discographyArray[$i]['cd']?>">
										</td>
										<td>
											<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$discographyArray[$i]['sort']?>" name="sort[]">
											<input type="hidden" value="<?=$discographyArray[$i]['cd']?>" name="cd_hidden[]">
										</td>
									</tr>
									<?php } ?>
								</tbody>

									<tr>
										<td class="button_cell" colspan="7">
											<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
										</td>
									</tr>

							</table>
						</div>
					</form>

					<?=$pager?>

					<?php } else { ?>
					<p class="record_none_message">現在、レコードが登録されていません。</p>
					<?php } ?>
				</div>

			</div>

		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>