$(function(){
  $(window).load(function(){
     $(".innerLeftBox1, .innerLeftBox2, .innerRightBox2, .slideNavi").mCustomScrollbar();
  });


    //半角数字チェック(半角数字以外削除)ここから
    $('.sort').blur(function() {

        var str = $(this).val();

        if ( str.match( /[^0-9]+/ ) ) {
            
            alert('半角数字をご入力ください。');
            $(this).val(str.replace(/[^0-9]/g, ""));

            return false;
        }

    });
    //半角数字チェック(半角数字以外削除)ここまで


    //画像をアップしたときにその画像を表示するここから
     // jQuery Upload Thumbs 
    // $('#file_up1').uploadThumbs({
    //     position  : "#preview1"    // any: arbitrarily jquery selector
    //     //alternate : '.alt1'         // selecter for alternate view input file names
    // });

    // $('#file_up2').uploadThumbs({
    //     position  : "#preview2"    // any: arbitrarily jquery selector
    //     //alternate : '.alt2'         // selecter for alternate view input file names
    // });

    var img_file_up_max = 11;

    for (var i = 1; i < img_file_up_max; i++) {

        $('#file_up' + i).uploadThumbs({
            position  : '#preview' + i
        });
        console.log('test');
    }
    //画像をアップしたときにその画像を表示するここまで

/*
    tinymce.init({
        selector: ".tinyErea", // class="tinyErea"の場所にTinyMCEを適用
        language: "ja", // 言語 = 日本語
        height: 300,      // 高さ = 300px
    });
*/

    $('.menu-trigger, .closeBtn').on('click', function() {
        $('.slideNavi').stop().slideToggle();
        if ( !$('.menu-trigger').hasClass('active') ) {
            $('.menu-trigger').addClass('active');
        } else {
            $('.menu-trigger').removeClass('active');
        }

        return false;
    });

});
