//Loadingイメージ表示関数
function dispLoading(msg){
    //画面表示メッセージ
    var dispMsg = "";
 
    //引数が空の場合は画像のみ
    if( msg != "" ){
        dispMsg = "<div class='loadingMsg'>" + msg + "</div>";
    }
    //ローディング画像が表示されていない場合のみ表示
    if ( $("#loading").size() == 0 ) {
        $("body").append("<div id='loading'>" + dispMsg + "</div>");
    } 
}


//Loadingイメージ削除関数
function removeLoading() {
    $("#loading").remove();
}


//二重送信無効関数
function dxTransProhibited() {
    $('input[type="button"], input[type="submit"]').attr('disabled', 'disabled');
    $('a').on('click.aClick', function() {
        return false;
    });
    $('input[type="button"], input[type="submit"]').trigger('aClick');
}


//二重送信無効解除関数(ajax処理後にlocationするので今は必要なし)
/*
function unDxTransProhibited() {
    $('input[type="button"], input[type="submit"]').attr('disabled', '');
}
*/