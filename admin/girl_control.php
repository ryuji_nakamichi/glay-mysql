<?php

require_once '../inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "girl";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);
	
	
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここから☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/
	switch ($_REQUEST['mode']) {


		/******************************************************
		登録処理ここから
		******************************************************/
		case 'insert':
		

		//データ整形
		$entry_date = $_REQUEST['entry_date'];

		if ($_REQUEST['entry_date']) {
			$dateArray  = explode("-", $entry_date);
			$year       = $dateArray[0];
			$month      = $dateArray[1];
			$day        = $dateArray[2];
			$entry_date = $year."-".$month."-".$day;
		}
		
		$name                    = $_REQUEST['name'];
		$name_kana               = $_REQUEST['name_kana'];
		$girl_rank               = $_REQUEST['girl_rank'];
		$girl_age                = $_REQUEST['girl_age'];
		$bust_cup                = $_REQUEST['bust_cup'];
		$tall                    = $_REQUEST['tall'];
		$bust                    = $_REQUEST['bust'];
		$waist                   = $_REQUEST['waist'];
		$hip                     = $_REQUEST['hip'];
		$occupation              = $_REQUEST['occupation'];
		$type_favorite_man       = $_REQUEST['type_favorite_man'];
		$hobby                   = $_REQUEST['hobby'];
		$charm_point             = $_REQUEST['charm_point'];
		$first_experience        = $_REQUEST['first_experience'];
		$erogenous_zone          = $_REQUEST['erogenous_zone'];
		$good_play               = $_REQUEST['good_play'];
		$girl_style              = $_REQUEST['girl_style'];
		$looks                   = $_REQUEST['looks'];
		$blood_type              = $_REQUEST['blood_type'];
		$constellation           = $_REQUEST['constellation'];
		$special_move            = $_REQUEST['special_move'];
		$about_me                = $_REQUEST['about_me'];
		$sweet_memories          = $_REQUEST['sweet_memories'];
		$first_date_place        = $_REQUEST['first_date_place'];
		$embarrassing_experience = $_REQUEST['embarrassing_experience'];
		$from_our_shop           = $_REQUEST['from_our_shop'];
		$comment                 = $_REQUEST['comment'];


		if ( ctype_digit(strval($_REQUEST['nomination_fee'])) ) {
			$nomination_fee  = $_REQUEST['nomination_fee'];
		}

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		if ($_REQUEST['new_face_flg'] == 1) {
			$new_face_flg  = $_REQUEST['new_face_flg'];
		} else {
			$new_face_flg  = 0;
		}

		if ( ctype_digit(strval($_REQUEST['favorite_posture'])) ) {
			$favorite_posture  = $_REQUEST['favorite_posture'];
		}

		if ($_REQUEST['play_option']) {

			$play_option_str = $_REQUEST['play_option'];

			$json_play_option = json_encode($play_option_str);

			$play_option = $json_play_option;

		} else {
			$play_option = '';
		}

		if ($_REQUEST['girl_type']) {

			$girl_type_str = $_REQUEST['girl_type'];

			$json_girl_type = json_encode($girl_type_str);

			$girl_type = $json_girl_type;

		} else {
			$girl_type = '';
		}

		$sort = 0;

		if ($_REQUEST['img_value1']) {
			$img1 = str_replace('"', "", $_REQUEST['img_value1']);
		} else {
			$img1 = '';
		}

		if ($_REQUEST['img_value2']) {
			$img2 = str_replace('"', "", $_REQUEST['img_value2']);
		} else {
			$img2 = '';
		}

		if ($_REQUEST['img_value3']) {
			$img3 = str_replace('"', "", $_REQUEST['img_value3']);
		} else {
			$img3 = '';
		}

		if ($_REQUEST['img_value4']) {
			$img4 = str_replace('"', "", $_REQUEST['img_value4']);
		} else {
			$img4 = '';
		}

		if ($_REQUEST['img_value5']) {
			$img5 = str_replace('"', "", $_REQUEST['img_value5']);
		} else {
			$img5 = '';
		}

		if ($_REQUEST['img_value6']) {
			$img6 = str_replace('"', "", $_REQUEST['img_value6']);
		} else {
			$img6 = '';
		}

		if ($_REQUEST['img_value7']) {
			$img7 = str_replace('"', "", $_REQUEST['img_value7']);
		} else {
			$img7 = '';
		}

		if ($_REQUEST['img_value8']) {
			$img8 = str_replace('"', "", $_REQUEST['img_value8']);
		} else {
			$img8 = '';
		}

		if ($_REQUEST['img_value9']) {
			$img9 = str_replace('"', "", $_REQUEST['img_value9']);
		} else {
			$img9 = '';
		}

		if ($_REQUEST['img_value10']) {
			$img10 = str_replace('"', "", $_REQUEST['img_value10']);
		} else {
			$img10 = '';
		}

		$insertDataArray = array(
			"entry_date"              => $entry_date,
			"new_face_flg"            => $new_face_flg,
			"girl_rank"               => $girl_rank,
			"name"                    => $name,
			"name_kana"               => $name_kana,
			"girl_age"                => $girl_age,
			"tall"                    => $tall,
			"bust"                    => $bust,
			"waist"                   => $waist,
			"hip"                     => $hip,
			"bust_cup"                => $bust_cup,
			"nomination_fee"          => $nomination_fee,
			"favorite_posture"        => $favorite_posture,
			"play_option"             => $play_option,
			"girl_type"               => $girl_type,
			"occupation"              => $occupation,
			"type_favorite_man"       => $type_favorite_man,
			"hobby"                   => $hobby,
			"charm_point"             => $charm_point,
			"first_experience"        => $first_experience,
			"erogenous_zone"          => $erogenous_zone,
			"good_play"               => $good_play,
			"girl_style"              => $girl_style,
			"looks"                   => $looks,
			"blood_type"              => $blood_type,
			"constellation"           => $constellation,
			"special_move"            => $special_move,
			"about_me"                => $about_me,
			"sweet_memories"          => $sweet_memories,
			"first_date_place"        => $first_date_place,
			"embarrassing_experience" => $embarrassing_experience,
			"from_our_shop"           => $from_our_shop,
			"comment"                 => $comment,
			"img1"                    => $img1,
			"img2"                    => $img2,
			"img3"                    => $img3,
			"img4"                    => $img4,
			"img5"                    => $img5,
			"img6"                    => $img6,
			"img7"                    => $img7,
			"img8"                    => $img8,
			"img9"                    => $img9,
			"img10"                   => $img10,
			"disp_flg"                => $disp_flg,
			"sort"                    => $sort,
			"new_record_time"         => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


		//並び順更新
		$recordControlObj->recordInsertSort($connect, $table_name);

				break;
		/******************************************************
		登録処理ここまで
		******************************************************/


		/******************************************************
		更新処理ここから
		******************************************************/
		case 'update':


		//データ整形
		$entry_date = $_REQUEST['entry_date'];

		if ($_REQUEST['entry_date']) {
			$dateArray  = explode("-", $entry_date);
			$year       = $dateArray[0];
			$month      = $dateArray[1];
			$day        = $dateArray[2];
			$entry_date = $year."-".$month."-".$day;
		}
		
		$name                    = $_REQUEST['name'];
		$name_kana               = $_REQUEST['name_kana'];
		$girl_rank               = $_REQUEST['girl_rank'];
		$girl_age                = $_REQUEST['girl_age'];
		$bust_cup                = $_REQUEST['bust_cup'];
		$tall                    = $_REQUEST['tall'];
		$bust                    = $_REQUEST['bust'];
		$waist                   = $_REQUEST['waist'];
		$hip                     = $_REQUEST['hip'];
		$occupation              = $_REQUEST['occupation'];
		$type_favorite_man       = $_REQUEST['type_favorite_man'];
		$hobby                   = $_REQUEST['hobby'];
		$charm_point             = $_REQUEST['charm_point'];
		$first_experience        = $_REQUEST['first_experience'];
		$erogenous_zone          = $_REQUEST['erogenous_zone'];
		$good_play               = $_REQUEST['good_play'];
		$girl_style              = $_REQUEST['girl_style'];
		$looks                   = $_REQUEST['looks'];
		$blood_type              = $_REQUEST['blood_type'];
		$constellation           = $_REQUEST['constellation'];
		$special_move            = $_REQUEST['special_move'];
		$about_me                = $_REQUEST['about_me'];
		$sweet_memories          = $_REQUEST['sweet_memories'];
		$first_date_place        = $_REQUEST['first_date_place'];
		$embarrassing_experience = $_REQUEST['embarrassing_experience'];
		$from_our_shop           = $_REQUEST['from_our_shop'];
		$comment                 = $_REQUEST['comment'];


		if ( ctype_digit(strval($_REQUEST['nomination_fee'])) ) {
			$nomination_fee  = $_REQUEST['nomination_fee'];
		}

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		if ($_REQUEST['new_face_flg'] == 1) {
			$new_face_flg  = $_REQUEST['new_face_flg'];
		} else {
			$new_face_flg  = 0;
		}

		if ( ctype_digit(strval($_REQUEST['favorite_posture'])) ) {
			$favorite_posture  = $_REQUEST['favorite_posture'];
		}

		if ($_REQUEST['play_option']) {

			$play_option_str = $_REQUEST['play_option'];

			$json_play_option = json_encode($play_option_str);

			$play_option = $json_play_option;

		} else {
			$play_option = '';
		}

		if ($_REQUEST['girl_type']) {

			$girl_type_str = $_REQUEST['girl_type'];

			$json_girl_type = json_encode($girl_type_str);

			$girl_type = $json_girl_type;

		} else {
			$girl_type = '';
		}

		if ($_REQUEST['file_up1']) {
			$img1 = str_replace('"', "", $_REQUEST['img_value1']);
		} else {
			$img1 = $_REQUEST['img_value1'];
		}

		if ($_REQUEST['file_up2']) {
			$img2 = str_replace('"', "", $_REQUEST['img_value2']);
		} else {
			$img2 = $_REQUEST['img_value2'];
		}

				if ($_REQUEST['img_value3']) {
			$img3 = str_replace('"', "", $_REQUEST['img_value3']);
		} else {
			$img3 = '';
		}

		if ($_REQUEST['img_value4']) {
			$img4 = str_replace('"', "", $_REQUEST['img_value4']);
		} else {
			$img4 = '';
		}

		if ($_REQUEST['img_value5']) {
			$img5 = str_replace('"', "", $_REQUEST['img_value5']);
		} else {
			$img5 = '';
		}

		if ($_REQUEST['img_value6']) {
			$img6 = str_replace('"', "", $_REQUEST['img_value6']);
		} else {
			$img6 = '';
		}

		if ($_REQUEST['img_value7']) {
			$img7 = str_replace('"', "", $_REQUEST['img_value7']);
		} else {
			$img7 = '';
		}

		if ($_REQUEST['img_value8']) {
			$img8 = str_replace('"', "", $_REQUEST['img_value8']);
		} else {
			$img8 = '';
		}

		if ($_REQUEST['img_value9']) {
			$img9 = str_replace('"', "", $_REQUEST['img_value9']);
		} else {
			$img9 = '';
		}

		if ($_REQUEST['img_value10']) {
			$img10 = str_replace('"', "", $_REQUEST['img_value10']);
		} else {
			$img10 = '';
		}

		$updateDataArray = array(
			"entry_date"              => $entry_date,
			"new_face_flg"            => $new_face_flg,
			"girl_rank"               => $girl_rank,
			"name"                    => $name,
			"name_kana"               => $name_kana,
			"girl_age"                => $girl_age,
			"tall"                    => $tall,
			"bust"                    => $bust,
			"waist"                   => $waist,
			"hip"                     => $hip,
			"bust_cup"                => $bust_cup,
			"nomination_fee"          => $nomination_fee,
			"favorite_posture"        => $favorite_posture,
			"play_option"             => $play_option,
			"girl_type"               => $girl_type,
			"occupation"              => $occupation,
			"type_favorite_man"       => $type_favorite_man,
			"hobby"                   => $hobby,
			"charm_point"             => $charm_point,
			"first_experience"        => $first_experience,
			"erogenous_zone"          => $erogenous_zone,
			"good_play"               => $good_play,
			"girl_style"              => $girl_style,
			"looks"                   => $looks,
			"blood_type"              => $blood_type,
			"constellation"           => $constellation,
			"special_move"            => $special_move,
			"about_me"                => $about_me,
			"sweet_memories"          => $sweet_memories,
			"first_date_place"        => $first_date_place,
			"embarrassing_experience" => $embarrassing_experience,
			"from_our_shop"           => $from_our_shop,
			"comment"                 => $comment,
			"img1"                    => $img1,
			"img2"                    => $img2,
			"img3"                    => $img3,
			"img4"                    => $img4,
			"img5"                    => $img5,
			"img6"                    => $img6,
			"img7"                    => $img7,
			"img8"                    => $img8,
			"img9"                    => $img9,
			"img10"                   => $img10,
			"disp_flg"                => $disp_flg,
			"update_record_time"      => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);

				break;


		/******************************************************
		削除処理ここから
		******************************************************/
		case 'delete':
		
			$recordControlObj->recordDelete($connect, $table_name, $cd);

			//並び順更新
			$recordControlObj->recordSort($connect, $table_name, $kensu_, 'delete');

			break;
		/******************************************************
		削除処理ここまで
		******************************************************/


		/******************************************************
		並び順更新処理ここから
		******************************************************/
		case 'sort':

			$recordControlObj->recordSort($connect, $table_name, $kensu_);

			break;

		/******************************************************
		並び順更新処理ここまで
		******************************************************/


	}
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここまで☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
