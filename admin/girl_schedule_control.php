<?php

require_once '../inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "girl_schedule";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);
	
	
	//スケジュール処理ここから

	$year  = date("Y");
	$month  = date("n");

	$girl_cdArray          = $_REQUEST['girl_cd'];
	$girl_cd_max           = count($girl_cdArray);
	$girl_scheduleAllArray = array();

	for ($i = 0; $i < $girl_cd_max; $i++) {


		//開始時間
	 	$girl_work_hour_startArray = $_REQUEST["girl_work_hour_start{$girl_cdArray[$i]}_1"];
		$girl_work_hour_start_max  = count($girl_work_hour_startArray);


		//終了時間
	 	$girl_work_hour_endArray = $_REQUEST["girl_work_hour_end{$girl_cdArray[$i]}_1"];
		$girl_work_hour_end_max  = count($girl_work_hour_endArray);

		for ($j = 0; $j < $girl_work_hour_start_max; $j++) {
			$hour_startArray[$i][$j] = $girl_work_hour_startArray[$j];
			$girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_start'][] = $hour_startArray[$i][$j];
		}

		for ($j = 0; $j < $girl_work_hour_end_max; $j++) {
			$hour_endArray[$i][$j] = $girl_work_hour_endArray[$j];
			$girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_end'][] = $hour_endArray[$i][$j];

		}

	}

	if ($month < 10) {
		$work_month = sprintf('%02d', $month);
	}


	$work_day = date("d");

	if ($work_day < 10) {
		$work_day = sprintf('%02d', $work_day);
	}

	$work_date_year = $year;
	$work_dateArray = array();

	for ($i = 0; $i < $girl_cd_max; $i++) {

		for ($j = 0; $j < $girl_work_hour_start_max; $j++) {

			 $work_day_count = $j + 1;
			
			if ($work_day_count < 10) {
				$work_date_day = sprintf('%02d', $work_day_count);
			} else {
				$work_date_day = $work_day_count;
			}

			$workdateArray['work_date'][$j] = $work_date_year.'-'.$work_month.'-'.$work_date_day;

			$sql = "SELECT cd FROM girl_schedule WHERE girl_cd = {$girl_cdArray[$i]} AND work_date = '{$workdateArray['work_date'][$j]}'";
			$girl_work_date_query = mysqli_query($connect, $sql);
			$girl_work_date_max   = mysqli_num_rows($girl_work_date_query);

			for ($k = 0; $k < $girl_work_date_max; $k++) {
				$girl_work_date_array = mysqli_fetch_assoc($girl_work_date_query);
				$cd                   = $girl_work_date_array['cd'];
			}

			$work_start_time = $girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_start'][$j];
			$work_end_time   = $girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_end'][$j];


			//更新(update)の場合
			if ($girl_work_date_max) {
				
				$updateDataArray = array(
					"girl_cd"            => $girl_cdArray[$i],
					"work_date"          => $workdateArray['work_date'][$j],
					"work_start_time"    => $work_start_time,
					"work_end_time"      => $work_end_time,
					"update_record_time" => "current_timestamp"
				);
			
				$data_query = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);


			//挿入(insert)の場合
			} else {

				$sort = 0;
				
				$insertDataArray = array(
					"girl_cd"         => $girl_cdArray[$i],
					"work_date"       => $workdateArray['work_date'][$j],
					"work_start_time" => $work_start_time,
					"work_end_time"   => $work_end_time,
					"sort"            => $sort,
					"new_record_time" => "current_timestamp"
				);
			
				$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


				//並び順更新
				$recordControlObj->recordInsertSort($connect, $table_name);

			}

			echo $sql.'<br>';

		}

	}
	//スケジュール処理ここまで


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
