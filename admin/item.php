<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = "item";
$table_seq_name = 'information_schema.tables';
$img_max        = 2;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//ITEM(編集用)取得
	$_SELECT     = "cd, date, name, comment, link, target, disp_flg, img1, img2, shipping_type, item_category_cd, price, sort";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$item_query2 = mysqli_query($connect,$sql);
	$item_max2   = mysqli_num_rows($item_query2);

	for ($i = 0; $i < $item_max2; $i++) {
		$itemArray2[$i] = mysqli_fetch_assoc($item_query2);

		$itemArray2[$i]['img_pass1'] = "{$img_pass}{$itemArray2[$i]['img1']}";
		$itemArray2[$i]['img_pass2'] = "{$img_pass}{$itemArray2[$i]['img2']}";

		if ($itemArray2[$i]['item_category_cd']) {
			$sql = "SELECT cd, name FROM item_category WHERE disp_flg = 1 AND cd = {$itemArray2[$i]['item_category_cd']}";
				$item_category_query2 = mysqli_query($connect,$sql);
				$item_category_max2   = mysqli_num_rows($item_category_query2);

				 if ($item_category_max2) {
				 	$item_categoryArray[$i] = mysqli_fetch_assoc($item_category_query2);
				 	$itemArray2[$i]['item_category'] = $item_categoryArray[$i];
				 } 

		}


		if ($itemArray2[$i]['shipping_type']) {
			$sql = "SELECT cd, name FROM shipping_type WHERE disp_flg = 1 AND cd = {$itemArray2[$i]['shipping_type']}";
				$shipping_type_query2 = mysqli_query($connect, $sql);
				$shipping_type_max2   = mysqli_num_rows($shipping_type_query2);

				 if ($shipping_type_max2) {
				 	$shipping_typeArray[$i] = mysqli_fetch_assoc($shipping_type_query2);
				 	$itemArray2[$i]['item_category'] = $shipping_typeArray[$i];
				 } 

		}

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}


//ITEM取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, date, name, comment, link, target, disp_flg, img1, img2, shipping_type, item_category_cd, price, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql        = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$item_query = mysqli_query($connect,$sql);
$item_max   = mysqli_num_rows($item_query);

for ($i = 0; $i < $item_max; $i++) {
	$itemArray[$i] = mysqli_fetch_assoc($item_query);

	if ($itemArray[$i]['item_category_cd']) {
		$sql = "SELECT cd, name FROM item_category WHERE disp_flg = 1 AND cd = {$itemArray[$i]['item_category_cd']}";
			$item_category_query = mysqli_query($connect,$sql);
			$item_category_max   = mysqli_num_rows($item_category_query);

			 if ($item_category_max) {
			 	$itemArray[$i]['item_category'] = mysqli_fetch_assoc($item_category_query);
			 } 

	}


	if ($itemArray[$i]['shipping_type']) {
		$sql = "SELECT cd, name FROM shipping_type WHERE disp_flg = 1 AND cd = {$itemArray[$i]['shipping_type']}";
			$shipping_type_query = mysqli_query($connect, $sql);
			$shipping_type_max   = mysqli_num_rows($shipping_type_query);

			 if ($shipping_type_max) {
			 	$shipping_typeArray[$i] = mysqli_fetch_assoc($shipping_type_query);
			 	$itemArray[$i]['shipping_type'] = $shipping_typeArray[$i];
			 } 

	}

}
//ITEM取得(ページャー有)ここまで


//ITEM_CATEGORY取得ここから
$sql = "SELECT cd, name FROM item_category WHERE disp_flg = 1 ORDER BY sort";
$item_category_query2 = mysqli_query($connect,$sql);
$item_category_max2   = mysqli_num_rows($item_category_query2);

for ($i = 0; $i < $item_category_max2; $i++) {
	$item_categoryArray2[$i] = mysqli_fetch_assoc($item_category_query2);
}
//ITEM_CATEGORY取得ここまで


//shipping_type取得ここから
$sql = "SELECT cd, name FROM shipping_type WHERE disp_flg = 1 ORDER BY sort";
$shipping_type_query2 = mysqli_query($connect, $sql);
$shipping_type_max2   = mysqli_num_rows($shipping_type_query2);

for ($i = 0; $i < $shipping_type_max2; $i++) {
	$shipping_typeArray2[$i] = mysqli_fetch_assoc($shipping_type_query2);
}
//shipping_type取得ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($item_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


/*
echo '<pre>';
print_r($itemArray);
echo '</pre>';
*/


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="name">商品名</label></th>
										<td>
											<p id="nameError"></p>
											<input id="name" type="text" name="name" value="<?=$itemArray2[0]['name']?>">
										</td>
									</tr>

									<tr>
										<th><label for="item_category">カテゴリー</label></th>
										<td>
											<p id="item_categoryError"></p>
											<select id="item_category" name="item_category">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $item_category_max2; $i++) { ?>

													<?php if ($item_categoryArray2[$i]['cd'] == $itemArray2[0]['item_category_cd']) { ?>
													<option value="<?=$item_categoryArray2[$i]['cd']?>" selected><?=$item_categoryArray2[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$item_categoryArray2[$i]['cd']?>"><?=$item_categoryArray2[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

									<tr>
										<th><label for="datepicker">日付</label></th>
										<td>
											<p id="datepickerError"></p>
											<input type="text" id="datepicker" name="date" value="<?=$itemArray2[0]['date']?>">
										</td>
									</tr>

									<tr>
										<th><label for="price">値段</label></th>
										<td>
											<p id="priceError"></p>
											<input type="text" id="price" name="price" value="<?=$itemArray2[0]['price']?>">
										</td>
									</tr>

									<tr>
										<th><label for="comment">コメント</label></th>
										<td>
											<p id="commentError"></p>
											<textarea id="comment" class="tinyErea" name="comment"><?=$itemArray2[0]['comment']?></textarea>
										</td>
									</tr>

									<tr>
										<th>画像1</th>
										<td>
											<label for="file_up1" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up1" name="file_up1" value="" style="display:none;"></label>
											<p id="preview1"></p>
											<div class="imgBox">
												<?php if ($itemArray2[0]['img1']) { ?>
												<img src="<?=$itemArray2[0]['img_pass1']?>">
												<?php } ?>
											</div>
										</td>
									</tr>

									<tr>
										<th>画像2</th>
										<td>
											<label for="file_up2" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up2" name="file_up2" value="" style="display:none;"></label>
											<p id="preview2"></p>
											<div class="imgBox">
												<?php if ($itemArray2[0]['img2']) { ?>
												<img src="<?=$itemArray2[0]['img_pass2']?>">
												<?php } ?>
											</div>
										</td>
									</tr>

									<tr>
										<th><label for="shipping_type">配送方法</label></th>
										<td>
											<p id="shipping_typeError"></p>
											<select id="shipping_type" name="shipping_type">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $shipping_type_max2; $i++) { ?>

													<?php if ($shipping_typeArray2[$i]['cd'] == $itemArray2[0]['shipping_type']) { ?>
													<option value="<?=$shipping_typeArray2[$i]['cd']?>" selected><?=$shipping_typeArray2[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$shipping_typeArray2[$i]['cd']?>"><?=$shipping_typeArray2[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

									<tr>
										<th><label for="link">リンク</label></th>
										<td>
											<input type="text" id="link" name="link" value="<?=$itemArray2[0]['link']?>">
										</td>
									</tr>

									<tr>
										<th>ターゲット<br>(ブランク設定)</th>
										<td class="dispFlgCell">
											<label><input type="radio" id="target1" name="target" value="_blank" <?php echo ($itemArray2[0]['target'] == '_blank') ? 'checked': ''; ?> >設定する</label>
											<label><input type="radio" id="target2" name="target" value="_self" <?php echo ($itemArray2[0]['target'] == '_self' || !$itemArray2[0]['target']) ? 'checked': ''; ?>>設定しない</label>
										</td>
									</tr>

									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($itemArray2[0]['disp_flg'] == '1') ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<?php if ( ctype_digit($_GET['cd']) ) { ?>
									<tr>
										<th><label for="disp_flg">プレビューモード</label></th>
										<td class="dispFlgCell">
											<a href="<?=$site_url_hp?>item_detail.php?cd=<?=$itemArray2[0]['cd']?>&preview=true" target="_blank">プレビューする</a>
										</td>
									</tr>
									<?php } ?>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="item.php?cd=<?=$itemArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>
								<input type="hidden" name="img_value1" id="img_value1" value="<?=$itemArray2[0]['img1']?>">
								<input type="hidden" name="img_value2" id="img_value2" value="<?=$itemArray2[0]['img2']?>">
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">
				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($item_max) { ?>
						<form id="item_del">
							<div class="table-wrap">
								<table class="adminTable updateTable">

									<thead>
										<tr>
											<th>商品名</th>
											<th>日付</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">

										<?php for ($i = 0; $i < $item_max; $i++) { ?>
											<?php

											if ($itemArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											$itemArray[$i]['date'] = date( "Y年m月d日",strtotime($itemArray[$i]['date']) );
											?>
										<tr <?=$bg_color?> >
											<td><?=$itemArray[$i]['name']?></td>
											<td><?=$itemArray[$i]['date']?></td>
											<td>
												<a class="update_button" href="item.php?cd=<?=$itemArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="item.php?cd=<?=$itemArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$itemArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$itemArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="7">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>

						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
					</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>