<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'girl_event';
$table_seq_name = 'information_schema.tables';
$img_max        = 1;
$imgCount       = 1;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";

	$_SELECT     = "cd, girl_cd, title, comment, img1, disp_flg";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$data_query2  = mysqli_query($connect,$sql);
	$data_max2    = mysqli_num_rows($data_query2);

	for ($i = 0; $i < $data_max2; $i++) {
		$dataArray2[$i] = mysqli_fetch_assoc($data_query2);


		//女性キャスト名取得ここから
		$_SELECT = "cd, name";
		$sql = "SELECT {$_SELECT} FROM girl WHERE cd = {$dataArray2[$i]['girl_cd']} AND disp_flg = 1 ORDER BY sort";
		$girl_query2  = mysqli_query($connect, $sql);
		$girl_max2    = mysqli_num_rows($girl_query2);

		if ($girl_max2) {
			$girlArray2 = mysqli_fetch_assoc($girl_query2);
			$dataArray2[$i]['name'] = $girlArray2['name'];
		}
		//女性キャスト名取得ここまで


		//画像パス取得ここから
		$imgCount = 1;

		for ($j = 0; $j < $img_max; $j++) {
			$dataArray2[$i]["img_pass{$imgCount}"] = $img_pass.$dataArray2[$i]["img{$imgCount}"];
			$imgCount++;
		}
		//画像パス取得ここまで

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}




//data取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, girl_cd, title, comment, img1, disp_flg, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql       = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$data_query = mysqli_query($connect,$sql);
$data_max   = mysqli_num_rows($data_query);

for ($i = 0; $i < $data_max; $i++) {
	$dataArray[$i] = mysqli_fetch_assoc($data_query);


	//女性キャスト名取得ここから
	$_SELECT = "cd, name";
	$sql = "SELECT {$_SELECT} FROM girl WHERE cd = {$dataArray[$i]['girl_cd']} AND disp_flg = 1 ORDER BY sort";
	$girl_query3  = mysqli_query($connect, $sql);
	$girl_max3    = mysqli_num_rows($girl_query3);

	if ($girl_max3) {
		$girlArray3 = mysqli_fetch_assoc($girl_query3);
		$dataArray[$i]['name'] = $girlArray3['name'];
	}
	//女性キャスト名取得ここまで

}
//data取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


//女性キャスト取得ここから
$_SELECT    = "cd, name";
$sql        = "SELECT {$_SELECT} FROM girl WHERE disp_flg = 1 ORDER BY sort";
$girl_query = mysqli_query($connect, $sql);
$girl_max   = mysqli_num_rows($girl_query);

for ($i = 0; $i < $girl_max; $i++) {
	$girlArray[$i] = mysqli_fetch_assoc($girl_query);
}

//女性キャスト取得ここまで

// ページャー(数字)
$pager = ($data_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';



// echo '<pre>';
// print_r($dataArray);
// echo '</pre>';



?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="favorite_posture">女性キャスト選択</label></th>
										<td>
											<p id="girl_cdError"></p>
											<select id="girl_cd" name="girl_cd">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $girl_max; $i++) { ?>

													<?php if ($girlArray[$i]['cd'] == $dataArray2[0]['girl_cd']) { ?>
													<option value="<?=$girlArray[$i]['cd']?>" selected><?=$girlArray[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$girlArray[$i]['cd']?>"><?=$girlArray[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

									<tr>
										<th><label for="title">タイトル</label></th>
										<td>
											<p id="titleError"></p>
											<input id="title" type="text" name="title" value="<?=$dataArray2[0]['title']?>">
										</td>
									</tr>

									<tr>
										<th><label for="comment">コメント</label></th>
										<td>
											<p id="commentError"></p>
											<textarea id="comment" class="tinyErea" name="comment"><?=$dataArray2[0]['comment']?></textarea>
										</td>
									</tr>

									<?php
									$imgCount = 1;
									for ($i = 0; $i < $img_max; $i++) {
									?>
									<tr>
										<th>画像<?=$imgCount?></th>
										<td>
											<label for="file_up<?=$imgCount?>" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up<?=$imgCount?>" name="file_up<?=$imgCount?>" value="" style="display:none;"></label>
											<p id="preview<?=$imgCount?>"></p>
											<div class="imgBox">
												<?php if ($dataArray2[0]["img{$imgCount}"]) { ?>
												<img src="<?=$dataArray2[0]["img_pass{$imgCount}"]?>">
												<?php } ?>
											</div>
										</td>
									</tr>
									<?php
									$imgCount++;
									}
									?>
					
									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($dataArray2[0]['disp_flg'] == 1) ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="<?=$table_name?>.php?cd=<?=$dataArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>

								<?php
								$imgCount = 1;
								for ($i = 0; $i < $img_max; $i++) {
								?>
								<input type="hidden" name="img_value<?=$imgCount?>" id="img_value<?=$imgCount?>" value="<?=$dataArray2[0]["img{$imgCount}"]?>">
								<?php
								$imgCount++;
								}
								?>

								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">

				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($data_max) { ?>
						<form id="<?=$table_name?>_del" method="POST">
							<div class="table-wrap">
								<table class="adminTable updateTable">
									<thead>
										<tr>
											<th>タイトル</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">
										<?php for ($i = 0; $i < $data_max; $i++) { ?>
											<?php

											if ($dataArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											?>
										<tr <?=$bg_color?> >
											<td><?=$dataArray[$i]['title']?></td>
											<td>
												<a class="update_button" href="<?=$table_name?>.php?cd=<?=$dataArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="<?=$table_name?>.php?cd=<?=$dataArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$dataArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$dataArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="6">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>
						
						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
				</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>