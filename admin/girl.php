<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = "girl";
$table_seq_name = 'information_schema.tables';
$img_max        = 10;
$imgCount       = 1;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//girl(編集用)取得
	$_SELECT     =
		"cd,
		name,
		name_kana,
		entry_date,
		new_face_flg,
		girl_rank,
		girl_age,
		bust_cup,
		tall,
		bust,
		waist,
		hip,
		girl_type,
		nomination_fee,
		occupation,
		hobby,
		type_favorite_man,
		charm_point,
		first_experience,
		place_first_experience,
		erogenous_zone,
		favorite_posture,
		good_play,
		comment,
		REPLACE(
			REPLACE(
				REPLACE(play_option, '[', ''), 
				']', 
				''
			), 
				'&quot;', 
			''
		) AS play_option,
		REPLACE(
			REPLACE(
				REPLACE(girl_type, '[', ''), 
				']', 
				''
			), 
				'&quot;', 
			''
		) AS girl_type,
		girl_style,
		looks,
		blood_type,
		constellation,
		special_move,
		about_me,
		sweet_memories,
		first_date_place,
		embarrassing_experience,
		from_our_shop,
		img1, 
		img2, 
		img3, 
		img4, 
		img5, 
		img6, 
		img7, 
		img8, 
		img9, 
		img10, 
		disp_flg, 
		sort";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$data_query2 = mysqli_query($connect, $sql);
	$data_max2   = mysqli_num_rows($data_query2);

	for ($i = 0; $i < $data_max2; $i++) {
		$dataArray2[$i] = mysqli_fetch_assoc($data_query2);


		//画像パス取得ここから
		$imgCount = 1;

		for ($j = 0; $j < $img_max; $j++) {
			$dataArray2[$i]["img_pass{$imgCount}"] = $img_pass.$dataArray2[$i]["img{$imgCount}"];
			$imgCount++;
		}
		//画像パス取得ここまで


		//ランク取得ここから
		if ($dataArray2[$i]['girl_rank']) {
			$sql = "SELECT cd, name FROM girl_rank WHERE disp_flg = 1 AND cd = {$dataArray2[$i]['girl_rank']}";
				$girl_rank_query2 = mysqli_query($connect, $sql);
				$girl_rank_max2   = mysqli_num_rows($girl_rank_query2);

				 if ($girl_rank_max2) {
				 	$girl_rankArray2[$i] = mysqli_fetch_assoc($girl_rank_query2);
				 	$dataArray2[$i]['girl_rank_name'] = $girl_rankArray2[$i];
				 } 

		}
		//ランク取得ここまで


		//指名料取得ここから
		if ($dataArray2[$i]['nomination_fee']) {
			$sql = "SELECT cd, name FROM nomination_fee WHERE disp_flg = 1 AND cd = {$dataArray2[$i]['nomination_fee']}";
				$nomination_fee_query2 = mysqli_query($connect, $sql);
				$nomination_fee_max2   = mysqli_num_rows($nomination_fee_query2);

				 if ($nomination_fee_max2) {
				 	$nomination_feeArray[$i] = mysqli_fetch_assoc($nomination_fee_query2);
				 	$dataArray2[$i]['nomination_fee_name'] = $nomination_feeArray[$i];
				 } 

		}
		//指名料取得ここまで


		//体位取得ここから
		if ($dataArray2[$i]['favorite_posture']) {
			$sql = "SELECT cd, name FROM favorite_posture WHERE disp_flg = 1 AND cd = {$dataArray2[$i]['favorite_posture']}";
				$data2_favorite_posture_query2 = mysqli_query($connect, $sql);
				$favorite_posture_max2   = mysqli_num_rows($data2_favorite_posture_query2);

				 if ($data2_favorite_posture_max2) {
				 	$data2_favorite_postureArray[$i] = mysqli_fetch_assoc($data2_favorite_posture_query2);
				 	$dataArray2[$i]['favorite_posture'] = $data2_favorite_postureArray[$i];
				 } 

		}
		//体位取得ここまで


		//タイプ取得ここから
		if ($dataArray2[$i]['girl_type']) {
			$dataArray2[$i]['girl_typeArray'] = explode(",", $dataArray2[$i]['girl_type']);
		}
		//タイプ取得ここまで


		//オプション取得ここから
		if ($dataArray2[$i]['play_option']) {
			$dataArray2[$i]['play_optionArray'] = explode(",", $dataArray2[$i]['play_option']);
		}
		//オプション取得ここまで

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}


//girl取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select =
		"cd,
		name,
		name_kana,
		entry_date,
		new_face_flg,
		girl_rank,
		girl_age,
		bust_cup,
		tall,
		bust,
		waist,
		hip,
		girl_type,
		nomination_fee,
		occupation,
		hobby,
		type_favorite_man,
		charm_point,
		first_experience,
		place_first_experience,
		erogenous_zone,
		favorite_posture,
		good_play,
		girl_style,
		looks,
		blood_type,
		constellation,
		special_move,
		about_me,
		sweet_memories,
		first_date_place,
		embarrassing_experience,
		from_our_shop,
		comment,
		REPLACE(
			REPLACE(
				REPLACE(play_option, '[', ''), 
				']', 
				''
			), 
				'&quot;', 
			''
		) AS play_option,
		REPLACE(
			REPLACE(
				REPLACE(girl_type, '[', ''), 
				']', 
				''
			), 
				'&quot;', 
			''
		) AS girl_type,
		img1, 
		img2, 
		img3, 
		img4, 
		img5, 
		img6, 
		img7, 
		img8, 
		img9, 
		img10, 
		disp_flg, 
		sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql        = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
//echo $sql;
//$sql        = "{$sql} ORDER BY cd LIMIT {$kensu} {$offset}";
$data_query = mysqli_query($connect, $sql);
$data_max   = mysqli_num_rows($data_query);

for ($i = 0; $i < $data_max; $i++) {
	$dataArray[$i] = mysqli_fetch_assoc($data_query);


	//ランク取得ここから
	if ($dataArray[$i]['girl_rank']) {
		$sql = "SELECT cd, name FROM girl_rank WHERE disp_flg = 1 AND cd = {$dataArray[$i]['girl_rank']}";
			$data_girl_rank_query = mysqli_query($connect, $sql);
			$data_girl_rank_max   = mysqli_num_rows($data_girl_rank_query);

			 if ($data_girl_rank_max) {
			 	$data_girl_rankArray[$i] = mysqli_fetch_assoc($data_girl_rank_query);
			 	$dataArray[$i]['girl_rank_name'] = $data_girl_rankArray[$i];
			 } 

	}
	//ランク取得ここまで


	//指名料取得ここから
	if ($dataArray[$i]['nomination_fee']) {

		$sql = "SELECT cd, name FROM nomination_fee WHERE disp_flg = 1 AND cd = {$dataArray[$i]['nomination_fee']}";

			$data_nomination_fee_query = mysqli_query($connect, $sql);
			$data_nomination_fee_max   = mysqli_num_rows($data_nomination_fee_query);

			 if ($data_nomination_fee_max) {
			 	$dataArray[$i]['nomination_fee_name'] = mysqli_fetch_assoc($data_nomination_fee_query);
			 }

	}
	//指名料取得ここまで


	//体位取得ここから
	if ($dataArray[$i]['favorite_posture']) {

		$sql = "SELECT cd, name FROM favorite_posture WHERE disp_flg = 1 AND cd = {$dataArray[$i]['favorite_posture']}";
			$data_favorite_posture_query = mysqli_query($connect, $sql);
			$data_favorite_posture_max   = mysqli_num_rows($data_favorite_posture_query);

			 if ($data_favorite_posture_max) {
			 	$data_favorite_postureArray[$i]    = mysqli_fetch_assoc($data_favorite_posture_query);
			 	$dataArray[$i]['favorite_posture'] = $data_favorite_postureArray[$i];
			 }

	}
	//体位取得ここまで

}
//girl取得(ページャー有)ここまで


//タイプ取得ここから
$sql = "SELECT cd, name FROM girl_type WHERE disp_flg = 1 ORDER BY sort";
$girl_type_query  = mysqli_query($connect, $sql);
$girl_type_max    = mysqli_num_rows($girl_type_query);

for ($i = 0; $i < $girl_type_max; $i++) {
	$girl_typeArray[$i] = mysqli_fetch_assoc($girl_type_query);
}
//タイプ取得ここまで


//オプション取得ここから
$sql = "SELECT cd, name FROM play_option WHERE disp_flg = 1 ORDER BY sort";
$play_option_query  = mysqli_query($connect, $sql);
$play_option_max    = mysqli_num_rows($play_option_query);

for ($i = 0; $i < $play_option_max; $i++) {
	$play_optionArray[$i] = mysqli_fetch_assoc($play_option_query);
}
//オプション取得ここまで


//ランク取得ここから
$sql = "SELECT cd, name FROM girl_rank WHERE disp_flg = 1 ORDER BY sort";
$girl_rank_query         = mysqli_query($connect, $sql);
$girl_rank_max     = mysqli_num_rows($girl_rank_query);

for ($i = 0; $i < $girl_rank_max; $i++) {
	$girl_rankArray[$i] = mysqli_fetch_assoc($girl_rank_query);
}
//ランク取得ここまで


//指名料取得ここから
$sql = "SELECT cd, name FROM nomination_fee WHERE disp_flg = 1 ORDER BY sort";
$nomination_fee_query         = mysqli_query($connect, $sql);
$nomination_fee_query_max     = mysqli_num_rows($nomination_fee_query);

for ($i = 0; $i < $nomination_fee_query_max; $i++) {
	$nomination_feeArray[$i] = mysqli_fetch_assoc($nomination_fee_query);
}
//指名料取得ここまで


//体位取得ここから
$sql = "SELECT cd, name FROM favorite_posture WHERE disp_flg = 1 ORDER BY sort";
$favorite_posture_query2  = mysqli_query($connect, $sql);
$favorite_posture_max2    = mysqli_num_rows($favorite_posture_query2);

for ($i = 0; $i < $favorite_posture_max2; $i++) {
	$favorite_postureArray2[$i] = mysqli_fetch_assoc($favorite_posture_query2);
}
//体位取得ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($data_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';



// echo '<pre>';
// print_r($dataArray2);
// echo '</pre>';



?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="name">名前</label></th>
										<td>
											<p id="nameError"></p>
											<input id="name" type="text" name="name" value="<?=$dataArray2[0]['name']?>">
										</td>
									</tr>
									<tr>
										<th><label for="name_kana">カナ</label></th>
										<td>
											<p id="name_kanaError"></p>
											<input id="name_kana" type="text" name="name_kana" value="<?=$dataArray2[0]['name_kana']?>">
										</td>
									</tr>

									<tr>
										<th><label for="entry_date">入店日</label></th>
										<td>
											<p id="entry_dateError"></p>
											<input type="text" id="entry_date" name="entry_date" value="<?=$dataArray2[0]['entry_date']?>">
										</td>
									</tr>

									<tr>
										<th><label for="new_face_flg">新人設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="new_face_flg" name="new_face_flg" value="1" <?php echo ($dataArray2[0]['new_face_flg'] == '1') ? 'checked': ''; ?>>設定する</label>
										</td>
									</tr>

									<tr>
										<th><label for="girl_rank">ランク</label></th>
										<td>
											<p id="girl_rankError"></p>
											<select id="girl_rank" name="girl_rank">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $girl_rank_max; $i++) { ?>

													<?php if ($girl_rankArray[$i]['cd'] == $dataArray2[0]['girl_rank']) { ?>
													<option value="<?=$girl_rankArray[$i]['cd']?>" selected><?=$girl_rankArray[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$girl_rankArray[$i]['cd']?>"><?=$girl_rankArray[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

									<tr>
										<th><label for="girl_age">年齢</label></th>
										<td>
											<p id="girl_ageError"></p>
											<input id="girl_age" type="text" name="girl_age" value="<?=$dataArray2[0]['girl_age']?>">
										</td>
									</tr>
									
									<tr>
										<th><label for="tall">女性情報</label></th>
										<td>

											<div class="collumn3Box">
												<p id="tallError"></p>
												<span>身長</span>
												<input id="tall" class="collumn3" type="text" name="tall" value="<?=$dataArray2[0]['tall']?>" maxlength="3">
											</div>

											<div class="collumn3Box">
												<p id="bustError"></p>
												<span>バスト</span>
												<input id="bust" class="collumn3" type="text" name="bust" value="<?=$dataArray2[0]['bust']?>" maxlength="3">
											</div>
											
											<div class="collumn3Box">
												<p id="waistError"></p>
												<span>ウエスト</span>
												<input id="waist" class="collumn3" type="text" name="waist" value="<?=$dataArray2[0]['waist']?>" maxlength="3">
											</div>

											<div class="collumn3Box">
												<p id="hipError"></p>
												<span>ヒップ</span>
												<input id="hip" class="collumn3" type="text" name="hip" value="<?=$dataArray2[0]['hip']?>" maxlength="3">
											</div>
										</td>
									</tr>

									<tr>
										<th><label for="bust_cup">バストカップ</label></th>
										<td>
											<p id="bust_cupError"></p>
											<input id="bust_cup" type="text" name="bust_cup" value="<?=$dataArray2[0]['bust_cup']?>">
										</td>
									</tr>

									<tr>
										<th><label for="nomination_fee">指名料</label></th>
										<td>
											<p id="nomination_feeError"></p>
											<select id="nomination_fee" name="nomination_fee">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $nomination_fee_query_max; $i++) { ?>

													<?php if ($nomination_feeArray[$i]['cd'] == $dataArray2[0]['nomination_fee']) { ?>
													<option value="<?=$nomination_feeArray[$i]['cd']?>" selected><?=$nomination_feeArray[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$nomination_feeArray[$i]['cd']?>"><?=$nomination_feeArray[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

									<tr>
										<th><label for="favorite_posture">好きな体位</label></th>
										<td>
											<p id="favorite_postureError"></p>
											<select id="favorite_posture" name="favorite_posture">

												<option value="">選択してください。</option>

												<?php for ($i = 0; $i < $favorite_posture_max2; $i++) { ?>

													<?php if ($favorite_postureArray2[$i]['cd'] == $dataArray2[0]['favorite_posture']) { ?>
													<option value="<?=$favorite_postureArray2[$i]['cd']?>" selected><?=$favorite_postureArray2[$i]['name']?></option>
													<?php } else { ?>
													<option value="<?=$favorite_postureArray2[$i]['cd']?>"><?=$favorite_postureArray2[$i]['name']?></option>
													<?php } ?>

												<?php } ?>

											</select>
										</td>
									</tr>

								<tr>
									<th><label for="play_option">可能オプション</label></th>
									<td class="checkboxCell">
										<p id="play_optionError"></p>

										<ul>
											<?php for ($i = 0; $i < $play_option_max; $i++) { ?>
											<li><label><?=$play_optionArray[$i]['name']?>
											<input type="checkbox" name="play_option[]" value="<?=$play_optionArray[$i]['cd']?>" <?php echo ( in_array($play_optionArray[$i]['cd'], $dataArray2[0]['play_optionArray']) ) ? 'checked': ''; ?>></label>
											</li>	
											<?php } ?>
										</ul>
									</td>
								</tr>

								<tr>
									<th><label for="girl_type">女性タイプ</label></th>
									<td class="checkboxCell">
										<p id="girl_typeError"></p>

										<ul>
											<?php for ($i = 0; $i < $girl_type_max; $i++) { ?>
											<li><label><?=$girl_typeArray[$i]['name']?>
											<input type="checkbox" name="girl_type[]" value="<?=$girl_typeArray[$i]['cd']?>" <?php echo ( in_array($girl_typeArray[$i]['cd'], $dataArray2[0]['girl_typeArray']) ) ? 'checked': ''; ?>></label>
											</li>	
											<?php } ?>
										</ul>
									</td>
								</tr>

								<tr>
									<th><label for="occupation">職業</label></th>
									<td>
										<p id="occupationError"></p>
										<input id="occupation" type="text" name="occupation" value="<?=$dataArray2[0]['occupation']?>">
									</td>
								</tr>
								<tr>
									<th><label for="hobby">趣味</label></th>
									<td>
										<p id="hobbyError"></p>
										<input id="hobby" type="text" name="hobby" value="<?=$dataArray2[0]['hobby']?>">
									</td>
								</tr>
								<tr>
									<th><label for="type_favorite_man">好きな男性のタイプ</label></th>
									<td>
										<p id="type_favorite_manError"></p>
										<input id="type_favorite_man" type="text" name="type_favorite_man" value="<?=$dataArray2[0]['type_favorite_man']?>">
									</td>
								</tr>
								<tr>
									<th><label for="charm_point">チャームポイント</label></th>
									<td>
										<p id="charm_pointError"></p>
										<input id="charm_point" type="text" name="charm_point" value="<?=$dataArray2[0]['charm_point']?>">
									</td>
								</tr>
								<tr>
									<th><label for="first_experience">初体験の場所</label></th>
									<td>
										<p id="first_experienceError"></p>
										<input id="first_experience" type="text" name="first_experience" value="<?=$dataArray2[0]['first_experience']?>">
									</td>
								</tr>
								<tr>
									<th><label for="erogenous_zone">性感帯</label></th>
									<td>
										<p id="erogenous_zoneError"></p>
										<input id="erogenous_zone" type="text" name="erogenous_zone" value="<?=$dataArray2[0]['erogenous_zone']?>">
									</td>
								</tr>
								<tr>
									<th><label for="good_play">得意プレイ</label></th>
									<td>
										<p id="good_playError"></p>
										<input id="good_play" type="text" name="good_play" value="<?=$dataArray2[0]['good_play']?>">
									</td>
								</tr>

								<tr>
									<th><label for="girl_style">スタイル</label></th>
									<td>
										<p id="girl_styleError"></p>
										<input id="girl_style" type="text" name="girl_style" value="<?=$dataArray2[0]['girl_style']?>">
									</td>
								</tr>

								<tr>
									<th><label for="looks">外見</label></th>
									<td>
										<p id="looksError"></p>
										<input id="looks" type="text" name="looks" value="<?=$dataArray2[0]['looks']?>">
									</td>
								</tr>

								<tr>
									<th><label for="blood_type">血液型</label></th>
									<td>
										<p id="blood_typeError"></p>
										<input id="blood_type" type="text" name="blood_type" value="<?=$dataArray2[0]['blood_type']?>">
									</td>
								</tr>

								<tr>
									<th><label for="constellation">星座</label></th>
									<td>
										<p id="constellationError"></p>
										<input id="constellation" type="text" name="constellation" value="<?=$dataArray2[0]['constellation']?>">
									</td>
								</tr>

								<tr>
									<th><label for="special_move">必殺技</label></th>
									<td>
										<p id="special_moveError"></p>
										<input id="special_move" type="text" name="special_move" value="<?=$dataArray2[0]['special_move']?>">
									</td>
								</tr>

								<tr>
									<th><label for="about_me">わたしはこんな子</label></th>
									<td>
										<p id="about_meError"></p>
										<input id="about_me" type="text" name="about_me" value="<?=$dataArray2[0]['about_me']?>">
									</td>
								</tr>

								<tr>
									<th><label for="sweet_memories">一番あま～ぃ思い出</label></th>
									<td>
										<p id="sweet_memoriesError"></p>
										<input id="sweet_memories" type="text" name="sweet_memories" value="<?=$dataArray2[0]['sweet_memories']?>">
									</td>
								</tr>

								<tr>
									<th><label for="first_date_place">初デートはここ！</label></th>
									<td>
										<p id="first_date_placeError"></p>
										<input id="first_date_place" type="text" name="first_date_place" value="<?=$dataArray2[0]['first_date_place']?>">
									</td>
								</tr>

								<tr>
									<th><label for="embarrassing_experience">恥ずかしいＨな体験</label></th>
									<td>
										<p id="embarrassing_experienceError"></p>
										<input id="embarrassing_experience" type="text" name="embarrassing_experience" value="<?=$dataArray2[0]['embarrassing_experience']?>">
									</td>
								</tr>

								<tr>
									<th><label for="from_our_shop">当店より</label></th>
									<td>
										<p id="from_our_shopError"></p>
										<textarea id="from_our_shop" class="tinyErea" name="from_our_shop"><?=$dataArray2[0]['from_our_shop']?></textarea>
									</td>
								</tr>

								<?php
								$imgCount = 1;
								for ($i = 0; $i < $img_max; $i++) {
								?>
								<tr>
									<th>画像<?=$imgCount?></th>
									<td>
										<label for="file_up<?=$imgCount?>" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up<?=$imgCount?>" name="file_up<?=$imgCount?>" value="" style="display:none;"></label>
										<p id="preview<?=$imgCount?>"></p>
										<div class="imgBox">
											<?php if ($dataArray2[0]["img{$imgCount}"]) { ?>
											<img src="<?=$dataArray2[0]["img_pass{$imgCount}"]?>">
											<?php } ?>
										</div>
									</td>
								</tr>
								<?php
								$imgCount++;
								}
								?>
								
<?php/*
								<tr>
									<th>画像2</th>
									<td>
										<label for="file_up2" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up2" name="file_up2" value="" style="display:none;"></label>
										<p id="preview2"></p>
										<div class="imgBox">
											<?php if ($dataArray2[0]['img2']) { ?>
											<img src="<?=$dataArray2[0]['img_pass2']?>">
											<?php } ?>
										</div>
									</td>
								</tr>*/?>

								<tr>
									<th><label for="comment">コメント</label></th>
									<td>
										<p id="commentError"></p>
										<textarea id="comment" class="tinyErea" name="comment"><?=$dataArray2[0]['comment']?></textarea>
									</td>
								</tr>

								<tr>
									<th><label for="disp_flg">表示設定</label></th>
									<td class="dispFlgCell">
										<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($dataArray2[0]['disp_flg'] == '1') ? 'checked': ''; ?>>表示する</label>
									</td>
								</tr>

								<tr>
									<td class="button_cell" colspan="2">
										
										<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
										<input class="editButton" name="editButton" type="button" value="編集" update_value="<?=$table_name?>.php?cd=<?=$dataArray2[0]['cd']?>">
										<?php } else { ?>
										<input class="addButton" name="addButton" type="button" value="登録">
										<?php } ?>

									</td>
								</tr>
								</table>

								<?php
								$imgCount = 1;
								for ($i = 0; $i < $img_max; $i++) {
								?>
								<input type="hidden" name="img_value<?=$imgCount?>" id="img_value<?=$imgCount?>" value="<?=$dataArray2[0]["img{$imgCount}"]?>">
								<?php
								$imgCount++;
								}
								?>

								<!-- <input type="hidden" name="img_value2" id="img_value2" value="<?=$dataArray2[0]['img2']?>"> -->
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">
				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($data_max) { ?>
						<form id="<?=$table_name?>_del">
							<div class="table-wrap">
								<table class="adminTable updateTable">

									<thead>
										<tr>
											<th>名前</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">

										<?php for ($i = 0; $i < $data_max; $i++) { ?>
											<?php

											if ($dataArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											?>
										<tr <?=$bg_color?> >
											<td><?=$dataArray[$i]['name']?></td>
											<td>
												<a class="update_button" href="<?=$table_name?>.php?cd=<?=$dataArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="<?=$table_name?>.php?cd=<?=$dataArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$dataArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$dataArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="7">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>

						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
					</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>