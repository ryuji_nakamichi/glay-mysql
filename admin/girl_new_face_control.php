<?php

require_once '../inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "girl_new_face";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);


	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここから☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/
	switch ($_REQUEST['mode']) {


		//登録処理ここから
		case 'insert':

		$girl_cd = '';

		if ( ctype_digit( strval($_REQUEST['girl_cd']) ) ) {
			$girl_cd = $_REQUEST['girl_cd'];
		}

		$comment = $_REQUEST['comment'];

		$sort = 0;

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		if ($_REQUEST['img_value1']) {
			$img1 = str_replace('"', "", $_REQUEST['img_value1']);
		} else {
			$img1 = '';
		}

		$insertDataArray = array(
			"girl_cd"         => $girl_cd,
			"comment"         => $comment,
			"img1"            => $img1,
			"disp_flg"        => $disp_flg,
			"sort"            => $sort,
			"new_record_time" => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


		//並び順更新
		$recordControlObj->recordInsertSort($connect, $table_name);

			break;


		/******************************************************
		登録処理ここまで
		******************************************************/


		//更新処理ここから
		case 'update':

		$girl_cd = '';
		
		if ( ctype_digit( strval($_REQUEST['girl_cd']) ) ) {
			$girl_cd = $_REQUEST['girl_cd'];
		}

		$comment = $_REQUEST['comment'];

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		if ($_REQUEST['img_value1']) {
			$img1 = str_replace('"', "", $_REQUEST['img_value1']);
		} else {
			$img1 = '';
		}

		$updateDataArray = array(
			"girl_cd"            => $girl_cd,
			"comment"            => $comment,
			"img1"               => $img1,
			"disp_flg"           => $disp_flg,
			"update_record_time" => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);

			break;


		/******************************************************
		更新処理ここまで
		******************************************************/

		/******************************************************
		削除処理ここから
		******************************************************/
		case 'delete':
		
			$recordControlObj->recordDelete($connect, $table_name, $cd);

			//並び順更新
			$recordControlObj->recordSort($connect, $table_name, $kensu_, 'delete');

			break;
		/******************************************************
		削除処理ここまで
		******************************************************/


		/******************************************************
		並び順更新処理ここから
		******************************************************/
		case 'sort':

			$recordControlObj->recordSort($connect, $table_name, $kensu_);

			break;
		/******************************************************
		並び順更新処理ここまで
		******************************************************/

	}
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここまで☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
