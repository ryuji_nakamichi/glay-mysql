<?php


/***************************************
変数定義ここから(名前の似ている変数が一部あるため)
***************************************/

//その日の曜日の数値(0～6まで)を格納
$the_day_week = '';

//カレンダーの空のセルを計算する用
$wd1 = '';
$wdx = '';

//カレンダーのcaptionに表示させる変数
$nowDate = '';

//カレンダーに表示する年
$_year  = '';
$_month = '';
$_day   = '';

//SQL判定用の変数
$month    = '';
$day      = '';
$nowDate_　= '';

//カレンダー移動ボタンのクエリ用変数
$prev_move_year = '';
$next_move_year = '';

//特定の日付)のタイムスタンプ変数
$targetPrevYearTime  = '';
$targetPrevMonthTime = '';

$targetNextYearTime  = '';
$targetNextMonthTime = '';

//特定の日付のタイムスタンプをフォーマットした変数
$prev_move_month = '';

//カレンダーのcaptionの月用変数
$prev_month = '';
$next_month = '';

//セル内のリンク用
$td_link   = '';

//WHERE句用
$WHERESTR = '';
/***************************************
変数定義ここまで(名前の似ている変数一部があるため)
***************************************/


//カレンダー関数
function calender($_year, $_month, $_day, $connect) {

	$_year = date("Y");
	$_day  = 1;
	$day   = 1;


	//その日付が存在するかを判定する(デバッグ用)
	//$judge = checkdate($_month, $_day, $_year);
	

	/***************************************************************
	月の移動ボタンを押したときの年の処理ここから
	***************************************************************/
	if ($_GET['year'] && $_GET['move_mode'] == 'prev') {
		$prev_move_year = $_GET['year'];
		$_year = $prev_move_year;
	} else {
		$prev_move_year = date("Y");
		$_year = $prev_move_year;
	}

	if ($_GET['year'] && $_GET['move_mode'] == 'next') {
		$next_move_year = $_GET['year'];
		$_year = $next_move_year;
	} else {
		$next_move_year = date("Y");
		$_year = $next_move_year;
	}
	/***************************************************************
	月の移動ボタンを押したときの年の処理ここまで
	***************************************************************/


	/***************************************************************
	月の移動ボタンを押したときの月の処理ここから
	***************************************************************/
	//「prev_month(先月)」を押したときの処理
	if ($_GET['month'] && $_GET['move_mode'] == 'prev') {


		//先月に移動する場合の処理ここから
		$targetPrevMonthTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
		$prev_move_month     = date('n', strtotime('-1 month', $targetPrevMonthTime));
		$prev_month          = date('n', $targetPrevMonthTime);


		//先月が去年の12月の場合
		if ($_GET['month'] == '1') {
			$targetPrevYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$prev_move_year     = date('Y', strtotime('-1 year', $targetPrevYearTime));
			$prev_move_month    = date('n', strtotime('-1 month', $targetPrevMonthTime));

			$_year  = $prev_move_year;
			$_month = $prev_month;


		//先月が去年にならない場合			
		} else if ($_GET['month'] != '12') {
			$targetPrevYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$prev_move_year  = date('Y', strtotime('-0 year', $targetPrevYearTime));
			$prev_move_month = date('n', strtotime('-1 month', $targetPrevMonthTime));

			$_year  = $prev_move_year;
			$_month = $prev_month;
		}
		//先月に移動する場合の処理ここまで


		//来月に移動する場合の処理ここから
		$targetNextMonthTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
		$next_move_month     = date('n', strtotime('+1 month', $targetNextMonthTime));
		$next_month          = date('n', $targetNextMonthTime);


		//来月が来年の1月の場合
		if ($_GET['month'] == '12') {
			$targetNextYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+1 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+1 month', $targetNextMonthTime));

			$_year  = $next_move_year;
			$_month = $next_month;


		//来月が来年にならない場合			
		} else if ($_GET['month'] != '1') {
			$targetNextYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+0 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+1 month', $targetNextMonthTime));

			$_year  = $next_move_year;
			$_month = $next_month;
		}
		//来月に移動する場合の処理ここまで




	//初めて「prev_month(先月)」を押したときの処理
	} else if (!$_GET['month'] && $_GET['move_mode'] == 'prev') {


		//先月に移動する場合の処理ここから
		$targetPrevMonthTime = date('n');
		$prev_month = $targetPrevMonthTime;

		//先月が去年の12月の場合
		if ($targetPrevMonthTime == '1') {
			$prev_move_year     = date('Y', strtotime('-1 year', $targetPrevYearTime));
			$prev_move_month    = date('n', strtotime('-1 month', $targetPrevMonthTime));

			$_year  = $prev_move_year;
			$_month = $prev_month;


		//先月が去年にならない場合			
		} else if ($_GET['month'] != '12') {
			$prev_move_year  = date('Y', strtotime('-0 year', $targetPrevYearTime));
			$prev_move_month = date('n', strtotime('-1 month', $targetPrevMonthTime));

			$_year  = $prev_move_year;
			$_month = $prev_month;
		}
		//先月に移動する場合の処理ここまで


	//「next_month(来月)」を押したときの処理
	} else if ($_GET['month'] && $_GET['move_mode'] == 'next') {


		//先月に移動する場合の処理ここから
		$targetPrevMonthTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
		$prev_move_month     = date('n', strtotime('-1 month', $targetPrevMonthTime));
		$prev_month          = date('n', $targetPrevMonthTime);


		//先月が去年の12月の場合
		if ($_GET['month'] == '1') {
			$targetPrevYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$prev_move_year     = date('Y', strtotime('-1 year', $targetPrevYearTime));
			$prev_move_month    = date('n', strtotime('-1 month', $targetPrevMonthTime));

			$_year  = $prev_move_year;
			$_month = $prev_month;


		//先月が去年にならない場合			
		} else if ($_GET['month'] != '12') {
			
			$targetPrevYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$prev_move_year     = date('Y', strtotime('+0 year', $targetPrevYearTime));
			$prev_move_month    = date('n', strtotime('-1 month', $targetPrevMonthTime));

			$_year  = $prev_move_month;
			$_month = $prev_month;
		}
		//先月に移動する場合の処理ここまで


		//来月に移動する場合の処理ここから
		$targetNextMonthTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
		$next_move_month     = date('n', strtotime('+1 month', $targetNextMonthTime));
		$next_month          = date('n', $targetNextMonthTime);


		//来月が来年の1月の場合
		if ($_GET['month'] == '12') {
			$targetNextYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+1 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+1 month', $targetNextMonthTime));

			$_year  = $next_move_year;
			$_month = $next_month;


		//来月が来年にならない場合			
		} else if ($_GET['month'] != '1') {
			$targetNextYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+0 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+1 month', $targetNextMonthTime));

			$_year  = $next_move_year;
			$_month = $next_month;
		}
		//来月に移動する場合の処理ここまで


	//初めて「next_month(来月)」を押したときの処理
	} else if (!$_GET['month'] && $_GET['move_mode'] == 'next') {


		//来月に移動する場合の処理ここから
		$targetNextMonthTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
		$next_move_month     = date('n', strtotime('+1 month', $targetNextMonthTime));
		$next_month          = date('n', $targetNextMonthTime);


		//来月が来年の1月の場合
		if ($_GET['month'] == '12') {
			$targetNextYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+1 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+1 month', $targetNextMonthTime));

			$_year  = $next_move_year;
			$_month = $next_month;


		//来月が来年にならない場合			
		} else if ($_GET['month'] != '1') {
			$targetNextYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+0 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+1 month', $targetNextMonthTime));

			$_year  = $next_move_year;
			$_month = $next_month;
		}
		//来月に移動する場合の処理ここまで


	//「何も押してないとき」&「$_GETに何もパラメータがないとき」の処理
	} else {

		if ($_month == '12') {

			$now_year  = date('Y');
			$now_month = date('m');

			$targetNextYearTime = strtotime("{$now_year}-{$now_month}-1 00:00:00");
			$next_move_year     = date('Y', strtotime('+1 year', $targetNextYearTime));
			$next_move_month    = date('n', strtotime('+0 month', $targetNextMonthTime));

			//$_year  = $next_move_year;
			//$_month = $next_month;

		} else if ($_month == '1') {
//改修中
			$targetPrevYearTime = strtotime("{$_GET['year']}-{$_GET['month']}-1 00:00:00");
			$prev_move_year     = date('Y', strtotime('+0 year', $targetPrevYearTime));
			$prev_move_month    = date('n', strtotime('-1 month', $targetPrevMonthTime));

			//$_year  = $prev_move_month;
			//$_month = $prev_month;

		} else {
			$prev_move_month =  date("n",strtotime("-1 month"));
			$next_move_month =  date("n",strtotime("+1 month"));

			$_month = date("n");
		}

	}
	/***************************************************************
	月の移動ボタンを押したときの月の処理ここまで
	***************************************************************/

	if ($_GET['year']) {
		$_year = $_GET['year'];
	}

	if ($_GET['month']) {
		$_month = $_GET['month'];
	}
	
	//カレンダーcaptionに「〇年〇月」表示する
	$nowDate = $_year.'年'.$_month.'月';

?>

<ul class="moveCarender_list">
	<li><a href="./schedule.php?year=<?=$prev_move_year?>&month=<?=$prev_move_month?>&move_mode=prev"><i class="fa fa-backward" aria-hidden="true"></i>prev_month</a></li>
	<li><a href="?year=<?=$next_move_year?>&month=<?=$next_move_month?>&move_mode=next">next_month<i class="fa fa-forward" aria-hidden="true"></i></a></li>
</ul>

<table class="calender">
	<caption class="month"><?=$nowDate?></caption>

	<tr>
	<th>日</th>
	<th>月</th>
	<th>火</th>
	<th>水</th>
	<th>木</th>
	<th>金</th>
	<th>土</th>
	</tr>

<?php
	echo "<tr>";


	// 月初めの1日の曜日を取得
	$wd1 = date("w", mktime(0, 0, 0, $_month, 1, $_year));


	// その数だけ空白を表示
	for ($i = 1; $i <= $wd1; $i++) {
		echo "<td>　</td>";
	}

	for ($i = 0; $i < 31; $i++) {
		
		if ( checkdate($_month, $_day, $_year) ) {


			//日付を出力
			$the_day_week = date("w", mktime(0, 0, 0, $_month, $_day, $_year) );


			 //曜日の数値でtdにクラスを付与
			 switch ($the_day_week) {
			 	case 0:
			 		$td_class = "class = \"sunday\"";
			 		break;

			 	case 6:
			 		$td_class = "class = \"saturday\"";
			 		break;
			 	
			 	default:
			 		$td_class = "";
			 		break;
			 }


			 //日付をsqlの判定に使うため、日が必ず2桁になるようにする
			$day   = sprintf("%02s", $_day);
			$month = sprintf("%02s", $_month);


			//リンク生成
			$td_link = "<a class=\"calender_link\" href=\"schedule.php?year={$_year}&month={$_month}&day={$_day}\">{$_day}</a>";


			//その日に紐づいたレコードを抽出
			$WHERESTR = "{$_year}-{$month}-{$day}";
			$sql = "SELECT * FROM schedule WHERE disp_flg = 1 AND date = '{$WHERESTR}' ORDER BY sort, update_record_time DESC, new_record_time DESC";
			//echo $sql.'<br>';


			//レコード配列変数初期化(初期化しないと予定を入れた次の日にも同じ内容が入ってしまうことがある)
			$scheduleArray  = array();
			$dataArray      = array();
			$dataArray_     = '';
			$dataArray_str  = '';
			$schedule_query = mysqli_query($connect, $sql);
			$schedule_max   = mysqli_num_rows($schedule_query);


			//配列生成
			for ($j = 0; $j < $schedule_max; $j++) {
				$scheduleArray[] = mysqli_fetch_assoc($schedule_query);

				//内容用配列
				$scheduleArray[$j]['comment'] = nl2br($scheduleArray[$j]['comment']);


				//色用配列
				$colorArray[$j] = $scheduleArray[$j]['color'];


				//セル内に表示させるレコードを連結
				$dataArray[$j][] = "<a class=\"schedule_link\" href=\"./schedule.php?cd={$scheduleArray[$j]['cd']}\"><dl class=\"scheduleList\"><dt style=\"background-color: #{$colorArray[$j]}\">".$scheduleArray[$j]['name'].'</dt>';
				$dataArray[$j][] = '<dd>'.$scheduleArray[$j]['comment'].'</dd></dl></a>';


				//その日のレコードがあれば表示させる
				if ($schedule_max) {
					$dataArray_[] = implode('', $dataArray[$j]);


				//なければ変数を空にする
				} else {
					$dataArray_[]= '';

				}
			}

			if ($schedule_max) {
				foreach ( (array)$dataArray_ AS $key => $val) {
					$dataArray_str .= $val.'<br>';
				}
			} else {
				$dataArray_str = '';
			}

			$nowDate_ = $_year.'-'.$month.'-'.$day;


			 //レコードのdateとカレンダーの日付が同じなら、レコードを表示する
			 if ($WHERESTR == $nowDate_) {
			 	echo "<td {$td_class}>{$td_link}{$dataArray_str}</td>";

			 //違うなら日付のみ表示
			 } else {
			 	echo "<td {$td_class}>{$td_link}</td>";
			 }

			
			//土曜日を表示したら、</tr>で改行
			if ($the_day_week == 6) {
				echo "</tr>";
				
				
				// 次の週がある場合は新たな行を準備
			    if ( checkdate($_month, $_day + 1, $_year) ) {
			        echo "<tr>";
			    }
			
			}


			//ここで日を1ずつプラスさせる
			$_day++;
			$day++;
			
		}
		
	}


	//最後の週の土曜日まで移動
	$wdx = date("w", mktime(0, 0, 0, $_month + 1, 0, $_year));


	//空セルを表示させる
	for ($i = 1; $i < 7 - $wdx; $i++) {
	    echo "<td>　</td>";
	}

	echo "</table>";

}