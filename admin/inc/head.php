<?php if ( strpos($site_url.'login.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>ログイン/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'index.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>HOME/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'news.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>ニュース/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'discography.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>ディスコグラフィー/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'live.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>ライブ情報/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'item.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>商品/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'item_category.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>商品カテゴリー/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'contact.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>問い合わせ/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'seo.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>SEO/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'basic.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>基本情報/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'setting.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>管理画面設定/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'shipping_type.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>商品送料設定/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'property.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>物件/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'area.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>都道府県/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'area_route.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>路線種別/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'area_route_line.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>路線詳細/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'area_route_station.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>駅/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'age.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>築年数/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'commitment.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>こだわり/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'shopping_member.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>会員情報/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'schedule.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>スケジュール/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'top_contents_sort.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>トップページコンテンツ管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>女性キャスト管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl_new_face.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>女性新人キャスト管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl_event.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>女性イベント管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl_schedule.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>女性キャスト出勤管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl_new_face.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>女性新人キャスト管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl_schedule_time.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>女性キャスト出勤時刻管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'play_option.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>オプション管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'nomination_fee.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>指名料管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'favorite_posture.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>体位管理/<?=$user_name?></title>
<?php } else if ( strpos($site_url.'girl_rank.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<title>ランク管理/<?=$user_name?></title>
<?php } else { ?>
<title>データ管理/<?=$user_name?></title>
<?php } ?>

<meta http-equiv=”X-UA-Compatible”content=”IE=Edge,chrome=1>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scaleble=no">

<link href="./css/reset.css" rel="stylesheet">
<link href="./css/common.css" rel="stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="./plugin/jquery-ui.css" rel="stylesheet">
<link href="./css/jquery.mCustomScrollbar.css" rel="stylesheet">

<script src="./js/jquery-1.11.0.min.js"></script>
<script src="./js/common.js"></script>
<script src="./js/tinymce/js/tinymce/tinymce.min.js"></script>

<script src="./plugin/external/jquery/jquery.js"></script>
<script src="./plugin/jquery-ui.js"></script>
<script src="./js/loading.js"></script>
<?php if ( strpos($site_url.'news.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'discography.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'item.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'item_category.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'property.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'schedule.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'girl.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'girl_new_face.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'girl_event.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/img_upload_control.js"></script>
<?php } ?>
<?php if ( strpos($site_url.'login.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/login_control.js"></script>
<?php } else if ( strpos($site_url.'news.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/news_control.js"></script>
<?php } else if ( strpos($site_url.'discography.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/discography_control.js"></script>
<?php } else if ( strpos($site_url.'live.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/live_control.js"></script>
<?php } else if ( strpos($site_url.'item.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/item_control.js"></script>
<?php } else if ( strpos($site_url.'item_category.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/item_category_control.js"></script>
<?php } else if ( strpos($site_url.'contact.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/contact_control.js"></script>
<script src="./js/jquery.autoKana.js"></script>
<script>
$(function() {
	$.fn.autoKana('#name',  '#kana', { katakana : false });
});
</script>
<?php } else if ( strpos($site_url.'seo.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/seo_control.js"></script>
<?php } else if ( strpos($site_url.'basic.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/basic_control.js"></script>
<?php } else if ( strpos($site_url.'setting.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/setting_control.js"></script>
<?php } else if ( strpos($site_url.'shipping_type.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/shipping_type_control.js"></script>
<?php } else if ( strpos($site_url.'property.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/property_control.js"></script>
<script src="./ajax/property_combo.js"></script>
<?php } else if ( strpos($site_url.'area.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/area_control.js"></script>
<?php } else if ( strpos($site_url.'area_route.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/area_route_control.js"></script>
<?php } else if ( strpos($site_url.'area_route_line.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/area_route_line_control.js"></script>
<script src="./ajax/area_route_line_combo.js"></script>
<?php } else if ( strpos($site_url.'area_route_station.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/area_route_station_control.js"></script>
<script src="./ajax/area_route_station_combo.js"></script>
<?php } else if ( strpos($site_url.'age.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/age_control.js"></script>
<?php } else if ( strpos($site_url.'commitment.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/commitment_control.js"></script>
<?php } else if ( strpos($site_url.'shopping_member.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./js/jquery.jpostal.js"></script>
<script src="./js/jquery.autoKana.js"></script>
<script src="./ajax/shopping_member_control.js"></script>
<?php } else if ( strpos($site_url.'job_type.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/job_type_control.js"></script>
<?php } else if ( strpos($site_url.'schedule.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/schedule_control.js"></script>
<script src="./js/jscolor.min.js"></script>
<?php } else if ( strpos($site_url.'top_contents_sort.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/top_contents_sort_control.js"></script>
<?php } else if ( strpos($site_url.'girl.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_control.js"></script>
<?php } else if ( strpos($site_url.'girl_new_face.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_new_face_control.js"></script>
<?php } else if ( strpos($site_url.'girl_event.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_event_control.js"></script>
<?php } else if ( strpos($site_url.'girl_schedule.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_schedule_control.js"></script>
<?php } else if ( strpos($site_url.'girl_schedule_time.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_schedule_time_control.js"></script>
<?php } else if ( strpos($site_url.'girl_rank.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_rank_control.js"></script>
<?php } else if ( strpos($site_url.'girl_type.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/girl_type_control.js"></script>
<?php } else if ( strpos($site_url.'play_option.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/play_option_control.js"></script>
<?php } else if ( strpos($site_url.'nomination_fee.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/nomination_fee_control.js"></script>
<?php } else if ( strpos($site_url.'favorite_posture.php', $_SERVER['SCRIPT_NAME']) ) { ?>
<script src="./ajax/favorite_posture_control.js"></script>
<?php } ?>

<?php if ( strpos($site_url.'news.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'discography.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'live.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'item.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'contact.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'property.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'schedule.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'girl.php', $_SERVER['SCRIPT_NAME']) ) { ?>

<script>
	$(function(){
		var dateFormat = 'yy-mm-dd';
		$("#datepicker, #entry_date").datepicker({
			//inline: true
			inline: true,
			dateFormat: dateFormat
		});
	});
</script>
<?php } ?>

<script src="./js/jquery.mCustomScrollbar.js"></script>
<script src="./js/jquery.uploadThumbs.js"></script>
<script>

$(function() {

    <?php
        if ( strpos($site_url.'area_route.php', $_SERVER['SCRIPT_NAME']) ) {
            $listSort = '.listSort';
        } else {
            $listSort = '#listSort';
        }
    ?>

    //並び順変更記述ここから
    $("<?=$listSort?>").sortable({
        update: function(event, ui) {
            event; // イベントオブジェクト

            ui; // UIオブジェクト


            //ページャーを跨ぐ場合でもsortを合わせる
            var cnt = <?=$from?> + 1;

            var target = $(this).find('.sort');

            $.each( target, function() {


                //直前のtr(レコード)が存在するかでsortの値の初期化の処理を実行
                if ( !$(this).parent('td').parent('tr').prev().length ) {
                    cnt = <?=$from?> + 1;
                    console.log($(this).parent('td').parent('tr').prev());
                }
                

                $(this).attr('id', 'sort' + cnt).val(cnt);
                cnt++;
            });

            $('.sortAttention').text('「並び替え」ボタンを押さないと並び順は変更されませんので、ご注意ください。');

        }
    });
    //並び順変更記述ここまで

    <?php if ( strpos($site_url.'item.php', $_SERVER['SCRIPT_NAME']) || strpos($site_url.'property.php', $_SERVER['SCRIPT_NAME']) ) { ?>
    //半角数字チェック(半角数字以外削除)ここから
    $('#price').blur(function() {

        var str = $(this).val();

        if ( str.match( /[^0-9]+/ ) ) {
            
            alert('半角数字をご入力ください。');
            $(this).val(str.replace(/[^0-9]/g, ""));

            return false;
        }

    });
    //半角数字チェック(半角数字以外削除)ここまで
    <?php } ?>

    <?php if ( strpos($site_url.'shopping_member.php', $_SERVER['SCRIPT_NAME']) ) { ?>
    //半角数字チェック(半角数字以外削除)ここから
    $('#post_code1').blur(function() {

        var str = $(this).val();

        if ( str.match( /[^0-9]+/ ) ) {
            
            alert('半角数字をご入力ください。');
            $(this).val(str.replace(/[^0-9]/g, ""));

            return false;
        }

    });

    $('#post_code2').blur(function() {

        var str = $(this).val();

        if ( str.match( /[^0-9]+/ ) ) {
            
            alert('半角数字をご入力ください。');
            $(this).val(str.replace(/[^0-9]/g, ""));

            return false;
        }

    });
    //半角数字チェック(半角数字以外削除)ここまで
    <?php } ?>


    <?php if ( strpos($site_url.'setting.php', $_SERVER['SCRIPT_NAME']) ) { ?>

        $(".item_tax, .shipping_cost, .shipping_cost_free, .add_point, .add_point_increment, .img_width, .img_height, .img_thum_width_l, .img_thum_height_l, .img_thum_width_s, .img_thum_height_s").spinner();


        //半角数字チェック(半角数字以外削除)ここから
        $('#item_tax, #shipping_cost, #shipping_cost_free, #add_point, #add_point_increment, #img_width, #img_height, #img_thum_width_l, #img_thum_height_l, #img_thum_width_s, #img_thum_height_s').on ('change', function() {

            var str = $(this).val();

            if ( str.match( /[^0-9]+/ ) ) {
                
                alert('半角数字をご入力ください。');
                $(this).val(str.replace(/[^0-9]/g, ""));

                return false;
            }

        });

    <?php } ?>

    <?php if ( strpos($site_url.'shopping_member.php', $_SERVER['SCRIPT_NAME']) ) { ?>

        $(".point").spinner();


        //半角数字チェック(半角数字以外削除)ここから
        $('#point').on ('change', function() {

            var str = $(this).val();

            if ( str.match( /[^0-9]+/ ) ) {
                
                alert('半角数字をご入力ください。');
                $(this).val(str.replace(/[^0-9]/g, ""));

                return false;
            }

        });

    <?php } ?>

});
</script>

<!--[if lt IE 9]>
<script src="./js/html5shiv.min.js"></script>
<![endif]-->