<div id="login_dialog" class="dialog" title="確認" style="display: none;">
	<p>ログイン処理を開始しますか？</p>
</div>

<div id="login_complete_dialog" class="dialog" title="ログイン処理中" style="display: none;">
	<p>ログイン処理が完了しました。</p>
</div>

<div id="insert_dialog" class="dialog" title="登録確認" style="display: none;">
	<p>データの登録を行います。<br />よろしいでしょうか？</p>
</div>

<div id="insert_complete_dialog" class="dialog" title="登録完了確認" style="display: none;">
	<p>データの登録をしました。</p>
</div>

<div id="update_dialog" class="dialog" title="編集確認" style="display: none;">
	<p>データの編集を行います。<br />よろしいでしょうか？</p>
</div>

<div id="update_complete_dialog" class="dialog" title="編集完了確認" style="display: none;">
	<p>データの編集をしました。</p>
</div>

<div id="delete_dialog" class="dialog" title="削除確認" style="display: none;">
	<p>データの削除を行います。<br />よろしいでしょうか？</p>
</div>

<div id="delete_complete_dialog" class="dialog" title="削除完了確認" style="display: none;">
	<p>データの削除をしました。</p>
</div>

<div id="sort_dialog" class="dialog" title="並び替え確認" style="display: none;">
	<p>並び替えを行います。<br />よろしいでしょうか？</p>
</div>

<div id="sort_complete_dialog" class="dialog" title="並び替え完了確認" style="display: none;">
	<p>並び替えの編集をしました。</p>
</div>

<div id="csv_download_dialog" class="dialog" title="登録確認" style="display: none;">
	<p>CSVのダウンロードを行います。<br />よろしいでしょうか？</p>
</div>

<div id="csv_download_complete_dialog" class="dialog" title="登録完了確認" style="display: none;">
	<p>CSVのダウンロードをしました。</p>
</div>