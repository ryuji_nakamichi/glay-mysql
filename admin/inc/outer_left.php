<!--outerLeftBox-->
<div class="outerLeftBox">

	<!--innerLeftBox1-->
	<div class="innerLeftBox1">

		<ul class="iconList">
				<li><a href="#"><i class="fa fa-archive"></i></a></li>
				<li><a href="#"><i class="fa fa-cube"></i></a></li>
				<li><a href="#"><i class="fa fa-bell"></i></a></li>
				<li><a href="#"><i class="fa fa-bug"></i></a></li>
				<li><a href="#"><i class="fa fa-certificate"></i></a></li>
				<li><a href="#"><i class="fa fa-long-arrow-up"></i></a></li>
				<li><a href="#"><i class="fa fa-phone"></i></a></li>
				<li><a href="#"><i class="fa fa-envelope"></i></a></li>

				<li><a href="#"><i class="fa fa-archive"></i></a></li>

			</ul>
	</div>
	<!--innerLeftBox1-->

	<!--innerLeftBox2-->
	<div class="innerLeftBox2">

		<!--header-->
		<?php require_once './inc/header.php'; ?>
		<!--header-->

		<!--navi-->
		<?php require_once './inc/navi.php'; ?>
		<!--/navi-->

	</div>
	<!--innerLeftBox2-->

</div>
<!--outerLeftBox-->