<?php

// ページャー(数字)
function pagerNum($maxpage, $page, $request = '') {
	$pagelink = '';

	if ($maxpage > 1) {
	    $pagelink .= "<ol class=\"pager cd mB30\">\n";

	    $back = ($page) ? $page-1 : '';
	    //if ($back) $pagelink .= "<li><a href=\"{$_SERVER['SCRIPT_NAME']}?page={$back}{$request}\">&lt;&lt;前のページ</a></li>\n";

	    $page_max = $maxpage;

		/*
	    // 5件表示
	    if ($maxpage >= 5) $maxpage = 5;
	    if ( ($page) && ($page_max >= 5) ) {
	        $start_num = ( ($page) >= $page_max ) ? $page_max-5 : $page-3;
	        $start_num = ($start_num >= 1) ? $start_num : 0;
	    } else {
	        $start_num = 0;
	    }
		*/

		// 10件表示
	    if ($maxpage >= 10) $maxpage = 10;
	    if ( ($page >= 5) && ($page_max >= 10) ) {
	        $start_num = ( ($page+5) >= $page_max ) ? $page_max-10 : $page-5;
	    } else {
	        $start_num = 0;
	    }

	    for ($i = 1; $i <= $maxpage; $i++) {
	        $start_num++;
	        $pagelink .= "<li>";

	        if ($start_num == $page) {
	            $pagelink .= "<em>{$start_num}</em>";
	            $pagelink .= "</li>\n";

	        } else {
	            $pagelink .= "<a href=\"{$_SERVER['SCRIPT_NAME']}?page={$start_num}{$request}\">{$start_num}</a>";
	            $pagelink .= "</li>\n";
	        }
	    }

	    $next = ($page) ? $page+1 : 2;
	    //if ($page_max >= $next) $pagelink .= "<li><a href=\"{$_SERVER['SCRIPT_NAME']}?page={$next}{$request}\">次のページ&gt;&gt;</a></li>\n";
	    $pagelink .= "</ol>\n";
	}

	return $pagelink;
}