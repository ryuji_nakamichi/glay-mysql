<div class="spModalBox">

	<div id="slideBtn">

		<?php if ($_SESSION['login']["login_flg_{$session_user_name}"] && $_SESSION['login']["login_flg_{$session_user_name}"] == 1) { ?>
		<p class="user_name"><a href="./"><?=$user_name?>管理画面</a></p>

		<a id="naviOpen" class="menu-trigger" href="#">
			<span></span>
			<span></span>
			<span></span>
		</a>
		<?php } ?>

	</div>

	<?php if ($_SESSION['login']["login_flg_{$session_user_name}"] && $_SESSION['login']["login_flg_{$session_user_name}"] == 1) { ?>
	<nav class="slideNavi">
		<ul>
			<li><a href="<?=$site_url_hp?>" target="_blank"><i class="fa fa-check" aria-hidden="true"></i>サイト確認</a></li>
			<li><a href="./"><i class="fa fa-home" aria-hidden="true"></i>HOME</a></li>
			<li><a href="./news.php"><i class="fa fa-newspaper-o" aria-hidden="true"></i>ニュース</a></li>
			<li><a href="./discography.php"><i class="fa fa-music" aria-hidden="true"></i>ディスコグラフィー</a></li>
			<li><a href="./live.php"><i class="fa fa-microphone" aria-hidden="true"></i>ライブ情報</a></li>
			<li><a href="./item_category.php"><i class="fa fa-share-alt" aria-hidden="true"></i>商品カテゴリー</a></li>
			<li><a href="./item.php"><i class="fa fa-shopping-bag" aria-hidden="true"></i>商品</a></li>
			<li><a href="./contact.php"><i class="fa fa-envelope" aria-hidden="true"></i>問い合わせ</a></li>
			<li><a href="./seo.php"><i class="fa fa-cog" aria-hidden="true"></i>SEO</a></li>
			<li><a href="./basic.php"><i class="fa fa-cogs" aria-hidden="true"></i>基本情報管理</a></li>
			<li><a href="./setting.php"><i class="fa fa-cogs" aria-hidden="true"></i>管理画面設定</a></li>
			<li><a href="./shipping_type.php"><i class="fa fa-cogs" aria-hidden="true"></i>商品配送設定</a></li>
			<li><a href="./property.php"><i class="fa fa-search" aria-hidden="true"></i>物件</a></li>
			<li><a href="./area.php"><i class="fa fa-search" aria-hidden="true"></i>都道府県登録</a></li>
			<li><a href="./area_route.php"><i class="fa fa-search" aria-hidden="true"></i>路線種別登録</a></li>
			<li><a href="./area_route_line.php"><i class="fa fa-search" aria-hidden="true"></i>路線詳細</a></li>
			<li><a href="./area_route_station.php"><i class="fa fa-search" aria-hidden="true"></i>駅登録</a></li>
			<li><a href="./age.php"><i class="fa fa-search" aria-hidden="true"></i>築年数</a></li>
			<li><a href="./commitment.php"><i class="fa fa-search" aria-hidden="true"></i>こだわり</a></li>
			<li><a href="./shopping_member.php"><i class="fa fa-user-circle" aria-hidden="true"></i>会員情報</a></li>
			<li><a href="./job_type.php"><i class="fa fa-user-circle" aria-hidden="true"></i>職業</a></li>
			<li><a href="./schedule.php"><i class="fa fa-calendar" aria-hidden="true"></i>スケジュール</a></li>
			<li><a href="./csv.php"><i class="fa fa-file" aria-hidden="true"></i>CSV管理</a></li>
			<li><a href="./top_contents_sort.php"><i class="fa fa-file" aria-hidden="true"></i>トップページ<br>コンテンツ管理</a></li>
			<li><a href="./girl.php"><i class="fa fa-file" aria-hidden="true"></i>女性キャスト管理</a></li>
			<li><a href="./girl_new_face.php"><i class="fa fa-file" aria-hidden="true"></i>女性キャスト新人管理</a></li>
			<li><a href="./girl_event.php"><i class="fa fa-file" aria-hidden="true"></i>女性イベント管理</a></li>
			<li><a href="./girl_schedule.php"><i class="fa fa-file" aria-hidden="true"></i>女性キャスト出勤管理</a></li>
			<li><a href="./girl_schedule_time.php"><i class="fa fa-file" aria-hidden="true"></i>女性キャスト出勤時刻管理</a></li>
			<li><a href="./girl_rank.php"><i class="fa fa-file" aria-hidden="true"></i>ランク管理</a></li>
			<li><a href="./play_option.php"><i class="fa fa-file" aria-hidden="true"></i>オプション管理</a></li>
			<li><a href="./girl_type.php"><i class="fa fa-file" aria-hidden="true"></i>タイプ管理</a></li>
			<li><a href="./nomination_fee.php"><i class="fa fa-file" aria-hidden="true"></i>指名料管理</a></li>
			<li><a href="./favorite_posture.php"><i class="fa fa-file" aria-hidden="true"></i>体位管理</a></li>
		</ul>
		</ul>
	</nav>
	<?php } ?>

</div>