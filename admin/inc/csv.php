<?php


if ($_POST['csvExport']) {


	//クロスサイドスクリプティング対策
	foreach ($_REQUEST as $key => $val) {

		if ( !is_array($val) ){
			$_REQUEST[$key] = htmlspecialchars($val);
			$_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
		}

	}

	if ($_POST['table_name']) {
		$table_name = $_POST['table_name'];
	}
	

	//データ取得
	$_SELECT     = "*";
	$_WHERE      = "";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} ORDER BY sort, cd";
	$csv_ex_query = mysqli_query($connect, $sql);
	$csv_ex_max   = mysqli_num_rows($csv_ex_query);

	for ($i = 0; $i < $csv_ex_max; $i++) {
		$csv_exArray[$i] = mysqli_fetch_assoc($csv_ex_query);
	}


	//CSVエクスポート
	$csvControllObj    = new csvControll;
	$csv_ex_data_query = $csvControllObj->csvExport($table_name, $csv_exArray);

}



//CSVインポート
if ($_POST['csvInport']) {

	if ($_FILES["csv_file_up"]['tmp_name']) {

		if ($_POST['table_name']) {
			$table_name = $_POST['table_name'];
		}

		$csvControllObj   = new csvControll;
		$csv_upload_query = $csvControllObj->csvUpload($table_name);

		if ($csv_upload_query) {
			$csv_inport_query = $csvControllObj->csvInport($connect, $table_name);
		}

		if($csv_inport_query === true) {
			$csv_inport_flg = 'success';
		} else if ($csv_inport_query === false) {
			$csv_inport_flg = 'faild';
		}
		
	} else {
		$csv_inport_flg = 'csv_file_none';

		header("Location: {$_SERVER['PHP_SELF']}?csv_inport_flg={$csv_inport_flg}");
		exit;
	}

	header("Location: {$_SERVER['PHP_SELF']}?csv_inport_flg={$csv_inport_flg}&table_name={$table_name}");
	exit;

}