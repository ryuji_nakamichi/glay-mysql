<?php

function calender ($_year, $_month, $_day, $connect) {

	echo $_year.'年'.$_month.'月'.'の出勤表';
	$_day = date('d');


	//女性キャスト取得
	$_SELECT         = "cd, name";
	$sql             = "SELECT {$_SELECT} FROM girl ORDER BY sort";
	$girl_cast_query = mysqli_query($connect, $sql);
	$girl_cast_max   = mysqli_num_rows($girl_cast_query);

	$girl_cdArray    = array();

	for ($i = 0; $i < $girl_cast_max; $i++) {
		$girl_castArray[] = mysqli_fetch_assoc($girl_cast_query);
		$girl_cdArray[]   = $girl_castArray[$i]['cd'];
	}


	//スケジュール取得
	$_SELECT = "cd, girl_cd, work_date, work_start_time, work_end_time";
	$sql     = "SELECT {$_SELECT} FROM girl_schedule ORDER BY work_date";
	$girl_schedule_query = mysqli_query($connect, $sql);
	$girl_schedule_max   = mysqli_num_rows($girl_schedule_query);

	$girl_scheduleAllArray = array();

	for ($i = 0; $i < $girl_schedule_max; $i++) {
		$girl_scheduleArray[] = mysqli_fetch_assoc($girl_schedule_query);
		$girl_scheduleAllArray[$girl_scheduleArray[$i]['girl_cd']][] = $girl_scheduleArray[$i];
	}

	$girl_scheduleAllArrayMax = count($girl_scheduleAllArray);


	//時刻取得
	$_SELECT = "cd, name";
	$sql     = "SELECT {$_SELECT} FROM girl_schedule_time ORDER BY sort";
	$girl_schedule_time_query = mysqli_query($connect, $sql);
	$girl_schedule_time_max   = mysqli_num_rows($girl_schedule_time_query);

	for ($i = 0; $i < $girl_schedule_time_max; $i++) {
		$girl_schedule_timeArray[] = mysqli_fetch_assoc($girl_schedule_time_query);
	}

	// echo '<pre>';
	// print_r($girl_cdArray);
	// echo '</pre>';
?>

	<table>
		<tr>
			<td class="nameCell">キャスト</td>
			
				
	<?php
	for ($i = 0; $i < 31; $i++) {
		
		if ( checkdate($_month, $_day, $_year) ) {


			//日付を出力
			$the_day_week = date("w", mktime(0, 0, 0, $_month, $_day, $_year) );


			 // //曜日の数値でtdにクラスを付与
			 // switch ($the_day_week) {
			 // 	case 0:
			 // 		$td_class = "class = \"sunday\"";
			 // 		break;

			 // 	case 6:
			 // 		$td_class = "class = \"saturday\"";
			 // 		break;
			 	
			 // 	default:
			 // 		$td_class = "";
			 // 		break;
			 // }


			 //日付をsqlの判定に使うため、日が必ず2桁になるようにする
			// $day   = sprintf("%02s", $_day);
			// $month = sprintf("%02s", $_month);


			//リンク生成
			// $td_link = "<a class=\"calender_link\" href=\"schedule.php?year={$_year}&month={$_month}&day={$_day}\">{$_day}</a>";


			//その日に紐づいたレコードを抽出
			// $WHERESTR = "{$_year}-{$month}-{$day}";
			// $sql = "SELECT * FROM schedule WHERE disp_flg = 1 AND date = '{$WHERESTR}' ORDER BY sort, update_record_time DESC, new_record_time DESC";
			//echo $sql.'<br>';


			//レコード配列変数初期化(初期化しないと予定を入れた次の日にも同じ内容が入ってしまうことがある)
			// $scheduleArray  = array();
			// $dataArray      = array();
			// $dataArray_     = '';
			// $dataArray_str  = '';
			// $schedule_query = mysqli_query($connect, $sql);
			// $schedule_max   = mysqli_num_rows($schedule_query);


			//配列生成
			// for ($j = 0; $j < $schedule_max; $j++) {
			// 	$scheduleArray[] = mysqli_fetch_assoc($schedule_query);

			// 	//内容用配列
			// 	$scheduleArray[$j]['comment'] = nl2br($scheduleArray[$j]['comment']);


			// 	//色用配列
			// 	$colorArray[$j] = $scheduleArray[$j]['color'];


			// 	//セル内に表示させるレコードを連結
			// 	$dataArray[$j][] = "<a class=\"schedule_link\" href=\"./schedule.php?cd={$scheduleArray[$j]['cd']}\"><dl class=\"scheduleList\"><dt style=\"background-color: #{$colorArray[$j]}\">".$scheduleArray[$j]['name'].'</dt>';
			// 	$dataArray[$j][] = '<dd>'.$scheduleArray[$j]['comment'].'</dd></dl></a>';


			// 	//その日のレコードがあれば表示させる
			// 	if ($schedule_max) {
			// 		$dataArray_[] = implode('', $dataArray[$j]);


			// 	//なければ変数を空にする
			// 	} else {
			// 		$dataArray_[]= '';

			// 	}
			// }

			// if ($schedule_max) {
			// 	foreach ( (array)$dataArray_ AS $key => $val) {
			// 		$dataArray_str .= $val.'<br>';
			// 	}
			// } else {
			// 	$dataArray_str = '';
			// }

			// $nowDate_ = $_year.'-'.$month.'-'.$day;


			 //レコードのdateとカレンダーの日付が同じなら、レコードを表示する
			 // if ($WHERESTR == $nowDate_) {
			 // 	echo "<td {$td_class}>{$td_link}{$dataArray_str}</td>";

			 // //違うなら日付のみ表示
			 // } else {
			 // 	echo "<td {$td_class}>{$td_link}</td>";
			 // }

			
			//土曜日を表示したら、</tr>で改行
			// if ($the_day_week == 6) {
			// 	echo "</tr>";
				
				
			// 	// 次の週がある場合は新たな行を準備
			//     if ( checkdate($_month, $_day + 1, $_year) ) {
			//         echo "<tr>";
			//     }
			
			// }


			echo '<td class="dateCell">';
			echo ($i + 1).'日';
			echo '</td>';

			//ここで日を1ずつプラスさせる
			// $_day++;
			// $day++;
			
		}
		
	}
	?>

			
		</tr>
	
		<?php foreach ( (array) $girl_castArray AS $key => $val ) { ?>
		<tr>
			<td class="girl_name">
				<span class="girl_cd"><?=$val['cd']?></span><br>
				<span class="girl_name"><?=$val['name']?></span>
				<input type="hidden" id="girl_cd" name="girl_cd[]" value="<?=$val['cd']?>">
			</td>
			<?php for ($i = 0; $i < 31; $i++) {
				
				$_count = 1;

				if ( checkdate($_month, $_day, $_year) ) {
				?>
				<td class="girl_work_hour">
	
					<select class="girl_work_hour_start" name="girl_work_hour_start<?=$val['cd']?>_<?=$_count?>[]">
						<?php for ($j = 0; $j < $girl_schedule_time_max; $j++) { ?>
						<option value="<?=$girl_schedule_timeArray[$j]['cd']?>" <?php echo ($girl_schedule_timeArray[$j]['cd'] == $girl_scheduleAllArray[$val['cd']][$i]['work_start_time']) ? 'selected': ''; ?> ><?=$girl_schedule_timeArray[$j]['name']?></option>
						<?php } ?>

					</select>
					<span>～</span>
					<select class="girl_work_hour_end" name="girl_work_hour_end<?=$val['cd']?>_<?=$_count?>[]">

						<?php for ($j = 0; $j < $girl_schedule_time_max; $j++) { ?>
						<option value="<?=$girl_schedule_timeArray[$j]['cd']?>" <?php echo ($girl_schedule_timeArray[$j]['cd'] == $girl_scheduleAllArray[$val['cd']][$i]['work_end_time']) ? 'selected': ''; ?> ><?=$girl_schedule_timeArray[$j]['name']?></option>
						<?php

					?>
						<?php } ?>

					</select>
					
				</td>
				<?php } ?>
				<?php $_count++; ?>
			<?php } ?>
		</tr>
		<?php } ?>
	</table>

<?php
}
?>