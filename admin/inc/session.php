<?php

define("SESSIONUSERNAME", "GLAYADMIN");


//セッションに名前を付ける
session_name(SESSIONUSERNAME.'PHPSESSID');


//SESSION開始
session_start();


//セッションID(PHPSESSID)を毎回変更する（セッションハイジャック対策）
session_regenerate_id(true);


//sessionユーザー名
$session_user_name = SESSIONUSERNAME;
