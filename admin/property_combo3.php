<?php

require_once '../inc/db.php';
require_once './class/class.php';


//area_route_stationコンボボックス取得ここから
if ($_REQUEST['area_route_line'] && $_REQUEST['area_route_line'] != '') {

	if ( ctype_digit(strval($_REQUEST['area_route_line'])) ) {
		$area_route_line_combo_cd = $_REQUEST['area_route_line'];
	}

	$sql                          = "SELECT cd, name FROM area_route_station WHERE area_route_line_cd = {$area_route_line_combo_cd} ORDER BY sort";
	$area_route_line_combo_query  = mysqli_query($connect, $sql);
	$area_route_line_combo_max    = mysqli_num_rows($area_route_line_combo_query);

	if ($area_route_line_combo_max) {

		for ($i = 0; $i < $area_route_line_combo_max; $i++) {
			$area_route_line_comboArray[] = mysqli_fetch_assoc($area_route_line_combo_query);
		}

		foreach ( (array)$area_route_line_comboArray AS $key => $value ) {
			$option .= "<option value=\"".$value['cd']."\">" .$value['name']. "</option>\n" ;
		}

		echo $option;

	} else {
		$area_route_line_comboArray = "";
	}	

}
//area_route_stationコンボボックス取得ここまで
