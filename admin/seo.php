<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'seo';
$table_seq_name = 'information_schema.tables';
$img_max        = 0;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//SEO(編集用)取得
	$_SELECT     = "cd, name, path, disp_flg, keyword, description, title";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$seo_query2  = mysqli_query($connect,$sql);
	$seo_max2    = mysqli_num_rows($seo_query2);

	for ($i = 0; $i < $seo_max2; $i++) {
		$seoArray2[$i] = mysqli_fetch_assoc($seo_query2);
	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}




//SEO取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, name, path, disp_flg, keyword, description, title, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql       = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$seo_query = mysqli_query($connect,$sql);
$seo_max   = mysqli_num_rows($seo_query);

for ($i = 0; $i < $seo_max; $i++) {
	$seoArray[$i] = mysqli_fetch_assoc($seo_query);
}
//SEO取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($seo_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


/*
echo '<pre>';
print_r($test);
echo '</pre>';
*/


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="seo" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="name">ページ名</label></th>
										<td>
											<p id="nameError"></p>
											<input id="name" type="text" name="name" value="<?=$seoArray2[0]['name']?>">
										</td>
									</tr>

									<tr>
										<th><label for="path">パス</label></th>
										<td>
											<p id="pathError"></p>
											<input type="text" id="path" name="path" value="<?=$seoArray2[0]['path']?>">
										</td>
									</tr>

									<tr>
										<th><label for="title">タイトル</label></th>
										<td>
											<p id="titleError"></p>
											<input type="text" id="title" name="title" value="<?=$seoArray2[0]['title']?>">
										</td>
									</tr>

									<tr>
										<th><label for="keyword">キーワード</label></th>
										<td>
											<textarea cols="40" rows="4" id="keyword" name="keyword"><?=$seoArray2[0]['keyword']?></textarea>
										</td>
									</tr>

									<tr>
										<th><label for="description">ディスクリプション</label></th>
										<td>
											<textarea cols="40" rows="4" id="description" name="description"><?=$seoArray2[0]['description']?></textarea>
										</td>
									</tr>
									
									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($seoArray2[0]['disp_flg'] == 1) ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="seo.php?cd=<?=$seoArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">
				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($seo_max) { ?>
						<form id="seo_del" method="POST">
							<div class="table-wrap">
								<table class="adminTable updateTable">
									<thead>
										<tr>
											<th>ページ名</th>
											<th>パス</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">
										<?php for ($i = 0; $i < $seo_max; $i++) { ?>
											<?php

											if ($seoArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											?>
										<tr <?=$bg_color?> >
											<td><?=$seoArray[$i]['name']?></td>
											<td><?=$seoArray[$i]['path']?></td>
											<td>
												<a class="update_button" href="seo.php?cd=<?=$seoArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="seo.php?cd=<?=$seoArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$seoArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$seoArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="6">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>
						
						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
					</div>
				</div>
			</div>
	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>