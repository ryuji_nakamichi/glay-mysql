<?php

require_once '../inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "shopping_member";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);


	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここから☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/
	switch ($_REQUEST['mode']) {


		/******************************************************
		登録処理ここから
		******************************************************/
		case 'insert':

		$name1        = $_REQUEST['name1'];
		$name2        = $_REQUEST['name2'];

		$kana1        = $_REQUEST['kana1'];
		$kana2        = $_REQUEST['kana2'];
		$company_name = $_REQUEST['company_name'];
		$birth_year   = $_REQUEST['birth_year'];
		$birth_month  = $_REQUEST['birth_month'];
		$birth_day    = $_REQUEST['birth_day'];

		if ( ctype_digit( strval($_REQUEST['sex']) ) ) {
			$sex = $_REQUEST['sex'];
		}

		if ($_REQUEST['job_type']) {
			$job_type = $_REQUEST['job_type'];
		}

		if ($_REQUEST['post_code1']) {
			$post_code1 = $_REQUEST['post_code1'];
		}

		if ($_REQUEST['post_code2']) {
			$post_code2 = $_REQUEST['post_code2'];
		}

		$address            = $_REQUEST['address'];
		$tel                = $_REQUEST['tel'];
		$fax                = $_REQUEST['fax'];
		$mail               = $_REQUEST['mail'];
		$phone_mail_address = $_REQUEST['phone_mail_address'];
		$user_id            = $_REQUEST['user_id'];
		$password           = $_REQUEST['password'];

		if ( is_numeric($_REQUEST['point']) ) {
			$point = $_REQUEST['point'];
		}

		$sort = 0;

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		$insertDataArray = array(
			"name1"              => $name1,
			"name2"              => $name2,
			"kana1"              => $kana1,
			"kana2"              => $kana2,
			"company_name"       => $company_name,
			"birth_year"         => $birth_year,
			"birth_month"        => $birth_month,
			"birth_day"          => $birth_day,
			"sex"                => $sex,
			"job_type"           => $job_type,
			"post_code1"         => $post_code1,
			"post_code2"         => $post_code2,
			"address"            => $address,
			"tel"                => $tel,
			"fax"                => $fax,
			"mail"               => $mail,
			"phone_mail_address" => $phone_mail_address,
			"user_id"            => $user_id,
			"password"           => $password,
			"point"              => $point,
			"disp_flg"           => $disp_flg,
			"sort"               => $sort,
			"new_record_time"    => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


		//並び順更新
		$recordControlObj->recordInsertSort($connect, $table_name);

			break;


		/******************************************************
		登録処理ここまで
		******************************************************/


		/******************************************************
		更新処理ここから
		******************************************************/
		case 'update':

		$name1        = $_REQUEST['name1'];
		$name2        = $_REQUEST['name2'];

		$kana1        = $_REQUEST['kana1'];
		$kana2        = $_REQUEST['kana2'];
		$company_name = $_REQUEST['company_name'];
		$birth_year   = $_REQUEST['birth_year'];
		$birth_month  = $_REQUEST['birth_month'];
		$birth_day    = $_REQUEST['birth_day'];

		if ( ctype_digit( strval($_REQUEST['sex']) ) ) {
			$sex = $_REQUEST['sex'];
		}

		if ($_REQUEST['job_type']) {
			$job_type = $_REQUEST['job_type'];
		}

		if ($_REQUEST['post_code1']) {
			$post_code1 = $_REQUEST['post_code1'];
		}

		if ($_REQUEST['post_code2']) {
			$post_code2 = $_REQUEST['post_code2'];
		}

		$address            = $_REQUEST['address'];
		$tel                = $_REQUEST['tel'];
		$fax                = $_REQUEST['fax'];
		$mail               = $_REQUEST['mail'];
		$phone_mail_address = $_REQUEST['phone_mail_address'];
		$user_id            = $_REQUEST['user_id'];

		if ($_REQUEST['password']) {
			$password = $_REQUEST['password'];
		}
		
		if ( is_numeric($_REQUEST['point']) ) {
			$point = $_REQUEST['point'];
		}

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		$updateDataArray = array(
			"name1"              => $name1,
			"name2"              => $name2,
			"kana1"              => $kana1,
			"kana2"              => $kana2,
			"company_name"       => $company_name,
			"birth_year"         => $birth_year,
			"birth_month"        => $birth_month,
			"birth_day"          => $birth_day,
			"sex"                => $sex,
			"job_type"           => $job_type,
			"post_code1"         => $post_code1,
			"post_code2"         => $post_code2,
			"address"            => $address,
			"tel"                => $tel,
			"fax"                => $fax,
			"mail"               => $mail,
			"phone_mail_address" => $phone_mail_address,
			"user_id"            => $user_id,
			"password"           => $password,
			"point"              => $point,
			"disp_flg"           => $disp_flg,
			"update_record_time" => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);

			break;


		/******************************************************
		更新処理ここまで
		******************************************************/


		/******************************************************
		削除処理ここから
		******************************************************/
		case 'delete':
		
			$recordControlObj->recordDelete($connect, $table_name, $cd);

			//並び順更新
			$recordControlObj->recordSort($connect, $table_name, $kensu_, 'delete');

			break;
		/******************************************************
		削除処理ここまで
		******************************************************/


		/******************************************************
		並び順更新処理ここから
		******************************************************/
		case 'sort':

			$recordControlObj->recordSort($connect, $table_name, $kensu_);

			break;
		/******************************************************
		並び順更新処理ここまで
		******************************************************/

	}
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここまで☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
