<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './class/class.php';
require_once './inc/function.php';

$table_name     = 'news';
$table_seq_name = 'information_schema.tables';
$img_max        = 2;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//新着情報(編集用)取得
	$_SELECT     = "cd, date, title, comment, link, target, disp_flg, img1, img2, sort";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} ORDER BY date DESC LIMIT 1";
	$news_query2 = mysqli_query($connect,$sql);
	$news_max2   = mysqli_num_rows($news_query2);

	for ($i = 0; $i < $news_max2; $i++) {
		$newsArray2[$i] = mysqli_fetch_assoc($news_query2);

		$newsArray2[$i]['img_pass1'] = "{$img_pass}{$newsArray2[$i]['img1']}";
		$newsArray2[$i]['img_pass2'] = "{$img_pass}{$newsArray2[$i]['img2']}";

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}


//新着情報取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, date, title, comment, link, target, disp_flg, img1, img2, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql        = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$news_query = mysqli_query($connect,$sql);
$news_max   = mysqli_num_rows($news_query);

for ($i = 0; $i < $news_max; $i++) {
	$newsArray[$i] = mysqli_fetch_assoc($news_query);
}
//新着情報取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($news_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';

/*
echo '<pre>';
print_r($newsArray);
echo '</pre>';
*/


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
<?php/*
<script>
	$(function() {
		$('input[type="file"]').on('change', function() {
			var file = new Image();
			file.src = document.news.file_up1.value;

			alert("file size=" + file.size);
			return false;
		});
	});
</script>*/?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>

					<form id="<?=$table_name?>" name="<?=$table_name?>" method="POST" enctype="multipart/form-data">
						<div class="table-wrap">
							<table class="adminTable">
								<tr>
									<th><label for="title">タイトル</label></th>
									<td>
										<p id="titleError"></p>
										<input id="title" type="text" name="title" value="<?=$newsArray2[0]['title']?>">
									</td>
								</tr>

								<tr>
									<th><label for="datepicker">日付</label></th>
									<td>
										<p id="datepickerError"></p>
										<input type="text" id="datepicker" name="date" value="<?=$newsArray2[0]['date']?>">
									</td>
								</tr>

								<tr>
									<th><label for="comment">コメント</label></th>
									<td>
										<p id="commentError"></p>
										<textarea id="comment" class="tinyErea" name="comment"><?=$newsArray2[0]['comment']?></textarea>
									</td>
								</tr>

								<tr>
									<th>画像1</th>
									<td>
										<label for="file_up1" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up1" name="file_up1" accept="image/*" value="" style="display:none;"></label>
										<p id="preview1"></p>
										<div class="imgBox">
											<?php if ($newsArray2[0]['img1']) { ?>
											<img src="<?=$newsArray2[0]['img_pass1']?>">
											<?php } ?>
										</div>
									</td>
								</tr>

								<tr>
									<th>画像2</th>
									<td>
										<label for="file_up2" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up2" name="file_up2" accept="image/*" value="" style="display:none;"></label>
										<p id="preview2"></p>
										<div class="imgBox">
											<?php if ($newsArray2[0]['img2']) { ?>
											<img src="<?=$newsArray2[0]['img_pass2']?>">
											<?php } ?>
										</div>
									</td>
								</tr>

								<tr>
									<th><label for="link">リンク</label></th>
									<td>
										<input type="text" id="link" name="link" value="<?=$newsArray2[0]['link']?>">
									</td>
								</tr>

								<tr>
									<th>ターゲット<br>(ブランク設定)</th>
									<td class="dispFlgCell">
										<label><input type="radio" id="target1" name="target" value="_blank" <?php echo ($newsArray2[0]['target'] == '_blank') ? 'checked': ''; ?> >設定する</label>
										<label><input type="radio" id="target2" name="target" value="_self" <?php echo ($newsArray2[0]['target'] == '_self' || !$newsArray2[0]['target']) ? 'checked': ''; ?>>設定しない</label>
									</td>
								</tr>

								<tr>
									<th><label for="disp_flg">表示設定</label></th>
									<td class="dispFlgCell">
										<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($newsArray2[0]['disp_flg'] == '1') ? 'checked': ''; ?>>表示する</label>
									</td>
								</tr>
								
								<tr>
									<td class="button_cell" colspan="2">
										
										<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
										<input class="editButton" name="editButton" type="button" value="編集" update_value="news.php?cd=<?=$newsArray2[0]['cd']?>">
										<?php } else { ?>
										<input class="addButton" name="addButton" type="button" value="登録">
										<?php } ?>

									</td>
								</tr>
							</table>
							<input type="hidden" name="img_value1" id="img_value1" value="<?=$newsArray2[0]['img1']?>">
							<input type="hidden" name="img_value2" id="img_value2" value="<?=$newsArray2[0]['img2']?>">
							<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
							<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
							<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
						</div>
					</form>
				</div>
			</div>
		</div>


		<div class="contentsBoxWrap">

			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> レコード</h2>
					<p class="sortAttention"></p>

					<?php if ($news_max) { ?>
					<form id="<?=$table_name?>_del">
						<div class="table-wrap">
							<table class="adminTable updateTable">

								<thead>
									<tr>
										<th>タイトル</th>
										<th>日付</th>
										<th>編集</th>
										<th>削除</th>
										<th>並び順</th>
									</tr>
								</thead>

								<tbody id="listSort">

									<?php for ($i = 0; $i < $news_max; $i++) { ?>
										<?php

										if ($newsArray[$i]['disp_flg'] != 1) {
											$bg_color = 'style="background-color: #ccc;"';
											$disp_text = '非表示中';
										} else {
											$bg_color = '';
											$disp_text = '表示中';
										}

										$newsArray[$i]['date'] = date( "Y年m月d日",strtotime($newsArray[$i]['date']) );
										?>
									<tr <?=$bg_color?> >
										<td><?=$newsArray[$i]['title']?></td>
										<td><?=$newsArray[$i]['date']?></td>
										<td>
											<a class="update_button" href="news.php?cd=<?=$newsArray[$i]['cd']?>">編集</a>
										</td>
										<td>
											<input type="button" name="delete" value="削除" class="del_mode" del_value="news.php?cd=<?=$newsArray[$i]['cd']?>">
										</td>
										<td>
											<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$newsArray[$i]['sort']?>" name="sort[]">
											<input type="hidden" value="<?=$newsArray[$i]['cd']?>" name="cd_hidden[]">
										</td>
									</tr>
									<?php } ?>
								</tbody>

									<tr>
										<td class="button_cell" colspan="7">
											<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
										</td>
									</tr>

							</table>
						</div>
					</form>

					<?=$pager?>

					<?php } else { ?>
					<p class="record_none_message">現在、レコードが登録されていません。</p>
					<?php } ?>
				</div>

			</div>

		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>