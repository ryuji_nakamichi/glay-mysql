<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './class/class.php';
require_once './inc/function.php';


//masterデータベースのmessageテーブルからレコード取得
$masterMessageObj = new masterMessageClass;
$messageArray     = $masterMessageObj->masterMessage($connect);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop" class="colum1">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2>ようこそ<?=$user_name?>様</h2>
					<p>当管理画面では、HPの情報の更新ができます。</p>
				</div>

				<div class="contents">
					<h2>MASTERからのメッセージ</h2>			
					<ul class="master_message">
						<?php foreach ( (array) $messageArray AS $key => $value ) { ?>
						<li>
							<h3 class="title"><?=$value['title']?></h3>
							<p class="comment"><?=nl2br($value['comment'])?></p>
						</li>
						<?php } ?>
					</ul>

				</div>

			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->
</body>

</html>