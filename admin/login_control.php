<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './class/class.php';

$XSSObj = new XSSClass;

$resArray = array();

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);
	
	
	//処理分岐ここから
	switch ($_REQUEST['mode']) {


		//登録処理ここから
		case 'login':

		if ($_REQUEST['login_id'] == $login_id && $_REQUEST['login_password'] == $login_password) {

			$resArray['status'] = 'success';
			$_SESSION['login']["login_flg_{$session_user_name}"] = 1;
		} else {

			$resArray['status'] = 'faild';
			$_SESSION['login']["login_flg_{$session_user_name}"] = 0;
		}

	}
	//処理分岐ここまで
	//echo $_SESSION['login']["login_flg_{$session_user_name}"];
} else {
	$resArray['status'] = 'faild';
}


//4秒処理を止める
//sleep(4);
echo json_encode($resArray);
