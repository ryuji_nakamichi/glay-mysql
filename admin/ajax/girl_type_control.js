$(function() {


	//テーブルの名前定義
	var table_name = $('#table_name').val();

	//次（もしくは現在）のcd番号取得
	var cd_number  = $('#next_cd').val();

	//画像の枚数
	var img_max    = $('#img_max').val();

	//画像のパスループ用(upload_path_nameArray用)
	var cnt        = 1;

	//画像のパスループ用(画像登録ループ用)
	var cnt2       = 1;

	//画像のパス用配列
	var upload_path_nameArray = [];

	//画像のhidden用配列
	var file_nameArray        = [];

	//削除処理(並び替え)のformタグ用
	var delForm     = $('#' + table_name + '_del');

	//location用(基本)
	var locationUrl = './' + table_name + '.php';

	//アップロード用の変数定義ここから
	var upload_path_name = cd_number;


	//画像のパス(file_up.phpに送る用)
	for (var i = 0; i < img_max; i++) {
		upload_path_nameArray[i] = upload_path_name + '_' + cnt;
		cnt++;
	}


	/**************
	新規登録処理ここから
	**************/
	$('.addButton').on('click',function() {


		//変数定義ここから
		var name = $('#name').val();
		//変数定義ここまで


		//エラーチェックここから
		if (name == '') {
			var nameError = 'オプション名を入力してください。';
			$('#nameError').text(nameError);
		} else {
			$('#nameError').text('');
		}

		if (name == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		/**********
		ダイアログここから
		**********/
		$("#insert_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//画像登録処理ここから
						/*
						for (var i = 0; i < img_max; i++) {

							var fileTag       = $("#file_up" + cnt2);
							var fileHiddenTag = $("#img_value" + cnt2);
							var fileObj       = "file_up" + cnt2;

							if ( fileTag.val()!== '' ) {

								var fd = new FormData();
						*/

								/******************************
								fakepathを削除して画像名のみにするここから
								******************************/
								/*
								fileVal = fileTag.val();

								if ( fileVal.indexOf('fakepath') != -1 ) {
									var fileVal = fileVal.replace( /C:\\fakepath\\/g , "" );
								} else {
									var fileVal = fileVal;
								}
								*/
								/******************************
								fakepathを削除して画像名のみにするここまで
								******************************/

								/*
								file_nameArray = fileVal;
								var img_ex     = file_nameArray.split('.');

								upload_path = upload_path_nameArray[i] + '.' + img_ex[1];

								fileHiddenTag.val(upload_path);


								//FormDataオブジェクトにファイルを追加する場合
								fd.append( fileObj, fileTag.prop("files")[0] );


								//画像のパス名を「file_up.php」に送る
								fd.append("upload_path_name", upload_path_nameArray[i]);
							
								fd.append("dir", fileVal);


								//画像ファイルをuploadフォルダに送る
								$.ajax({
									url: './file_up.php?mode=insert' + '&cd='+ cd_number + '&table_name=' + table_name + '&img_max=' + img_max,
									type: "POST",
									data: fd,
									dataType: "json",
									processData: false, //data:に指定したオブジェクトをGETメソッドのクエリ文字への変換有無を設定する項目(POSTの場合はfalse)
									contentType: false //データ送信時のcontent-typeヘッダの値になるが、FormDataオブジェクトの場合は適切なcontentTypeが設定されるので、falseを設定
								}).done(function(d,status,xhr){
									console.log( JSON.stringify(d) + 'insert中' );
								});


							}

						cnt2++;
						}
						*/
						//画像登録処理ここまで


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						var serializeData = form.serialize();


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();

						$.ajax({
						    method: 'POST',
						    url: './' + table_name + '_control.php?mode=insert',
							data: serializeData,
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data0,status0,xh0) {


						    //エラー処理
						    alert( 'データ登録に失敗しました。' );

						}).done(function (data0,status0,xh0) {


			                // Loadingイメージを消す
			                removeLoading();

						    //成功処理
							$( "#insert_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl + '?mode=insert';
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**************
	新規登録処理ここまで
	**************/


	//更新処理ここから
	$('.editButton').on('click',function() {


		//変数定義ここから
		var name = $('#name').val();
		//変数定義ここまで


		//エラーチェックここから
		if (name == '') {
			var nameError = 'オプション名を入力してください。';
			$('#nameError').text(nameError);
		} else {
			$('#nameError').text('');
		}
		
		if (name == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('update_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=update&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$("#update_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//画像登録処理ここから
						/*
						for (var i = 0; i < img_max; i++) {

							var fileTag       = $("#file_up" + cnt2);
							var fileHiddenTag = $("#img_value" + cnt2);
							var fileObj       = "file_up" + cnt2;

							if ( fileTag.val()!== '' ) {

								var fd = new FormData();

						*/
								/******************************
								fakepathを削除して画像名のみにするここから
								******************************/
								/*
								fileVal = fileTag.val();

								if ( fileVal.indexOf('fakepath') != -1 ) {
									var fileVal = fileVal.replace( /C:\\fakepath\\/g , "" );
								} else {
									var fileVal = fileVal;
								}
								*/
								/******************************
								fakepathを削除して画像名のみにするここまで
								******************************/

								/*
								file_nameArray = fileVal;
								var img_ex     = file_nameArray.split('.');

								upload_path = upload_path_nameArray[i] + '.' + img_ex[1];

								fileHiddenTag.val(upload_path);


								//FormDataオブジェクトにファイルを追加する場合
								fd.append( fileObj, fileTag.prop("files")[0] );


								//画像のパス名を「file_up.php」に送る
								fd.append("upload_path_name", upload_path_nameArray[i]);
							
								fd.append("dir", fileVal);


								//画像ファイルをuploadフォルダに送る
								$.ajax({
									url: './file_up.php?' + 'mode=update' + '&cd='+ cd[1] + '&table_name=' + table_name + '&img_max=' + img_max,
									type: 'POST',
									dataType: 'json',
									data: fd,
									processData: false, //data:に指定したオブジェクトをGETメソッドのクエリ文字への変換有無を設定する項目(POSTの場合はfalse)
									contentType: false //データ送信時のcontent-typeヘッダの値になるが、FormDataオブジェクトの場合は適切なcontentTypeが設定されるので、falseを設定
								}).done(function (d,status,xhr) {
									console.log( JSON.stringify(d) + 'update中' );
								}).fail(function (d,status,xhr) {
								    // エラー処理
								});


							}

						cnt2++;
						}
						*/
						//画像登録処理ここまで


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: form.serialize(),
							datatype: 'json',
						    timeout: 10000
						}).fail(function (d,status,xhr) {


						    // エラー処理
						    alert( 'データ編集に失敗しました。' );

						}).done(function (d,status,xhr) {


			                // Loadingイメージを消す
			                removeLoading();


						    // 成功処理
							$( "#update_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl + '?mode=update';
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	//更新処理ここまで


	/**********
	削除処理ここから
	**********/
	$('.del_mode').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('del_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=delete&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$( "#delete_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//画像ファイルがある場合、削除する
						/*
						$.ajax({
							url: './file_up.php?mode=delete' + '&cd=' + cd[1] + '&table_name=' + table_name + '&img_max=' + img_max,
							type: 'POST',
							processData: false, //data:に指定したオブジェクトをGETメソッドのクエリ文字への変換有無を設定する項目(POSTの場合はfalse)
							contentType: false, //データ送信時のcontent-typeヘッダの値になるが、FormDataオブジェクトの場合は適切なcontentTypeが設定されるので、falseを設定
							datatype: 'json',
						    timeout: 10000
						}).fail(function (d,status,xh) {

						}).done(function (d,status,xh) {
							console.log( JSON.stringify(d) + 'delete中' );
						}).complete(function(d,status,xh) {


							//画像が削除されてからレコード削除
							//ajaxレコード削除処理
							$.ajax({
							    method: 'POST',
							    url: url_params,
								data: cd[1],
								datatype: 'json',
							    timeout: 10000
							}).fail(function (data,status,xhr) {

							}).done(function (data,status,xhr) {


							    //成功処理
							   $( "#delete_complete_dialog" ).dialog({
									autoOpen: true,
									width: 300,
									buttons: [
										{
											text: "Ok",
											click: function() {
												$( this ).dialog( "close" );
												location.href = locationUrl;
											}
										}
									]
								});
							});
							

				        });
						*/


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: cd[1],
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data,status,xhr) {

						}).done(function (data,status,xhr) {


			                // Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#delete_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**********
	削除処理ここまで
	**********/


	//並び替え処理ここから
	$('.sortEditButton').on('click',function() {

		var url_params = './' + table_name + '_control.php?mode=sort';


		//ダイアログここから
		$( "#sort_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード並び替え処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: delForm.serialize(),
							datatype: 'json',
						    timeout: 10000

						}).fail(function (data,status,xhr) {

						}).done(function (data0,status0,xhr0) {

			                // Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#sort_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		//ダイアログここまで

	});
	//並び替え処理ここまで

});