$(function() {


	//テーブルの名前定義
	var table_name = $('#table_name').val();

	//次（もしくは現在）のcd番号取得
	var cd_number  = $('#next_cd').val();

	//画像の枚数
	var img_max    = $('#img_max').val();

	//画像のパスループ用(upload_path_nameArray用)
	var cnt        = 1;

	//画像のパスループ用(画像登録ループ用)
	var cnt2       = 1;

	//画像のパス用配列
	var upload_path_nameArray = [];

	//画像のhidden用配列
	var file_nameArray        = [];

	//削除処理(並び替え)のformタグ用
	var delForm     = $('#' + table_name + '_del');

	//location用(基本)
	var locationUrl = './' + table_name + '.php';

	//アップロード用の変数定義ここから
	var upload_path_name = cd_number;


	//画像のパス(file_up.phpに送る用)
	for (var i = 0; i < img_max; i++) {
		upload_path_nameArray[i] = upload_path_name + '_' + cnt;
		cnt++;
	}


	/**************
	新規登録処理ここから
	**************/
	$('.addButton').on('click',function() {


		//変数定義ここから
		var shipping_cost_free  = $('#shipping_cost_free').val();
		var add_point           = $('#add_point').val();
		var add_point_increment = $('#add_point_increment').val();
		var img_width           = $('#img_width').val();
		var img_height          = $('#img_height').val();
		var img_thum_width_l    = $('#img_thum_width_l').val();
		var img_thum_height_l   = $('#img_thum_height_l').val();
		var img_thum_width_s    = $('#img_thum_width_s').val();
		var img_thum_height_s   = $('#img_thum_height_s').val();
		//変数定義ここまで


		//エラーチェックここから
		if (shipping_cost_free == '') {
			var shipping_cost_freeError = '送料無料条件を入力してください';
			$('#shipping_cost_freeError').text(shipping_cost_freeError);
		} else {
			$('#shipping_cost_freeError').text('');
		}

		if (add_point == '') {
			var add_pointError = 'ポイント加算条件を入力してください';
			$('#add_pointError').text(add_pointError);
		} else {
			$('#add_pointError').text('');
		}

		if (add_point_increment == '') {
			var add_point_incrementError = 'ポイント加算増加率を入力してください';
			$('#add_point_incrementError').text(add_point_incrementError);
		} else {
			$('#add_point_incrementError').text('');
		}

		if (img_width == '') {
			var img_widthError = '画像横幅（大）を入力してください';
			$('#img_widthError').text(img_widthError);
		} else {
			$('#img_widthError').text('');
		}

		if (img_height == '') {
			var img_heightError = '画像縦幅（大）を入力してください';
			$('#img_heightError').text(img_heightError);
		} else {
			$('#img_heightError').text('');
		}

		if (img_thum_width_l == '') {
			var img_thum_width_lError = '画像横幅（中）を入力してください';
			$('#img_thum_width_lError').text(img_thum_width_lError);
		} else {
			$('#img_thum_width_lError').text('');
		}

		if (img_thum_height_l == '') {
			var img_thum_height_lError = '画像縦幅（中）を入力してください';
			$('#img_thum_height_lError').text(img_thum_height_lError);
		} else {
			$('#img_thum_height_lError').text('');
		}

		if (img_thum_width_s == '') {
			var img_thum_width_sError = '画像横幅（小）を入力してください';
			$('#img_thum_width_sError').text(img_thum_width_sError);
		} else {
			$('#img_thum_width_sError').text('');
		}

		if (img_thum_height_s == '') {
			var img_thum_height_sError = '画像縦幅（小）を入力してください';
			$('#img_thum_height_sError').text(img_thum_height_sError);
		} else {
			$('#img_thum_height_sError').text('');
		}

		if (shipping_cost_free == ''　|| add_point == '' || add_point_increment == '' || img_width == '' || img_height == '' || img_thum_width_l == '' || img_thum_height_l == '' || img_thum_width_s == '' || img_thum_height_s == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		/**********
		ダイアログここから
		**********/
		$("#insert_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						var serializeData = form.serialize();

						$.ajax({
						    method: 'POST',
						    url: './' + table_name + '_control.php?mode=insert',
							data: serializeData,
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data0,status0,xh0) {


						    //エラー処理
						    alert( 'データ登録に失敗しました。' );

						}).done(function (data0,status0,xh0) {


			                // Loadingイメージを消す
			                removeLoading();


						    //成功処理
							$( "#insert_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl + '?mode=insert';
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**************
	新規登録処理ここまで
	**************/


	//更新処理ここから
	$('.editButton').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('update_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=update&cd='+ cd[1];


		//変数定義ここから
		var shipping_cost_free  = $('#shipping_cost_free').val();
		var add_point           = $('#add_point').val();
		var add_point_increment = $('#add_point_increment').val();
		var img_width           = $('#img_width').val();
		var img_height          = $('#img_height').val();
		var img_thum_width_l    = $('#img_thum_width_l').val();
		var img_thum_height_l   = $('#img_thum_height_l').val();
		var img_thum_width_s    = $('#img_thum_width_s').val();
		var img_thum_height_s   = $('#img_thum_height_s').val();
		//変数定義ここまで


		//エラーチェックここから
		if (shipping_cost_free == '') {
			var shipping_cost_freeError = '送料無料条件を入力してください';
			$('#shipping_cost_freeError').text(shipping_cost_freeError);
		} else {
			$('#shipping_cost_freeError').text('');
		}

		if (add_point == '') {
			var add_pointError = 'ポイント加算条件を入力してください';
			$('#add_pointError').text(add_pointError);
		} else {
			$('#add_pointError').text('');
		}

		if (add_point_increment == '') {
			var add_point_incrementError = 'ポイント加算増加率を入力してください';
			$('#add_point_incrementError').text(add_point_incrementError);
		} else {
			$('#add_point_incrementError').text('');
		}

		if (img_width == '') {
			var img_widthError = '画像横幅（大）を入力してください';
			$('#img_widthError').text(img_widthError);
		} else {
			$('#img_widthError').text('');
		}

		if (img_height == '') {
			var img_heightError = '画像縦幅（大）を入力してください';
			$('#img_heightError').text(img_heightError);
		} else {
			$('#img_heightError').text('');
		}

		if (img_thum_width_l == '') {
			var img_thum_width_lError = '画像横幅（中）を入力してください';
			$('#img_thum_width_lError').text(img_thum_width_lError);
		} else {
			$('#img_thum_width_lError').text('');
		}

		if (img_thum_height_l == '') {
			var img_thum_height_lError = '画像縦幅（中）を入力してください';
			$('#img_thum_height_lError').text(img_thum_height_lError);
		} else {
			$('#img_thum_height_lError').text('');
		}

		if (img_thum_width_s == '') {
			var img_thum_width_sError = '画像横幅（小）を入力してください';
			$('#img_thum_width_sError').text(img_thum_width_sError);
		} else {
			$('#img_thum_width_sError').text('');
		}

		if (img_thum_height_s == '') {
			var img_thum_height_sError = '画像縦幅（小）を入力してください';
			$('#img_thum_height_sError').text(img_thum_height_sError);
		} else {
			$('#img_thum_height_sError').text('');
		}

		if (shipping_cost_free == ''　|| add_point == '' || add_point_increment == '' || img_width == '' || img_height == '' || img_thum_width_l == '' || img_thum_height_l == '' || img_thum_width_s == '' || img_thum_height_s == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		/**********
		ダイアログここから
		**********/
		$("#update_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: form.serialize(),
							datatype: 'json',
						    timeout: 10000
						}).fail(function (d,status,xhr) {


						    // エラー処理
						    alert( 'データ編集に失敗しました。' );

						}).done(function (d,status,xhr) {


			                // Loadingイメージを消す
			                removeLoading();


						    // 成功処理
							$( "#update_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl + '?mode=update';
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	//更新処理ここまで


	/**********
	削除処理ここから
	**********/
	$('.del_mode').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('del_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=delete&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$( "#delete_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: cd[1],
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data,status,xhr) {

						}).done(function (data,status,xhr) {


			                // Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#delete_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**********
	削除処理ここまで
	**********/


	//並び替え処理ここから
	$('.sortEditButton').on('click',function() {

		var url_params = './' + table_name + '_control.php?mode=sort';


		//ダイアログここから
		$( "#sort_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: delForm.serialize(),
							datatype: 'json',
						    timeout: 10000

						}).fail(function (data,status,xhr) {

						}).done(function (data0,status0,xhr0) {


			                // Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#sort_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		//ダイアログここまで

	});
	//並び替え処理ここまで

});