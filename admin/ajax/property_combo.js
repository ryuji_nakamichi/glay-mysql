$(function() {


	//AREAのセレクトボックスとAREA_ROUTEのセレクトボックスがどちらも「選択してください」の場合
	if( $( '#area option:selected' ).val() == '' && $( '#area_route option:selected' ).val() == '' && $( '#area_route_line option:selected' ).val() == '' && $( '#area_route_station option:selected' ).val() == '' ){
			$( '#area_route').attr({
				disabled: "disabled"
			});

			$( '#area_route_line').attr({
				disabled: "disabled"
			});

			$( '#area_route_station').attr({
				disabled: "disabled"
			});
	}

	$( '#area').change(function(){


		//AREAのセレクトボックスのvalue値が空の場合
		if( $( '#area option:selected' ).val() == '' ){


			//AREA_ROUTEのセレクトボックスを選択不可にする
			$( '#area_route').attr({
				disabled: "disabled"
			});

		} else {


			//AREA_ROUTEのセレクトボックスを選択可能にする
			$( '#area_route').removeAttr('disabled');
		}


		//選択されたAREAのセレクトボックスのvalue値を取得する
		var area_val = $('#area option:selected').val();

		$.ajax({
		    method: 'POST',
		    url: './property_combo.php',
		    data: {
		    	area: $('#area option:selected').val()
		    },
		    datatype: 'json',
		    timeout: 10000
		}).fail(function (data0,status0,xh0) {
		    // エラー処理
		    alert( 'データ取得に失敗しました。' );
		}).done(function (data0,status0,xh0) {
		    // 成功処理
		   // console.log(data0);
		    $( '#area_route' ).html( '<option value="" selected="selected">選択してください</option>' );
		    $(data0).insertAfter( '#area_route option:first-child' );
		});

	});


	$( '#area_route').change(function(){

		//AREA_ROUTEのセレクトボックスのvalue値が空の場合
		if( $( '#area_route option:selected' ).val() == '' ){


			//AREA_ROUTE_LINEのセレクトボックスを選択不可にする
			$( '#area_route_line').attr({
				disabled: "disabled"
			});

		} else {


			//AREA_ROUTE_LINEのセレクトボックスを選択可能にする
			$( '#area_route_line').removeAttr('disabled');
		}


		//選択されたAREA_ROUTEのセレクトボックスのvalue値を取得する
		var area_route_val = $('#area_route option:selected').val();

		$.ajax({
		    method: 'POST',
		    url: './property_combo2.php',
		    data: {
		    	area_route: $('#area_route option:selected').val()
		    },
		    datatype: 'json',
		    timeout: 10000
		}).fail(function (data0,status0,xh0) {
		    // エラー処理
		    alert( 'データ取得に失敗しました。' );
		}).done(function (data0,status0,xh0) {
		    // 成功処理
		   // console.log(data0);
		    $( '#area_route_line' ).html( '<option value="" selected="selected">選択してください</option>' );
		    $(data0).insertAfter( '#area_route_line option:first-child' );
		});

	});


	$( '#area_route_line').change(function(){

		//AREA_ROUTE_LINEのセレクトボックスのvalue値が空の場合
		if( $( '#area_route_line option:selected' ).val() == '' ){


			//AREA_ROUTE_STATIONのセレクトボックスを選択不可にする
			$( '#area_route_station').attr({
				disabled: "disabled"
			});

		} else {


			//AREA_ROUTE_STATIONのセレクトボックスを選択可能にする
			$( '#area_route_station').removeAttr('disabled');
		}


		//選択されたAREA_ROUTE_LINEのセレクトボックスのvalue値を取得する
		var area_route_line_val = $('#area_route_line option:selected').val();

		$.ajax({
		    method: 'POST',
		    url: './property_combo3.php',
		    data: {
		    	area_route_line: $('#area_route_line option:selected').val()
		    },
		    datatype: 'json',
		    timeout: 10000
		}).fail(function (data0,status0,xh0) {
		    // エラー処理
		    alert( 'データ取得に失敗しました。' );
		}).done(function (data0,status0,xh0) {
		    // 成功処理
		   // console.log(data0);
		    $( '#area_route_station' ).html( '<option value="" selected="selected">選択してください</option>' );
		    $(data0).insertAfter( '#area_route_station option:first-child' );
		});

	});

});