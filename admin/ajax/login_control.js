$(function() {


	//ログイン処理ここから
	$('.loginButton').on('click',function() {

		//ログイン処理
		var form = $('#login');
		$.ajax({
		    method: 'POST',
		    url: './login_control.php?mode=login',
			data: form.serialize(),
			datatype: 'json',
		    timeout: 10000
		}).fail(function (d,status,xhr) {


		    // エラー処理
		    location.href = './login.php?mode=login_error';

		}).done(function (d,status,xhr) {

			if (d['status'] == 'success') {
				location.href = './?mode=login_success';
			} else {
				location.href = './login.php?mode=login_faild';
			}

		});


		//ダイアログここから
		/*
		$("#login_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


							//Loadingイメージ表示
							//引数は画像と共に表示するメッセージ
							//dispLoading("処理中...");


							//ログイン処理
							var form = $('#login');
							$.ajax({
							    method: 'GET',
							    url: './login_control.php?mode=login',
								data: form.serialize(),
								datatype: 'json',
							    timeout: 10000
							}).fail(function (d,status,xhr) {


							    // エラー処理
							    //alert( 'ログインに失敗しました。' );
							    location.href = './login.php?mode=login_error';

							}).done(function (d,status,xhr) {

								if (d['status'] == 'success') {
									location.href = './?mode=login_success';
								} else {
									location.href = './login.php?mode=login_faild';
								}


				                //Loadingイメージを消す
				                //removeLoading();


							    //成功処理
							    
								$( "#login_complete_dialog" ).dialog({
									autoOpen: true,
									width: 300,
									buttons: [
										{
											text: "Ok",
											click: function() {
												$( this ).dialog( "close" );
												// console.log(data0);
												// console.log(status0);
												// console.log(xh0);

												if (d['status'] == 'success') {
													location.href = './?mode=login_success';
												} else {
													location.href = './login.php?mode=login_faild';
												}

											}
										}
									]
								});
							});
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});*/
		//ダイアログここまで
	});
	//ログイン処理ここまで
});