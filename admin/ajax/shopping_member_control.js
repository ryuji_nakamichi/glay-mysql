$(function() {


	//テーブルの名前定義
	var table_name = $('#table_name').val();

	//次（もしくは現在）のcd番号取得
	var cd_number  = $('#next_cd').val();

	//画像の枚数
	var img_max    = $('#img_max').val();

	//画像のパスループ用(upload_path_nameArray用)
	var cnt        = 1;

	//画像のパスループ用(画像登録ループ用)
	var cnt2       = 1;

	//画像のパス用配列
	var upload_path_nameArray = [];

	//画像のhidden用配列
	var file_nameArray        = [];

	//削除処理(並び替え)のformタグ用
	var delForm     = $('#' + table_name + '_del');

	//location用(基本)
	var locationUrl = './' + table_name + '.php';

	//アップロード用の変数定義ここから
	var upload_path_name = cd_number;


	//画像のパス(file_up.phpに送る用)
	for (var i = 0; i < img_max; i++) {
		upload_path_nameArray[i] = upload_path_name + '_' + cnt;
		cnt++;
	}

	$.fn.autoKana('#name1',  '#kana1', { katakana : false });
	$.fn.autoKana('#name2',  '#kana2', { katakana : false });


	// 郵便番号
	$('#post_code1').jpostal({
		postcode : [
			'#post_code1', //郵便番号上3ケタ
			'#post_code2'  //郵便番号下4ケタ
		],
		address : {
			'#address'  : '%3%4%5' //都道府県 市区町村 町域
			//'#city'  : '%4%5' //市区町村 町域
		}
	});


	/**************
	新規登録処理ここから
	**************/
	$('.addButton').on('click',function() {


		//変数定義ここから
		var name1      = $('#name1').val();
		var name2      = $('#name2').val();

		var kana1      = $('#kana1').val();
		var kana2      = $('#kana2').val();

		var post_code1 = $('#post_code1').val();
		var post_code2 = $('#post_code2').val();

		var address    = $('#address').val();
		var tel        = $('#tel').val();
		var mail       = $('#mail').val();
		var user_id    = $('#user_id').val();
		var password   = $('#password').val();
		var point      = $('#point').val();
		//変数定義ここまで


		//エラーチェックここから
		if (name1 == '') {
			var name1Error = '名前（姓）を入力してください。';
			$('#name1Error').text(name1Error);
		} else {
			$('#name1Error').text('');
		}

		if (name2 == '') {
			var name2Error = '名前（名）を入力してください。';
			$('#name2Error').text(name2Error);
		} else {
			$('#name2Error').text('');
		}

		if (kana1 == '') {
			var kana1Error = 'カナ（姓）を入力してください。';
			$('#kana1Error').text(kana1Error);
		} else {
			$('#kana1Error').text('');
		}

		if (kana2 == '') {
			var kana2Error = 'カナ（名）を入力してください。';
			$('#kana2Error').text(kana2Error);
		} else {
			$('#kana2Error').text('');
		}

		if (post_code1 == '') {
			var post_code1Error = '郵便番号を入力してください。';
			$('#post_code1Error').text(post_code1Error);
		} else {
			$('#post_code1Error').text('');
		}

		if (post_code2 == '') {
			var post_code2Error = '郵便番号を入力してください。';
			$('#post_code2Error').text(post_code2Error);
		} else {
			$('#post_code2Error').text('');
		}

		if (address == '') {
			var addressError = '住所を入力してください。';
			$('#addressError').text(addressError);
		} else {
			$('#addressError').text('');
		}

		if (tel == '') {
			var telError = '電話番号を入力してください。';
			$('#telError').text(telError);
		} else {
			$('#telError').text('');
		}

		if (mail == '') {
			var mailError = 'メールアドレスを入力してください。';
			$('#mailError').text(mailError);
		} else {
			$('#mailError').text('');
		}

		if (user_id == '') {
			var user_idError = 'ユーザーIDを入力してください。';
			$('#user_idError').text(user_idError);
		} else {
			$('#user_idError').text('');
		}

		if (password == '') {
			var passwordError = 'パスワードを入力してください。';
			$('#passwordError').text(passwordError);
		} else {
			$('#passwordError').text('');
		}

		if (point == '') {
			var pointError = 'ポイントを入力してください。';
			$('#pointError').text(pointError);
		} else {
			$('#pointError').text('');
		}

		if (name1 == '' || name2 == '' || kana1 == '' || kana2 == '' || post_code1 == '' || post_code2 == '' || address == '' || tel == '' || mail == '' || user_id == '' || password == ''　|| point == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		/**********
		ダイアログここから
		**********/
		$("#insert_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						var serializeData = form.serialize();

						$.ajax({
						    method: 'POST',
						    url: './' + table_name + '_control.php?mode=insert',
							data: serializeData,
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data0,status0,xh0) {


						    //エラー処理
						    alert( 'データ登録に失敗しました。' );

						}).done(function (data0,status0,xh0) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
							$( "#insert_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl + '?mode=insert';
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**************
	新規登録処理ここまで
	**************/


	//更新処理ここから
	$('.editButton').on('click',function() {


		//変数定義ここから
		var name1      = $('#name1').val();
		var name2      = $('#name2').val();

		var kana1      = $('#kana1').val();
		var kana2      = $('#kana2').val();

		var post_code1 = $('#post_code1').val();
		var post_code2 = $('#post_code2').val();

		var address    = $('#address').val();
		var tel        = $('#tel').val();
		var mail       = $('#mail').val();
		var user_id    = $('#user_id').val();
		var password   = $('#password').val();
		//変数定義ここまで


		//エラーチェックここから
		if (name1 == '') {
			var name1Error = '名前（姓）を入力してください。';
			$('#name1Error').text(name1Error);
		} else {
			$('#name1Error').text('');
		}

		if (name2 == '') {
			var name2Error = '名前（名）を入力してください。';
			$('#name2Error').text(name2Error);
		} else {
			$('#name2Error').text('');
		}

		if (kana1 == '') {
			var kana1Error = 'カナ（姓）を入力してください。';
			$('#kana1Error').text(kana1Error);
		} else {
			$('#kana1Error').text('');
		}

		if (kana2 == '') {
			var kana2Error = 'カナ（名）を入力してください。';
			$('#kana2Error').text(kana2Error);
		} else {
			$('#kana2Error').text('');
		}

		if (post_code1 == '') {
			var post_code1Error = '郵便番号を入力してください。';
			$('#post_code1Error').text(post_code1Error);
		} else {
			$('#post_code1Error').text('');
		}

		if (post_code2 == '') {
			var post_code2Error = '郵便番号を入力してください。';
			$('#post_code2Error').text(post_code2Error);
		} else {
			$('#post_code2Error').text('');
		}

		if (address == '') {
			var addressError = '住所を入力してください。';
			$('#addressError').text(addressError);
		} else {
			$('#addressError').text('');
		}

		if (tel == '') {
			var telError = '電話番号を入力してください。';
			$('#telError').text(telError);
		} else {
			$('#telError').text('');
		}

		if (mail == '') {
			var mailError = 'メールアドレスを入力してください。';
			$('#mailError').text(mailError);
		} else {
			$('#mailError').text('');
		}

		if (user_id == '') {
			var user_idError = 'ユーザーIDを入力してください。';
			$('#user_idError').text(user_idError);
		} else {
			$('#user_idError').text('');
		}

		if (password == '') {
			var passwordError = 'パスワードを入力してください。';
			$('#passwordError').text(passwordError);
		} else {
			$('#passwordError').text('');
		}

		if (point == '') {
			var pointError = 'ポイントを入力してください。';
			$('#pointError').text(pointError);
		} else {
			$('#pointError').text('');
		}

		if (name1 == '' || name2 == '' || kana1 == '' || kana2 == '' || post_code1 == '' || post_code2 == '' || address == '' || tel == '' || mail == '' || user_id == '' || password == ''　|| point == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('update_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=update&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$("#update_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: form.serialize(),
							datatype: 'json',
						    timeout: 10000
						}).fail(function (d,status,xhr) {


						    // エラー処理
						    alert( 'データ編集に失敗しました。' );

						}).done(function (d,status,xhr) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
							$( "#update_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl + '?mode=update';
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	//更新処理ここまで


	/**********
	削除処理ここから
	**********/
	$('.del_mode').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('del_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=delete&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$( "#delete_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: cd[1],
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data,status,xhr) {

						}).done(function (data,status,xhr) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#delete_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**********
	削除処理ここまで
	**********/


	//並び替え処理ここから
	$('.sortEditButton').on('click',function() {

		var url_params = './' + table_name + '_control.php?mode=sort';


		//ダイアログここから
		$( "#sort_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: delForm.serialize(),
							datatype: 'json',
						    timeout: 10000

						}).fail(function (data,status,xhr) {

						}).done(function (data0,status0,xhr0) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   console.log(xhr0.responseText);

						   $( "#sort_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		//ダイアログここまで

	});
	//並び替え処理ここまで

});