function imgUpload (mode) {

	//テーブルの名前定義
	var table_name = $('#table_name').val();

	//次（もしくは現在）のcd番号取得
	var cd_number  = $('#next_cd').val();

	//画像の枚数
	var img_max    = $('#img_max').val();

	//古い画像パス用配列
	var old_path_nameArray = [];

	//画像のパスループ用(upload_path_nameArray用)
	var cnt        = 1;

	//画像のパスループ用(画像登録ループ用)
	var cnt2       = 1;

	//画像のパス用配列
	var upload_path_nameArray = [];

	//画像のhidden用配列
	var file_nameArray        = [];

	//削除処理(並び替え)のformタグ用
	var delForm     = $('#' + table_name + '_del');

	//location用(基本)
	var locationUrl = './' + table_name + '.php';

	//アップロード用の変数定義ここから
	var upload_path_name = cd_number;


	//画像名用時刻取得
	var now_date = new Date();

	var now_year    = now_date.getFullYear();
	var now_month   = now_date.getMonth() + 1;
	var now_day     = now_date.getDate();
	var now_hours   = now_date.getHours();
	var now_minutes = now_date.getMinutes();
	var now_seconds = now_date.getSeconds();

	var img_date_path = '' + now_year + now_month + now_day + now_hours + now_minutes + now_seconds;


	//画像のパス(file_up.phpに送る用)
	for (var i = 0; i < img_max; i++) {
		upload_path_nameArray[i] = table_name + '_' + upload_path_name + '_' + cnt + '_' + img_date_path;
		cnt++;
	}

	var uploadResult = [];

	for (var i = 0; i < img_max; i++) {

		var fileTag       = $("#file_up" + cnt2);
		var fileHiddenTag = $("#img_value" + cnt2);
		var fileObj       = "file_up" + cnt2;

		if ( fileTag.val() !== '' ) {

/*
			fileTag.bind('change', function() {
				var imgSize1 = this.files[0].size; //ここでファイルサイズを取得
				//$('span.filesize1').text(imgSize1);

				if (imgSize1 < 300000) {
					//alert(imgSize1);
					return false;
				}
			});
*/
			var fd = new FormData();


			/******************************
			fakepathを削除して画像名のみにするここから
			******************************/
			fileVal = fileTag.val();

			if ( fileVal.indexOf('fakepath') != -1 ) {
				var fileVal = fileVal.replace( /C:\\fakepath\\/g , "" );
			} else {
				var fileVal = fileVal;
			}
			/******************************
			fakepathを削除して画像名のみにするここまで
			******************************/


			//古い画像パス
			old_path_nameArray[i] = fileHiddenTag.val();

			file_nameArray = fileVal;
			var img_ex     = file_nameArray.split('.');

			upload_path = upload_path_nameArray[i] + '.' + img_ex[1];

			fileHiddenTag.val(upload_path);


			//FormDataオブジェクトにファイルを追加する場合
			fd.append( fileObj, fileTag.prop("files")[0] );


			//古い画像のパス名を「file_up.php」に送る
			fd.append("old_path_name", old_path_nameArray[i]);


			//画像のパス名を「file_up.php」に送る
			fd.append("upload_path_name", upload_path_nameArray[i]);
		
			fd.append("dir", fileVal);


			//画像ファイルをuploadフォルダに送る
			$.ajax({
				url: './file_up.php?mode=' + mode + '&cd='+ cd_number + '&table_name=' + table_name + '&img_max=' + img_max,
				type: "POST",
				data: fd,
				dataType: "json",
				processData: false, //data:に指定したオブジェクトをGETメソッドのクエリ文字への変換有無を設定する項目(POSTの場合はfalse)
				contentType: false //データ送信時のcontent-typeヘッダの値になるが、FormDataオブジェクトの場合は適切なcontentTypeが設定されるので、falseを設定
			}).fail(function (d,status,xhr) {
				uploadResult[i] = 'imgUpload_faild';
			}).done(function (d,status,xhr) {
				uploadResult[i] = 'imgUpload_success';
				console.log(d);
			});


		} else {
			uploadResult[i] = 'no_image';
		}

	cnt2++;
	}

	return uploadResult;

}


function imgUploadResult (uploadResult) {

	var uploadResult_    = uploadResult;
	var uploadResult_str = '';


    // すべて成功した時の処理
    if ($.inArray('imgUpload_faild', uploadResult_) !== 1 && $.inArray('no_image', uploadResult_) !== 1) {
	    uploadResult_str = 'imgUploadResult_success';
	} else if ($.inArray('no_image', uploadResult_) !== -1) {
		uploadResult_str = 'imgUploadResult_success';
    } else {
    	uploadResult_str = 'imgUploadResult_none';
    }

    return uploadResult_str;

}