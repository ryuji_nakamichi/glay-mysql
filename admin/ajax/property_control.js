$(function() {


	//テーブルの名前定義
	var table_name = $('#table_name').val();

	//次（もしくは現在）のcd番号取得
	var cd_number  = $('#next_cd').val();

	//画像の枚数
	var img_max    = $('#img_max').val();

	//画像のパスループ用(upload_path_nameArray用)
	var cnt        = 1;

	//画像のパスループ用(画像登録ループ用)
	var cnt2       = 1;

	//画像のパス用配列
	var upload_path_nameArray = [];

	//画像のhidden用配列
	var file_nameArray        = [];

	//削除処理(並び替え)のformタグ用
	var delForm     = $('#' + table_name + '_del');

	//location用(基本)
	var locationUrl = './' + table_name + '.php';

	//アップロード用の変数定義ここから
	var upload_path_name = cd_number;


	//画像のパス(file_up.phpに送る用)
	for (var i = 0; i < img_max; i++) {
		upload_path_nameArray[i] = table_name + '_' + upload_path_name + '_' + cnt;
		cnt++;
	}


	/**************
	新規登録処理ここから
	**************/
	$('.addButton').on('click',function() {


		//変数定義ここから
		var name       = $('#name').val();
		var datepicker = $('#datepicker').val();
		var price      = $('#price').val();
		var comment    = $('#comment').val();
		var area_cd    = $('#area_cd').val();
		var age_cd     = $('#age_cd').val();
		//変数定義ここまで


		//エラーチェックここから
		if (name == '') {
			var nameError = '物件名を入力してください。';
			$('#nameError').text(nameError);
		} else {
			$('#nameError').text('');
		}

		if (datepicker == '') {
			var datepickerError = '日付を入力してください。';
			$('#datepickerError').text(datepickerError);
		} else {
			$('#datepickerError').text('');
		}

		if (price == '') {
			var priceError = '金額を入力してください。';
			$('#priceError').text(priceError);
		} else {
			$('#priceError').text('');
		}

		if (comment == '') {
			var commentError = '内容を入力してください。';
			$('#commentError').text(commentError);
		} else {
			$('#commentError').text('');
		}

		if (area_cd == '') {
			var area_cdError = 'エリアを選択してください。';
			$('#area_cdError').text(area_cdError);
		} else {
			$('#area_cdError').text('');
		}

		if (age_cd == '') {
			var age_cdError = '築年数を選択してください。';
			$('#age_cdError').text(age_cdError);
		} else {
			$('#age_cdError').text('');
		}
		
		if (name == '' || datepicker == '' || price == '' || comment == '' || area_cd == '' || age_cd == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		/**********
		ダイアログここから
		**********/
		$("#insert_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						/*****************************************
						画像登録関係処理をしてからレコード登録処理ここから
						******************************************/
						$.when(
							uploadResult        = imgUpload('insert'),
							uploadResult_str    = imgUploadResult(uploadResult)
						)
						.done(function(data_a, data_b) {

							if (uploadResult_str !== 'imgUploadResult_none') {
								console.log('画像登録あり');
							} else {
								console.log('画像登録なし');
							}


								//Loadingイメージ表示
								//引数は画像と共に表示するメッセージ
								dispLoading("処理中...");

								//二重投稿禁止関数
								dxTransProhibited();

								/********************
								ajaxレコード登録処理ここから
								*********************/
								var form = $('#' + table_name);
								var serializeData = form.serialize();
								$.ajax({
								    method: 'POST',
								    url: './' + table_name + '_control.php?mode=insert',
									data: serializeData,
									datatype: 'json',
								    timeout: 10000
								}).fail(function (data0,status0,xh0) {


								    //エラー処理
								    alert( 'データ登録に失敗しました。' );

								}).done(function (data0,status0,xh0) {


					                //Loadingイメdata0ージを消す
					                removeLoading();


								    //成功処理
									$( "#insert_complete_dialog" ).dialog({
										autoOpen: true,
										width: 300,
										buttons: [
											{
												text: "Ok",
												click: function() {
													$( this ).dialog( "close" );
													location.href = locationUrl + '?mode=insert';
												}
											}
										]
									});
								});
						})
						.fail(function() {
						    //error
						});
						/*****************************************
						画像登録関係処理をしてからレコード登録処理ここまで
						******************************************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**************
	新規登録処理ここまで
	**************/


	//更新処理ここから
	$('.editButton').on('click',function() {


		//変数定義ここから
		var name       = $('#name').val();
		var datepicker = $('#datepicker').val();
		var price      = $('#price').val();
		var comment    = $('#comment').val();
		var area_cd    = $('#area_cd').val();
		var age_cd     = $('#age_cd').val();
		//変数定義ここまで


		//エラーチェックここから
		if (name == '') {
			var nameError = '物件名を入力してください。';
			$('#nameError').text(nameError);
		} else {
			$('#nameError').text('');
		}

		if (datepicker == '') {
			var datepickerError = '日付を入力してください。';
			$('#datepickerError').text(datepickerError);
		} else {
			$('#datepickerError').text('');
		}

		if (price == '') {
			var priceError = '金額を入力してください。';
			$('#priceError').text(priceError);
		} else {
			$('#priceError').text('');
		}

		if (comment == '') {
			var commentError = '内容を入力してください。';
			$('#commentError').text(commentError);
		} else {
			$('#commentError').text('');
		}

		if (area_cd == '') {
			var area_cdError = 'エリアを選択してください。';
			$('#area_cdError').text(area_cdError);
		} else {
			$('#area_cdError').text('');
		}

		if (age_cd == '') {
			var age_cdError = '築年数を選択してください。';
			$('#age_cdError').text(age_cdError);
		} else {
			$('#age_cdError').text('');
		}
		
		if (name == '' || datepicker == '' || price == '' || comment == '' || area_cd == '' || age_cd == '') {
			alert('入力されていない項目があります。');
			return false;
		}
		//エラーチェックここまで


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('update_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=update&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$("#update_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						/*****************************************
						画像登録関係処理をしてからレコード登録処理ここから
						******************************************/
						$.when(
							uploadResult        = imgUpload('update'),
							uploadResult_str    = imgUploadResult(uploadResult)
						)
						.done(function(data_a, data_b) {

							if (uploadResult_str !== 'imgUploadResult_none') {
								console.log('画像登録あり');
							} else {
								console.log('画像登録なし');
							}


							// Loadingイメージ表示
							// 引数は画像と共に表示するメッセージ
							dispLoading("処理中...");


							//二重投稿禁止関数
							dxTransProhibited();

							/********************
							ajaxレコード登録処理ここから
							*********************/
							var form = $('#' + table_name);
							$.ajax({
							    method: 'POST',
							    url: url_params,
								data: form.serialize(),
								datatype: 'json',
							    timeout: 10000
							}).fail(function (d,status,xhr) {


							    // エラー処理
							    alert( 'データ編集に失敗しました。' );

							}).done(function (d,status,xhr) {


				                // Loadingイメージを消す
				                removeLoading();


							    // 成功処理
								$( "#update_complete_dialog" ).dialog({
									autoOpen: true,
									width: 300,
									buttons: [
										{
											text: "Ok",
											click: function() {
												$( this ).dialog( "close" );
												location.href = locationUrl + '?mode=update';
											}
										}
									]
								});
							});
							/********************
							ajaxレコード登録処理ここまで
							*********************/
						})
						.fail(function() {
						    //error
						});
						/*****************************************
						画像登録関係処理をしてからレコード登録処理ここまで
						******************************************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	//更新処理ここまで


	/**********
	削除処理ここから
	**********/
	$('.del_mode').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('del_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=delete&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$( "#delete_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//画像ファイルがある場合、削除する
						$.ajax({
							url: './file_up.php?mode=delete' + '&cd=' + cd[1] + '&table_name=' + table_name + '&img_max=' + img_max,
							type: 'POST',
							processData: false, //data:に指定したオブジェクトをGETメソッドのクエリ文字への変換有無を設定する項目(POSTの場合はfalse)
							contentType: false, //データ送信時のcontent-typeヘッダの値になるが、FormDataオブジェクトの場合は適切なcontentTypeが設定されるので、falseを設定
							datatype: 'json',
						    timeout: 10000
						}).fail(function (d,status,xh) {

						}).done(function (d,status,xh) {
							console.log( JSON.stringify(d) + 'delete中' );
						}).complete(function(d,status,xh) {


							//Loadingイメージ表示
							//引数は画像と共に表示するメッセージ
							dispLoading("処理中...");


							//二重投稿禁止関数
							dxTransProhibited();


							//画像が削除されてからレコード削除
							//ajaxレコード削除処理
							$.ajax({
							    method: 'POST',
							    url: url_params,
								data: cd[1],
								datatype: 'json',
							    timeout: 10000
							}).fail(function (data,status,xhr) {

							}).done(function (data,status,xhr) {


				                //Loadingイメージを消す
				                removeLoading();


							    //成功処理
							   $( "#delete_complete_dialog" ).dialog({
									autoOpen: true,
									width: 300,
									buttons: [
										{
											text: "Ok",
											click: function() {
												$( this ).dialog( "close" );
												location.href = locationUrl;
											}
										}
									]
								});
							});
							

				        });



					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**********
	削除処理ここまで
	**********/


	//並び替え処理ここから
	$('.sortEditButton').on('click',function() {

		var url_params = './' + table_name + '_control.php?mode=sort';


		//ダイアログここから
		$( "#sort_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: delForm.serialize(),
							datatype: 'json',
						    timeout: 10000

						}).fail(function (data,status,xhr) {

						}).done(function (data0,status0,xhr0) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#sort_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		//ダイアログここまで

	});
	//並び替え処理ここまで

});