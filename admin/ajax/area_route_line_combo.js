$(function() {


	//AREAのセレクトボックスとAREA_ROUTEのセレクトボックスがどちらも「選択してください」の場合
	if( $( '#area option:selected' ).val() == '' && $( '#area_route option:selected' ).val() == '' ){
			$( '#area_route').attr({
				disabled: "disabled"
			});
	}

	$( '#area').change(function(){


		//AREAのセレクトボックスのvalue値が空の場合
		if( $( '#area option:selected' ).val() == '' ){


			//AREA_ROUTEのセレクトボックスを選択不可にする
			$( '#area_route').attr({
				disabled: "disabled"
			});

		} else {


			//AREA_ROUTEのセレクトボックスを選択可能にする
			$( '#area_route').removeAttr('disabled');
		}


		//選択されたAREAのセレクトボックスのvalue値を取得する
		var AREA_val = $('#area option:selected').val();

		$.ajax({
		    method: 'POST',
		    url: './area_route_line_combo.php',
		    data: {
		    	area: $('#area option:selected').val()
		    },
		    datatype: 'json',
		    timeout: 10000
		}).fail(function (data0,status0,xh0) {
		    // エラー処理
		    alert( 'データ取得に失敗しました。' );
		}).done(function (data0,status0,xh0) {
		    // 成功処理
		   // console.log(data0);
		    $( '#area_route' ).html( '<option value="" selected="selected">選択してください</option>' );
		    $(data0).insertAfter( '#area_route option:first-child' );
		});

	});

/*
	$( '#area_route').change(function(){


	});*/

});