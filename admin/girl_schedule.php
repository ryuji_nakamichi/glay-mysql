<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';
// require_once './class/class.php';
require_once './inc/girl_calender.php';

$table_name     = "girl_schedule";
$table_seq_name = 'information_schema.tables';
$img_max        = 2;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//schedule(編集用)取得
	$_SELECT     = "cd, work_date, work_start_time, work_end_time, disp_flg, sort";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$schedule_query2 = mysqli_query($connect,$sql);
	$schedule_max2   = mysqli_num_rows($schedule_query2);

	for ($i = 0; $i < $schedule_max2; $i++) {
		$scheduleArray2[$i] = mysqli_fetch_assoc($schedule_query2);
	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}

if ($_REQUEST['year'] && $_REQUEST['month'] && $_REQUEST['day']) {

	$c_date = '';

	$c_year  = $_REQUEST['year'];
	$c_month = $_REQUEST['month'];
	$c_day   = $_REQUEST['day'];

	$c_date = $c_year.'-'.$c_month.'-'.$c_day;
} else {

	if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {
		$c_date = $scheduleArray2[0]['date'];
	}
	
}


//schedule取得(ページャー有)ここから
// 1ページ表示件数

$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, work_date, work_start_time, work_end_time ,disp_flg, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql        = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$schedule_query = mysqli_query($connect, $sql);
$schedule_max   = mysqli_num_rows($schedule_query);

for ($i = 0; $i < $schedule_max; $i++) {
	$scheduleArray[$i] = mysqli_fetch_assoc($schedule_query);
}
//schedule取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($schedule_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


// echo '<pre>';
// print_r($_REQUEST);
// echo '</pre>';


//カレンダーに渡す引数用
if ($_REQUEST['prev_year']) {
	$year  = date( "Y", strtotime("-1 year") );
} else if ($_REQUEST['next_year']) {
	$year  = date( "Y", strtotime("+1 year") );
} else {
	$year  = date("Y");
}


//カレンダーに渡す引数用
if ($_REQUEST['prev_month']) {
	$month  = date( "n", strtotime("-1 month") );
} else if ($_REQUEST['next_month']) {
	$month  = date( "n", strtotime("+1 month") );
} else {
	$month  = date("n");
}




//女性キャスト取得ここから
$_SELECT    = "cd, name";
$sql        = "SELECT {$_SELECT} FROM girl WHERE disp_flg = 1 ORDER BY sort";
$girl_query = mysqli_query($connect, $sql);
$girl_max   = mysqli_num_rows($girl_query);

for ($i = 0; $i < $girl_max; $i++) {
	$girlArray[$i] = mysqli_fetch_assoc($girl_query);
}
//女性キャスト取得ここまで



// //スケジュール処理ここから
// $recordControlObj = new recordControlClass;

// $girl_cdArray          = $_REQUEST['girl_cd'];
// $girl_cd_max           = count($girl_cdArray);
// $girl_scheduleAllArray = array();

// for ($i = 0; $i < $girl_cd_max; $i++) {


// 	//開始時間
//  	$girl_work_hour_startArray = $_REQUEST["girl_work_hour_start{$girl_cdArray[$i]}_1"];
// 	$girl_work_hour_start_max  = count($girl_work_hour_startArray);


// 	//終了時間
//  	$girl_work_hour_endArray = $_REQUEST["girl_work_hour_end{$girl_cdArray[$i]}_1"];
// 	$girl_work_hour_end_max  = count($girl_work_hour_endArray);

// 	for ($j = 0; $j < $girl_work_hour_start_max; $j++) {
// 		$hour_startArray[$i][$j] = $girl_work_hour_startArray[$j];
// 		$girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_start'][] = $hour_startArray[$i][$j];
// 	}

// 	for ($j = 0; $j < $girl_work_hour_end_max; $j++) {
// 		$hour_endArray[$i][$j] = $girl_work_hour_endArray[$j];
// 		$girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_end'][] = $hour_endArray[$i][$j];

// 	}

// }

// if ($month < 10) {
// 	$work_month = sprintf('%02d', $month);
// }


// $work_day = date("d");

// if ($work_day < 10) {
// 	$work_day = sprintf('%02d', $work_day);
// }

// $work_date_year = $year;
// $work_dateArray = array();
// for ($i = 0; $i < $girl_cd_max; $i++) {

// 	for ($j = 0; $j < $girl_work_hour_start_max; $j++) {

// 		 $work_day_count = $j + 1;
		
// 		if ($work_day_count < 10) {
// 			$work_date_day = sprintf('%02d', $work_day_count);
// 		} else {
// 			$work_date_day = $work_day_count;
// 		}

// 		$workdateArray['work_date'][$j] = $work_date_year.'-'.$work_month.'-'.$work_date_day;

// 		$sql = "SELECT cd FROM girl_schedule WHERE girl_cd = {$girl_cdArray[$i]} AND work_date = '{$workdateArray['work_date'][$j]}'";
// 		$girl_work_date_query = mysqli_query($connect, $sql);
// 		$girl_work_date_max   = mysqli_num_rows($girl_work_date_query);

// 		for ($k = 0; $k < $girl_work_date_max; $k++) {
// 			$girl_work_date_array = mysqli_fetch_assoc($girl_work_date_query);
// 			$cd                   = $girl_work_date_array['cd'];
// 		}

// 		$work_start_time = $girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_start'][$j];
// 		$work_end_time   = $girl_scheduleAllArray[$girl_cdArray[$i]]['girl_work_hour_end'][$j];


// 		//更新(update)の場合
// 		if ($girl_work_date_max) {
			
// 			$updateDataArray = array(
// 				"girl_cd"            => $girl_cdArray[$i],
// 				"work_date"          => $workdateArray['work_date'][$j],
// 				"work_start_time"    => $work_start_time,
// 				"work_end_time"      => $work_end_time,
// 				"update_record_time" => "current_timestamp"
// 			);
		
// 			$data_query = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);


// 		//挿入(insert)の場合
// 		} else {

// 			$sort = 0;
			
// 			$insertDataArray = array(
// 				"girl_cd"         => $girl_cdArray[$i],
// 				"work_date"       => $workdateArray['work_date'][$j],
// 				"work_start_time" => $work_start_time,
// 				"work_end_time"   => $work_end_time,
// 				"sort"            => $sort,
// 				"new_record_time" => "current_timestamp"
// 			);
		
// 			$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


// 			//並び順更新
// 			$recordControlObj->recordInsertSort($connect, $table_name);

// 		}

// 		//  echo $sql.'<br>';

// 	}

// }
// //スケジュール処理ここまで

// echo '<pre>';
// print_r($girl_scheduleAllArray);
// echo '</pre>';
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap girl_schedule">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">

								<div class="girl_scheduleBox">
								<?=calender($year, $month, $day, $connect)?>
<!-- 								<button type="submit" name="<?=$table_name?>" value="test">test</button>
 -->								</div>

								<div class="girl_scheduleBtn">
									<input class="editButton" name="editButton" type="button" value="編集" update_value="girl_schedule.php">
								</div>
								
								<input type="hidden" name="img_value1" id="img_value1" value="<?=$scheduleArray2[0]['img1']?>">
								<input type="hidden" name="img_value2" id="img_value2" value="<?=$scheduleArray2[0]['img2']?>">
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>

					</div>
				</div>
			</div>

		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>