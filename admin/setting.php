<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = 'setting';
$table_seq_name = 'information_schema.tables';
$img_max        = 0;


//管理画面設定取得
$_SELECT     = "cd, login_id, login_password, item_tax, shipping_cost, shipping_cost_free, add_point, add_point_increment, img_width, img_height, img_thum_width_s, img_thum_height_s, img_thum_width_l, img_thum_height_l";
$sql         = "SELECT {$_SELECT} FROM {$table_name}";
$setting_query = mysqli_query($connect,$sql);
$setting_max   = mysqli_num_rows($setting_query);

for ($i = 0; $i < $setting_max; $i++) {
	$settingArray[$i] = mysqli_fetch_assoc($setting_query);
}

$titleDispText = '編集フォーム';


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


/*
echo '<pre>';
//print_r($settingArray);
var_dump($settingArray);
echo '</pre>';
*/


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop" class="colum1">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
					<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
						<div class="table-wrap">
							<table class="adminTable">
								<tr>
									<th colspan="2">管理画面設定</th>
								</tr>

								<tr>
									<th><label for="login_id">ログインID</label></th>
									<td>
										<p id="login_idrror"></p>
										<input id="login_id" type="text" name="login_id" value="<?=$settingArray[0]['login_id']?>">
									</td>
								</tr>

								<tr>
									<th><label for="login_password">ログインPASSWORD</label></th>
									<td>
										<p id="login_passwordError"></p>
										<input type="text" id="login_password" name="login_password" value="<?=$settingArray[0]['login_password']?>">
									</td>
								</tr>

								<tr>
									<th colspan="2">EC設定</th>
								</tr>

								<tr>
									<th><label for="item_tax">商品税率</label></th>
									<td class="spinner">
										<p id="item_taxError"></p>
										<input class="item_tax" type="text" id="item_tax" name="item_tax" value="<?=$settingArray[0]['item_tax']?>">
									</td>
								</tr>

								<tr>
									<th><label for="shipping_cost">商品送料</label></th>
									<td class="spinner">
										<p id="shipping_costError"></p>
										<input class="shipping_cost" type="text" id="shipping_cost" name="shipping_cost" value="<?=$settingArray[0]['shipping_cost']?>">
									</td>
								</tr>

								<tr>
									<th><label for="shipping_cost_free">商品送料条件</label></th>
									<td class="spinner">
										<p id="shipping_cost_freeError"></p>
										<input class="shipping_cost_free" type="text" id="shipping_cost_free" name="shipping_cost_free" value="<?=$settingArray[0]['shipping_cost_free']?>">
									</td>
								</tr>

								<tr>
									<th><label for="add_point">ポイント加算条件</label></th>
									<td class="spinner">
										<p id="add_pointError"></p>
										<input class="add_point" type="text" id="add_point" name="add_point" value="<?=$settingArray[0]['add_point']?>">
									</td>
								</tr>

								<tr>
									<th><label for="add_point_increment">ポイント増加率</label></th>
									<td class="spinner">
										<p id="add_point_incrementError"></p>
										<input class="add_point_increment" type="text" id="add_point_increment" name="add_point_increment" value="<?=$settingArray[0]['add_point_increment']?>">
									</td>
								</tr>

								<tr>
									<th colspan="2">画像幅設定</th>
								</tr>

								<tr>
									<th><label for="img_width">画像横幅（大）</label></th>
									<td class="spinner">
										<p id="img_widthError"></p>
										<input class="img_width" type="text" id="img_width" name="img_width" value="<?=$settingArray[0]['img_width']?>">
									</td>
								</tr>

								<tr>
									<th><label for="img_height">画像縦幅（大）</label></th>
									<td class="spinner">
										<p id="img_heightError"></p>
										<input class="img_height" type="text" id="img_height" name="img_height" value="<?=$settingArray[0]['img_height']?>">
									</td>
								</tr>

								<tr>
									<th><label for="img_thum_width_l">画像横幅（中）</label></th>
									<td class="spinner">
										<p id="img_thum_width_lError"></p>
										<input class="img_thum_width_l" type="text" id="img_thum_width_l" name="img_thum_width_l" value="<?=$settingArray[0]['img_thum_width_l']?>">
									</td>
								</tr>

								<tr>
									<th><label for="img_thum_height_l">画像縦幅（中）</label></th>
									<td class="spinner">
										<p id="img_thum_height_lError"></p>
										<input class="img_thum_height_l" type="text" id="img_thum_height_l" name="img_thum_height_l" value="<?=$settingArray[0]['img_thum_height_l']?>">
									</td>
								</tr>

								<tr>
									<th><label for="img_thum_width_s">画像横幅（小）</label></th>
									<td class="spinner">
										<p id="img_thum_width_sError"></p>
										<input class="img_thum_width_s" type="text" id="img_thum_width_s" name="img_thum_width_s" value="<?=$settingArray[0]['img_thum_width_s']?>">
									</td>
								</tr>

								<tr>
									<th><label for="img_thum_height_s">画像縦幅（小）</label></th>
									<td class="spinner">
										<p id="img_thum_height_sError"></p>
										<input class="img_thum_height_s" type="text" id="img_thum_height_s" name="img_thum_height_s" value="<?=$settingArray[0]['img_thum_height_s']?>">
									</td>
								</tr>

								<tr>
									<td class="button_cell" colspan="2">
										
										<?php if ($setting_max) { ?>
										<input class="editButton" name="editButton" type="button" value="編集" update_value="setting.php?cd=<?=$settingArray[0]['cd']?>">

										<?php } else { ?>
										<input class="addButton" name="addButton" type="button" value="登録">
										<?php } ?>

									</td>
								</tr>
							</table>
							<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
							<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
							<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>