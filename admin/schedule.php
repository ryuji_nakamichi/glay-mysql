<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

require_once './inc/calender.php';

$table_name     = "schedule";
$table_seq_name = 'information_schema.tables';
$img_max        = 2;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//schedule(編集用)取得
	$_SELECT     = "cd, date, name, comment, color, disp_flg, img1, img2, sort";
	$sql         = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$schedule_query2 = mysqli_query($connect,$sql);
	$schedule_max2   = mysqli_num_rows($schedule_query2);

	for ($i = 0; $i < $schedule_max2; $i++) {
		$scheduleArray2[$i] = mysqli_fetch_assoc($schedule_query2);

		$scheduleArray2[$i]['img_pass1'] = "{$img_pass}{$scheduleArray2[$i]['img1']}";
		$scheduleArray2[$i]['img_pass2'] = "{$img_pass}{$scheduleArray2[$i]['img2']}";

	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}

if ($_REQUEST['year'] && $_REQUEST['month'] && $_REQUEST['day']) {

	$c_date = '';

	$c_year  = $_REQUEST['year'];
	$c_month = $_REQUEST['month'];
	$c_day   = $_REQUEST['day'];

	$c_date = $c_year.'-'.$c_month.'-'.$c_day;
} else {

	if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {
		$c_date = $scheduleArray2[0]['date'];
	}
	
}


//schedule取得(ページャー有)ここから
// 1ページ表示件数

$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, date, name, comment, color, disp_flg, img1, img2, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql        = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$schedule_query = mysqli_query($connect,$sql);
$schedule_max   = mysqli_num_rows($schedule_query);

for ($i = 0; $i < $schedule_max; $i++) {
	$scheduleArray[$i] = mysqli_fetch_assoc($schedule_query);
}
//schedule取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($schedule_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


/*
echo '<pre>';
print_r($scheduleArray);
echo '</pre>';
*/

//カレンダーに渡す引数用
if ($_REQUEST['prev_year']) {
	$year  = date( "Y", strtotime("-1 year") );
} else if ($_REQUEST['next_year']) {
	$year  = date( "Y", strtotime("+1 year") );
} else {
	$year  = date("Y");
}


//カレンダーに渡す引数用
if ($_REQUEST['prev_month']) {
	$month  = date( "n", strtotime("-1 month") );
} else if ($_REQUEST['next_month']) {
	$month  = date( "n", strtotime("+1 month") );
} else {
	$month  = date("n");
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">

								<div class="carenderBox">
								<?=calender($year, $month, $day, $connect)?>
								</div>
								

								<table class="adminTable">
									<tr>
										<th><label for="name">予約者名</label></th>
										<td>
											<p id="nameError"></p>
											<input id="name" type="text" name="name" value="<?=$scheduleArray2[0]['name']?>">
										</td>
									</tr>

									<tr>
										<th><label for="datepicker">予約日</label></th>
										<td>
											<p id="datepickerError"></p>
											<input type="text" id="datepicker" name="date" value="<?=$c_date?>">
										</td>
									</tr>

									<tr>
										<th><label for="comment">内容</label></th>
										<td>
											<p id="commentError"></p>
											<textarea id="comment" class="tinyErea" name="comment"><?=$scheduleArray2[0]['comment']?></textarea>
										</td>
									</tr>

									<tr>
										<th><label for="color">色付け</label></th>
										<td>
											<p id="colorError"></p>
											<input type="text" id="color" class="jscolor" name="color" value="<?=$scheduleArray2[0]['color']?>">
										</td>
									</tr>

									<tr>
										<th>画像1</th>
										<td>
											<label for="file_up1" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up1" name="file_up1" value="" style="display:none;"></label>
											<p id="preview1"></p>
											<div class="imgBox">
												<?php if ($scheduleArray2[0]['img1']) { ?>
												<img src="<?=$scheduleArray2[0]['img_pass1']?>">
												<?php } ?>
											</div>
										</td>
									</tr>

									<tr>
										<th>画像2</th>
										<td>
											<label for="file_up2" class="fileInpit"><span>＋写真を選択</span><input type="file" id="file_up2" name="file_up2" value="" style="display:none;"></label>
											<p id="preview2"></p>
											<div class="imgBox">
												<?php if ($scheduleArray2[0]['img2']) { ?>
												<img src="<?=$scheduleArray2[0]['img_pass2']?>">
												<?php } ?>
											</div>
										</td>
									</tr>

									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($scheduleArray2[0]['disp_flg'] == '1') ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="schedule.php?cd=<?=$scheduleArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>
								<input type="hidden" name="img_value1" id="img_value1" value="<?=$scheduleArray2[0]['img1']?>">
								<input type="hidden" name="img_value2" id="img_value2" value="<?=$scheduleArray2[0]['img2']?>">
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">
				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($schedule_max) { ?>
						<form id="schedule_del">
							<div class="table-wrap">
								<table class="adminTable updateTable">

									<thead>
										<tr>
											<th>予約者名</th>
											<th>予約日</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">

										<?php for ($i = 0; $i < $schedule_max; $i++) { ?>
											<?php

											if ($scheduleArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											$scheduleArray[$i]['date'] = date( "Y年m月d日",strtotime($scheduleArray[$i]['date']) );
											?>
										<tr <?=$bg_color?> >
											<td><?=$scheduleArray[$i]['name']?></td>
											<td><?=$scheduleArray[$i]['date']?></td>
											<td>
												<a class="update_button" href="schedule.php?cd=<?=$scheduleArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="schedule.php?cd=<?=$scheduleArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$scheduleArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$scheduleArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="7">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>

						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
					</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>