<?php

require_once './inc/session.php';
require_once '../inc/db.php';
require_once './inc/login_check.php';
require_once './inc/function.php';

$table_name     = "contact";
$table_seq_name = 'information_schema.tables';
$img_max        = 0;

if ( $_GET['cd'] && is_numeric($_GET['cd']) ) {

	$cd     = $_GET['cd'];
	$_WHERE = " WHERE cd = {$cd}";


	//CONTACT(編集用)取得
	$_SELECT         = "cd, date, name, kana, mail, tel, msg, disp_flg";
	$sql             = "SELECT {$_SELECT} FROM {$table_name} {$_WHERE} LIMIT 1";
	$contact_query2  = mysqli_query($connect, $sql);
	$contact_max2    = mysqli_num_rows($contact_query2);

	for ($i = 0; $i < $contact_max2; $i++) {
		$contactArray2[$i] = mysqli_fetch_assoc($contact_query2);
	}

	$titleDispText = '編集フォーム';
} else {
	$titleDispText = '登録フォーム';
}




//contact取得(ページャー有)ここから
// 1ページ表示件数
$kensu  = $kensu_;

$sqlAdd = '';
$params = '';

$_select = "cd, date, name, kana, mail, tel, msg, disp_flg, sort";
$sql     = "SELECT {$_select} FROM {$table_name} {$sqlAdd}";
$query   = mysqli_query($connect, $sql);
$max     = mysqli_num_rows($query);

$maxpage = ceil($max/$kensu);
$page    = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$from    = ($page-1) * $kensu;
$offset  = ($from > 0) ? " OFFSET {$from}": '';


$sql           = "{$sql} ORDER BY sort LIMIT {$kensu} {$offset}";
$contact_query = mysqli_query($connect, $sql);
$contact_max   = mysqli_num_rows($contact_query);

for ($i = 0; $i < $contact_max; $i++) {
	$contactArray[$i] = mysqli_fetch_assoc($contact_query);
}
//contact取得(ページャー有)ここまで


//次のcd番号取得ここから
if (!$_GET['cd']) {

	$_SELECT    = "auto_increment";
	$_WHERE     = "table_name = '{$table_name}'";
	$sql        = "SELECT {$_SELECT} FROM {$table_seq_name} WHERE {$_WHERE}";
	$next_query = mysqli_query($connect, $sql);
	$next_max   = mysqli_num_rows($next_query);

	for ($i = 0; $i < $next_max; $i++) {
		$nextArray = mysqli_fetch_assoc($next_query);
		$next_cd   = $nextArray['auto_increment'];
	}

} else {
	$next_cd = $_GET['cd'];
}
//次のcd番号取得ここまで


// ページャー(数字)
$pager = ($contact_max) ? pagerNum($maxpage, $page, $pagerRequest) : '';


/*
echo '<pre>';
print_r($contactArray);
echo '</pre>';
*/


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop">

<!--sp_modal-->
<?php require_once './inc/sp_modal.php'; ?>
<!--sp_modal-->

<!--outer_left-->
<?php require_once './inc/outer_left.php'; ?>
<!--outer_left-->

<!--outerRightBox-->
<div class="outerRightBox">

	<!--innerRightBox1-->
	<div class="innerRightBox1">

		<div class="contentsBoxWrap">
			<div class="contentsBox">

				<div class="contents">
					<h2><?=$table_name?> <?=$titleDispText?></h2>
						<form id="<?=$table_name?>" method="POST" enctype="multipart/form-data">
							<div class="table-wrap">
								<table class="adminTable">
									<tr>
										<th><label for="name">名前</label></th>
										<td>
											<p id="nameError"></p>
											<input id="name" type="text" name="name" value="<?=$contactArray2[0]['name']?>">
										</td>
									</tr>

									<tr>
										<th><label for="kana">ふりがな</label></th>
										<td>
											<p id="kanaError"></p>
											<input id="kana" type="text" name="kana" value="<?=$contactArray2[0]['kana']?>">
										</td>
									</tr>

									<tr>
										<th><label for="mail">メールアドレス</label></th>
										<td>
											<p id="mailError"></p>
											<input id="mail" type="text" name="mail" value="<?=$contactArray2[0]['mail']?>">
										</td>
									</tr>

									<tr>
										<th><label for="tel">電話番号</label></th>
										<td>
											<p id="telError"></p>
											<input id="tel" type="text" name="tel" value="<?=$contactArray2[0]['tel']?>">
										</td>
									</tr>

									<tr>
										<th><label for="datepicker">日付</label></th>
										<td>
											<p id="datepickerError"></p>
											<input type="text" id="datepicker" name="date" value="<?=$contactArray2[0]['date']?>">
										</td>
									</tr>

									<tr>
										<th><label for="msg">お問い合わせ内容</label></th>
										<td>
											<p id="msgError"></p>
											<textarea id="msg" class="tinyErea" name="msg"><?=$contactArray2[0]['msg']?></textarea>
										</td>
									</tr>
					
									<tr>
										<th><label for="disp_flg">表示設定</label></th>
										<td class="dispFlgCell">
											<label><input type="checkbox" id="disp_flg" name="disp_flg" value="1" <?php echo ($contactArray2[0]['disp_flg'] == 1) ? 'checked': ''; ?>>表示する</label>
										</td>
									</tr>

									<tr>
										<td class="button_cell" colspan="2">
											
											<?php if ( $_REQUEST['cd'] && is_numeric($_REQUEST['cd']) ) { ?>
											<input class="editButton" name="editButton" type="button" value="編集" update_value="contact.php?cd=<?=$contactArray2[0]['cd']?>">
											<?php } else { ?>
											<input class="addButton" name="addButton" type="button" value="登録">
											<?php } ?>

										</td>
									</tr>
								</table>
								<input type="hidden" name="next_cd" id="next_cd" value="<?=$next_cd?>">
								<input type="hidden" name="table_name" id="table_name" value="<?=$table_name?>">
								<input type="hidden" name="img_max" id="img_max" value="<?=$img_max?>">
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="contentsBoxWrap">
				<div class="contentsBox">

					<div class="contents">
						<h2><?=$table_name?> レコード</h2>
						<p class="sortAttention"></p>

						<?php if ($contact_max) { ?>
						<form id="contact_del" method="POST">
							<div class="table-wrap">
								<table class="adminTable updateTable">
									<thead>
										<tr>
											<th>名前</th>
											<th>日付</th>
											<th>編集</th>
											<th>削除</th>
											<th>並び順</th>
										</tr>
									</thead>

									<tbody id="listSort">
										<?php for ($i = 0; $i < $contact_max; $i++) { ?>
											<?php

											if ($contactArray[$i]['disp_flg'] != 1) {
												$bg_color = 'style="background-color: #ccc;"';
												$disp_text = '非表示中';
											} else {
												$bg_color = '';
												$disp_text = '表示中';
											}

											?>
										<tr <?=$bg_color?> >
											<td><?=$contactArray[$i]['name']?></td>
											<td><?=$contactArray[$i]['date']?></td>
											<td>
												<a class="update_button" href="contact.php?cd=<?=$contactArray[$i]['cd']?>">編集</a>
											</td>
											<td>
												<input type="button" name="delete" value="削除" class="del_mode" del_value="contact.php?cd=<?=$contactArray[$i]['cd']?>">
											</td>
											<td>
												<input id="sort<?=$i+1?>" class="sort" type="text" value="<?=$contactArray[$i]['sort']?>" name="sort[]">
												<input type="hidden" value="<?=$contactArray[$i]['cd']?>" name="cd_hidden[]">
											</td>
										</tr>
										<?php } ?>
									</tbody>

										<tr>
											<td class="button_cell" colspan="6">
												<input class="sortEditButton" name="sortEditButton" type="button" value="並び替え">
											</td>
										</tr>

								</table>
							</div>
						</form>

						<?=$pager?>
						
						<?php } else { ?>
						<p class="record_none_message">現在、レコードが登録されていません。</p>
						<?php } ?>
				</div>
			</div>
		</div>

	</div>
	<!--innerRightBox1-->

	<!--innerRightBox2-->
	<div class="innerRightBox2">
	<?php require_once './inc/inner_right.php'; ?>
	</div>
	<!--innerRightBox2-->

</div>
<!--outerRightBox-->

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--dailog-->
</body>

</html>