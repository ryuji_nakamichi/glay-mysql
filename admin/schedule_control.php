<?php

require_once '../inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "schedule";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイドスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd = $XSSObj->cdCheck($_REQUEST['cd']);
	
	
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここから☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/
	switch ($_REQUEST['mode']) {


		/******************************************************
		登録処理ここから
		******************************************************/
		case 'insert':
		

		//データ整形
		$date = $_REQUEST['date'];

		if ($_REQUEST['date']) {
			$dateArray = explode("-",$date);
			$year      = $dateArray[0];
			$month     = $dateArray[1];
			$day       = $dateArray[2];
			$date      = $year."-".$month."-".$day;
		}
		
		$name      = $_REQUEST['name'];
		$comment   = $_REQUEST['comment'];
		$color     = $_REQUEST['color'];

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		$sort = 0;

		if ($_REQUEST['img_value1']) {
			$img1 = str_replace('"', "", $_REQUEST['img_value1']);
		} else {
			$img1 = '';
		}

		if ($_REQUEST['img_value2']) {
			$img2 = str_replace('"', "", $_REQUEST['img_value2']);
		} else {
			$img2 = '';
		}

		$insertDataArray = array(
			"date"            => $date,
			"name"            => $name,
			"comment"         => $comment,
			"color"           => $color,
			"img1"            => $img1,
			"img2"            => $img2,
			"disp_flg"        => $disp_flg,
			"sort"            => $sort,
			"new_record_time" => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


		//並び順更新
		$recordControlObj->recordInsertSort($connect, $table_name);

				break;


		/******************************************************
		登録処理ここまで
		******************************************************/


		/******************************************************
		更新処理ここから
		******************************************************/
		case 'update':


		//データ整形

		$date = $_REQUEST['date'];

		if ($_REQUEST['date']) {
			$dateArray = explode("-",$date);
			$year      = $dateArray[0];
			$month     = $dateArray[1];
			$day       = $dateArray[2];
			$date      = $year."-".$month."-".$day;
		}
		
		$name      = $_REQUEST['name'];
		$comment   = $_REQUEST['comment'];
		$color     = $_REQUEST['color'];

		if ($_REQUEST['disp_flg'] == 1) {
			$disp_flg  = $_REQUEST['disp_flg'];
		} else {
			$disp_flg  = 0;
		}

		if ($_REQUEST['file_up1']) {
			$img1 = str_replace('"', "", $_REQUEST['img_value1']);
		} else {
			$img1 = $_REQUEST['img_value1'];
		}

		if ($_REQUEST['file_up2']) {
			$img2 = str_replace('"', "", $_REQUEST['img_value2']);
		} else {
			$img2 = $_REQUEST['img_value2'];
		}

		$updateDataArray = array(
			"date"               => $date,
			"name"               => $name,
			"comment"            => $comment,
			"color"              => $color,
			"img1"               => $img1,
			"img2"               => $img2,
			"disp_flg"           => $disp_flg,
			"update_record_time" => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);

				break;


		/******************************************************
		更新処理ここまで
		******************************************************/


		/******************************************************
		削除処理ここから
		******************************************************/
		case 'delete':
		
			$recordControlObj->recordDelete($connect, $table_name, $cd);

			//並び順更新
			$recordControlObj->recordSort($connect, $table_name, $kensu_, 'delete');

			break;
		/******************************************************
		削除処理ここまで
		******************************************************/


		/******************************************************
		並び順更新処理ここから
		******************************************************/
		case 'sort':

			$recordControlObj->recordSort($connect, $table_name, $kensu_);

			break;
		/******************************************************
		並び順更新処理ここまで
		******************************************************/




	}
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここまで☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
