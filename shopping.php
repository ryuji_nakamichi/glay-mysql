﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//会員の情報を取得
$user_infoObj   = new userInfoClass;
$user_infoArray = $user_infoObj->userInfo();

// echo '<pre>';
// print_r($user_infoArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>Shopping</h2>

      <p>お客様情報を入力してください。</p>

      <?php if ($_GET['flg'] == 1) { ?>

      <p class="completeMessage">お買い上げありがとうございました。</p>
      <a class="backBtn" href="./">戻る</a>

      <?php } else { ?>

      <?php if ($_GET['flg'] == 2) { ?>
      <p style="color: #bf0000;">入力されてない項目があります。</p>
      <?php } ?>

      <form id="form1" name="form1" method="post" action="./shopping_confirm.php">

        <table class="form_1">
          <tr>
            <th>名前<em>必須</em></th>
            <td>
              <p id="nameError" class="errorMessage"></p>
              <input type="text" id="name" name="name" value="<?=$user_infoArray['name']?>">
            </td>
          </tr>

          <tr>
            <th>ふりがな<em>必須</em></th>
            <td>
              <p id="kanaError" class="errorMessage"></p>
              <input type="text" id="kana" name="kana" value="<?=$user_infoArray['kana']?>">
            </td>
          </tr>

          <tr>
            <th>メールアドレス<em>必須</em></th>
            <td>
              <p id="mailError" class="errorMessage"></p>
              <input type="text" id="mail" name="mail" value="<?=$user_infoArray['mail']?>">
            </td>
          </tr>

          <tr>
            <th>電話番号<em>必須</em></th>
            <td>
              <p id="telError" class="errorMessage"></p>
              <input type="text" id="tel" name="tel" value="<?=$user_infoArray['tel']?>">
            </td>
          </tr>

          <tr>
            <th>住所<em>必須</em></th>
            <td class="addressCell">
              <p id="zipError" class="errorMessage"></p>
              〒<input type="text" id="zip1" name="zip1" value="<?=$user_infoArray['post_code1']?>" maxlength="3">-
              <input type="text" id="zip2" name="zip2" value="<?=$user_infoArray['post_code2']?>" maxlength="4">

              <p id="addressError" class="errorMessage"></p>
              <input type="text" id="address" name="address" value="<?=$user_infoArray['address']?>">
            </td>
          </tr>
            
        </table>

        <ul class="btnList">
          <li><input type="reset" id="reset" class="button" value="リセット"></li>
          <li><input type="submit" id="submit" class="button" value="確認画面へ"></li>
        </ul>

        <input type="hidden" name="flg" value="1">

      </form>

      <?php } ?>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
