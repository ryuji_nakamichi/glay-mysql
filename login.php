<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//ログイン処理
$loginObj                 = new loginClass;
$loginArray['error_text'] = $loginObj->login();

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body id="pTop" class="colum1">

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

		<h2>ID・PASSWORD入力</h2>
		<?=$loginArray['error_text']?>

		<?php if ($_REQUEST['mode'] == 'login_faild') { ?>
		<p>ログインできませんでした。<br>
		もう一度お試しください。
		</p>
		<?php } ?>

		<form id="login">
			<div class="table-wrap">
				<table class="form_1">
					<tr>
						<th><label for="login_id">ID</label></th>
						<td>
							<input id="login_id" type="text" name="login_id">
						</td>
					</tr>

					<tr>
						<th><label for="login_password">PASSWORD</label></th>
						<td>
							<input id="login_password" type="password" name="login_password">
						</td>
					</tr>

					<tr>
						<td class="button_cell" colspan="2">
							<input class="loginButton" name="loginButton" type="button" value="ログイン">
						</td>
					</tr>

				</table>
			</div>
		</form>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>

<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->

<!--dailog-->
<?php require_once './inc/dailog.php'; ?>
<!--/dailog-->

  </div>

</div>
</body>

</html>