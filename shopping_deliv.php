﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/shopping_check.php';


//会員の情報を取得
$user_infoObj   = new userInfoClass;
$user_infoArray = $user_infoObj->userInfo();


//会員のお届け先一覧を取得
$mypageClassObj      = new mypageClass;
$other_delivAllArray = $mypageClassObj->getDelivList($connect);
$other_delivArray    = $other_delivAllArray['other_delivArray'];

// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <ol class="list-shopping-flow">
        <li class="active">STEP1<br><span class="pc">お届け先指定</span></li>
        <li>STEP2<br><span class="pc">お支払い方法等選択</span></li>
        <li>STEP3<br><span class="pc">内容確認</span></li>
        <li>STEP4<br><span class="pc">完了</span></li>
      </ol>

      <h2>お届け先の指定</h2>

      <div id="address_area" class="clearfix">
        <div class="information">
          <p>下記一覧よりお届け先住所を選択して、「選択したお届け先に送る」ボタンをクリックしてください。</p>
          <p>一覧にご希望の住所が無い場合は、「新しいお届け先を追加する」より追加登録してください。</p>
          <p class="mini attention">※最大2件まで登録できます。</p>
        </div>
        <div class="add_multiple">
          <p>この商品を複数の<br>お届け先に送りますか？</p>
          <ul class="btnList">
            <li><a href="./shopping_multiple.php">お届け先を<br>複数指定する</a></li>
          </ul>
        </div>
      </div>

      <p class="addbtn"><a href="./mypage_delivery_addr.php">新しいお届け先を追加する</a></p>

      <form id="shopping_deliv" name="shopping_deliv" method="POST" action="./shopping_payment.php">
        <table class="shoppingTbl">
          <tbody>
            <tr>
              <th class="alignC">選択</th>
              <th class="alignC">住所種類</th>
              <th class="alignC">お届け先</th>
              <th class="alignC">変更</th>
              <th class="alignC">削除</th>
            </tr>

            <tr>
              <td class="alignC">
                <input type="radio" name="deliv_check" id="chk_id_1" value="-1" checked="checked">
              </td>
              <td class="alignC">
                <label for="chk_id_1">会員登録住所</label>
              </td>
              <td><?=$user_infoArray['company_name']?> <?=$user_infoArray['name']?></td>
              <td class="alignC">-</td>
              <td class="alignC">-</td>
            </tr>

            <?php foreach ($other_delivArray AS $key => $value) { ?>
              
            <tr>
              <td class="alignC"><input type="radio" name="deliv_check" id="chk_id_<?=$value['cd']?>" value="<?=$value['cd']?>"></td>
              <td><label for="add1">お届け先住所</label></td>
              <td>
              〒<?=$value['zip1']?>-<?=$value['zip2']?><br>
              <?=$value['address1']?><br>
              <?=$value['company_name']?>&nbsp;<?=$value['name1']?>&nbsp;<?=$value['name2']?>
              </td>
              <td class="alignC">
                <a href="./mypage_delivery_addr.php?cd=<?=$value['cd']?>">変更</a>
              </td>
              <td class="alignC">
                <a href="./mypage_delivery_addr.php?cd=<?=$value['cd']?>">削除</a>
              </td>
            </tr>

            <?php } ?>

          </tbody>
        </table>

        <ul class="btnList">
          <li><a class="backBtn" href="./cart.php">戻る</a></li>
          <li><input class="nextBtn" type="submit" value="次へ"></li>
        </ul>

        <input type="hidden" name="shopping_deliv_flg" value="1">
      </form>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
