﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>Member_TAKURO</h2>
        
        <ul class="member">
        
            <li>
              <img src="image/takuro02_member.jpg" width="235" height="330" alt="teru">
            </li>
            
            <li>
              <img src="image/takuro03_member.jpg" width="235" height="330" alt="teru">
            </li>
        </ul>
        
        <p class="bio_honbun">
        

          <section>
          
          <h3>1988年</h3>
         
小学校・中学校と同級生であったTERU、同じ函館稜北高校に通うHISASHIとGLAYを結成。       
         </section>

          <section>
          
          <h3>1990年</h3>
         
         高校卒業後、上京。        
  </section>

          <section>
          
          <h3>1991年</h3>
         
彼らと同じく函館出身であったJIROと出会う。         
          </section>

          <section>
          
          <h3>1994年</h3>
         
          X JAPANのメンバーであるYOSHIKIに目を留められ、プラチナム・レコードから「RAIN」でデビュー。        
         </section>

          <section>
          
          <h3>1997年</h3>
         
女性アーティストMijuをプロデュースし、シングル「SUMMER SHAKES」をリリース。         </section>

          <section>
          
          <h3>1998年</h3>
         
女性アーティストRomiをプロデュースし、シングル「見つめていたい」をリリース。
中山美穂のシングル「LOVE CLOVER」及びカップリングの「empty pocket」を提供。 </section>

          <section>
          
          <h3>2001年</h3>
         
坂本龍一を中心とする地雷排除活動「地雷ZERO」に協力し、チャリティーソング「ZERO LANDMINE」にギタリストとして参加。        
          </section>

         <section>

          <h3>2002年</h3>
         
松本孝弘（B'z）のソロアルバム『華』収録の「ENGAGED」を松本と共作。
インディーズ時代に知り合ったことがきっかけで親友のTOKI（現C4,Kill=slayd）のプロジェクト『Stealth』の「灼熱」を作曲。               
          </section>

         <section>

          <h3>2003年</h3>
         
          イラク戦争開戦の報に接し、メッセージサイト「TAKURO-NO-WAR.jp」を個人で立ち上げ、攻撃の大義を世界に問う。
         </section>

         <section>

          <h3>2004年</h3>
         
          映画『CASSHERN』（監督:紀里谷和明）に、HISASHIとエキストラで虐殺される民間人役として出演。同映画のサウンドトラックにもGLAYとして参加している。
          女性ファッション雑誌「ViVi」の専属モデルであった岩堀せりと結婚。
         </section>

         <section>

          <h3>2005年</h3>
         
          渡辺美里に楽曲「Kiss from a rose」を提供。
         </section>

         <section>

          <h3>2006年</h3>
         
          中島美嘉主演の映画『NANA』の主題歌「一色」の作曲とカップリングの「EYES FOR THE MOON」の作詞曲を担当。歌うのは中島美嘉。作詞は矢沢あい。また「一色」が収録されたアルバム『THE END』では、「一色」の別バージョンでベースを弾いている。         </section>

         <section>

          <h3>2007年</h3>
         
           JACK IN THE BOX 2007にてTERU、L'Arc〜en〜Cielのhydeと共演（演奏した曲：「誘惑」「HONEY」）
         </section>

         <section>

          <h3>2009年</h3>
         
          C4のTOKIと共に起こしたプロジェクトStealthのギタリストとして渋谷AXにて1st LIVEを敢行
          デザイナーの片山勇のドキュメンタリー映画「イサム・カタヤマ=アルチザナル・ライフ」が公開。テーマミュージックとして書き下ろした「LET ME BE」をGLAYとして提供し、映画全編のサウンドトラックをHISASHIとタッグを組み「AUDIO 2 AUDIO」名義で担当した（AUDIO 2 AUDIOの音源はCD化されていない）。
         </section>

         <section>

          <h3>2011年</h3>
         
          映画「EXILE PRIDE」のサウンドトラックをAUDIO 2 AUDIOとして担当。
         </section>

         <section>

          <h3>2013年</h3>
         
           C4のTOKIとのプロジェクト「STEALTH」のワンマン公演を渋谷O-EASTにて行い、GLAYからHISASHIと共にギターで参加。
            VAMPS主催のライブイベント「HALLOWEEN PARTY 2013」に、TERUと共に出演。
            台湾のロックバンドMayday(五月天)のベストアルバム「Mayday×五月天 the Best of 1999‐2013」のリード曲「Dancin' Dancin' feat. TERU (GLAY)」に、TERUと共に参加。
         </section>



         <section>

          <h3>人物</h3>
         
母親はシャンソン歌手で、幼い頃から音楽に興味を持つ。3歳の頃に、交通事故で父親を亡くしている（『胸懐』では「不可抗力ではなかったらしい」と自殺を示唆）。<br>

あまり裕福な少年時代を送っておらず、後に、シングル「Yes, Summerdays」の印税で母親の借金を完済した時は、「自分の力で金の苦労を解決できたのが嬉しかった」と語っている。<br>


中学時代、サッカー部でバックスとして活躍していたが、ジョン・レノンに多大な影響を受け、ビートルズ特集の組まれたラジオ番組を聴くため、自主退部した。
人生で腰を抜かすほど驚いたのは、BOØWYの「B・BLUE」を聴いた時と、GLAYのボーカルを探していた時に、当時ドラム担当だったTERUが、何気なく曲に入れた声を聴いた時の2回だけだという。<br>


その他、影響を受けたミュージシャンに尾崎豊やブライアン・アダムスなどを挙げている。近年では、オアシスのファンであることを公言しており、「圧倒的なギターサウンドが、自分にとってすごい心地いい」と語っている。ラジオ番組で、ノエル・ギャラガーと対談を果たした。<br>


GLAYのほとんどの楽曲の作詞・作曲をしている。GLAY内での名義はTAKUROだが、宇多田ヒカルのシングル「For You／タイム・リミット」収録の「タイム・リミット」では、本名のKubo Takuro名義を、HISASHIとの共作の場合はKombinat-12名義を用いている。ただ、本人は自分はプロデューサーは向いていないと語っている。<br>


作詞・作曲に関しては、自身が手がけた「HOWEVER」と「BE WITH YOU」「誘惑」「SOUL LOVE」「Winter,again」「とまどい／SPECIAL THANKS」の6曲がミリオンセラーを記録している。<br>


ブレイクしてから忙しさのあまりに売れている実感はなかったが、テレビで共演した渡辺徹に「CD買ったよ」と言われ、「売れているんだ」と実感したという。<br>


1994年 - 1999年に『週刊少年マガジン』で連載された『Harlem Beat』の中に登場したキャラクター「甲斐万丈」のデザインモデルにもなっている。
TERU曰く、「これほどGLAYを好きな人は他にいない」とのこと。<br>


GLAYのオフィシャルショップ「G-DIRECT」の企画を立てた際、他のGLAYのメンバーに「G-DIRECT」についての自らの考えをメールで送ったが、TERUとHISASHIはメールを読まずにTAKUROの考えを支持する旨を返信した。HISASHI曰く、「そこまでTAKUROが考えたことならきっといいことなんだろうと思った」とのこと。<br>


Twitterに関しては、「他者と接している時に、目の前の相手をないがしろにしてTwitterをやること」に苦言を呈したり、「自分でTwitterをやる時間があるなら、その時間を他の『やるべきこと』のために費やす」といった旨の発言をしている。<br>


JIROと同じ幼稚園に通っており、小学校もTAKUROが転校するまでは一緒だった。転校先の小学校にはTERUが在籍しており、自宅が近くだったのでよく一緒に遊んでいたという。

 </section>


         <section>

          <h3>交友関係</h3>
         
          B'zの松本孝弘とは、アメリカのギブソン社のギター工場へ一緒に見学に行ったり、大晦日には松本の自宅へ駆けつけたりするほど。GLAYのファンクラブ会報誌には、松本の話題が掲載されることもしばしば。 1999年5月7日に放送されたNHK-BS2「松本孝弘ソロ・プロジェクト～B'zへの挑戦～」では対談を行った。 松本のソロ・アルバム『華』の収録曲「ENGAGED」は、2人での共作である。<br>
          
          
the pillowsの山中さわおとは一緒に飲んだりLIVEに行くこともある。女優の中山美穂、歌手の宇多田ヒカル、松山千春や漫画家の小林よしのり、及川光博、平岡祐太、上川隆也、唐沢寿明、堺正章、flumpool、シドとも交流がある。

 </section>

        
        </p>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
