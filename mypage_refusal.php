﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


if ($_POST['mypage_refusal_flg'] === '1') {
  
  echo '<pre>';
  print_r($_POST);
  echo '</pre>';
  //mypageClass(会員退会)呼び出し
  $mypageObj = new mypageClass;
  //$memberRefusal_query = $mypageObj->memberRefusal($connect, $_SESSION['user']['user_info']['cd']);

}

?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>MYページ</h2>

      <div id="mynavi_area">
        <ul class="mynavi_list clearfix">
          <li><a href="./mypage.php">購入履歴一覧</a></li>
          <li><a href="./mypage_favorite.php">お気に入り一覧</a></li>
          <li><a href="./mypage_change.php">会員登録内容変更</a></li>
          <li><a href="./mypage_delivery.php">お届け先追加・変更</a></li>
          <li><a href="./mypage_refusal.php">退会手続き</a></li>
        </ul>

        <!--▼現在のポイント-->
        <div class="point_announce">
          <p>ようこそ&nbsp;／&nbsp;<span class="user_name"><?=$_SESSION['user']['user_info']['name1'].$_SESSION['user']['user_info']['name2']?>様</span>
            &nbsp;現在の所持ポイントは&nbsp;<span class="point st"><?=$_SESSION['user']['user_info']['point']?>pt</span>&nbsp;です。</p>
        </div>
        <!--▲現在のポイント-->

      </div>

      <h3>退会手続き</h3>
      <div class="message">
      <?php if ($_POST['mypage_refusal_flg'] === '1') { ?>
      退会しました。<br>
      またのご利用お待ちしております。
      <?php } else { ?>
      会員を退会された場合には、現在保存されている購入履歴や、<br>
      お届け先などの情報は、全て削除されますがよろしいでしょうか？
      <?php } ?>
      </div>

      <?php if ($_POST['mypage_refusal_flg'] !== '1') { ?>
      <form id="mypage_refusal" name="mypage_refusal" method="post" action="./mypage_refusal_complete.php">
        <div class="message_area">
          <p>退会手続きが完了した時点で、現在保存されている購入履歴や、<br>
          お届け先等の情報は全てなくなりますのでご注意ください。</p>
          <div class="btn_area">
            <ul>
              <li>
                <input type="submit" class="hover_change_image" name="refusal" id="refusal" value="会員退会する">
              </li>
            </ul>
          </div>
        </div>

        <input type="hidden" name="mypage_refusal_flg" value="1">
      </form>
      <?php } ?>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
