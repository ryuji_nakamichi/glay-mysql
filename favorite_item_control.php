<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

$recordControlObj = new recordControlClass;
$XSSObj           = new XSSClass;

$resArray = array();

$table_name = "favorite_item";

if ($_REQUEST['mode'] && $_REQUEST['mode'] != '') {

	$resArray['status'] = '成功';


	//クロスサイトスクリプティング対策
	$_REQUEST = $XSSObj->XSSEscape($_REQUEST, $connect);


	//cd番号チェック
	$cd      = $XSSObj->cdCheck($_REQUEST['cd']);
	$item_cd = $cd;


	//会員cd番号
	$shopping_member_cd = $_SESSION['user']['user_info']['cd'];


	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここから☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/
	switch ($_REQUEST['mode']) {


		//登録処理ここから
		case 'insert':

		$disp_flg = 1;
		$sort     = 0;

		$insertDataArray = array(
			"shopping_member_cd" => $shopping_member_cd,
			"item_cd"            => $item_cd,
			"disp_flg"           => $disp_flg,
			"sort"               => $sort,
			"new_record_time"    => "current_timestamp"
		);
		
		$data_query = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);


		//並び順更新
		$recordControlObj->recordInsertSort($connect, $table_name);


			break;


		/******************************************************
		登録処理ここまで
		******************************************************/


		/******************************************************
		削除処理ここから
		******************************************************/
		case 'delete':

			$recordControlObj->recordDelete($connect, $table_name, $cd);

			//並び順更新
			$recordControlObj->recordSort($connect, $table_name, $kensu_, 'delete');

		break;
		/******************************************************
		削除処理ここまで
		******************************************************/

	}
	/**********************************************************
	☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆処理分岐ここまで☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
	**********************************************************/


} else {
	$resArray['status'] = '失敗';
}


//4秒処理を止める
sleep(4);
echo json_encode($resArray);
