<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';

$recordControlObj = new recordControlClass;

$table_name = "other_deliv";


//クロスサイトスクリプティング対策
foreach ($_REQUEST AS $key => $val) {

	if ( !is_array($val) ){
		$_REQUEST[$key] = htmlspecialchars($val);
		$_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
	}

}


//会員のcd番号格納
$member_cd = $_SESSION['user']['user_info']['cd'];


//お届け先の変更の場合
if ( ctype_digit (strval($_REQUEST['cd']) ) ) {
	$cd        = $_REQUEST['cd'];
}


//POSTされてきたデータを変数に定義
$flg                = $_REQUEST['flg'];
$update_flg         = $_REQUEST['update_flg'];

$name1              = $_REQUEST['name1'];
$name2              = $_REQUEST['name2'];
$kana1              = $_REQUEST['kana1'];
$kana2              = $_REQUEST['kana2'];
$company_name       = $_REQUEST['company_name'];
$zip1               = $_REQUEST['post_code1'];
$zip2               = $_REQUEST['post_code2'];
$address1           = $_REQUEST['address'];
$tel                = $_REQUEST['tel'];
$fax                = $_REQUEST['fax'];
$disp_flg           = 1;
$sort               = 0;

if ($flg == 1) {


	//お届け先の変更の場合
	if ($update_flg === '1') {

		$updatetDataArray = array(
			"name1"              => $name1,
			"name2"              => $name2,
			"kana1"              => $kana1,
			"kana2"              => $kana2,
			"company_name"       => $company_name,
			"zip1"               => $zip1,
			"zip2"               => $zip2,
			"address1"           => $address1,
			"tel"                => $tel,
			"fax"                => $fax,
			"update_record_time" => "current_timestamp"
		);


	//お届け先の追加の場合
	} else {

		$insertDataArray = array(
			"member_cd"          => $member_cd,
			"name1"              => $name1,
			"name2"              => $name2,
			"kana1"              => $kana1,
			"kana2"              => $kana2,
			"company_name"       => $company_name,
			"zip1"               => $zip1,
			"zip2"               => $zip2,
			"address1"           => $address1,
			"tel"                => $tel,
			"fax"                => $fax,
			"disp_flg"           => $disp_flg,
			"sort"               => $sort,
			"new_record_time"   => "current_timestamp"
		);

	}


	//お届け先の変更の場合
	if ($update_flg === '1') {
		$updateQueryResult = $recordControlObj->recordUpdate($connect, $table_name, $updatetDataArray, $cd);


	//お届け先の追加の場合
	} else {
		$insertQueryResult = $recordControlObj->recordInsert($connect, $table_name, $insertDataArray);
	}
		
	

	if ($insertQueryResult) {
		//並び替え処理
	  	$recordControlObj->recordInsertSort($connect, $table_name);
	}



	//お届け先の追加･変更に成功した場合
	if ($insertQueryResult || $updateQueryResult) {


		//お届け先の追加･変更に成功した場合、statusにsuccessを代入
		$resArray['status'] = 'success';


	//お届け先の追加･変更に失敗した場合
	} else {


		//お届け先の追加･変更に失敗した場合、faildを代入
		$resArray['status'] = 'faild';

	}

	
} else {
	$resArray['status'] = 'query_faild';
}


//3秒処理を止める
//sleep(3);

echo json_encode($resArray);