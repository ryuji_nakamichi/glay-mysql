﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


//mypageClass(お気に入り一覧)呼び出し
$mypageObj        = new mypageClass;
$itemArray        = $mypageObj->historyGet($connect);
$favoriteAllArray = $mypageObj->getFavoriteList($connect);

$favorite_max     = $favoriteAllArray['favorite_max'];
$favoriteArray    = $favoriteAllArray['favoriteArray'];

$table_name = "favorite_item";

// echo '<pre>';
// print_r($favoriteArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>MYページ</h2>

      <div id="mynavi_area">
        <ul class="mynavi_list clearfix">
          <li><a href="./mypage.php">購入履歴一覧</a></li>
          <li><a href="./mypage_favorite.php">お気に入り一覧</a></li>
          <li><a href="./mypage_change.php">会員登録内容変更</a></li>
          <li><a href="./mypage_delivery.php">お届け先追加・変更</a></li>
          <li><a href="./mypage_refusal.php">退会手続き</a></li>
        </ul>

        <!--▼現在のポイント-->
        <div class="point_announce">
          <p>ようこそ&nbsp;／&nbsp;<span class="user_name"><?=$_SESSION['user']['user_info']['name1'].$_SESSION['user']['user_info']['name2']?>様</span>
            &nbsp;現在の所持ポイントは&nbsp;<span class="point st"><?=$_SESSION['user']['user_info']['point']?>pt</span>&nbsp;です。</p>
        </div>
        <!--▲現在のポイント-->

      </div>

      <h3>お気に入り一覧</h3>
      <p><span class="attention"><?=$favorite_max?>件</span>のお気に入りがあります。</p>
      <table class="shoppingTbl">
        <tr>
          <th class="alignC">削除</th>
          <th class="alignC">商品画像</th>
          <th class="alignC">商品名</th>
          <th class="alignC">販売価格(税込)</th>
        </tr>

        <?php foreach ( (array) $favoriteArray AS $key => $value ) { ?>
        <tr>
          <td class="alignC">
            <input type="button" name="delete" value="削除" class="del_mode" del_value="./favorite_item_control.php?cd=<?=$value['f_cd']?>">
          </td>
          <td class="alignC">
            <a href="./item_detail.php?cd=<?=$value['i_cd']?>" target="_blank">
              <img src="<?=$value['img_pass1']?>" alt="<?=$value['i_name']?>" style="width: 65px;">
            </a>
          </td>
          <td>
            <a href="./item_detail.php?cd=<?=$value['i_cd']?>" target="_blank">
              <?=$value['i_name']?>
            </a>
          </td>
          <td class="alignR sale_price">
            <span class="price"><?=number_format($value['i_price'])?>円</span>
          </td>
        </tr>
        <?php } ?>
      </table>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
