﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

if ( ctype_digit($_GET['cd']) ) {
  $cd = $_GET['cd'];

  $cartInFlg = true;

  if ($_SESSION['cart'] ) {

    if ( in_array($_GET['cd'], $_SESSION['cart']) ) {
      $errorText = 'すでにカートに入っています。';
      $cartInFlg = false; 
    }

  }

} else {
  header("Location: ./item.php");
  exit;
}


$table_name = 'property';


//物件取得ここから
  $_select     = "cd, 
          date, 
          name, 
          comment, 
          price, 
          area_cd, 
          age_cd, 
          REPLACE(
            REPLACE(
              REPLACE(commitment_cd, '[', ''), 
              ']', 
              ''
            ), 
            '\"', 
            ''
          ) AS commitment_cd, 
          disp_flg, 
          img1, 
          img2, 
          sort";
$sql     = "SELECT {$_select} FROM {$table_name} WHERE disp_flg = 1 AND cd = {$cd}";

$property_query = mysqli_query($connect,$sql);
$property_max   = mysqli_num_rows($property_query);

for ($i = 0; $i < $property_max; $i++) {
  $propertyArray[$i] = mysqli_fetch_assoc($property_query);


  //エリア取得ここから
  if ( ctype_digit( strval($propertyArray[$i]['area_cd']) ) ) {

    $sql = "SELECT * FROM area WHERE disp_flg = 1 AND cd = {$propertyArray[$i]['area_cd']}";
    $area_query = mysqli_query($connect, $sql);
    $area_max   = mysqli_num_rows($area_query);

    if ($area_max) {
      $areaArray = mysqli_fetch_assoc($area_query);

      $propertyArray[$i]['areaArray'] = $areaArray;
    }

  }
  //エリア取得ここまで


  //築年数取得ここから
  if ( ctype_digit( strval($propertyArray[$i]['age_cd']) ) ) {

    $sql = "SELECT cd, name FROM age WHERE disp_flg = 1 AND cd = {$propertyArray[$i]['age_cd']}";
    $age_query = mysqli_query($connect, $sql);
    $age_max   = mysqli_num_rows($age_query);

    if ($age_max) {
      $ageArray = mysqli_fetch_assoc($age_query);

      $propertyArray[$i]['ageArray'] = $ageArray;
    }

  }
  //築年数取得ここまで


  //こだわり条件取得ここから
  if ($propertyArray[$i]['commitment_cd']) {

    $sql = "SELECT cd, name FROM commitment WHERE disp_flg = 1 AND cd IN ({$propertyArray[$i]['commitment_cd']})";

    $commitment_query = mysqli_query($connect, $sql);
    $commitment_max   = mysqli_num_rows($commitment_query);

    if ($commitment_max) {

      for ($j = 0; $j < $commitment_max; $j++) {
        $commitmentArray[] = mysqli_fetch_assoc($commitment_query);
        $propertyArray[$i]['commitment_cdArray'][] = $commitmentArray[$j]['name'];
      }
    }

    $propertyArray[$i]['commitment_cdArray_'] = implode(",", $propertyArray[$i]['commitment_cdArray']);

  }
  //こだわり条件取得ここまで

  for ($j = 1; $j < 3; $j++) {

    if ($propertyArray[$i]["img{$j}"]) {
      $propertyArray[$i]["img_pass{$j}"] = $img_pass.$propertyArray[$i]["img{$j}"];
      $propertyArray[$i]['imgArray'][] = $propertyArray[$i]["img_pass{$j}"];
    } else {
      $propertyArray[$i]['imgArray'][$j] = "./image/no_image.png";
    }

  }

}
//物件取得ここまで



// echo '<pre>';
// print_r($propertyArray[0]['imgArray']);
// echo '</pre>';


?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2><?=$itemDetailArray[0]['name']?></h2>
        
        <div class="itemDetailBox">


          <ul class="imgDetailList">

            <?php foreach ( (array)$propertyArray[0]['imgArray'] AS $key => $val ) { ?>
            <?php $cnt = 1; ?>

              <?php if ($val) { ?>
              <li><img src="<?=$val?>" alt="<?=$propertyArray[0]['name']?>"></li>
              <?php } ?>

              <?php $cnt++; ?>
            <?php } ?>

          </ul>

          <div class="itemRightBox">
            <p class="detailItem_cord">商品コード ： <?=$propertyArray[0]['cd']?></p>
            <p class="detailItem_category">エリア ： <?=$propertyArray[0]['areaArray']['name']?></p>
            <p class="detailItem_category">築年数 ： <?=$propertyArray[0]['ageArray']['name']?></p>
            <p class="detailItem_category">こだわり条件 ： <?=$propertyArray[0]['commitment_cdArray_']?></p>
            <p class="detailPrice">値段 : <?=number_format($propertyArray[0]['price'])?>円</p>
            
            <p class="detailComment"><?=nl2br($propertyArray[0]['comment'])?></p>

          </div>

        </div>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
