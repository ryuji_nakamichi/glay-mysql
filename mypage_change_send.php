<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';

$recordControlObj = new recordControlClass;

$table_name = "shopping_member";


//クロスサイトスクリプティング対策
foreach ($_REQUEST AS $key => $val) {

	if ( !is_array($val) ){
		$_REQUEST[$key] = htmlspecialchars($val);
		$_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
	}

}


//会員のcd番号格納
$cd      = $_SESSION['user']['user_info']['cd'];
//echo $cd

//POSTされてきたデータを変数に定義
$flg     = $_REQUEST['flg'];
$name1   = $_REQUEST['name1'];
$name2   = $_REQUEST['name2'];
$kana1   = $_REQUEST['kana1'];
$kana2   = $_REQUEST['kana2'];

if ($_REQUEST['sex']) {
	$sex = $_REQUEST['sex'];
}

if ($_REQUEST['job_type']) {
	$job_type = $_REQUEST['job_type'];
}

$company_name       = $_REQUEST['company_name'];
$birth_year         = $_REQUEST['birth_year'];
$birth_month        = $_REQUEST['birth_month'];
$birth_day          = $_REQUEST['birth_day'];
$post_code1         = $_REQUEST['post_code1'];
$post_code2         = $_REQUEST['post_code2'];
$address            = $_REQUEST['address'];
$tel                = $_REQUEST['tel'];
$fax                = $_REQUEST['fax'];
$mail               = $_REQUEST['mail'];
$phone_mail_address = $_REQUEST['phone_mail_address'];


if ($flg == 1) {

	$updateDataArray = array(
		"name1"              => $name1,
		"name2"              => $name2,
		"kana1"              => $kana1,
		"kana2"              => $kana2,
		"company_name"       => $company_name,
		"sex"                => $sex,
		"job_type"           => $job_type,
		"birth_year"         => $birth_year,
		"birth_month"        => $birth_month,
		"birth_day"          => $birth_day,
		"post_code1"         => $post_code1,
		"post_code2"         => $post_code2,
		"address"            => $address,
		"tel"                => $tel,
		"fax"                => $fax,
		"mail"               => $mail,
		"phone_mail_address" => $phone_mail_address,
		"update_record_time" => "current_timestamp"
	);
		
	$queryResult = $recordControlObj->recordUpdate($connect, $table_name, $updateDataArray, $cd);


	//会員登録内容変更に成功した場合
	if ($queryResult) {


		//会員登録内容変更に成功した場合、statusにsuccessを代入
		$resArray['status'] = 'success';


	//会員登録内容変更に失敗した場合
	} else {


		//会員登録内容変更に失敗した場合、faildを代入
		$resArray['status'] = 'faild';

	}

	
} else {
	$resArray['status'] = 'query_faild';
}


//3秒処理を止める
//sleep(3);

echo json_encode($resArray);