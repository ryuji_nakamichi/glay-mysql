﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/shopping_check.php';


// クロスサイドスクリプティング対策
foreach ($_REQUEST AS $key => $val) {

  if ( !is_array($val) ) {
    $_REQUEST[$key] = htmlspecialchars($val);
    $_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
  }

}


//変数定義ここから
if ($_POST['deliv_id']) {
  $deliv_id = $_POST['deliv_id'];
  $_SESSION['shopping']['deliv_id'] = $deliv_id;
}

if ($_POST['payment_id']) {
  $payment_id = $_POST['payment_id'];
  $_SESSION['shopping']['payment_id'] = $payment_id;
}

if ($_POST['deliv_time_id0']) {
  $deliv_time_id0 = $_POST['deliv_time_id0'];
  $_SESSION['shopping']['deliv_time_id0'] = $deliv_time_id0;
}

if ($_POST['point_check']) {
  $point_check = $_POST['point_check'];
  $_SESSION['shopping']['point_check'] = $point_check;
}

if ($_POST['use_point']) {
  $use_point = $_POST['use_point'];
  $_SESSION['shopping']['use_point'] = $use_point;
} else {
  $_SESSION['shopping']['use_point'] = 0;
}

if ($_POST['message']) {
  $message = $_POST['message'];
  $_SESSION['shopping']['message'] = $message;
}


if ($_SESSION['user_info']['sex'] == 0) {
  $_SESSION['user']['user_info']['sex_'] = '男性';
} else {
  $_SESSION['user']['user_info']['sex_'] = '女性';
}

if ($_SESSION['shopping']['payment_id'] == 1) {
  $_SESSION['shopping']['payment_id_'] = '郵便振替';
} else if ($_SESSION['shopping']['payment_id'] == 2) {
  $_SESSION['shopping']['payment_id_'] = '現金書留';
} else if ($_SESSION['shopping']['payment_id'] == 3) {
  $_SESSION['shopping']['payment_id_'] = '銀行振込';
} else if ($_SESSION['shopping']['payment_id'] == 4) {
  $_SESSION['shopping']['payment_id_'] = '代金引換';
}

if ($_SESSION['shopping']['deliv_time_id0'] === '1') {
  $_SESSION['shopping']['deliv_time_id0_'] = '午前中';
} else if ($_SESSION['shopping']['deliv_time_id0'] === '2') {
  $_SESSION['shopping']['deliv_time_id0_'] = '午後';
} else {
  $_SESSION['shopping']['deliv_time_id0_'] = '指定なし';
}


//ポイント計算結果
$_SESSION['shopping']['result_point'] = $_SESSION['user']['user_info']['point'] - $_SESSION['shopping']['use_point'] + $_SESSION['cartInfo']['add_point_increment'];
//変数定義ここまで


//会員のお届け先を取得
$mypageClassObj      = new mypageClass;
$other_delivAllArray = $mypageClassObj->getDelivDetail($connect, $_SESSION['shopping']['deliv_check']);
$other_delivArray    = $other_delivAllArray['other_delivArray'];


//お届け先の住所が会員の住所を選択されている場合
if (!$other_delivArray) {
  $other_delivArray[0]['name1']        = $_SESSION['user']['user_info']['name1'];
  $other_delivArray[0]['name2']        = $_SESSION['user']['user_info']['name2'];
  $other_delivArray[0]['kana1']        = $_SESSION['user']['user_info']['kana1'];
  $other_delivArray[0]['kana2']        = $_SESSION['user']['user_info']['kana2'];
  $other_delivArray[0]['company_name'] = $_SESSION['user']['user_info']['company_name'];
  $other_delivArray[0]['zip1']         = $_SESSION['user']['user_info']['post_code1'];
  $other_delivArray[0]['zip2']         = $_SESSION['user']['user_info']['post_code2'];
  $other_delivArray[0]['address1']     = $_SESSION['user']['user_info']['address'];
  $other_delivArray[0]['tel']          = $_SESSION['user']['user_info']['tel'];
  $other_delivArray[0]['fax']          = $_SESSION['user']['user_info']['fax'];

} else {
  $_SESSION['other_delivArray'] = $other_delivArray[0];
}


//複数のお届け先に送る場合ここから
if ($_SESSION['cartInfo']['cartInfoCopyArray']) {
  $cartInfoCopyArray = $_SESSION['cartInfo']['cartInfoCopyArray'];


  //商品の個数分ループ
  foreach ( (array) $cartInfoCopyArray AS $key => $value ) {


    //お届け先取得
    $_SELECT = "o.cd AS o_cd, o.member_cd AS o_member_cd, o.name1 AS o_name1, o.name2 AS o_name2, o.kana1 AS o_kana1, o.kana2 AS o_kana2, o.company_name AS o_company_name, o.zip1 AS o_zip1, o.zip2 AS o_zip2, o.address1 AS o_address1, o.tel AS o_tel, o.fax AS o_fax";
    $sql     = "SELECT {$_SELECT} FROM other_deliv AS o WHERE o.disp_flg = 1 AND o.cd = {$value['shipping']} ORDER BY o.sort";
    $other_deliv_shipping_query = mysqli_query($connect, $sql);
    $other_deliv_shipping_max   = mysqli_num_rows($other_deliv_shipping_query);

    for ($i = 0; $i < $other_deliv_shipping_max; $i++) {

      $other_deliv_shippingArray[] = mysqli_fetch_assoc($other_deliv_shipping_query);
      $value['o_cd']               = $other_deliv_shippingArray[$key]['o_cd'];
      $value['shppingArray']       = $other_deliv_shippingArray[$key];

    }

    $value['multiple_quantity_total_price'] = $value['quantity'] * $value['price_plus_tax'];


    //お届け先のcd番号基準に、商品の配列を代入
    $copyArray                        = $value;
    $copyAllArray[$copyArray['cd']][] = $value;
    
  }


  //各お届け先に紐づいた商品の配列を、商品のcd番号別に統合
  foreach ( (array) $copyAllArray AS $key => $value ) {

    foreach ( (array) $value AS $key2 => $value2 ) {
      $orderItemArray = $value2;
      $orderAllItemArray[$orderItemArray['o_cd']][] = $orderItemArray;
      $cdArray[$orderItemArray['o_cd']][] = $value2['cd'];
    }

  }


  //各お届け先に紐づいた商品について、同じ商品が被らないように整理ここから
  $orderAllDataItemArray = [];
  $searchDataArray       = [];
  foreach ( (array) $orderAllItemArray AS $key => $value ) {

    $loop_cnt = 0;
    $quantity_cnt = 1;

    foreach ( (array) $value AS $key2 => $value2 ) {

      if ( !in_array($value2['cd'], (array)$searchDataArray) ) {
        $searchDataArray                   = $value2['cd'];
        $value2['multiple_quantity_total'] = $value2['item_number'] - $value2['quantity'];      
        $orderAllDataItemArray[$key][]     = $value2;
      } else {
        // $value2['multiple_quantity_total'] += $quantity_cnt;
        // $orderAllDataItemArray[$key][]     = $value2;

      }

      //$quantity_cnt++;

     }
  }
  //各お届け先に紐づいた商品について、同じ商品が被らないように整理ここまで


  //各お届け先に紐づいた商品の配列を、商品のcd番号別に統合した配列を代入
  $_SESSION['shopping_multiple']['mailSendArray'] = $orderAllDataItemArray;

}
//複数のお届け先に送る場合ここまで


// echo '<pre>';
// print_r($orderAllDataItemArray);
// echo '</pre>';

?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <ol class="list-shopping-flow">
        <li>STEP1<br><span class="pc">お届け先指定</span></li>
        <li>STEP2<br><span class="pc">お支払い方法等選択</span></li>
        <li class="active">STEP3<br><span class="pc">内容確認</span></li>
        <li>STEP4<br><span class="pc">完了</span></li>
      </ol>

      <h2>入力内容のご確認</h2>

      <p class="information">下記ご注文内容で送信してもよろしいでしょうか？<br>
            よろしければ、「ご注文完了ページへ」ボタンをクリックしてください。</p>

      <form name="form1" id="form1" method="post" action="./shopping_complete.php">
        <input type="hidden" name="shopping_confirm_flg" value="1">
        <ul class="btnList">
          <li><a class="backBtn" href="./shopping_payment.php" onclick="history.back();">戻る</a></li>
          <li><input class="nextBtn" type="submit" name="next" value="ご注文完了ページへ" id="next"></li>
        </ul>

        <table class="shoppingTbl">
          <tbody>
            <tr>
              <th>商品写真</th>
              <th>商品名</th>
              <th>単価</th>
              <th>数量</th>
              <th>小計</th>
            </tr>

            <?php foreach ($_SESSION['cartInfo']['cartInfoArray'] AS $key => $value) { ?>
            <tr>
              <td class="alignC">
                <a href="<?=$value['img_pass1']?>" target="_blank"><img src="<?=$value['img_pass1']?>" alt="<?=$value['name']?>" style="width: 100px;"></a>
              </td>
              <td class="alignC"><?=$value['name']?></td>
              <td class="alignR"><?=$value['price_plus_tax_format']?>円</td>
              <td class="alignR"><?=$value['item_number']?></td>
              <td class="alignR"><?=$value['total_price_format']?>円</td>
            </tr>
            <?php } ?>

            <tr>
              <th colspan="4" class="alignR">小計</th>
              <td class="alignR"><?=$_SESSION['cartInfo']['all_total_price_format']?>円</td>
            </tr>
            <?php if ($_SESSION['shopping']['use_point']) { ?>
            <tr>
              <th colspan="4" class="alignR">値引き（ポイントご使用時）</th>
              <td class="alignR">-<?=number_format($_SESSION['shopping']['use_point'])?>円</td>
            </tr>
            <?php } ?>
            <tr>
              <th colspan="4" class="alignR">送料</th>
              <td class="alignR"><?=number_format($_SESSION['cartInfo']['shipping_cost'])?>円</td>
            </tr>
            <tr>
              <th colspan="4" class="alignR">手数料</th>
              <td class="alignR">0円</td>
            </tr>
            <tr>
              <th colspan="4" class="alignR">合計</th>
              <td class="alignR"><span class="price"><?=number_format($_SESSION['cartInfo']['all_total_price']+$_SESSION['cartInfo']['shipping_cost']-$_SESSION['shopping']['use_point'])?>円</span></td>
            </tr>
          </tbody>
        </table>

        <table class="shoppingTbl delivname">
          <tbody>
            <tr>
                <th>ご注文前のポイント</th>
                <td><?=$_SESSION['cartInfo']['point']?>Pt</td>
            </tr>
            <tr>
                <th>ご使用ポイント</th>
                <td>-<?=$_SESSION['shopping']['use_point']?>Pt</td>
            </tr>
            <tr>
              <th>今回加算予定のポイント</th>
              <td>+<?=$_SESSION['cartInfo']['add_point_increment']?>Pt</td>
            </tr>
            <tr>
              <th>加算後のポイント</th>
              <td><?=$_SESSION['shopping']['result_point']?>Pt</td>
            </tr>
          </tbody>
        </table>
                        
          <h3>ご注文者</h3>
          <table class="shoppingTbl customerTbl">
            <tbody>
              <tr>
                <th>お名前</th>
                <td><?=$_SESSION['user']['user_info']['name1']?> <?=$_SESSION['user']['user_info']['name2']?></td>
              </tr>
              <tr>
                <th>お名前(フリガナ)</th>
                <td><?=$_SESSION['user']['user_info']['kana1']?> <?=$_SESSION['user']['user_info']['kana2']?></td>
              </tr>
              <tr>
                <th>会社名</th>
                <td><?=$_SESSION['user']['user_info']['company_name']?></td>
              </tr>
              <tr>
                <th>郵便番号</th>
                <td>〒<?=$_SESSION['user']['user_info']['post_code1']?>-<?=$_SESSION['user']['user_info']['post_code2']?></td>
              </tr>
              <tr>
                <th>住所</th>
                <td><?=$_SESSION['user']['user_info']['address']?></td>
              </tr>
              <tr>
                <th>電話番号</th>
                <td><?=$_SESSION['user']['user_info']['tel']?></td>
              </tr>
              <tr>
                <th>FAX番号</th>
                <td><?=$_SESSION['user']['user_info']['fax']?></td>
              </tr>
              <tr>
                <th>メールアドレス</th>
                <td><?=$_SESSION['user']['user_info']['mail']?></td>
              </tr>
              <tr>
                <th>携帯メールアドレス</th>
                <td><?=$_SESSION['user']['user_info']['phone_mail_address']?></td>
              </tr>
              <tr>
                <th>性別</th>
                <td><?=$_SESSION['user']['user_info']['sex_']?></td>
              </tr>
              <tr>
                <th>職業</th>
                <td><?=$_SESSION['user']['user_info']['job_type_name']?></td>
              </tr>
              <tr>
                <th>生年月日</th>
                <td><?=$_SESSION['user']["user_info"]['full_birth']?></td>
              </tr>
            </tbody>
          </table>


          <?php
          /******************************************************************
          お届け先が複数の場合
          ******************************************************************/
          ?>

          <?php if ($_SESSION['shopping_multiple']) { ?>

            <?php
            $cnt = 1;
            foreach ( (array) $orderAllDataItemArray AS $key => $value ) {


              //お届け先毎の商品の小計初期化
              $total_price_tax = 0;
            ?>
            <h3>お届け先<?=$cnt?></h3>

            <table class="shoppingTbl delivname">
              <tbody>
                <tr>
                  <th>お名前</th>
                  <td><?=$value[0]['shppingArray']['o_name1']?> <?=$value[0]['shppingArray']['o_name2']?></td>
                </tr>
                <tr>
                  <th>お名前(フリガナ)</th>
                  <td><?=$value[0]['shppingArray']['o_kana1']?> <?=$value[0]['shppingArray']['o_kana2']?></td>
                </tr>
                <tr>
                  <th>会社名</th>
                  <td><?=$value[0]['shppingArray']['o_company_name']?></td>
                </tr>
                <tr>
                  <th>郵便番号</th>
                  <td>〒<?=$value[0]['shppingArray']['o_zip1']?>-<?=$value[0]['shppingArray']['o_zip2']?></td>
                </tr>
                <tr>
                  <th>住所</th>
                  <td><?=$value[0]['shppingArray']['o_address1']?></td>
                </tr>
                <tr>
                    <th>電話番号</th>
                    <td><?=$value[0]['shppingArray']['o_tel']?></td>
                </tr>
                <tr>
                  <th>FAX番号</th>
                  <td><?=$value[0]['shppingArray']['o_fax']?></td>
                </tr>
                <tr>
                  <th>お届け日</th>
                  <td>指定なし</td>
                </tr>
                <tr>
                    <th>お届け時間</th>
                    <td><?=$_SESSION['shopping']['deliv_time_id0_']?></td>
                </tr>
              </tbody>
            </table>

              
              <table class="shoppingTbl">
                <caption>お届け先<?=$cnt?>の商品</caption>
                <?php
                
                foreach ( (array) $value AS $key2 => $value2 ) {


                  //お届け先毎の商品の小計計算
                  $total_price_tax += $value2['total_price_tax'];
                  ?>
                <tr>
                  <td class="alignC">
                    <a href="<?=$value2['img_pass1']?>" target="_blank"><img src="<?=$value2['img_pass1']?>" alt="<?=$value2['name']?>" style="width: 100px;"></a>
                  </td>
                  <td class="alignC"><?=$value2['name']?></td>
                  <td class="alignR"><?=$value2['price_plus_tax_format']?>円</td>
                  <td class="alignR"><?=$value2['item_number']?></td>
                  <td class="alignR"><?=$value2['total_price_tax']?>円</td>
                </tr>
              <?php } ?>

              <tr>
                <th colspan="4" class="alignR">小計</th>
                <td class="alignR"><?=$total_price_tax?>円</td>
              </tr>
              </table>

            <?php
            $cnt++;
            }
            ?>     

          <?php } else { ?>
          <h3>お届け先</h3>
          <table class="shoppingTbl delivname">
            <tbody>
              <tr>
                <th>お名前</th>
                <td><?=$other_delivArray[0]['name1']?> <?=$other_delivArray[0]['name2']?></td>
              </tr>
              <tr>
                <th>お名前(フリガナ)</th>
                <td><?=$other_delivArray[0]['kana1']?> <?=$other_delivArray[0]['kana2']?></td>
              </tr>
              <tr>
                <th>会社名</th>
                <td><?=$other_delivArray[0]['company_name']?></td>
              </tr>
              <tr>
                <th>郵便番号</th>
                <td>〒<?=$other_delivArray[0]['zip1']?>-<?=$other_delivArray[0]['zip2']?></td>
              </tr>
              <tr>
                <th>住所</th>
                <td><?=$other_delivArray[0]['address1']?></td>
              </tr>
              <tr>
                  <th>電話番号</th>
                  <td><?=$other_delivArray[0]['tel']?></td>
              </tr>
              <tr>
                <th>FAX番号</th>
                <td><?=$other_delivArray[0]['fax']?></td>
              </tr>
              <tr>
                <th>お届け日</th>
                <td>指定なし</td>
              </tr>
              <tr>
                  <th>お届け時間</th>
                  <td><?=$_SESSION['shopping']['deliv_time_id0_']?></td>
              </tr>
            </tbody>
          </table>
          <?php } ?>
                        
          <h3>配送方法・お支払方法・その他お問い合わせ</h3>
          <table class="shoppingTbl delivname">
            <tbody>

              <?php
              //配送方法(table)表示ここから
              $cartInfo_max = ($_SESSION['cartInfo']['cartInfoArray']) ? count($_SESSION['cartInfo']['cartInfoArray']): 0;

              $cartInfo_rowspan = ($cartInfo_max) ? " rowspan=\"{$cartInfo_max}\"": '';
              $cnt = 0;

              foreach ( (array) $_SESSION['cartInfo']['cartInfoArray'] AS $key => $value ) {
              $cartInfo_colspan  = ($cnt !== 0) ? ' colspan="2"': '';
              $cartInfo_tr_start = ($cnt !== 0) ? '<tr>': '';
              $cartInfo_tr_end   = ($cnt !== 0) ? '</tr>': '';
              ?>

                <?php if ($cnt === 0) { ?>
                <tr>
                    <th<?=$cartInfo_rowspan?>>配送方法</th>
                    <td><?=$value['name']?></td>
                    <td<?=$cartInfo_colspan?>><?=$value['shipping_type_name']?></td>
                </tr>
                <?php } else { ?>
                <tr>
                    <td><?=$value['name']?></td>
                    <td<?=$cartInfo_colspan?>><?=$value['shipping_type_name']?></td>
                </tr>
                <?php } ?>

              <?php $cnt++; ?>
              <?php
              }
              //配送方法(table)表示ここまで
              ?>

              <tr>
                  <th>お支払方法</th>
                  <td colspan="2"><?=$_SESSION['shopping']['payment_id_']?></td>
              </tr>
              <tr>
                  <th>その他お問い合わせ</th>
                  <td colspan="2"><?=$_SESSION['shopping']['message']?></td>
              </tr>
              </tbody>
          </table>

          <ul class="btnList">
            <li><a class="backBtn" href="./shopping_payment.php" onclick="history.back();">戻る</a></li>
            <li><input class="nextBtn" type="submit" name="next" value="ご注文完了ページへ" id="next"></li>
          </ul>
        </form>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
