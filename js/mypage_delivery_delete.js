$(function() {


	//テーブルの名前定義
	var table_name = 'other_deliv';

	//location用(基本)
	var locationUrl = './mypage_delivery.php';


	/**********
	削除処理ここから
	**********/
	$('.del_mode').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('del_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=delete&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$( "#delete_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: cd[1],
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data,status,xhr) {

						}).done(function (data,status,xhr) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#delete_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**********
	削除処理ここまで
	**********/

});