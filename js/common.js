$(function() {


	/********************************************************************
	ハンバーガーメニュー処理
	********************************************************************/
	$('.hamburger').on('click', function() {

	var navigation = $('#navigation');
	var hamburger  = $('.hamburger');

	if (navigation.css('display') == 'block') {
		hamburger.removeClass('navi_fixed');
		hamburger.removeClass('active');
		$('#navigation').stop().fadeOut(600);
	} else {
		hamburger.addClass('navi_fixed');
		hamburger.addClass('active');
		$('#navigation').stop().fadeIn(600);
	}

	});

});