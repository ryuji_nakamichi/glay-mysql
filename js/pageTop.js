﻿/********************************************************************
2017_04_09 nakamichi ページトップ処理作成
********************************************************************/

$(function() {

	var $main         = $('html, body');
	var $pTop         = $('#pagetop');
	var $header       = $('header');
	var $targetTop    = '';
	var $headerHeight = $header.outerHeight();


	//ページトップボタン非表示
	$pTop.hide();

	$(window).scroll(function() {

		$targetTop = $(this).scrollTop();

		if ( $headerHeight <= $targetTop ) {
			$pTop.show();
		} else {
			$pTop.hide();
		}

	});

	$pTop.on('click', function() {

		$main.animate({
			scrollTop:$main.offset().top
		}, 2000, "easeOutQuint");

		return false;

	});

})