$(function() {


	//テーブルの名前定義
	var table_name = 'favorite_item';

	//次（もしくは現在）のcd番号取得
	var cd_number  = $('#next_cd').val();

	//location用(基本)
	var locationUrl = './mypage_favorite.php';

	//location用(追加後)
	var url_          = location.href;
	var insert_url_cd = url_.split('=');
	var insert_url    = 'item_detail.php?cd=' + insert_url_cd[1];

	/**************
	新規登録処理ここから
	**************/
	$('.addButton').on('click',function() {


		/**********
		ダイアログここから
		**********/
		$("#insert_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						// Loadingイメージ表示
						// 引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						/********************
						ajaxレコード登録処理ここから
						*********************/
						var form = $('#' + table_name);
						var serializeData = form.serialize();
						//console.log(serializeData);
						$.ajax({
						    method: 'POST',
						    url: './' + table_name + '_control.php?mode=insert',
							data: serializeData,
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data,status,xhr) {
							console.log(data);

						    //エラー処理
						    alert( 'データ登録に失敗しました。' );

						}).done(function (data,status,xhr) {
							console.log(data);


			                // Loadingイメージを消す
			                removeLoading();


						    //成功処理
							$( "#insert_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = insert_url;
										}
									}
								]
							});
						});
						/********************
						ajaxレコード登録処理ここまで
						*********************/

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**************
	新規登録処理ここまで
	**************/


	/**********
	削除処理ここから
	**********/
	$('.del_mode').on('click',function() {


		//ダイアログ内で以下の３つの変数を定義するとエラーが出るためここに記述
		var url        = $(this).attr('del_value');
		var cd         = url.split('=');
		var url_params = './' + table_name + '_control.php?mode=delete&cd='+ cd[1];


		/**********
		ダイアログここから
		**********/
		$( "#delete_dialog" ).dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


						//Loadingイメージ表示
						//引数は画像と共に表示するメッセージ
						dispLoading("処理中...");


						//二重投稿禁止関数
						dxTransProhibited();


						//ajaxレコード削除処理
						$.ajax({
						    method: 'POST',
						    url: url_params,
							data: cd[1],
							datatype: 'json',
						    timeout: 10000
						}).fail(function (data,status,xhr) {

						}).done(function (data,status,xhr) {


			                //Loadingイメージを消す
			                removeLoading();


						    //成功処理
						   $( "#delete_complete_dialog" ).dialog({
								autoOpen: true,
								width: 300,
								buttons: [
									{
										text: "Ok",
										click: function() {
											$( this ).dialog( "close" );
											location.href = locationUrl;
										}
									}
								]
							});
						});

					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/**********
		ダイアログここまで
		**********/
	});
	/**********
	削除処理ここまで
	**********/

});