// Loadingイメージ表示関数
function dispLoading(msg){
    // 画面表示メッセージ
    var dispMsg = "";
 
    // 引数が空の場合は画像のみ
    if( msg != "" ){
        dispMsg = "<div class='loadingMsg'>" + msg + "</div>";
    }
    // ローディング画像が表示されていない場合のみ表示
    if ( $("#loading").size() == 0 ) {
        $("body").append("<div id='loading'>" + dispMsg + "</div>");
    } 
}
 
// Loadingイメージ削除関数
function removeLoading() {
 $("#loading").remove();
}


$(function() {

	$.fn.autoKana('#name',  '#kana', { katakana : false });


	/*****************************************************************
	各変数定義ここから
	*****************************************************************/

	//メール送信成功後に飛ばす先のURL
	var url  = './contact.php';

	//formタグのID属性の値
	var form = $('#form1');

	//ajaxによって送信されるデータを、どのPHPファイルに渡すか
	var send_url = './send.php';

	//formで送信されるデータを「data」変数に格納
	var data_ = form.serialize();

	//どの形式でデータを受け取るか
	var data_type = 'json';

	//何秒まで待つか(過ぎるとfalseの処理が始まる)
	var timeout_ = 10000;

	//処理が成功した場合に表示されるメッセージ
	var success_message = '正常に送信できました。';

	//処理が失敗した場合に表示されるメッセージ
	var fail_message = '正常に送信できませんでした。もう一度お試しください。';

	//処理が成功した場合にurlに足されるflg
	var success_flg_url = '?flg=1';

	/*****************************************************************
	各変数定義ここまで
	*****************************************************************/

	$('#submit').on('click',function() {


		/*****************************************************************
		入力チェックここから
		*****************************************************************/

		var name = $('#name').val();
		var kana = $('#kana').val();
		var mail = $('#mail').val();
		var msg  = $('#msg').val();

		if (name == '') {
			var nameError = '名前を入力してください。';
			$('#nameError').text(nameError);
		} else {
			$('#nameError').text('');
		}

		if (kana == '') {
			var kanaError = 'ふりがなを入力してください。';
			$('#kanaError').text(kanaError);
		} else {
			$('#kanaError').text('');
		}

		if (mail == '') {
			var mailError = 'メールアドレスを入力してください。';
			$('#mailError').text(mailError);
		} else if ( !mail.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/) ) {
			var mailError = 'メールアドレスの形式が正しくありません。';
			$('#mailError').text(mailError);
		} else {
			$('#mailError').text('');
		}

		if (msg == '') {
			var msgError = 'お問い合わせ内容を入力してください。';
			$('#msgError').text(msgError);
		} else {
			$('#msgError').text('');
		}

		/*****************************************************************
		入力チェックここまで
		*****************************************************************/

		if (name == '' || kana == '' || mail == '' || msg == '') {
			alert('入力されていない項目があります。');
			return false;

		} else {

			// Loadingイメージ表示
	        // 引数は画像と共に表示するメッセージ
	        dispLoading("処理中...");

			$.ajax({
				method: form.attr('method'),
			    url: send_url,
			    data: form.serialize(),
			    dataType: data_type,
			    timeout: timeout_
			}).done(function(data,status,xh){
			    alert(success_message);
			    
			}).fail(function(data,status,xh){
			    alert(fail_message);
			}).complete(function(data) {
                // Loadingイメージを消す
                removeLoading();
                location.href = url + success_flg_url;
            });

		}

	});
});