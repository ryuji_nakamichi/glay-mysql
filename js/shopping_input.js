$(function() {

	$.fn.autoKana('#name',  '#kana', { katakana : false });


	// 郵便番号
	$('#zip1').jpostal({
		postcode : [
			'#zip1', //郵便番号上3ケタ
			'#zip2'  //郵便番号下4ケタ
		],
		address : {
			'#address'  : '%3%4%5' //都道府県 市区町村 町域
			//'#city'  : '%4%5' //市区町村 町域
		}
	});

	$('#submit').on('click',function() {


		/*****************************************************************
		入力チェックここから
		*****************************************************************/

		var name    = $('#name').val();
		var kana    = $('#kana').val();
		var mail    = $('#mail').val();
		var tel     = $('#tel').val();
		var zip1    = $('#zip1').val();
		var zip2    = $('#zip2').val();
		var address = $('#address').val();

		if (name == '') {
			var nameError = '名前を入力してください。';
			$('#nameError').text(nameError);
		} else {
			$('#nameError').text('');
		}

		if (kana == '') {
			var kanaError = 'ふりがなを入力してください。';
			$('#kanaError').text(kanaError);
		} else {
			$('#kanaError').text('');
		}

		if (mail == '') {
			var mailError = 'メールアドレスを入力してください。';
			$('#mailError').text(mailError);
		} else if ( !mail.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/) ) {
			var mailError = 'メールアドレスの形式が正しくありません。';
			$('#mailError').text(mailError);
		} else {
			$('#mailError').text('');
		}

		if (zip1 == '' || zip2 == '') {
			var zipError = '郵便番号入力してください。';
			$('#zipError').text(zipError);
		} else {
			$('#zipError').text('');
		}

		if (address == '') {
			var addressError = '住所を入力してください。';
			$('#addressError').text(addressError);
		} else {
			$('#addressError').text('');
		}

		if (tel == '') {
			var telError = '電話番号を入力してください。';
			$('#telError').text(telError);
		} else {
			$('#telError').text('');
		}


		/*****************************************************************
		入力チェックここまで
		*****************************************************************/

		if (name == '' || kana == '' || mail == '' || !mail.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/) || tel == '') {
			alert('入力されていない項目があります。');
			return false;

		} else {


		}

	});
});