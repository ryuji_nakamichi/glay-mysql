$(function(){

    $('.slider').slick(
    {
        dots: true,
        fade: false,
        centerMode: true,
        variableWidth: true,
        centerPadding: '0px',
        slidesToShow: 3,
        responsive: [

        {
            breakpoint: 768,
            settings: {
            arrows: true,
            centerMode: true,
            variableWidth: false,
            centerPadding: '0px',
            slidesToShow: 1
        }
    },
        {
            breakpoint: 480,
            settings: {
            arrows: true,
            centerMode: true,
            variableWidth: false,
            centerPadding: '0px',
            slidesToShow: 1
        }
    }
    ]
    });

});