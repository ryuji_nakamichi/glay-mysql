// Loadingイメージ表示関数
function dispLoading(msg){
    // 画面表示メッセージ
    var dispMsg = "";
 
    // 引数が空の場合は画像のみ
    if(msg != ""){
        dispMsg = "<div class='loadingMsg'>" + msg + "</div>";
    }
    // ローディング画像が表示されていない場合のみ表示
    if ( $("#loading").size() == 0 ) {
        $("body").append("<div id='loading'>" + dispMsg + "</div>");
    } 
}
 
// Loadingイメージ削除関数
function removeLoading() {
	$("#loading").remove();
}


$(function() {

	$.fn.autoKana('#name1',  '#kana1', { katakana : false });
	$.fn.autoKana('#name2',  '#kana2', { katakana : false });


	// 郵便番号
	$('#zip1').jpostal({
		postcode : [
			'#post_code1', //郵便番号上3ケタ
			'#post_code2'  //郵便番号下4ケタ
		],
		address : {
			'#address'  : '%3%4%5' //都道府県 市区町村 町域
			//'#city'  : '%4%5' //市区町村 町域
		}
	});


	/*****************************************************************
	各変数定義ここから
	*****************************************************************/

	//メール送信成功後に飛ばす先のURL
	var url  = './mypage_delivery_addr.php';

	//formタグのID属性の値
	var form = $('#form1');

	//ajaxによって送信されるデータを、どのPHPファイルに渡すか
	var send_url = './mypage_delivery_addr_send.php';

	//formで送信されるデータを「data」変数に格納
	var data_ = form.serialize();

	//どの形式でデータを受け取るか
	var data_type = 'json';

	//何秒まで待つか(過ぎるとfalseの処理が始まる)
	var timeout_ = 10000;

	//処理が成功した場合に表示されるメッセージ
	var success_message = '登録できました。';

	//処理が失敗した場合に表示されるメッセージ
	var fail_message = '登録できませんでした。もう一度お試しください。';

	//処理が成功した場合にurlに足されるflg
	var success_flg_url = '?flg=1';

	/*****************************************************************
	各変数定義ここまで
	*****************************************************************/

	$('#submit').on('click',function() {


		/*****************************************************************
		入力チェックここから
		*****************************************************************/

		var name1      = $('#name1').val();
		var name2      = $('#name2').val();

		var kana1      = $('#kana1').val();
		var kana2      = $('#kana2').val();

		var post_code1 = $('#post_code1').val();
		var post_code2 = $('#post_code2').val();

		var address    = $('#address').val();
		var tel        = $('#tel').val();

		if (name1 == '') {
			var name1Error = '名前（姓）を入力してください。';
			$('#name1Error').text(name1Error);
		} else {
			$('#name1Error').text('');
		}

		if (name2 == '') {
			var name2Error = '名前（名）を入力してください。';
			$('#name2Error').text(name2Error);
		} else {
			$('#name2Error').text('');
		}

		if (kana1 == '') {
			var kana1Error = 'カナ（姓）を入力してください。';
			$('#kana1Error').text(kana1Error);
		} else {
			$('#kana1Error').text('');
		}

		if (kana2 == '') {
			var kana2Error = 'カナ（名）を入力してください。';
			$('#kana2Error').text(kana2Error);
		} else {
			$('#kana2Error').text('');
		}

		if (post_code1 == '') {
			var post_code1Error = '郵便番号を入力してください。';
			$('#post_code1Error').text(post_code1Error);
		} else {
			$('#post_code1Error').text('');
		}

		if (post_code2 == '') {
			var post_code2Error = '郵便番号を入力してください。';
			$('#post_code2Error').text(post_code2Error);
		} else {
			$('#post_code2Error').text('');
		}

		if (address == '') {
			var addressError = '住所を入力してください。';
			$('#addressError').text(addressError);
		} else {
			$('#addressError').text('');
		}

		if (tel == '') {
			var telError = '電話番号を入力してください。';
			$('#telError').text(telError);
		} else {
			$('#telError').text('');
		}


		/*****************************************************************
		入力チェックここまで
		*****************************************************************/

		if (name1 == '' || name2 == '' || kana1 == '' || kana2 == '' || post_code1 == '' || post_code2 == '' || address == '' || tel == '') {
			alert('入力されていない項目があります。');
			return false;

		} else {

			/**********
			ダイアログここから
			**********/
			$("#confirm_dialog").dialog({
				autoOpen: true,
				width: 300,
				buttons: [
					{
						text: "Ok",
						click: function() {
							$( this ).dialog( "close" );

							//Loadingイメージ表示
					        //引数は画像と共に表示するメッセージ
					        //dispLoading("処理中...");

							$.ajax({
								method: form.attr('method'),
							    url: send_url,
							    data: form.serialize(),
							    dataType: data_type,
							    timeout: timeout_
							}).done(function(d,status,xhr){
								//console.log(d);

								//Loadingイメージ削除
								//removeLoading();

							    //成功処理
								$( "#confirm_complete_dialog" ).dialog({
									autoOpen: true,
									width: 300,
									buttons: [
										{
											text: "Ok",
											click: function() {
												$( this ).dialog( "close" );

													if (d['status'] === 'success') {
														location.href = url + success_flg_url;
													} else if (d['status'] === 'query_faild') {
														location.href = url + '?mode=query_faild';
													} else {
														location.href = url + '?mode=entry_faild_' + d['status'];
													}

											}
										}
									]
								});


							    
							}).fail(function(d,status,xhr){
								//console.log(d);
							    alert(fail_message);
				                location.href = url + '?mode=entry_faild';
				            });

						}
					},
					{
						text: "Cancel",
						click: function() {
							$( this ).dialog( "close" );
						}
					}
				]
			});
			/**********
			ダイアログここまで
			**********/

		}

	});
});