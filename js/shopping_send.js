// Loadingイメージ表示関数
function dispLoading(msg){
    // 画面表示メッセージ
    var dispMsg = "";
 
    // 引数が空の場合は画像のみ
    if( msg != "" ){
        dispMsg = "<div class='loadingMsg'>" + msg + "</div>";
    }
    // ローディング画像が表示されていない場合のみ表示
    if ( $("#loading").size() == 0 ) {
        $("body").append("<div id='loading'>" + dispMsg + "</div>");
    } 
}
 
// Loadingイメージ削除関数
function removeLoading() {
 $("#loading").remove();
}


$(function() {


	/*****************************************************************
	各変数定義ここから
	*****************************************************************/

	//メール送信成功後に飛ばす先のURL
	var url  = './shopping_complete.php';

	//formタグのID属性の値
	var form = $('#form1');

	//ajaxによって送信されるデータを、どのPHPファイルに渡すか
	var send_url = './shopping_send.php';

	//formで送信されるデータを「data」変数に格納
	var data_ = form.serialize();

	//どの形式でデータを受け取るか
	var data_type = 'json';

	//何秒まで待つか(過ぎるとfalseの処理が始まる)
	var timeout_ = 10000;

	//処理が成功した場合に表示されるメッセージ
	var success_message = '正常に購入できました。';

	//処理が失敗した場合に表示されるメッセージ
	var fail_message = '正常に購入できませんでした。もう一度お試しください。';

	//処理が成功した場合にurlに足されるflg
	var success_flg_url = '?flg=1';

	/*****************************************************************
	各変数定義ここまで
	*****************************************************************/

	$('#submit').on('click',function() {


		// Loadingイメージ表示
        // 引数は画像と共に表示するメッセージ
        dispLoading("処理中...");

		$.ajax({
			method: form.attr('method'),
		    url: send_url,
		    data: form.serialize(),
		    dataType: data_type,
		    timeout: timeout_
		}).done(function(data,status,xh){
		    alert(success_message);
		    
		}).fail(function(data,status,xh){
		    alert(fail_message);
		}).complete(function(data) {
            // Loadingイメージを消す
            removeLoading();
            location.href = url + success_flg_url;
        });


	});
});