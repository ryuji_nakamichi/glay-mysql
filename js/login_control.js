$(function() {


	//ログイン処理ここから
	$('.loginButton').on('click', function() {

		//ログイン処理
		var form = $('#login');
		$.ajax({
		    method: 'POST',
		    url: './login_control.php?mode=login',
			data: form.serialize(),
			datatype: 'json',
		    timeout: 10000
		}).fail(function (d,status,xhr) {


		    // エラー処理
		    location.href = './login.php?mode=login_error';

		}).done(function (d,status,xhr) {


		    // 成功処理
			if (d['status'] == 'success') {
				location.href = './?mode=login_success';
			} else {
				location.href = './login.php?mode=login_faild';
			}

		});


		//ダイアログここから
		/*
		$("#login_dialog").dialog({
			autoOpen: true,
			width: 300,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );


							//ログイン処理
							var form = $('#login');
							$.ajax({
							    method: 'POST',
							    url: './login_control.php?mode=login',
								data: form.serialize(),
								datatype: 'json',
							    timeout: 10000
							}).fail(function (d,status,xhr) {


							    // エラー処理
							    alert( 'ログインに失敗しました。' );
							    location.href = './login.php?mode=login_error';

							}).done(function (d,status,xhr) {


							    // 成功処理
							    // console.log(data0);

								$( "#login_complete_dialog" ).dialog({
									autoOpen: true,
									width: 300,
									buttons: [
										{
											text: "Ok",
											click: function() {
												$( this ).dialog( "close" );
												// console.log(data0);
												// console.log(status0);
												// console.log(xh0);

												if (d['status'] == 'success') {
													location.href = './index.php?mode=login_success';
												} else {
													location.href = './login.php?mode=login_faild';
												}
												
											}
										}
									]
								});
							});
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});*/
		//ダイアログここまで
	});
	//ログイン処理ここまで
});