﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

if ( ctype_digit($_GET['cd']) ) {
  $cd = $_GET['cd'];

  $cartInFlg = true;

  if ($_SESSION['cart'] ) {

    if ( in_array($_GET['cd'], $_SESSION['cart']['itemCd']) ) {
      $errorText = 'すでにカートに入っています。';
      $cartInFlg = false; 
    }

  }

} else {
  header("Location: ./item.php");
  exit;
}


//item詳細取得
$itemDetailObj   = new itemDetailClass;
$itemDetailArray = $itemDetailObj->getItemDetail($connect, $cd);


//mypageClass(お気に入り)呼び出し
$mypageObj     = new mypageClass;
$favoriteAllArray = $mypageObj->getFavoriteItem($connect, $_REQUEST['cd']);

$favorite_itemArray = $favoriteAllArray['favorite_itemArray'];
$btn_text           = $favoriteAllArray['btn_text'];
$table_name         = $favoriteAllArray['table_name'];
$favorite_item_max  = $favoriteAllArray['favorite_item_max'];

if ($_GET['preview'] == 'true') {
  $preview = true;
}


// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';


?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2><?=$itemDetailArray[0]['name']?></h2>
        
        <div class="itemDetailBox">


          <ul class="imgDetailList">

            <?php foreach ( (array)$itemDetailArray[0]['imgArray'] AS $key => $val ) { ?>
            <?php $cnt = 1; ?>

              <?php if ($val) { ?>
              <li><img src="<?=$val?>" alt="<?=$itemDetailArray[0]['name']?>"></li>
              <?php } ?>

              <?php $cnt++; ?>
            <?php } ?>

          </ul>

          <div class="itemRightBox">
            <p class="detailItem_cord">商品コード ： <?=$itemDetailArray[0]['cd']?></p>
            <p class="detailItem_category">カテゴリー ： <?=$itemDetailArray[0]['item_categoryArray']['name']?></p>
            <p class="detailPrice">値段 : <?=$itemDetailArray[0]['price_format']?>円</p>
            
            <p class="detailComment"><?=nl2br($itemDetailArray[0]['comment'])?></p>

            <div class="detailCartBox">
              <form id="cartForm" name="cartForm" method="post" action="./cart.php">
                <input type="hidden" name="itemCd" value="<?=$itemDetailArray[0]['cd']?>">

                <ul class="cartInputList">

                  <?php if ($cartInFlg === true && $preview !== true) { ?>
                  <li><span class="itemNumberText">数量</span><input id="spinner" class="item_number" name="item_number" type="text" value="1" maxlength="2"></li>
                  <?php } ?>
                  
                  <li><span class="errorText" style="color: red;"><?=$errorText?>

                  <?php if ($preview === true) { ?>
                  プレビューモードではお買い物できません。
                  <?php } ?>
                  </span>

                      <?php if ($cartInFlg === true && $preview !== true) { ?>
                      <input class="cartBtn" type="submit" value="カートに入れる">
                      <?php } ?>

                      <?php if ($preview !== true) { ?>
                        <p class="linkBtn"><a href="./item.php">商品一覧へ</a></p>
                      <?php } ?>

                      <div class="favorite_buttonBox">

                      <?php if ($_SESSION['user_login']["login_flg"] === 1) { ?>

                        <?php if ($favorite_item_max) { ?>
                          <input type="button" id="favorite_button" class="addButton" name="favorite_button" value="<?=$btn_text?>" disabled>
                        <?php } else { ?>
                          <input type="button" id="favorite_button" class="addButton" name="favorite_button" value="<?=$btn_text?>">
                        <?php } ?>
                        
                      <?php } ?>

                      </div>

                  </li>
                </ul>
                
              </form>

              <form id="<?=$table_name?>" name="<?=$table_name?>">
                <input type="hidden" name="cd" value="<?=$cd?>">
              </form>
            </div>

          </div>

        </div>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
