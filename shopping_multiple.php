﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/shopping_check.php';


//会員の情報を取得
$user_infoObj   = new userInfoClass;
$user_infoArray = $user_infoObj->userInfo();


//会員のお届け先一覧を取得
$mypageClassObj      = new mypageClass;
$other_delivAllArray = $mypageClassObj->getDelivList($connect);
$other_delivArray    = $other_delivAllArray['other_delivArray'];


//カートの商品(multiple用)
$cartInfoArray = $_SESSION['cartInfo']['cartInfoArray'];

foreach ( (array) $cartInfoArray AS $key => $value ) {

  $item_number_max = $value['item_number'];

  for ($i = 0; $i < $item_number_max; $i++) {
    $cartInfoCopyArray[] = $value;
  }

}

if ($cartInfoCopyArray) {
  $_SESSION['cartInfo']['cartInfoCopyArray'] = $cartInfoCopyArray;
}

// echo '<pre>';
// print_r($cartInfoCopyArray);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <ol class="list-shopping-flow">
        <li class="active">STEP1<br><span class="pc">お届け先指定</span></li>
        <li>STEP2<br><span class="pc">お支払い方法等選択</span></li>
        <li>STEP3<br><span class="pc">内容確認</span></li>
        <li>STEP4<br><span class="pc">完了</span></li>
      </ol>

      <h2>お届け先の複数指定</h2>

      <div id="address_area" class="clearfix">
        <div class="information">
          <p class="information">各商品のお届け先を選択してください。<br>（※数量の合計は、カゴの中の数量と合わせてください。）</p>
          <p class="mini attention">※最大2件まで登録できます。</p>
        </div>
      </div>

      <p class="addbtn"><a href="./mypage_delivery_addr.php">新しいお届け先を追加する</a></p>

      <form id="shopping_multiple" name="shopping_multiple" method="POST" action="./shopping_payment.php">
        <table class="shoppingTbl">
          <tbody>
            <tr>
              <th class="alignC">商品写真</th>
              <th class="alignC">商品名</th>
              <th class="alignC">数量</th>
              <th class="alignC">お届け先</th>
            </tr>

            <?php foreach ( (array) $cartInfoCopyArray AS $key => $value ) { ?>
              
            <tr>
              <td class="alignC">
                  <a><img src="<?=$value['img_pass1']?>" alt="<?=$value['name']?>"></a>
              </td>
              <td><?=$value['name']?><br>
                  <?=number_format($value['price'])?>円
              </td>
              <td>
                  <input type="text" name="quantity[]" value="1">
              </td>
              <td>
                  <select name="shipping[]">
                      <option value="">選択してください</option>
                      <?php foreach ( (array)$other_delivArray AS $key2 => $value2 ) { ?>
                      <option value="<?=$value2['cd']?>" selected="selected"><?=$value2['name1']?><?=$value2['name2']?> <?=$value2['address1']?></option>
                      <?php } ?>
                  </select>
              </td>
          </tr>

            <?php } ?>

          </tbody>
        </table>

        <ul class="btnList">
          <li><a class="backBtn" href="./cart.php">戻る</a></li>
          <li><input class="nextBtn" type="submit" value="次へ"></li>
        </ul>

        <input type="hidden" name="shopping_deliv_flg" value="1">
      </form>
    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
