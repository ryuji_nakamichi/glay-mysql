﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


if (!$_POST['flg'] == 1) {
  header('Location: ./');
  exit;
} else {


  // クロスサイドスクリプティング対策
  foreach ($_REQUEST as $key => $val) {

    if ( !is_array($val) ) {
      $_REQUEST[$key] = htmlspecialchars($val);
      $_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
    }

  }


  //POSTされてきたデータを変数に定義
  $date = date( 'Y-m-d', strtotime("today") );

  if ($_REQUEST['date']) {
    $dateArray = explode("-", $date);
    $year      = $dateArray[0];
    $month     = $dateArray[1];
    $day       = $dateArray[2];
    $date      = $year."-".$month."-".$day;
  }

  $name    = $_POST['name'];
  $kana    = $_POST['kana'];
  $mail    = $_POST['mail'];
  $tel     = $_POST['tel'];
  $zip1    = $_POST['zip1'];
  $zip2    = $_POST['zip2'];
  $address = $_POST['address'];
  //$sort     = 0;
  //$disp_flg = 1;


//会員のcd番号
$member_cd = $_SESSION['user']['user_info']['cd'];



  //カートに入っている商品情報取得ここから

  if ($_SESSION['cart']) {
    $cartArray = $_SESSION['cart'];
  }

  if ($_SESSION['item_number']) {
    $item_numberArray = $_SESSION['item_number'];
  }

  $cartArrayMax = count($cartArray);

  for ($i = 0; $i < $cartArrayMax; $i++) {
    $_SELECT    = '*';
    $sql        = "SELECT {$_SELECT} FROM item WHERE disp_flg = 1 AND cd = {$cartArray[$i]} ORDER BY sort";
    $cart_query = mysqli_query($connect, $sql);
    $cart_max   = mysqli_num_rows($cart_query);

    if ($cart_max) {
      $cartInArray[] = mysqli_fetch_assoc($cart_query);


      //カートに入っている商品の個数と値段の計算
      $cartInArray[$i]['item_number'] = $item_numberArray[$i];
      $cartInArray[$i]['total_price'] = $cartInArray[$i]['price'] * $cartInArray[$i]['item_number'];


    }

  }
  //カートに入っている商品情報取得ここまで





  //------------------------------------
  // メール本文の整形 
  //------------------------------------

  $body = 'ご購入頂いた商品' . "\n";
  $body = '会員No.'. $member_cd . "\n";
  $body .= '--------------------------------------------------------' . "\n";

  for ($i = 0; $i < $cartArrayMax; $i++) {

    $total_priceArray[$i] = $cartInArray[$i]['price']*$cartInArray[$i]['item_number'];
    $item_name_Array[$i]  = $cartInArray[$i]['name'];
    $item_numberArray[$i] = $cartInArray[$i]['item_number'];

    $body .= '商品名 :' . $item_name_Array[$i] . ' 個数 :'. $item_numberArray[$i] . '個' . ' 値段 :' . $total_priceArray[$i] .'円' . "\n";

    $total_priceMax += $total_priceArray[$i];
  }
  $body .= "\n";

  $body .= '合計金額 :' .$total_priceMax . '円' . "\n";
  
  $body .= '--------------------------------------------------------' . "\n";
  $body .= '購入日時 :' . $date . "\n";
  $body .= '名前 :' . $name . "\n";
  $body .= 'ふりがな :' . $kana . "\n";
  $body .= 'メールアドレス :' . $mail . "\n";
  $body .= '電話番号 :' . $tel . "\n";
  $body .= '郵便番号 :' . $zip1.'-'.$zip2 . "\n";
  $body .= '住所 :' . $address . "\n";


  //------------------------------------
  // 言語・文字コード指定
  //------------------------------------

  mb_language("Japanese");
  mb_internal_encoding("UTF-8");


  //------------------------------------
  // ユーザーに自動返信メール
  //------------------------------------

  $subject = "お買い上げありがとうございました。";
  $mailto = $mail;
  $from = "From:ryujiglay@yahoo.co.jp";
  $return1 = mb_send_mail($mailto,$subject,$body,$from);


  //------------------------------------
  // 管理者に送るメール
  //------------------------------------

  $subject = "商品の購入がありました。";
  $mailto = "ryujiglay@yahoo.co.jp";
  $from = "From:ryujiglay@yahoo.co.jp";
  $return2 = mb_send_mail($mailto,$subject,$body,$from);


  //------------------------------------
  // メールが正常に送信されたかを判定
  //------------------------------------

  $res = array();

  if ($return1 && $return2)
  {
    $res['status'] = 'ok';


    //メールが正常に送信されたら購入情報をデータベースに登録するここから
    /*
    $sql = "INSERT INTO {$table_name} (
      cd,
      date,
      name,
      kana,
      mail,
      tel,
      msg,
      disp_flg,
      sort
      )
    VALUES(
      nextval('contact_cd_seq'),
      '{$date}',
      '{$name}',
      '{$kana}',
      '{$mail}',
      '{$tel}',
      '{$msg}',
      '{$disp_flg}',
       {$sort}
      )";

    $queryResult = mysqli_query($connect, $sql);
    */
    

    //並び順更新
    /*
    $sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
    $sort_query = mysqli_query($connect, $sort_sql);
    $sort_max   = mysqli_num_rows($sort_query);

    for ($i = 0; $i < $sort_max; $i++) {
      $sortArray = mysqli_fetch_array($sort_query, 0, PGSQL_ASSOC);

      $countSortArray = $i + 1;

      $sort_up_sql = "UPDATE {$table_name} SET ";
      $sort_up_sql .= "sort = ".$countSortArray." ";
      $sort_up_sql .= "WHERE cd = ".$sortArray['cd']."";
      // echo $sort_up_sql.'test<br>';

      $sort_up_query = mysqli_query($connect, $sort_up_sql);

    }
    //メールが正常に送信されたら購入情報をデータベースに登録するここまで
    */


  } else {
    $res['status'] = 'ng';
  }


  //カート情報を削除する
  $_SESSION['cart']        = array();
  $_SESSION['item_number'] = array();

  $_SESSION['shopping_flg'] = true;
  header('Location: ./shopping_complete.php');
  exit;
}