﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';
require_once './inc/mypage_check.php';


if ($_POST['mypage_refusal_flg'] !== '1') {

  header("Location: ./");
  exit;

} else {

  //mypageClass(会員退会完了)呼び出し
  $mypageObj = new mypageClass;
  $memberRefusalComplete_query = $mypageObj->memberRefusalComplete();

}

?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>MYページ</h2>

      <h3>退会手続き完了</h3>
      <div class="message">
        退会しました。<br>
        またのご利用お待ちしております。
        <p><a href="./">トップページへ</a></p>
      </div>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
