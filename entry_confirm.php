﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

echo '<pre>';
print_r($_REQUEST);
echo '</pre>';

//クロスサイトスクリプティング対策
foreach ($_REQUEST AS $key => $val) {

  if ( !is_array($val) ){
    $_REQUEST[$key] = htmlspecialchars($val);
    $_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
  }

}

$_SESSION['entry'] = $_REQUEST;

/*POSTされた内容を変数に格納*/
$name1 = $_SESSION['entry']['name1'];
$name2 = $_SESSION['entry']['name2'];
$kana1 = $_SESSION['entry']['kana1'];
$kana2 = $_SESSION['entry']['kana2'];
$company_name = $_SESSION['entry']['company_name'];
$sex      = $_SESSION['entry']['sex'];

if ($sex == 1) {
  $sex_ = '男性';
} else {
  $sex_ = '女性';
}

$job_type = $_SESSION['entry']['job_type'];


//job_type取得ここから
$_SELECT = "cd, name";
$_TABLE  = "job_type";
$_WHERE  = "WHERE disp_flg = 1 AND cd = {$job_type}";
$_ORDER  = "sort";

$job_typeControldObj  = new recordControlClass;
$job_typeArray2       = $job_typeControldObj->getRecord($connect, $_SELECT, $_TABLE, $_WHERE, $_ORDER);
$job_type_name = $job_typeArray2[0]['name'];
//job_type取得ここまで


$birth_year = $_SESSION['entry']['birth_year'];
$birth_month = $_SESSION['entry']['birth_month'];
$birth_day = $_SESSION['entry']['birth_day'];
$post_code1 = $_SESSION['entry']['post_code1'];
$post_code2 = $_SESSION['entry']['post_code2'];
$address = $_SESSION['entry']['address'];
$tel = $_SESSION['entry']['tel'];
$fax = $_SESSION['entry']['fax'];
$mail = $_SESSION['entry']['mail'];
$phone_mail_address = $_SESSION['entry']['phone_mail_address'];
$login_id = $_SESSION['entry']['login_id'];
$password = $_SESSION['entry']['password'];
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">

<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->

<div class="contents">

  <div class="float">

    <div class="left_box">

      <h2>Entry</h2>

      <form id="form1" name="form1" method="post" action="./entry_send.php">

        <table class="form_1">
          <tr>
            <th>姓名</th>
            <td class="column2_cell">
              <?=$name1?>
              <?=$name2?>
            </td>
          </tr>

          <tr>
            <th>かな</th>
            <td class="column2_cell">
              <?=$kana1?>
              <?=$kana2?>
            </td>
          </tr>

          <tr>
            <th>会社名</th>
            <td>
              <?=$company_name?>
            </td>
          </tr>

          <tr>
            <th>性別</th>
            <td class="addressCell">
              <?=$sex_?>
            </td>
          </tr>

          <tr>
            <th>職業</th>
            <td>
                <?=$job_type_name?>
            </td>
          </tr>

          <tr>
            <th>生年月日</th>
            <td>
              <?=$birth_year?><?=$birth_month?><?=$birth_day?>
            </td>
          </tr>

          <tr>
            <th>郵便番号</th>
            <td class="addressCell">
              <?=$post_code1?>
              -
              <?=$post_code2?>
            </td>
          </tr>

          <tr>
            <th>住所</th>
            <td>
              <?=$address?>
            </td>
          </tr>

          <tr>
            <th>電話番号</th>
            <td>
              <?=$tel?>
            </td>
          </tr>

          <tr>
            <th>FAX</th>
            <td>
              <?=$fax?>
            </td>
          </tr>

          <tr>
            <th>メールアドレス</th>
            <td>
              <?=$mail?>
            </td>
          </tr>

          <tr>
            <th>携帯メールアドレス</th>
            <td>
              <?=$mail?>
            </td>
          </tr>

          <tr>
            <th>ユーザーID</th>
            <td>
              <?=$login_id?>
            </td>
          </tr>

          <tr>
            <th>パスワード</th>
            <td>
              <?=$password?>
            </td>
          </tr>
            
        </table>

        <ul class="btnList">
          <li>
            <input type="reset" id="reset" class="button" value="修正する" onclick="history.back();">
          </li>
          <li>
            <input type="button" id="submit" class="button" value="送信する">
          </li>
        </ul>

        <input type="hidden" name="flg" value="1">

        <input type="hidden" name="name1" value="<?=$name1?>">
        <input type="hidden" name="name2" value="<?=$name2?>">
        <input type="hidden" name="kana1" value="<?=$kana1?>">
        <input type="hidden" name="kana2" value="<?=$kana2?>">
        <input type="hidden" name="company_name" value="<?=$company_name?>">
        <input type="hidden" name="sex" value="<?=$sex_?>">
        <input type="hidden" name="job_type" value="<?=$job_type_name?>">
        <input type="hidden" name="birth_year" value="<?=$birth_year?>">
        <input type="hidden" name="birth_month" value="<?=$birth_month?>">
        <input type="hidden" name="birth_day" value="<?=$birth_day?>">
        <input type="hidden" name="post_code1" value="<?=$post_code1?>">
        <input type="hidden" name="post_code2" value="<?=$post_code2?>">
        <input type="hidden" name="address" value="<?=$address?>">
        <input type="hidden" name="tel" value="<?=$tel?>">
        <input type="hidden" name="fax" value="<?=$fax?>">
        <input type="hidden" name="mail" value="<?=$mail?>">
        <input type="hidden" name="phone_mail_address" value="<?=$phone_mail_address?>">
        <input type="hidden" name="login_id" value="<?=$login_id?>">
        <input type="hidden" name="password" value="<?=$password?>">

      </form>

    </div>

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

  </div>
</div>
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
