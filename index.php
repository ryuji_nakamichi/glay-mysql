﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">

  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
<!--main_visual-->
<?php require_once './inc/main_visual.php'; ?>
<!--/main_visual-->

    <div class="contents">

      <div class="float">

<!--left_box-->
<?php require_once './inc/left_box.php'; ?>
<!--/left_box-->

<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>

<!--instdramBox-->
<?php require_once './inc/instgram.php'; ?>
<!--/instdramBox-->

    </div> 
    	<?php //test ?>
    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>
</body>
</html>
