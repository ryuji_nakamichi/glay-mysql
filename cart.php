﻿<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';


//カートクラスからインスタンス作成
$cartObj  = new cartClass;
$cartInfo = $cartObj->cart($connect);


//表示に必要な変数を格納
$cartArrayMax = $cartInfo['cartArrayMax']; 
$cartInArray  = $cartInfo['cartInfoArray']; 
$cartNoneText = $cartInfo['cartNoneText'];


//ログインしていないが、カートに商品がある場合
if ($_SESSION['user_login']['login_flg'] !== 1 && $_SESSION['cartInfo']['cartArrayMax']) {
  $shopping_deliv_link = './shopping.php';


//ログインしているが、カートに商品がない場合
} else if ($_SESSION['user_login']['login_flg'] === 1 && !$_SESSION['cartInfo']['cartArrayMax']) {
  $shopping_deliv_link = '';


//ログインして、カートに商品がある場合
} else if ($_SESSION['user_login']['login_flg'] === 1 && $_SESSION['cartInfo']['cartArrayMax']) {
  $shopping_deliv_link = './shopping_deliv.php';
}

// echo '<pre>';
// print_r($cartInfo);
// echo '</pre>';
?>
<!doctype html>
<html>
<head>
<?php require_once './inc/head.php'; ?>
</head>

<body>

<div id="w_wrapper">


  <div id="wrapper">
  
<!--header-->
<?php require_once './inc/header.php'; ?>
<!--/header-->
    
    
    
    
    <div class="contents">
    
      <div class="float">
    
      <div class="left_box">
      
        <h2>カート</h2>
        
        <div class="cartListBox">

        <?php if ($cartArrayMax) { ?>

        <form id="cartForm" name="cartForm" method="post">

          <ul class="cartList">

            <?php $cnt = 0; ?>

            <?php foreach ( (array)$cartInArray AS $key => $val ) { ?>

            <li>
              <p class="itemId">商品ID:<?=$val['cd']?></p>
              <dl>
                <dt class="name"><a href="./item_detail.php?cd=<?=$val['cd']?>">商品名:<?=$val['name']?></a></dt>
                <dd class="cartImgBox">
                  <a href="./item_detail.php?cd=<?=$val['cd']?>"><img src="<?=$val['img_pass1']?>" alt="<?=$val['name']?>"></a>
                </dd>
                <dd>金額:<span><?=$val['total_price_format']?></span>円</dd>
              </dl>

              <label>商品を削除する<input type="checkbox" name="cart_delete_flg<?=$cnt?>" value="on"></label><br>
              数量<input id="spinner" class="item_number" type="text" name="cart<?=$cnt?>" value="<?=$val['item_number']?>" maxlength="2">
              
            </li>
            <?php $cnt++; ?>
            <?php } ?>

          </ul>

          <div class="btnWrapBox">
            <ul>
              <li class="submitBtn"><input type="submit" name="submit" value="内容を変更する">
              </li>
              <li class="cartBtn">
                <a href="./item.php">買い物を続ける</a>
              </li>
            </ul>

            <p class="cartBtn"><a href="<?=$shopping_deliv_link?>">商品購入へ</a></p>
          </div>

        </form>

        <?php } else { ?>
        <p style="text-align: center;"><?=$cartNoneText?></p>

        <div class="btnWrapBox">
          <p class="cartBtn"><a href="./item.php">買い物を続ける</a></p>
        </div>
        <?php } ?>

        </div>
      
      </div>
      
<div class="rightWrapBox">
<!--right_box-->
<?php require_once './inc/right_box.php'; ?>
<!--/right_box-->
      
<!--right_bottom-->
<?php require_once './inc/right_bottom.php'; ?>
<!--/right_bottom-->
      
<!--twitter-->
<?php require_once './inc/twitter.php'; ?>
<!--/twitter-->
</div>
    
    
    </div> 

    </div>
    
    
<!--footer-->
<?php require_once './inc/footer.php'; ?>
<!--/footer-->
  

  </div>

</div>

</body>
</html>
