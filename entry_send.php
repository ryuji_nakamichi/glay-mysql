<?php

require_once './inc/session.php';
require_once './inc/db.php';
require_once './class/class.php';

$table_name = "shopping_member";


	//クロスサイドスクリプティング対策
	foreach ($_REQUEST AS $key => $val) {

		if ( !is_array($val) ){
			$_REQUEST[$key] = htmlspecialchars($val);
			$_REQUEST[$key] = mysqli_real_escape_string($connect, $val);
		}

	}


//POSTされてきたデータを変数に定義

$name1   = $_REQUEST['name1'];
$name2   = $_REQUEST['name2'];
$kana1   = $_REQUEST['kana1'];
$kana2   = $_REQUEST['kana2'];

if ($_REQUEST['sex']) {

	$sex = $_REQUEST['sex'];

	if ($sex == 1) {
		$sex_ = '男性';
	} else {
		$sex_ = '女性';
	}
}

if ($_REQUEST['job_type']) {
	$job_type = $_REQUEST['job_type'];
}

$company_name       = $_REQUEST['company_name'];
$birth_year         = $_REQUEST['birth_year'];
$birth_month        = $_REQUEST['birth_month'];
$birth_day          = $_REQUEST['birth_day'];
$post_code1         = $_REQUEST['post_code1'];
$post_code2         = $_REQUEST['post_code2'];
$address            = $_REQUEST['address'];
$tel                = $_REQUEST['tel'];
$fax                = $_REQUEST['fax'];
$mail               = $_REQUEST['mail'];
$phone_mail_address = $_REQUEST['phone_mail_address'];
$user_id            = $_REQUEST['login_id'];
$password           = $_REQUEST['password'];
$md5_password       = md5($_REQUEST['password']);
$sort               = 0;
$disp_flg           = 1;


//すでにユーザーIDか、パスワードが使用されていたら、登録させない
$sql        = "SELECT cd FROM {$table_name} WHERE user_id = '{$user_id}' OR MD5(password) = '{$md5_password}'";
$user_query = mysqli_query($connect, $sql);
$user_max   = mysqli_num_rows($user_query);

$resArray = array();

if ($user_max == 0) {


	//データベースに登録するここから
	$sql = "INSERT INTO {$table_name} (
		name1,
		name2,
		kana1,
		kana2,
		company_name,
		sex,
		job_type,
		birth_year,
		birth_month,
		birth_day,
		post_code1,
		post_code2,
		address,
		tel,
		fax,
		mail,
		phone_mail_address,
		user_id,
		password,
		disp_flg,
		sort,
		new_record_time
		)
	VALUES(
		'{$name1}',
		'{$name2}',
		'{$kana1}',
		'{$kana2}',
		'{$company_name}',
		{$sex},
		{$job_type},
		'{$birth_year}',
		'{$birth_month}',
		'{$birth_day}',
		'{$post_code1}',
		'{$post_code2}',
		'{$address}',
		'{$tel}',
		'{$fax}',
		'{$mail}',
		'{$phone_mail_address}',
		'{$user_id}',
		'{$password}',
		{$disp_flg},
		{$sort},
		 current_timestamp
		)";

	$queryResult = mysqli_query($connect, $sql);
	

	//並び順更新
	$sort_sql   = "SELECT cd, sort FROM {$table_name} ORDER BY sort";
	$sort_query = mysqli_query($connect, $sort_sql);
	$sort_max   = mysqli_num_rows($sort_query);

	for ($i = 0; $i < $sort_max; $i++) {
		$sortArray = mysqli_fetch_assoc($sort_query);

		$countSortArray = $i + 1;

		$sort_up_sql   = "UPDATE {$table_name} SET ";
		$sort_up_sql  .= "sort = ".$countSortArray." ";
		$sort_up_sql  .= "WHERE cd = ".$sortArray['cd']."";
		$sort_up_query = mysqli_query($connect, $sort_up_sql);

	}
	//データベースに登録するここまで

	
} else {
	$resArray['status'] = 'query_faild';
}



//会員登録に成功した場合
if ($queryResult) {


	//職業が選択されていたら項目名取得（メール送信用）ここから
	if ($job_type) {

		$sql = "SELECT cd, name FROM job_type WHERE disp_flg = 1 AND cd = {$job_type} LIMIT 1";
		$job_type_query = mysqli_query($connect, $sql);
		$job_type_max   = mysqli_num_rows($job_type_query);

		for ($i = 0; $i < $job_type_max; $i++) {
			$job_typeArray[$i] = mysqli_fetch_assoc($job_type_query);
			$job_type_name = $job_typeArray[0]['name'];
		}

	}
	//職業が選択されていたら項目名取得（メール送信用）ここまで


	$body = '名前 :' . $name1.$name2 . "\n";
	$body .= 'かな :' . $kana1.$kana2 . "\n";
	$body .= '性別 :' . $sex_ . "\n";
	$body .= '職業 :' . $job_type_name . "\n";
	$body .= '生年月日 :' . $birth_year .'年'.$birth_month.'月'.$birth_day.'日'. "\n";
	$body .= '会社名 :' . $company_name . "\n";
	$body .= '郵便番号 :' . $post_code1.'-'.$post_code2 . "\n";
	$body .= '住所 :' . $address . "\n";
	$body .= '電話番号 :' . $tel . "\n";
	$body .= 'FAX :' . $fax . "\n";
	$body .= 'メールアドレス :' . $mail . "\n";
	$body .= '携帯メールアドレス :' . $phone_mail_address . "\n";
	$body .= 'ユーザーID :' . $user_id . "\n";
	$body .= 'パスワード :' . $md5_password . "\n";

	$_SELECT = "cd, name1, name2, kana1, kana2, company_name, birth_year, birth_month, birth_day, sex, job_type, post_code1, post_code2, address, tel, fax, mail, phone_mail_address, user_id, MD5(password) AS password, point, disp_flg, sort, new_record_time, update_record_time";
	$sql = "SELECT {$_SELECT} FROM shopping_member WHERE user_id = '{$user_id}' AND MD5(password) = '{$md5_password}'";

	$user_query2 = mysqli_query($connect, $sql);
	$user_max2   = mysqli_num_rows($user_query2);

	if ($user_max2) {

		$userArray = mysqli_fetch_assoc($user_query2);

		$_SESSION['user']["user_info"] = $userArray;

		$_SESSION['user_login']["login_flg"] = 1;

	}


	//会員登録に成功した場合、statusにsuccessを代入
	$resArray['status'] = 'success';


	//会員登録に成功した場合メールを送信する
	if ($resArray['status'] === 'success') {


		//------------------------------------
		// 言語・文字コード指定
		//------------------------------------

		mb_language("Japanese");
		mb_internal_encoding("UTF-8");


		//------------------------------------
		// ユーザーに自動返信メール
		//------------------------------------

		$subject = "会員登録ありがとうございました。";
		$mailto  = $mail;
		$from    = "From:{$user_mail}";
		$return1 = mb_send_mail($mailto,$subject,$body,$from);


		//------------------------------------
		// 管理者に送るメール
		//------------------------------------

		$subject = "会員登録がありました。";
		$mailto  = $user_mail;
		$from    = "From:{$user_mail}";
		$return2 = mb_send_mail($mailto,$subject,$body,$from);


		//------------------------------------
		// メールが正常に送信されたかを判定
		//------------------------------------

		if ($return1 && $return2)
		{

			//statusはsuccessで、会員・管理者双方へのメール送信が成功した場合
			$resArray['errMsg'] = 'mail_send_success';

		} else {

			//statusはsuccessだが、会員・管理者片方（若しくは双方）へのメール送信が失敗した場合
			$resArray['errMsg'] = 'mail_send_faild';

		}

	}


//会員登録に失敗した場合
} else {


	//会員登録に失敗した場合であり、ユーザーIDまたはパスワードがすでに登録されている場合は、statusにquery_faildを代入、そうでない場合は、faildを代入
	if ($resArray['status'] !== 'query_faild') {
		$resArray['status'] = 'faild';
	}

}


//3秒処理を止める
//sleep(3);

echo json_encode($resArray);